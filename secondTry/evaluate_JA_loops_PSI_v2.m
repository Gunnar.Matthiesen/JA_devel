function [M_sim] = evaluate_JA_loops_PSI_v2(PSI_meas,Curr_meas,param_fix,param)
    % an der Stelle PSI_sim(1,1) muss der Startwert M0 stehen und in Loops in Curr_sim müssen die gleiche Länge auf auf und absteigendem Ast haben 
    % Curr_sim is a matrix 
    

    M01 =  PSI_meas(1);
    M02 =  PSI_meas(1);
    M_sim1 = zeros(size(Curr_meas));
    M_sim2 = zeros(size(Curr_meas));

    j_max = 1;
    for j = 1:j_max
                
                M_sim1(1) = M01;
                M_sim2(1) = M02; 
                [M_sim1 ,M_sim2] = integrate_JA_loop_discr_PSI_v2(Curr_meas,M_sim1,M_sim2,param_fix,param);     
                M01 = M_sim1(end);
                M02 = M_sim2(end);  
     end
       M_sim = M_sim1 + M_sim2;

    
    
end