 
%Load MSM BH-Curve-Data

run('D:\01DissIT\Simulation\JA_devel\JA_devel\secondTry\MaterialModel\MSM_Material_parameter.m');
 
 figure('name','MSM Material data')
  plot(H,M)
  xlabel('H [A/m]');
  ylabel('M [A/m]');
  grid on
  %%
  figure
 
  plot(H,B)
  xlabel('H [A/m]');
  ylabel('B [T]');

  
%% Approximate MSM H-M Curve from MSM-Measurements "MS A APLHA"   

     param = [16e5,1000, 1e-3];
     lb = [0.5,0.000001,0.001];
     ub = [1.5,5,100];
     call_optimJa = @(param_opt) min_anhyst(H(2:end),M(2:end),param,param_opt);   
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,3,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
       disp(num2str(bestParam));
       disp(num2str(param.*bestParam));

    figure
    ax=axes();
    estimate = calculate_M_an(H,M,param.*bestParam);
    plot(ax,H,estimate);
    hold on
    plot(H,M);
    xlabel('H [A/m]');
    ylabel('M [A/m]');
    grid on


%%
figure('name','MSM valve measurement data');
ax2 = axes();
load('D:\01DissIT\Simulation\JA_devel\JA_devel\secondTry\Measurements\MSM\AnhystCurve\poly_psi_an_max.mat');
current = [0.01:0.01:0.7]';
position = ones(size(current))*6.1232271;       

i_meas = current;
psi_meas = feval(poly_psi_an_max,current,position);

hold on
plot(ax2,i_meas,psi_meas)
grid on
xlabel('i [A]');
ylabel('psi [Wb]');

%% Estimate Factor
% WICHITG: Werden alpha und psi festgehalten, gelingt die minimierung nicht
% --> i hat hier keinen einfluss
% --> wenn alpha fest ist, muss psi variabel sein und i kann fest sein
% --> !!! Funktioniert bereits ausschließlich durch variation von alpha und
% f_psi

    N = 1400;
    l_est = 0.1;
    A_est = 0.01;
    fi_start = 1/l_est*N *0.5 ;
    f_psi_start = 1/(A_est^2*pi/4*N);

    my0 = 4.*pi.*1e-7;
    
    Ms_start = max(psi_meas*f_psi_start/my0-fi_start*i_meas);
    a_start = 1722.3764831      ;    
    alpha_start = 0.00001617270805;

    param = [Ms_start,a_start,alpha_start,fi_start,f_psi_start];

     call_optimJa = @(param_opt) min_anhyst_psi_meas(i_meas,psi_meas,param,param_opt);           
     lb = [0.1,1,0.1,1,0.1];
     ub = [50,1,10,1,10];
            options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
            [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,5,lb,ub,options);
            disp(['Fval:  ', num2str(fval)]);
            disp(num2str(bestParam));
            disp(num2str(param.*bestParam));
     new_param = param.*bestParam;

     %%
    plot(ax2,i_meas,calculate_M_an_psi_meas(i_meas,psi_meas,param.*bestParam));
    xlabel('i [A]');
    ylabel('psi_meas [Wb]');
    grid on  
    M = psi_meas*new_param(5)/my0-i_meas*new_param(4);
    

  %%
  
  plot(i_meas,calculate_M_an_psi_meas_vpasolve(i_meas,param.*bestParam));
%% USE THIS Function to work with split lines
    param = [Ms_start,a_start,alpha_start,fi_start,f_psi_start,5500,1];
     call_optimJa = @(param_opt) min_anhyst_psi_meas_split(i_meas,psi_meas,param,param_opt);           
     lb = [0.1,1,0.1,1,0.1,1e-3,1e-3];
     ub = [50,1,10,1,10,4,100];
            options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
            [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,7,lb,ub,options);
            disp(['Fval:  ', num2str(fval)]);
            disp(num2str(bestParam));
            disp(num2str(param.*bestParam));

%%
%     update_figure('compare fit');
%  clf
 plot(i_meas,calculate_M_an_psi_meas_split(i_meas,psi_meas,param.*bestParam))
%  hold on
%  grid on
%  plot(i_meas,psi_meas)
%  legend('fit','measurement')
%%   USE FOR APPROXIMATION OF H-H CURVE PARAMS  "MS A APLHA"   
function SSEmin = min_anhyst(H,M,param,param_opt)    
        SSEmin = sum((M - calculate_M_an(H,M,param.*param_opt)).^2);    
end
function [M_an] = calculate_M_an(H,M,param)
        Ms = param(1);
        a = param(2);
        alpha = param(3);
        M_an=  Ms.*(coth((H+alpha*M)./a)-a./(H+alpha*M));        
end      

%%
function SSEmin = min_anhyst_psi_meas(i,psi,param,param_opt)
    
  SSEmin = sum((psi - calculate_M_an_psi_meas(i,psi,param.*param_opt)).^2);
    
end

function [psi_an] = calculate_M_an_psi_meas(i,psi,param)        
        Ms  = param(1);
        a   = param(2);
        alpha = param(3);
        fi  = param(4); 
        fpsi= param(5);

        H = i*fi;
        B = psi*fpsi;
        my0 = 4e-7;        
        M   = B/my0-H;       
        M_an=  Ms.*(coth((H+alpha*M)./a)-a./(H+alpha*M));    
        
        psi_an = my0.*(M_an+H)/fpsi;
end

function SSEmin = min_anhyst_psi_meas_split(i,psi,param,param_opt)
    
  SSEmin = sum((psi - calculate_M_an_psi_meas_split(i,psi,param.*param_opt)).^2);
    
end

function [psi_an] = calculate_M_an_psi_meas_split(i,psi,param)        
        Ms  = param(1);
        a   = param(2);
        alpha = param(3);
        fi  = param(4); 
        fpsi= param(5);

        He_sw= param(6);
       
        f =  param(7);
        
        b =  Ms*(a/He_sw^2 - ((coth(He_sw/a)^2 - 1))/a)    / (He_sw^(1/f - 1)/f); % ((He_sw^(1/f)*log(He_sw))/f^2 )  ;     
        k_sw =  Ms.*(coth(He_sw./a)-a./(He_sw))-b*(He_sw).^(1/f);
        
        H = i*fi;
        B = psi*fpsi;
        my0 = 4*1e-7;        
        M   = B/my0-H;       
        
        He = H + alpha*M;
        M_an = ones(size(He));

                M_an(abs(He)<= He_sw) =  Ms.*(coth(He(abs(He)<= He_sw)./a)-a./(He(abs(He)<= He_sw)));

                M_an(He>He_sw) =   b*(He(He>He_sw)).^(1/f) + k_sw;
                %% ## careful -> check
                M_an(He<-He_sw) =   -b*(abs(He(He<-He_sw))).^(1/f) - k_sw;

      psi_an = my0.*(M_an+H)/fpsi;
        
end

function [psi_an] = calculate_M_an_psi_meas_vpasolve(current,param)        
        syms Mi
        Ms  = param(1);
        a   = param(2);
        alpha = param(3);
        fi  = param(4); 
        fpsi= param(5);

        H = current*fi;
%         B = psi*fpsi;
        my0 = 4e-7;        
        M_an  =zeros(size(H));
        for i = 1:length(H)
            Hi = H(i);    
            eqn=  Ms.*(coth((Hi+alpha*Mi)./a)-a./(Hi+alpha*Mi))==Mi;    
            M_an(i) = vpasolve(eqn,Mi);
        end
%           M   = B/my0-H;             
        psi_an = my0.*(M_an+H)/fpsi;
end

