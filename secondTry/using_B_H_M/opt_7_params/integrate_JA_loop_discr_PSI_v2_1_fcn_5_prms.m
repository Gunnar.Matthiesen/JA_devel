function [M_sim1,dM_dH1] = integrate_JA_loop_discr_PSI_v2_1_fcn_5_prms(H_int,M_sim1,param)


        Ms1     = param(1);
        a1      =  param(2);
        alpha1  = param(3);       
        k1      = param(4);
        c1      = param(5);
        alpha2      = param(8);

        
        dM_dH1 = zeros(size(H_int));
        delta = zeros(size(H_int));

         my0 = 4.*pi.*1e-7;
        

        for k = 1:length(H_int)-1  
                   
        
            delta(k) = sign(H_int(k+1)-H_int(k));
            He1 = H_int(k)+alpha1*M_sim1(k);   
            
            M_an1 = Ms1*(coth(He1/a1)-a1/He1); 
            
            dMan_dH1 =-Ms1*((coth((H_int(k) + M_sim1(k)*alpha1)/a1)^2 - 1)/a1 - a1/(H_int(k) + M_sim1(k)*alpha1)^2); 
         
            dM_dH1(k) = (1/(1+ c1)) * ( M_an1 - M_sim1(k)) /(   k1*delta(k)/my0 - alpha2*(M_an1 - M_sim1(k))) + c1/(1+c1)*dMan_dH1;
            
            M_sim1(k+1) = M_sim1(k) + dM_dH1(k)*(H_int(k+1)-H_int(k));

        end
        
end