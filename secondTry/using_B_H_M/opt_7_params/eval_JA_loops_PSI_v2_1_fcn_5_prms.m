function [psi_an] = eval_JA_loops_PSI_v2_1_fcn_5_prms(PSI,CRR,param_in)

f_psi = param_in(7);
f_i = param_in(6);
my0 = 4e-7;
psi_an = cell(size(PSI));

    for i=1:size(CRR,2)

    B_meas = f_psi*PSI{i}; % my0 steckt dann in f_psi
    H_meas = f_i*CRR{i};
    M_meas =  B_meas/my0-H_meas;

 
    [M_sim,~] = evaluate_JA_loops_PSI_v2_1_fcn_5_prms(M_meas,H_meas,param_in);
    psi_an{i} = my0.*(M_sim+H_meas)/f_psi;
    end
end