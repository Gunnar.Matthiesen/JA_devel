function [sse] = opt_JA_loops_PSI_v2_1_fcn_5_prms(PSI,CRR,param_in,param_opt)
% function [psi_an] = opt_JA_loops_PSI_v2_1_fcn_5_prms(PSI,CRR,param_in,param_opt)
sse = 0;


my0=4e-7;

param = param_in.*param_opt;


f_psi = param(7);
f_i = param(6);

for i=1:size(CRR,2)

B_meas = f_psi*PSI{i}; % my0 steckt dann in f_psi
H_meas = f_i*CRR{i};
M_meas =  B_meas/my0-H_meas;

[M_sim,~] = evaluate_JA_loops_PSI_v2_1_fcn_5_prms(M_meas,H_meas,param);
psi_an= my0.*(M_sim+H_meas)/f_psi;
sse = sse + sum( (psi_an-PSI{i}).^2);


% psi_an{i}= my0.*(M_sim+H_meas)/f_psi;
% sse = sse + sum( (psi_an{i}-PSI{i}).^2);

end



end