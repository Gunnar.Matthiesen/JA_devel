function [M_sim,dM_dH1] = evaluate_JA_loops_PSI_v2_1_fcn_5_prms(M_meas,H_meas,param)
    % an der Stelle PSI_sim(1,1) muss der Startwert M0 stehen und in Loops in Curr_sim müssen die gleiche Länge auf auf und absteigendem Ast haben 
    % Curr_sim is a matrix 
 
    M01 =  M_meas(1);
    M_sim1 = zeros(size(H_meas));
    j_max = 2;
    
    for j = 1:1
                
                M_sim1(1) = M01;
                [M_sim1,dM_dH1] = integrate_JA_loop_discr_PSI_v2_1_fcn_5_prms(H_meas,M_sim1,param);     
                M01 = M_sim1(end);

%                 figure
% %                 plot(H_meas,M_sim1)
%                 hold on
%                 plot(H_meas,M_meas)

     end
     M_sim = M_sim1;

    
    
end