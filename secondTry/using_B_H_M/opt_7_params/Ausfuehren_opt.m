
addpath('D:\01DissIT\Simulation\JA_devel\JA_devel\secondTry');
load('D:\01DissIT\Simulation\JA_devel\JA_devel\secondTry\Measurements\MSM\HystLoops\220621_Neukurve_13Positionen_v3_pr');
%%  
idx_meas = 1;
idx_sel = [10,14,18];
% idx_sel = [10,15,18];
 [PSI_meas,Curr_meas,POS_meas,weights,PSI_meas_an,Curr_meas_an]  = auto_select(meas_raw,idx_sel,idx_meas);
figure('name','selected data for optimization');
for i = 1:size(PSI_meas,2)

    plot(Curr_meas{i},PSI_meas{i});
    hold on
    plot(Curr_meas_an{i},PSI_meas_an{i});
end
grid on
    
%% First shot for the anhysteretic function

    my0 = 4e-7;
    N = 1400;
    l_est = 0.1;
    A_est = 0.01;
    fi_start = 1/l_est*N  ;
    f_psi_start = 1/(A_est^2*pi/4*N);
    my0 = 4.*pi.*1e-7;
    Ms_start = max(psi_meas*f_psi_start/my0-fi_start*i_meas);
    a_start = 1722.3764831      ;    
    alpha_start = 0.001617270805;


%% Now fit Full J-A Model            
% 31281523.5361       1722.3764831  0.000123631689163               7000      64.4032770845         


%Fval:  0.0081556
% 0.92277      1.1768     0.90016      2.8639      1.9978           1         0.9      1.7928
% 91675686.3119      2026.89526452  3.42055384085e-05  0.000286389159659     0.998895067303               7000      58.1354123051  6.81241663539e-05


%         clear param_in_5_opt
%         param_in_opt(1) = 90483928.3437 ;%91675686.3119  ; %Ms1     = param(1);
%         param_in_opt(2) = 2026.89526452 ; %a1     = param(2);
%         param_in_opt(3) = 3.42055384085e-05 ;%alpha_start; %alpha1      = param(3);       
%         param_in_opt(4) = 0.0001; % k
%         param_in_opt(5) = 2;  % c
%         param_in_opt(6) = 7000;
%         param_in_opt(7) =   58.1354123051 ;% f_psi_start;
%         param_in_opt(8) = 5.87653219873e-05;% f_psi_start;
%         eval_opt_anhyst = @(param_opt) opt_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas,Curr_meas,param_in_opt,param_opt);

        clear param_in_5_opt
        param_in_opt(1) = 62413374.7398   ;%91675686.3119  ; %Ms1     = param(1);
        param_in_opt(2) = 1722.3764831 ; %a1     = param(2);
        param_in_opt(3) = 6.04952166766e-05 ;%alpha_start; %alpha1      = param(3);       
        param_in_opt(4) = 0.0001; % k
        param_in_opt(5) = 0.5;  % c
        param_in_opt(6) = 7000;
        param_in_opt(7) =   40.5825428871 ;% f_psi_start;
         param_in_opt(8) = 6.04952166766e-05;% f_psi_start;
        eval_opt_anhyst = @(param_opt) opt_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas,Curr_meas,param_in_opt,param_opt);


        lb = [1,1,1 ,0.001 ,0.001   ,1,1,0.25];
        ub = [1,1,1 ,10000  ,2      ,1,1,2];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(eval_opt_anhyst,8,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam));
        disp(num2str(param_in_opt.*bestParam));
    
        
%% unterer BEreich 
% Fval:  0.15263
% 1           1           1     0.69848           2           1           1        0.25
% 62413374.7398       1722.3764831  6.04952166766e-05  6.98480613693e-05                  1               7000      40.5825428871  1.51238041691e-05
%% oberer BEreich
% Fval:  0.055205
% 1           1           1      2.7097           2           1           1      1.3221
% 62413374.7398       1722.3764831  6.04952166766e-05  0.000270973324551                  1               7000      40.5825428871  7.99797190452e-05
%         
%%
% param_in_opt2(5)= param_in_opt(5)*1;
%  param_in_opt(4) = 0.001; % k
%  param_in_opt(5) = 0.1;
% PSI_sim = eval_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas,Curr_meas,param_in_opt);
this_param = param_in_opt.*bestParam;
this_param(8) = this_param(8);
PSI_sim = eval_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas,Curr_meas,this_param);
 
% PSI_sim =  opt_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas,Curr_meas,param_in_opt,bestParam);

% figure('name','Simulated JA PSI');
sse = 0;
for i = 1:size(PSI_meas,2)

    sse = sse +sum((PSI_sim{i}-PSI_meas{i}).^2);
    plot(Curr_meas{i},PSI_sim{i});
    hold on
end
grid on

%% symbolically solve anhyst equation
current_line_anhyst = [0.025:0.025:0.7];
this_param = param_in_opt.*bestParam;
psi_line_anhyst = calculate_M_an_psi_meas(current_line_anhyst,this_param);
plot(current_line_anhyst,psi_line_anhyst);
%%
function SSEmin = min_anhyst_psi_meas(i,psi,param,param_opt)
    
  SSEmin = sum(i.*(psi - calculate_M_an_psi_meas(i,psi,param.*param_opt)).^2);
    
end

function [psi_an] = calculate_M_an_psi_meas(current,param)        
        syms Mi
        Ms  = param(1);
        a   = param(2);
        alpha = param(3);
        fi  = param(6); 
        fpsi= param(7);

        H = current*fi;
%         B = psi*fpsi;
        my0 = 4e-7;        
        M_an  =zeros(size(H));
        for i = 1:length(H)
            Hi = H(i);    
            eqn=  Ms.*(coth((Hi+alpha*Mi)./a)-a./(Hi+alpha*Mi))==Mi;    
            M_an(i) = vpasolve(eqn,Mi);
        end
%           M   = B/my0-H;             
        psi_an = my0.*(M_an+H)/fpsi;
end

