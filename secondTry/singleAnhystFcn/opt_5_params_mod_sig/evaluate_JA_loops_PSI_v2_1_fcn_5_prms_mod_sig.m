function [M_sim,dM_dH1] = evaluate_JA_loops_PSI_v2_1_fcn_5_prms_mod_sig(PSI_meas,Curr_meas,param)
    % an der Stelle PSI_sim(1,1) muss der Startwert M0 stehen und in Loops in Curr_sim müssen die gleiche Länge auf auf und absteigendem Ast haben 
    % Curr_sim is a matrix 
    

    M01 =  PSI_meas(1);
%     M02 =  PSI_meas(1);
    M_sim1 = zeros(size(Curr_meas));
%     M_sim2 = zeros(size(Curr_meas));

    j_max = 2;
    for j = 1:j_max
                
                M_sim1(1) = M01;
%                 M_sim2(1) = M02; 
                [M_sim1,dM_dH1] = integrate_JA_loop_discr_PSI_v2_1_fcn_5_prms_mod_sig(Curr_meas,M_sim1,param);     
                M01 = M_sim1(end);
%                 M02 = M_sim2(end);  
     end
       M_sim = M_sim1;% + M_sim2;

    
    
end