function  sse = opt_anhyst_1_fcn_5_prms_mod_sig(x,y,param,param_opt)

param = param.*param_opt;

Ms1  = param(1);
a1 = param(2);
alpha1   = param(3);
m   = param(4);


x1 = x+alpha1*y; 


y_dach = Ms1*(coth(x1/a1)-a1./x1).*(1-m).*x1;%  

sse = sum((y-y_dach).^2);
end
