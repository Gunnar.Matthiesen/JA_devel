function [M_sim,dM_dH1] = eval_JA_loops_PSI_v2_1_fcn_5_prms(PSI,CRR,param_in)


for i=1 %1:size(CRR,2)

Curr_meas = CRR{i};
PSI_meas = PSI{i};


hold on;
[M_sim,dM_dH1] = evaluate_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas,Curr_meas,param_in);


end

end