function [M_sim1,dM_dH1] = integrate_JA_loop_discr_PSI_v2_1_fcn_5_prms_mod_sig(H_int,M_sim1,param)


        Ms1     = param(1);
        a1     = param(2);
        alpha1      = param(3);       
        k1      = param(4);
        c1      = param(5);
        m = param(6);
        
        dM_dH1 = zeros(size(H_int));
        delta = zeros(size(H_int));

        
        

        for k = 1:length(H_int)-1  
            
       
        
            delta(k) = sign(H_int(k+1)-H_int(k));
%             He1 = H_int(k)+alpha1*M_sim1(k);            
%             M_an1 = Ms1*(coth(He1/a1)-a1/He1); 
            
%             dMan_dH1 =-Ms1*((coth((H_int(k) + M_sim1(k)*alpha1)/a1)^2 - 1)/a1 - a1/(H_int(k) + M_sim1(k)*alpha1)^2); 
         
             M_an1 = Ms1*(coth( (H_int(k)+alpha1*M_sim1(k))/a1)-a1/(H_int(k)+alpha1*M_sim1(k)))*(1-m)*(H_int(k)+alpha1*M_sim1(k));

            dMan_dH1 = Ms1*((coth((H_int(k) + M_sim1(k)*alpha1)/a1)^2 - 1)/a1 - a1/(H_int(k) + M_sim1(k)*alpha1)^2)*(H_int(k) + M_sim1(k)*alpha1)*(m - 1) - Ms1*(coth((H_int(k) + M_sim1(k)*alpha1)/a1) - a1/(H_int(k) + M_sim1(k)*alpha1))*(m - 1);

            dM_dH1(k) = (1/(1+ c1)) * ( M_an1 - M_sim1(k)) /(   k1*delta(k) - alpha1/400*(M_an1 - M_sim1(k))) + c1/(1+c1)*dMan_dH1;
            
            M_sim1(k+1) = M_sim1(k) + dM_dH1(k)*(H_int(k+1)-H_int(k));

        end
        
end