function [M_sim1,dM_dH1,M_sim_combined] = integrate_vector_JA_loop_dirsc_5prms(H_int,M_sim1,param,param_sig)

        
        nmbr_models = size(param_sig,1);

        Ms1     = param(:,1); % Spalte hat Dimension Anzahl der Modelle
        a1      = param(:,2);
        alpha1  = param(:,3);       
        k1      = param(:,4);
        c1      = param(:,5);        
        
        dM_dH1 = zeros(nmbr_models,size(H_int,2));
        sigm   = ones(nmbr_models,size(H_int,2));
        delta = zeros(size(H_int)); % nur als ein Spalenvektor
        M_sim_combined = zeros(size(H_int)); % nur als ein Spalenvektor
        M_sim_combined(1) = M_sim1(1);
        for k = 1:length(H_int)-1  
        
            % hier das Argmuent der Sigmoiden bestimmen
            % param_sig ist eine Zelle und enthält die Sigmoidenvektoren
            % als Spaltenvektoren [a1 ;a2 ; a3..] + x *[b1 ;b2 ; b3..]
            
            for i =  1:nmbr_models             
                for m = 1:size(param_sig{i,1},1)                                    
                    sigm(i,k) = sigm(i,k)*0.5*(1+tanh(param_sig{i}(m,1) + param_sig{i}(m,2)*abs(H_int(k))));     
                end
            end     
            
            % Sigmoiden auswerten
            if k>1
                M_sim1(:,k) = M_sim_combined(k);
            end
            
            delta(k) = sign(H_int(k+1)-H_int(k));            
            He1 = H_int(k)+alpha1.*M_sim1(:,k);            
            M_an1 = Ms1.*(coth(He1./a1)-a1./He1); 
            
            dMan_dH1 = -Ms1.*((coth((H_int(k) + M_sim1(:,k).*alpha1)./a1).^2 - 1)./a1 - a1./(H_int(k) + M_sim1(:,k).*alpha1).^2); 
         
            dM_dH1(:,k) = (1./(1+ c1)).*( M_an1 - M_sim1(:,k)) ./(   k1.*delta(k) - alpha1.*(M_an1 - M_sim1(:,k))) + c1./(1+c1).*dMan_dH1;
            
            M_sim1(:,k+1) = M_sim1(:,k) + dM_dH1(:,k).*(H_int(k+1)-H_int(k));
            
            M_sim_combined(k+1)= sum(M_sim1(:,k+1).* sigm(:,k));
        end
        
end