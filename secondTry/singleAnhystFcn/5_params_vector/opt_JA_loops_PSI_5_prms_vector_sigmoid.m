function [sse] = opt_JA_loops_PSI_5_prms_vector_sigmoid(PSI,CRR,param_JA,param_in,param_opt)

sse = 0;
% nmbr_models = size(param_in,1);
sig_param_eval = param_in.*reshape(param_opt',size(param_in));
nmbr_models = size(sig_param_eval,1)+1;
param_sig = cell(nmbr_models,1);

    for i = 1:nmbr_models   
        for j=1:i-1
            param_sig{i}(j,:) =    - sig_param_eval(j,:);
        end        
        
        if i<nmbr_models  
           param_sig{i}(i,:) =    sig_param_eval(i,:);
        end
    end

for i=1:size(CRR,2)

    Curr_meas = CRR{i};
    PSI_meas = PSI{i};
    PSI_meas_in = repmat(PSI_meas,nmbr_models,1);
    [~,~,M_sim_combined] = integrate_vector_JA_loop_dirsc_5prms(Curr_meas,PSI_meas_in,param_JA,param_sig);
    sse = sse + sum(sum( (M_sim_combined(2:end)-PSI_meas(2:end)).^2)); %% .*Curr_meas(2:end).^2
    % sse = sse + sum( (M_sim-PSI_meas).^2);
 
end

end