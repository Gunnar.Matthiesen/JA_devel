addpath('./opt_2params')
addpath('./opt_5_params')
addpath('./5_params_and_k')
    

clear fitmodels

fitmodels(1).index = [6:11];
% fitmodels(2).index = [9:10];
% fitmodels(3).index = [11:12];
% fitmodels(4).index = [14];
% fitmodels(5).index = [17];
%%
close all
ha = axes();
hold on

for i = 1:size(fitmodels,2)


[PSI_meas,Curr_meas,POS_meas,weights] = auto_select(meas_raw,fitmodels(i).index,1);

for k=1:size(Curr_meas,2)
    
    if Curr_meas{k}(1)~=Curr_meas{k}(end)
    
        Curr_meas{k}(end+1)=Curr_meas{k}(1);
        PSI_meas{k}(end+1)=PSI_meas{k}(1);
    end
end




fitmodels(i).PSI_meas = PSI_meas;
fitmodels(i).Curr_meas = Curr_meas;
fitmodels(i).POS_meas = POS_meas;
fitmodels(i).weights = weights;



%          param_in_5_opt_grad =  [0.49111     0.14379     0.39561    0.011033      3.3565]; % MSM
%           param_in_5_opt_grad =  [0.442    0.096471     0.19784    0.013046      3.6353]
%           param_in_5_opt_grad =  [0.43312     0.10612     0.25838    0.011485      4.7424]

%          param_in_5_opt_grad =  [0.40957    0.082735   0.0015549    0.010747      8.2977]; % ERA SIB
%             param_in_5_opt_grad =  [0.41361    0.086289    0.003693   0.010548      4.8056]; % ERA SIB
            param_in_5_opt_grad =  [0.40877    0.082547  1.3633e-05    0.016148      3.6657]; % ERA SIB
%         
%           0.42753     0.11673     0.32392    0.010657      6.2246
          
        eval_opt_anhyst = @(param_opt) opt_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas,Curr_meas,param_in_5_opt_grad,param_opt);
                 
%         lb = [0.9,0.9,0.5,0.5,0.5];
%         ub = [1.1,1.1,1,2,2];

        lb = [0.1,0.1,0.0001,0.1,0.1];
        ub = [1.5,5,10000,5,5];

        options = optimoptions('particleswarm','FunctionTolerance',1e-4,'UseParallel',true,'SwarmSize',6000,'PlotFcn',@pswplotbestf);
        [bestParam5_grad,fval,exitflag,output] = particleswarm(eval_opt_anhyst,5,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam5_grad));
        disp(num2str(param_in_5_opt_grad.*bestParam5_grad));
 
    fitmodels(i).param = param_in_5_opt_grad.*bestParam5_grad;

    PSI_sim=cell(size(PSI_meas));
        
%       figure('name',['flux 5 params:  ',num2str(param_in_5_opt_grad.*bestParam5_grad),'    Fval:' , num2str(fval)])
      for j = 1:size(Curr_meas,2)
            [M_sim,dM_dH1] = eval_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas(j),Curr_meas(j),param_in_5_opt_grad.*bestParam5_grad) ;        
            plot(ha,Curr_meas{j},M_sim,'k')            
            plot(ha,Curr_meas{j},PSI_meas{j},'r')
            PSI_sim{j} = M_sim;
      end
    xlim([0,0.7])
    ylim([0,0.5])
fitmodels(i).PSI_sim = PSI_sim;
fitmodels(i).Curr_meas = Curr_meas;
end


%%