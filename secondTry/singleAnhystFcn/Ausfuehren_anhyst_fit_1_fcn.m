
addpath('./opt_2params')
addpath('./opt_5_params')
this_an_curve = meas_raw(1).freq(1).anhyst_curve_max;
close all
figure
plot(this_an_curve.current,this_an_curve.psi)
hold on;


for i = 4:4:16
  this_curve = meas_raw(1).freq(1).current(i).results(3).flux_curve ;   
  
  plot(this_curve.current_increase_sm,this_curve.psi_increase_sm);
  plot(this_curve.current_decrease_sm,this_curve.psi_decrease_sm);
end
%%

        current = [0.01:0.01:0.7]';
        position = ones(size(current))*6.1232271;       
        
        x = current;
        y = feval(poly_psi_an_max,current,position);

        clear param_in
        param_in(1) = 1.5;% Ms1
        param_in(2) = 1;% a1 
        param_in(3) = 1;% alpha1
        eval_opt_anhyst = @(param_opt) opt_anhyst_1_fcn(x,y,param_in,param_opt);                
        lb = [0.5,0.0001,0.00001];
        ub = [5,5,100]; 
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(eval_opt_anhyst,3,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam));
        disp(num2str(param_in.*bestParam));
        

                
        
% %         param_in(1) = 0.45;% Ms1
% %         param_in(2) = 0.07;% a1 
% %         param_in(3) = 0.11;% alpha1
% %         param_in(4) = 0.4;% 
% %         param_in(5) = 1.8;% 
% %         eval_opt_anhyst = @(param_opt) opt_anhyst_1_fcn_2_cos(x,y,param_in,param_opt);                
% %         lb = [0.0001,0.0001,0.00001,0.0001,0.0001];
% %         ub = [2,2,2,2,5]; 
% %         options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
% %         [bestParam,fval,exitflag,output] = particleswarm(eval_opt_anhyst,5,lb,ub,options);
% %         disp(['Fval:  ', num2str(fval)]);
% %         % param = [Ms,a,alpha,k,c,f_psi,f_i];
% %         disp(num2str(bestParam));
% %         disp(num2str(param_in.*bestParam));
% %         
% %         
% %         param = param_in.*bestParam;
% %         Ms1  = param(1);
% %         a1 = param(2);
% %         alpha1   = param(3);
% %         Ms2  = param(4);
% %         a2 = param(5);
% % 
% % 
% %         x1 = x(:)+alpha1*y(:); 
% % 
% % 
% %         y_dach = Ms1*(coth(x1/a1)-a1./x1)+Ms2*(coth(x1/a2)-a2./x1) ;% + Ms2*(coth(x2/a2)-a2./x2);
% %         figure
% %         plot(x,y_dach)
% %         
% %         hold on
% %         plot(x,y,'k-')
% %         legend('fit','data');
        
  
        
        
        
        param = param_in.*bestParam;
        Ms1  = param(1);
        a1 = param(2);
        alpha1   = param(3);

        x1 = x(:)+alpha1*y(:); 


        y_dach = Ms1*(coth(x1/a1)-a1./x1) ;% + Ms2*(coth(x2/a2)-a2./x2);
        figure
        plot(x,y_dach)
        
        hold on
        plot(x,y,'k-')
        
        %%
        grad_PSI_meas = cell(size(PSI_meas));
        grad_Curr_meas = cell(size(PSI_meas));
        grad_dPSI_meas_dCurr_meas = cell(size(PSI_meas));
        for i = 1: size(PSI_meas,2)
            grad_PSI_meas{i} = gradient(PSI_meas{i});
            grad_Curr_meas{i} = gradient(Curr_meas{i});
            grad_dPSI_meas_dCurr_meas{i} = grad_PSI_meas{i}./ grad_Curr_meas{i};
        end
        param_fix = param_in.*bestParam; % ???
        
        param_in_opt(1) = 0.1;% k1
        param_in_opt(2) = 0.1; % c1
        
        eval_opt_anhyst = @(param_opt) opt_JA_loops_PSI_v2_1_fcn(PSI_meas(3),grad_PSI_meas(3),Curr_meas(3),param_fix,param_in_opt,param_opt);
                
        lb = [0.00001,0.00001];
        ub = [10,10];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam2,fval,exitflag,output] = particleswarm(eval_opt_anhyst,2,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam2));
        disp(num2str(param_in_opt.*bestParam2));
        
        figure
        for i = 3%1:size(PSI_meas,2)
        [M_sim] = eval_JA_loops_PSI_v2_1_fcn(PSI_meas{i},Curr_meas{i},param_fix,param_in_opt.*bestParam2) ;
        
        plot(Curr_meas{i},M_sim)
        hold on
        plot(Curr_meas{i},PSI_meas{i})
        end
  %%
            

  
        param_fix2 =  param_in_opt.*bestParam2;
        clear param_in_5_opt
        param_in_5_opt(1) = param_fix(1); %Ms1     = param(1);
        param_in_5_opt(2) = param_fix(2); %a1     = param(2);
        param_in_5_opt(3) = param_fix(3); %alpha1      = param(3);       
        param_in_5_opt(4) = param_fix2(1);%k1      = param(4);
        param_in_5_opt(5) = param_fix2(2);%c1      = param(5);
  
        %param_in_5_opt =     0.9154    1.5747    5.0000    0.0340    0.1477
        
        eval_opt_anhyst = @(param_opt) opt_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas,Curr_meas,param_in_5_opt,param_opt);
                 
        lb = [0.01,0.01,0.001,0.01,0.1];
        ub = [5,5,5,5,5];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',6000,'PlotFcn',@pswplotbestf);
        [bestParam5,fval,exitflag,output] = particleswarm(eval_opt_anhyst,5,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam5));
        disp(num2str(param_in_5_opt.*bestParam5));
%%        
    figure
  for i = 1:size(Curr_meas,2)
        
        [M_sim,dM_dH1] = eval_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas(i),Curr_meas(i),param_in_5_opt.*bestParam5) ;        
        plot(Curr_meas{i},M_sim,'k')
        hold on
        plot(Curr_meas{i},PSI_meas{i},'r')
  end
  
syms y  

param = param_in_5_opt.*bestParam5;

Ms1  = param(1);
a1 = param(2);
alpha1   = param(3);

plt_current = linspace(0.005,max(Curr_meas{end}),100); 
M_an = zeros(size(plt_current));
for i = 1:length(plt_current)
this_crr= double(plt_current(i));
eqn = @(y) y-Ms1*(coth((this_crr+alpha1*y)/a1)-a1./(this_crr+alpha1*y));
M_an(i) = vpasolve(eqn(y)==0 , y);
end
% figure 
plot(plt_current,M_an,'b');
plot(this_an_curve.current,this_an_curve.psi,'g')
grid on;

      figure
  for i =1:size(Curr_meas,2)
        
        [M_sim,dM_dH1] = eval_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas(i),Curr_meas(i),param_in_5_opt.*bestParam5) ;        
        plot(Curr_meas{i},dM_dH1,'k')
        hold on
        plot(Curr_meas{i},grad_PSI_meas{i}./grad_Curr_meas{i},'r')
  end
  

 %% Opt Gradient

 
        param_in_5_opt_grad = param_in_5_opt.*bestParam5;

        
        
        eval_opt_anhyst = @(param_opt) opt_JA_loops_PSI_v2_1_fcn_5_prms_plus_grad(PSI_meas,grad_dPSI_meas_dCurr_meas,Curr_meas,param_in_5_opt_grad,param_opt);
                 
        lb = [0.2,0.2,0.2,0.2,0.2];
        ub = [5,5,100,5,19000];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',6000,'PlotFcn',@pswplotbestf);
        [bestParam5_grad,fval,exitflag,output] = particleswarm(eval_opt_anhyst,5,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam5_grad));
        disp(num2str(param_in_5_opt_grad.*bestParam5_grad));
 
        
        
            figure
  for i = 1:size(Curr_meas,2)
        
        [M_sim,dM_dH1] = eval_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas(i),Curr_meas(i),param_in_5_opt_grad.*bestParam5_grad) ;        
        plot(Curr_meas{i},M_sim,'k')
        hold on
        plot(Curr_meas{i},PSI_meas{i},'r')
  end
  syms y  

param = param_in_5_opt_grad.*bestParam5_grad;

Ms1  = param(1);
a1 = param(2);
alpha1   = param(3);

plt_current = linspace(0.005,max(Curr_meas{end}),100); 
M_an = zeros(size(plt_current));
for i = 1:length(plt_current)
this_crr= double(plt_current(i));
eqn = @(y) y-Ms1*(coth((this_crr+alpha1*y)/a1)-a1./(this_crr+alpha1*y));
M_an(i) = vpasolve(eqn(y)==0 , y);
end
% figure 
plot(plt_current,M_an,'b');
plot(this_an_curve.current,this_an_curve.psi,'g')
grid on;
  
  
  
 
       figure
  for i = 1:size(Curr_meas,2)
        
        [M_sim,dM_dH1] = eval_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas(i),Curr_meas(i),param_in_5_opt_grad.*bestParam5_grad) ;        
        plot(Curr_meas{i},dM_dH1,'k')
        hold on
        plot(Curr_meas{i},grad_PSI_meas{i}./grad_Curr_meas{i},'r')
  end
  
 
 
 %%
 
 
 %
 
         param_in_6_opt =  param_in_5_opt.*bestParam5;
         param_in_6_opt(6) = 0;  

        param_in_6_opt(6) = param_in_6_opt(5)*0.05;
        
        %param_in_5_opt =     0.9154    1.5747    5.0000    0.0340    0.1477
        
        eval_opt_anhyst = @(param_opt) opt_JA_loops_PSI_v2_1_fcn_5_prms_plus_k(PSI_meas,Curr_meas,param_in_6_opt,param_opt);
 
                
        lb = [0.01,0.01,0.001,0.01,0.1,-20];
        ub = [5,5,5,5,0.5,10];
        
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',6000,'PlotFcn',@pswplotbestf);
        [bestParam6,fval,exitflag,output] = particleswarm(eval_opt_anhyst,6,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam6));
        disp(num2str(param_in_6_opt.*bestParam6));
 %%       
        figure('name',num2str(param_in_6_opt.*bestParam6))
  for i = 1:3
        
        [M_sim] = eval_JA_loops_PSI_v2_1_fcn_5_prms_plus_k(PSI_meas(i),Curr_meas(i),param_in_6_opt.*bestParam) ;
        
        plot(Curr_meas{i},M_sim,'k')
        hold on
        plot(Curr_meas{i},PSI_meas{i},'r')
  end    
  grid on
  grid minor
  param = param_in_6_opt.*bestParam6;

Ms1  = param(1);
a1 = param(2);
alpha1   = param(3);

plt_current = linspace(0.005,max(Curr_meas{end}),100); 
M_an = zeros(size(plt_current));
for i = 1:length(plt_current)
this_crr= double(plt_current(i));
eqn = @(y) y-Ms1*(coth((this_crr+alpha1*y)/a1)-a1./(this_crr+alpha1*y));
M_an(i) = vpasolve(eqn(y)==0 , y);
end
% figure 
plot(plt_current,M_an,'b');
plot(this_an_curve.current,this_an_curve.psi,'g')
grid on;