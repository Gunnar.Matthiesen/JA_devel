function  sse = opt_anhyst_1_fcn_2_cos(x,y,param,param_opt)

param = param.*param_opt;

Ms1  = param(1);
a1 = param(2);
alpha1   = param(3);
Ms2  = param(4);
a2 = param(5);


x1 = x+alpha1*y; 
% x2 = x+alpha2*y;

y_dach = Ms1*(coth(x1/a1)-a1./x1)+Ms2*(coth(x1/a2)-a2./x1);%  + Ms2*(coth(x2/a2)-a2./x2);

sse = sum((y-y_dach).^2);
end
