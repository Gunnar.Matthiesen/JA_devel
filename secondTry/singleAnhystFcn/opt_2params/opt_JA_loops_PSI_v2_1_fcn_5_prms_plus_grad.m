function [sse] = opt_JA_loops_PSI_v2_1_fcn_5_prms_plus_grad(PSI,grad_dPSI_meas_dCurr_meas,weights,CRR,param_in,param_opt)

sse = 0;

param = param_in.*param_opt;

    for i=1:size(CRR,2)

    Curr_meas = CRR{i};
    PSI_meas = PSI{i};

    [M_sim,dM_dH1] = evaluate_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas,Curr_meas,param);

%     sse = sse + sum( (M_sim-PSI_meas).^2)+sum((dM_dH1-grad_dPSI_meas_dCurr_meas{i}).^2 .* Curr_meas.^2.*weights{i})/5;
    sse = sse + sum( (M_sim-PSI_meas).^2.*(1+Curr_meas.^2))+sum((dM_dH1-grad_dPSI_meas_dCurr_meas{i}).^2 .*Curr_meas.^2.*weights{i})/10;
    end

end