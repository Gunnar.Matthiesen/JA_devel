function [sse] = opt_JA_loops_PSI_v2_1_fcn(PSI,grad_PSI_meas,CRR,param_fix,param_in,param_opt)

sse = 0;

param = param_in.*param_opt;

for i=1 %1:size(CRR,2)

Curr_meas = CRR{i};
PSI_meas = PSI{i};

[M_sim,~] = evaluate_JA_loops_PSI_v2_1_fcn(PSI_meas,Curr_meas,param_fix,param);

sse = sse + sum( (M_sim-PSI_meas).^2);
end

end