function  sse = opt_anhyst_1_fcn(x,y,param,param_opt)

param = param.*param_opt;

Ms1     = param(1);
a1      = param(2);
alpha1  = param(3);
% f_psi   = param(4);% f_psi
% f_i     = param(5);% f_i

% my0 = 4e-7;
% M = f_psi*y/my0 - x*f_i;
% x1 = x*f_i+alpha1*M; 
x1 = x+alpha1*y; 
y_dach = Ms1*(coth(x1/a1)-a1./x1);%  + Ms2*(coth(x2/a2)-a2./x2);

sse = sum((y-y_dach).^2);
end
