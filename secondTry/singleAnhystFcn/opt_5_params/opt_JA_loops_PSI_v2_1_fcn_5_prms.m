function [sse] = opt_JA_loops_PSI_v2_1_fcn_5_prms(PSI,CRR,param_in,param_opt)

sse = 0;

param = param_in.*param_opt;

for i=1:size(CRR,2)

Curr_meas = CRR{i};
PSI_meas = PSI{i};

[M_sim,dM_dH1] = evaluate_JA_loops_PSI_v2_1_fcn_5_prms(PSI_meas,Curr_meas,param);

sse = sse + sum( (M_sim-PSI_meas).^2.*abs(Curr_meas+0.2).^2)*100;
% sse = sse + sum( (M_sim-PSI_meas).^2);
end

end