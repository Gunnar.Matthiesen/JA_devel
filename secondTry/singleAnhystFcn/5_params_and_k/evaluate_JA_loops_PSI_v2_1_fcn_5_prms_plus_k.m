function [M_sim,dM_dH1] = evaluate_JA_loops_PSI_v2_1_fcn_5_prms_plus_k(PSI_meas,Curr_meas,param)
    % an der Stelle PSI_sim(1,1) muss der Startwert M0 stehen und in Loops in Curr_sim müssen die gleiche Länge auf auf und absteigendem Ast haben 
    % Curr_sim is a matrix 
    

    M01 =  PSI_meas(1);
    M_sim1 = zeros(size(Curr_meas));


    j_max = 1;
    for j = 1:j_max
                
                M_sim1(1) = M01;
                [M_sim1,dM_dH1] = integrate_JA_loop_discr_PSI_v2_1_fcn_5_prms_plus_k(Curr_meas,M_sim1,param);     
                M01 = M_sim1(end);
 
     end
     M_sim = M_sim1;
    
    
end