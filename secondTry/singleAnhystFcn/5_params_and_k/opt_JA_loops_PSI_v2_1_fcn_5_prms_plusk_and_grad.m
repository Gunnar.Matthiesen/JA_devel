function [sse] = opt_JA_loops_PSI_v2_1_fcn_5_prms_plusk_and_grad(PSI,grad_dPSI_meas_dCurr_meas,weights,CRR,param_in,param_opt)

sse = 0;

param = param_in.*param_opt;

for i=1:3%size(CRR,2)

Curr_meas = CRR{i};
PSI_meas = PSI{i};

[M_sim,dM_dH1] = evaluate_JA_loops_PSI_v2_1_fcn_5_prms_plus_k(PSI_meas,Curr_meas,param);

sse = sse + sum( (M_sim-PSI_meas).^2)+sum((dM_dH1(1:end-1)-grad_dPSI_meas_dCurr_meas{i}(1:end-1)).^2 .* Curr_meas(1:end-1).^2.*weights{i}(1:end-1))/5;
end

end