

close all
figure

  ha =axes();
  hold on

  a_opt=[];
  alpha_opt=[];
  k_opt = [];
  best_param = [];
  c_opt = [];
  MS_opt = [];
  
  clear  best_param  opt_param
  %%
    direction ='forward';
% direction ='backwards';
  i_loop =1;
% for i =6:13
for i =6:6
   i_loop = i_loop+1; 
    PSI_meas{1}=  ja_fit_res(1).postion(i).PSI_meas;
    Curr_meas{1}=  ja_fit_res(1).postion(i).Curr_meas;

    grad_PSI_meas = cell(size(PSI_meas));
    grad_Curr_meas = cell(size(PSI_meas));
    grad_dPSI_meas_dCurr_meas = cell(size(PSI_meas));
for j = 1: size(PSI_meas,2)
    grad_PSI_meas{j} = gradient(PSI_meas{j});
    grad_Curr_meas{j} = gradient(Curr_meas{j});
    grad_dPSI_meas_dCurr_meas{j} = grad_PSI_meas{j}./ grad_Curr_meas{j};
end

    
    this_crr =  max(Curr_meas{1});
    this_alpha = fit_alpha(this_crr);
    this_c = fit_c(this_crr);
    this_Ms = fit_Ms(this_crr);
    this_k = 0.015;
        
    param_fix = [this_Ms,this_alpha,this_c,this_k];
        
    if i_loop==1
        param_in_opt(1) = 0.5;% a
        param_in_opt(2) = 0.015; % k
        param_in_opt(3) = this_c;
        param_in_opt(4) = this_alpha;
        lb = [0.01,0.01,0.5];
        ub = [10,1.1,3,3];
    else   
        
        if strcmp(direction,'forward')
            param_in_opt(1) = a_opt(i-1);% a
            param_in_opt(2) = k_opt(i-1); % k
            param_in_opt(3) = 9;%c_opt(i-1);
            param_in_opt(4) = alpha_opt(i-1);
            lb = [0.5,0.5,0.95,0.75];
            ub = [2,1.1,3,1.25];
        else
            param_in_opt(1) = a_opt(i+1);% a
            param_in_opt(2) = k_opt(i+1); % k
            param_in_opt(3) = c_opt(i+1);
            param_in_opt(4) = alpha_opt(i+1);
            lb = [0.5,0.5,0.5,0.75];
            ub = [2,1,1,1.25];
        end

    end
        eval_opt_anhyst = @(param_opt) opt_JA_loops_PSI_v2_1_fcn(PSI_meas(1),grad_dPSI_meas_dCurr_meas(1),weights(1),Curr_meas(1),param_fix,param_in_opt,param_opt);
                

        options = optimoptions('particleswarm','FunctionTolerance',1e-4,'UseParallel',true,'SwarmSize',4000,'PlotFcn',@pswplotbestf);
        [bestParam2,fval,exitflag,output] = particleswarm(eval_opt_anhyst,4,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        disp(['bestParam2:  ', num2str(bestParam2)]);
                
          best_param(i,:) = bestParam2;
          opt_param(i,:) = param_in_opt.*bestParam2;
          a_opt(i) = opt_param(i,1);
          k_opt(i) = opt_param(i,2);
          c_opt(i)  = opt_param(i,3); 
          alpha_opt(i) = opt_param(i,4); 
        
        disp(num2str(bestParam2));
        disp(num2str(param_in_opt.*bestParam2));     
        [M_sim] = eval_JA_loops_PSI_v2_1_fcn(PSI_meas{1},Curr_meas{1},param_fix,param_in_opt.*bestParam2) ;   
        
        plot(ha,Curr_meas{:},M_sim,'k')      
        plot(ha,Curr_meas{:},PSI_meas{:},'r')
        
end
%%
figure
 plot(a_opt)
 hold on
 plot(k_opt)
 plot(c_opt)
 plot(alpha_opt)
        