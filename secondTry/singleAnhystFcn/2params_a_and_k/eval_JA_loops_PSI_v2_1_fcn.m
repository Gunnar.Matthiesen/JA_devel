function [M_sim] = eval_JA_loops_PSI_v2_1_fcn(PSI,CRR,param_fix,param_in)


for i=1;%:size(CRR,2)

Curr_meas = CRR;
PSI_meas = PSI;


hold on;
M_sim = evaluate_JA_loops_PSI_v2_1_fcn(PSI_meas,Curr_meas,param_fix,param_in);


end

end