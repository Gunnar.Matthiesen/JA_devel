%%

clear current Ms alpha Ms a c k
%%
j=1
for i = 1:13
hold on;
 alpha(i) = ja_fit_res(i).postion(j).param(3);
 current(i) =max(ja_fit_res(i).postion(j).Curr_meas);
 Ms(i)= ja_fit_res(i).postion(j).param(1);
  a(i)= ja_fit_res(i).postion(j).param(2);
  c(i)= ja_fit_res(i).postion(j).param(5);
  k(i)= ja_fit_res(i).postion(j).param(4);
  if mod(i,14)==0
     j = j+1; 
  end
end
%%
j=1;
for i = 1:182
   pos(i) = j; 
    
  if mod(i,14)==0
     j = j+1; 
  end
    
end

%%

close all
figure('name','Ms');
for i = 1:14
hold on;
 scatter(i,ja_fit_res(i).param(1));
end

figure('name','a');
for i = 1:14
hold on;
scatter(i,ja_fit_res(i).param(2));
end

figure('name','alpha');
for i = 1:14
hold on;
scatter(i,ja_fit_res(i).param(3));
end


figure('name','k');
for i = 1:14
hold on;
scatter(i,ja_fit_res(i).param(4));
end

figure('name','c');
for i = 1:14
hold on;
scatter(i,ja_fit_res(i).param(5));
end