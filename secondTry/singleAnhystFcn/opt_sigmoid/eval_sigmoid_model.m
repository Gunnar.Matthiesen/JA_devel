function fitmodels = eval_sigmoid_model(fitmodels,sig_param)

%     param_opt = ones(size(sig_param));
    
    %  reshape([1 2, 3 4]',2,2) leads to    [1 3; 2 4]
    nmbr_models = size(fitmodels,2);
        nmbr_sig_datapoints = 200;
    sig_datapoints = linspace(0,0.7,nmbr_sig_datapoints);  
    
    model = calc_sigmoid_model(fitmodels,sig_param);
    
    % interpoliere die Sigmoiden auf den Modellausgang
    my_sse = 0;   
    for i = 1:nmbr_models    
         % berechne die gewichtete Modellantwort
          fitmodels(i).sigmoid = model(i,:);
         for j = 1:size( fitmodels(i).Curr_meas,2)  
            fitmodels(i).PSI_sim_weighted{j} = fitmodels(i).PSI_sim{j}.* ...
            interp1(sig_datapoints,model(i,:), abs(fitmodels(i).Curr_meas{j}),'linear') ;
         end
    end
    
end