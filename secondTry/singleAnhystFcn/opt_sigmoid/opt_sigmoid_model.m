function my_sse = opt_sigmoid_model(fitmodels,sig_param,param_opt)

%     param_opt = ones(size(sig_param));
    
    nmbr_sig_datapoints = 200;
    sig_datapoints = linspace(0,0.7,nmbr_sig_datapoints);    

    %  reshape([1 2, 3 4]',2,2) leads to    [1 3; 2 4]
    nmbr_models = size(fitmodels,2);
%     param_opt = reshape(param_opt',2,2);
    param_opt = reshape(param_opt',size(sig_param));
    sig_param= sig_param.*param_opt;
    
    model = calc_sigmoid_model(fitmodels,sig_param);
    
    % interpoliere die Sigmoiden auf den Modellausgang
    my_sse = 0;   
    for i = 1:nmbr_models    
         % berechne die gewichtete Modellantwort
         for j = 1:size( fitmodels(i).Curr_meas,2)  
            my_sse = my_sse +  sum ((fitmodels(i).PSI_meas{j}- fitmodels(i).PSI_sim{j}).^2.* ...
            interp1(sig_datapoints,model(i,:), abs(fitmodels(i).Curr_meas{j}),'linear') );
         end
    end
end