function model = calc_sigmoid_model(fitmodels,sig_param)

    nmbr_models = size(fitmodels,2);    
    nmbr_sig_datapoints = 200;
    sig_datapoints = linspace(0,0.7,nmbr_sig_datapoints);    
    
    % berechne die sigmoiden über dem eingangsbereich
    eps = sig_param(:,1)+ sig_param(:,2).*repmat(sig_datapoints,nmbr_models-1,1);% Strom als Zeilenvektoren 
    psi = 0.5*(1+tanh(eps));
    psi_bar = 0.5*(1+tanh(-eps));

    % setzte die sigmoiden für die Modelle zusammen
    model = ones(nmbr_models,nmbr_sig_datapoints);
    for i = 1:nmbr_models   
        for j=1:i-1
            model(i,:) =    model(i,:).*psi_bar(j,:);
        end        
        
         if i<nmbr_models  
           model(i,:) =    model(i,:).*psi(i,:);
         end
%         plot(model(i,:))
    end
    
end