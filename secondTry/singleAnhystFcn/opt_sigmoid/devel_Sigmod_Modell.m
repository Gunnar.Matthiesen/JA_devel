
    nmbr_models = size(fitmodels,2);
    % platziere eine gewichtungsfunktion unter jedem model

    % get max current from model1 and model 2
    CRR_max = zeros(nmbr_models,1);
    for i = 1:nmbr_models   
        fitmodels(i).curr_max = max([fitmodels(i).Curr_meas{:}]);
        CRR_max(i)=fitmodels(i).curr_max;
    end

    % put active sigmoide to be one there and chose initial b to be in the
    % model structure
    % [a1 ;a2 ; a3..] + sign_vec *[b1 ;b2 ; b3..].*[x1(t+0*dt),x1(t+1*dt),x1(t+2*dt),... ;  x2(t+0*dt),x2(t+1*dt),x2(t+2*dt); ... x3(t+0*dt),x3(t+1*dt),x3(t+2*dt),...]
    % middle of the two funktions
    CRR_middle = CRR_max(1:end-1)+diff(CRR_max)/2;
    sig_param = zeros(2,nmbr_models-1  );
    for i = 1:nmbr_models-1  
        sig_param(i,:) = [inv([[1;2],[CRR_max(i);CRR_middle(i)]])*[10;0]];
    end
    % each row! carries a models parameters

    param_opt = ones(size(sig_param));

    nmbr_models = size(fitmodels,2);
    nmbr_sig_datapoints = 200;
    sig_datapoints = linspace(0,0.7,nmbr_sig_datapoints);    
    % lade alle Modellantworten
    sig_param_opt = sig_param.*param_opt;
    % berechne die sigmoiden über dem eingangsbereich
    eps = sig_param_opt(:,1)+ sig_param_opt(:,2).*repmat(sig_datapoints,nmbr_models-1,1);% Strom als Zeilenvektoren 
    psi = 0.5*(1+tanh(-eps));
    psi_bar = 0.5*(1+tanh(eps));
    % setzte die sigmoiden für die Modelle zusammen
    model = ones(nmbr_models,nmbr_sig_datapoints);
    for i = 1:nmbr_models        
        if i<nmbr_models             
           i_max =  i-1;
           model(i,:) =    model(i,:).*psi(i,:);
        else
           i_max =  i-2; 
           model(i,:) =    model(i,:).*psi_bar(i-1,:);
        end
        
        for j=1:i_max
            model(i,:) =    model(i,:).*psi_bar(j,:);
        end
                    
    end
    
    % interpoliere die Sigmoiden auf den Modellausgang
    my_sse = 0;   

    for i = 1:nmbr_models    
         % berechne die gewichtete Modellantwort
         for j = 1:size( fitmodels(i).Curr_meas,2)  
            my_sse = my_sse +  sum ((fitmodels(i).PSI_meas{j}- fitmodels(i).PSI_sim{j}).^2.* ...
            interp1(sig_datapoints,model(i,:), abs(fitmodels(i).Curr_meas{j}),'linear') );
         end
    end
    
    