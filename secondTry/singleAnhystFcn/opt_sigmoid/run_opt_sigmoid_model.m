
        addpath('./opt_2params')
        addpath('./opt_5_params')
        addpath('./5_params_and_k')
        addpath('./opt_5_params')
        addpath('./opt_sigmoid')
        addpath('./5_params_vector');

        nmbr_models = size(fitmodels,2);
        % platziere eine gewichtungsfunktion unter jedem model

        % get max current from model1 and model 2
        CRR_max = zeros(nmbr_models,1);
        for i = 1:nmbr_models   
            fitmodels(i).curr_max = max([fitmodels(i).Curr_meas{:}]);
            CRR_max(i) = fitmodels(i).curr_max;
        end
        % put active sigmoide to be ione there and chose initial b to be in the
        % model structure
        % [a1 ;a2 ; a3..] + sign_vec *[b1 ;b2 ; b3..].*[x1(t+0*dt),x1(t+1*dt),x1(t+2*dt),... ;  x2(t+0*dt),x2(t+1*dt),x2(t+2*dt); ... x3(t+0*dt),x3(t+1*dt),x3(t+2*dt),...]
        % middle of the two funktions
        CRR_middle = CRR_max(1:end-1)+diff(CRR_max)/2;
        sig_param = zeros(nmbr_models-1,2  );
        for i = 1:nmbr_models-1  
            sig_param(i,:) = [inv([[1;1],[CRR_max(i);CRR_middle(i)]])*[10;0]];
        end
%%      in order to smooth the transition between the model's neighboring current measurements have to
 %      be added to the set an calculated 
        fitmodels_opt = fitmodels;
        nmbr_samples_add_max =  5;
        for i = 1:nmbr_models 
            fitmodels_opt(i).index_old = 1:length(fitmodels_opt(i).index);
        end
        for i = 1:nmbr_models             
            if i ==1
                add_end = true;
                add_lead = false; 
            elseif i==nmbr_models      
                add_lead = true;
                add_end= false;
            else                
                add_lead = true;
                add_end = true;
            end    
            if add_lead 
                
                nmbr_samples_prev_set = length(fitmodels_opt(i-1).index_old);
                if nmbr_samples_add_max <= nmbr_samples_prev_set
                   nmbr_samples_add = nmbr_samples_add_max;
                else
                    nmbr_samples_add = nmbr_samples_prev_set;
                end
                fitmodels_opt(i).index_old = fitmodels_opt(i).index_old+nmbr_samples_add;
                fitmodels_opt(i).Curr_meas(nmbr_samples_add+1:end+nmbr_samples_add) = fitmodels_opt(i).Curr_meas;
                index_copy = fitmodels_opt(i-1).index_old(end+1-nmbr_samples_add:end);
                fitmodels_opt(i).Curr_meas(1:nmbr_samples_add) = fitmodels_opt(i-1).Curr_meas(index_copy);
                fitmodels_opt(i).PSI_meas(nmbr_samples_add+1:end+nmbr_samples_add) = fitmodels_opt(i).PSI_meas;                
                fitmodels_opt(i).PSI_meas(1:nmbr_samples_add) = fitmodels_opt(i-1).PSI_meas(index_copy); 
                fitmodels_opt(i).index = fitmodels_opt(i).index+nmbr_samples_add;
            end            
            if add_end                
                nmbr_samples_follow_set = length(fitmodels_opt(i+1).index_old);
                
                if nmbr_samples_add_max <= nmbr_samples_follow_set
                   nmbr_samples_add = nmbr_samples_add_max;
                else
                    nmbr_samples_add = nmbr_samples_follow_set;
                end   
                
                index_copy = fitmodels_opt(i+1).index_old(1:nmbr_samples_add);
                fitmodels_opt(i).Curr_meas(end+1:end+nmbr_samples_add) = fitmodels_opt(i+1).Curr_meas(index_copy);
                fitmodels_opt(i).PSI_meas(end+1:end+nmbr_samples_add) = fitmodels_opt(i+1).PSI_meas(index_copy);
            end            
        end
        
%% 
figure     
ha=axes();
hold on
% figure
% ha2=axes();
% hold on
 for i = 1:nmbr_models    
      for j = 1:size(fitmodels_opt(i).Curr_meas,2)
            [M_sim,dM_dH1] = eval_JA_loops_PSI_v2_1_fcn_5_prms(fitmodels_opt(i).PSI_meas(j),fitmodels_opt(i).Curr_meas(j),fitmodels_opt(i).param) ;        
            plot(ha,fitmodels_opt(i).Curr_meas{j},M_sim,'k')            
            plot(ha,fitmodels_opt(i).Curr_meas{j},fitmodels_opt(i).PSI_meas{j},'r')
            fitmodels_opt(i).PSI_sim{j} = M_sim;      
      end
 end       
        
        %%         
        eval_opt_sigmoid_model = @(param_opt) opt_sigmoid_model(fitmodels_opt,sig_param,param_opt);                
        lb = [0.95,0.95,0.95,0.95,0.1,0.1,0.1,0.1];
        ub = [1.05,1.05,1.05,1.05,100,100,100,100]; 
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(eval_opt_sigmoid_model,8,lb,ub,options);
        disp(bestParam);
        disp(sig_param.*reshape(bestParam',size(sig_param)))
        %%
        
         sig_param_eval = sig_param.*reshape(bestParam',size(sig_param));
%         sig_param_eval  = sig_param;      

        nmbr_sig_datapoints = 200;
        sig_datapoints = linspace(0,0.7,nmbr_sig_datapoints);
        
        fitmodels = eval_sigmoid_model(fitmodels,sig_param_eval);
        
        nmbr_models = size(fitmodels,2);
        figure
        hold on
        for i = 1:nmbr_models
         plot(sig_datapoints,fitmodels(i).sigmoid) 
        end
        
        figure
        hold on
        
        for i = 1:nmbr_models
            for j = 1:size(fitmodels(i).PSI_meas,2)
            plot(fitmodels(i).Curr_meas{j},fitmodels(i).PSI_sim_weighted{j});
            end
        end
        
%%
figure     
ha=axes();
hold on
fitmodels_eval = fitmodels;


model = calc_sigmoid_model(fitmodels,sig_param);

 for i = 1:nmbr_models 
      for j = 1:size(fitmodels_eval(i).Curr_meas,2)
            [M_sim,dM_dH1] = eval_JA_loops_PSI_v2_1_fcn_5_prms(fitmodels_eval(i).PSI_meas(j),fitmodels_eval(i).Curr_meas(j),fitmodels_eval(i).param) ;        
            fitmodels_eval(i).PSI_sim{j} = M_sim;
            fitmodels_eval(i).PSI_sim_weighted{j} = M_sim.*interp1(sig_datapoints,model(i,:), abs(fitmodels_eval(i).Curr_meas{j}),'linear');
      end
 end      
 
for j = 1:size(fitmodels_eval(1).Curr_meas,2)
     PSI_sum = fitmodels_eval(1).PSI_sim_weighted{j};
     for i = 2:nmbr_models 
        PSI_sum = PSI_sum + fitmodels_eval(i).PSI_sim_weighted{j}; 
     end   
     plot(ha,fitmodels_eval(i).Curr_meas{j},PSI_sum);
end


    for i = 1:nmbr_models   
        for j=1:i-1
            model(i,:) =    model(i,:).*psi_bar(j,:);
        end        
        
         if i<nmbr_models  
           model(i,:) =    model(i,:).*psi(i,:);
         end
%         plot(model(i,:))
    end
%% SET UP SIGMOID CELL FOR EVALUATION
sig_param_eval = sig_param.*reshape(bestParam',2,2);
param_sig = cell(nmbr_models,1);
    for i = 1:nmbr_models   
        for j=1:i-1
            param_sig{i}(j,:) =    - sig_param_eval(j,:);
        end        
        
        if i<nmbr_models  
           param_sig{i}(i,:) =    sig_param_eval(i,:);
        end
    end
%set up vector for JA Models

param_JA = [];
for i = 1:size(fitmodels,2)
    param_JA(i,:) = fitmodels(i).param;
end

%% Integration der Modelle parallel

    

    addpath('./5_params_vec tor')
    H_int = linspace(0,0.25,500);
    H_int = [H_int,flip(H_int)];
    H_int = [H_int,-flip(H_int)];
    H_int = repmat(H_int,1,1);
%
    H_int2 = linspace(0,0.15,500);
    H_int2 = [H_int2,flip(H_int2)];
    H_int2 = [H_int2,-flip(H_int2)];
    H_int2 = repmat(H_int2,1,2);
    H_int = [H_int,H_int2];

    M_sim = zeros(nmbr_models,size(H_int,2));
    M_sim(:,1) = 0.0001;

    [M_sim1,dM_dH1,M_sim_combined] = integrate_vector_JA_loop_dirsc_5prms(H_int,M_sim,param_JA,param_sig);

    figure
    plot(repmat(H_int,nmbr_models,1)',M_sim1')
    grid on
    hold on;
    plot(H_int,M_sim_combined,'k')
%%         



%     CRR = fitmodels;%(5).Curr_meas(2);
%     PSI = fitmodels;%(5).PSI_meas(2);

        CRR = {meas_raw(6).freq.current(14).results(1).init_curve.current};
        PSI = {meas_raw(6).freq.current(14).results(1).init_curve.psi};

    eval_opt_sigmoid_model = @(param_opt) opt_JA_loops_PSI_5_prms_vector_sigmoid(PSI,CRR,param_JA,sig_param,param_opt);             
        lb = [0.9,0.9,0.9,0.9,0.1,0.1,0.1,0.1];
        ub = [1.1,1.1,1.1,1.1,10,10,10,10];
    options = optimoptions('particleswarm','FunctionTolerance',1e-3,'UseParallel',true,'SwarmSize',1000,'PlotFcn',@pswplotbestf);
    [bestParam,fval,exitflag,output] = particleswarm(eval_opt_sigmoid_model,8,lb,ub,options);
    disp(bestParam);
    disp(sig_param.*reshape(bestParam',size(sig_param)))
    
    
    
    sig_param_eval = sig_param.*reshape(bestParam',size(sig_param));
    param_sig = cell(nmbr_models,1);
    for i = 1:nmbr_models   
        for j=1:i-1
            param_sig{i}(j,:) =    - sig_param_eval(j,:);
        end        
        
        if i<nmbr_models  
           param_sig{i}(i,:) =    sig_param_eval(i,:);
        end
    end    
    
    [M_sim1,dM_dH1,M_sim_combined] = integrate_vector_JA_loop_dirsc_5prms(CRR{:},repmat(PSI{:},5,1),param_JA,param_sig);

    figure
%     plot(repmat(CRR{:},3,1)',M_sim1')
    plot(CRR{:},PSI{:},'--r')
    grid on
    hold on;
    plot(CRR{:},M_sim_combined,'k')
    
    
