
    addpath('./opt_2params')
    addpath('./opt_5_params')
    addpath('./5_params_and_k')
    
    this_an_curve = meas_raw(1).freq(1).anhyst_curve_max;
    close all
    figure
    plot(this_an_curve.current,this_an_curve.psi,'-k')
    hold on;


    for i = 4:4:16
      this_curve = meas_raw(1).freq(1).current(i).results(3).flux_curve ; 
      plot(this_curve.current_increase_sm,this_curve.psi_increase_sm);
      plot(this_curve.current_decrease_sm,this_curve.psi_decrease_sm);
    end
    grid on
    
%% First shot for the anhysteretic function

        current = [0.01:0.01:0.7]';
        position = ones(size(current))*6.1232271;       
        
        x = current;
        y = feval(poly_psi_an_max,current,position);

        clear param_in
        param_in(1) = 1.5;% Ms1
        param_in(2) = 1;% a1 
        param_in(3) = 1;% alpha1
        param_in(4) = 0.1;% alpha1
        eval_opt_anhyst = @(param_opt) opt_anhyst_1_fcn_5_prms_mod_sig(x,y,param_in,param_opt);                
        lb = [0.000001,0.0001,0.00001,0.00001];
        ub = [5,10,500,100]; 
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(eval_opt_anhyst,4,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam));
        disp(num2str(param_in.*bestParam));
        
        % evaluate and compate
        param = param_in.*bestParam;
        Ms1  = param(1);
        a1 = param(2);
        alpha1   = param(3);
        m   = param(4);
        x1 = x(:)+alpha1*y(:); 
        y_dach = Ms1*(coth(x1/a1)-a1./x1).*(1-m).*x1; 
        figure
        plot(x,y_dach)        
        hold on
        plot(x,y,'k-')
        legend('fit','data')
        

       dMan_dH1 = Ms1*((coth((x + y*alpha1)./a1).^2 - 1)/a1 - a1./(x + y).^2).*(x + y*alpha1)*(m - 1) - Ms1*(coth((x + y*alpha1)./a1) - a1./(x + y*alpha1)).*(m - 1);

 
 %%

        param_in_6_opt(1) = 0.7670346  ;
        param_in_6_opt(2) =  0.01046715 ;
        param_in_6_opt(3) =  314.0714;
        param_in_6_opt(4) =  1;
        param_in_6_opt(5) =  1;
        param_in_6_opt(6) =  0.9958661;
        
  
        
        eval_opt_anhyst = @(param_opt) opt_JA_loops_PSI_v2_1_fcn_5_prms_mod_sig(PSI_meas,grad_dPSI_meas_dCurr_meas,weights,Curr_meas,param_in_6_opt,param_opt);
 
                
        lb = [0.01,0.01,0.001,0.01,0.1,0.9];
        ub = [5,5,5,5,5,1.1];
        
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',6000,'PlotFcn',@pswplotbestf);
        [bestParam6,fval,exitflag,output] = particleswarm(eval_opt_anhyst,6,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam6));
        disp(num2str(param_in_6_opt.*bestParam6));
 %%       
      figure('name',['flux 6 params:  ',num2str(param_in_6_opt.*bestParam6),'    Fval:' , num2str(fval)])
  for i = 1:size(PSI_meas,2)
        
        [M_sim] = eval_JA_loops_PSI_v2_1_fcn_5_prms_plus_k(PSI_meas(i),grad_dPSI_meas_dCurr_meas(i),Curr_meas(i),param_in_6_opt.*bestParam6) ;
        
        plot(Curr_meas{i},M_sim,'k')
        hold on
        plot(Curr_meas{i},PSI_meas{i},'r')
  end    
  grid on
  grid minor
  param = param_in_6_opt.*bestParam6;
    xlim([0,0.7])
    ylim([0,0.5])

    Ms1  = param(1);
    a1 = param(2);
    alpha1   = param(3);

    plt_current = linspace(0.005,max(Curr_meas{end}),100); 
    M_an = zeros(size(plt_current));
    for i = 1:length(plt_current)
    this_crr= double(plt_current(i));
    eqn = @(y) y-Ms1*(coth((this_crr+alpha1*y)/a1)-a1./(this_crr+alpha1*y));
    M_an(i) = vpasolve(eqn(y)==0 , y);
    end
    % figure 
    plot(plt_current,M_an,'b');
    plot(this_an_curve.current,this_an_curve.psi,'g')
    grid on;
   %% 
   figure('name',['flux 5 grad params:  ',num2str(param_in_5_opt_grad.*bestParam5_grad),'    Fval:' , num2str(fval)])
    for i = 1:size(Curr_meas,2)

            [M_sim,dM_dH1] = eval_JA_loops_PSI_v2_1_fcn_5_prms_plus_k(PSI_meas(i),grad_dPSI_meas_dCurr_meas(i),Curr_meas(i),param_in_6_opt.*bestParam6) ;        
            plot(Curr_meas{i},dM_dH1,'k')
            hold on
            plot(Curr_meas{i},grad_PSI_meas{i}./grad_Curr_meas{i},'r')
  end

    