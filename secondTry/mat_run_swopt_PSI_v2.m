       

% % select Data
% select_dataMAT
% load('test_data_sh')
%% parameter set working quite well

        my0 = 4.*pi.*1e-7;

        ja_Ms     = 250437.862814894;
        ja_a      = 1;
        ja_alpha  = 1.0e-05;
        ja_k      = 5*my0^2*100000000;
        ja_c      = 0.7;
        
        param(1) = ja_Ms;
        param(2) = ja_a;
        param(3) = ja_alpha;
        param(4) = ja_k;
        param(5) = ja_c;
        param(6) = 1;
        param(7) = 1;
        
%        param = [1248145.54219593,0.0485382948605262,6.68428000725425e-09,3.55161871939975e-08,1.41149009717994,3.85519935908942,388379.779026173];
%%      

% set up initial cell struct for sim output        
        nmbr_sets =size(B_meas,2);
        for i = 1:nmbr_sets
            PSI_sim{i} = zeros(size(B_meas{i}));
            M_sim{i} = zeros(size(B_meas{i}));
            H_sim{i} = zeros(size(B_meas{i}));            
        end
        
        param = param_start;
% % param(1) =param(1)*0.1;
%         param(2) =param(2)*10;
%         param(4) =param(4)*1;
%         param(6) =param(6)*1;
        
        call_optimJa = @(param_opt) eval_JaDatasetMAT_PSI_SSE(B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,param,param_opt);

        lb = [0.005,0.0005,0.0005,0.000005,0.0001,0.0001,0.0001];
        ub = [2,5,5,5,50, 3,10];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,7,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam));
        disp(num2str(param.*bestParam));
        
% plot optimization vs measurement
      
%         param = [18039688.4411349,3357.60893420928,0.000369962813185498,1942.94289820142,0.762985016540044,39.5235201952689,18421.4106985863];
 %%   
        nmbr_sets =size(B_meas,2);
        for i = 1:nmbr_sets
            PSI_sim{i} = zeros(size(B_meas{i}));
            M_sim{i} = zeros(size(B_meas{i}));
            H_sim{i} = zeros(size(B_meas{i}));            
        end
       
%         param =    param_start;
%         param(5) = param(5)*5;

        [Curr_sim_out,PSI_sim1,sse] = eval_JaDatasetMAT_PSI(B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,param.*bestParam);  
 
        update_figure('results')
        clf
    for i = 1:1:nmbr_sets
        hold on
        
        
         plot(H_meas{i}(:),B_meas{i}(:),'-k');
%            scatter(H_meas{i}(:),B_meas{i}(:),'k');
%            plot(abs(Curr_meas{i}(:)),abs(M_sim_out{i}(:)),'-r'); 
        hold on
% 	         plot(abs(Curr_meas{i}(:)),abs(PSI_meas{i}(:)),'-k'); 
        plot((H_meas{i}),(PSI_sim1{i}),'-r'); 
%           scatter(Curr_sim_out{i}(:),B_meas{i}(:));
    end
 grid on
 box on
 
 
 
 



