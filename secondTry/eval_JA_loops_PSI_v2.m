function [M_sim] = eval_JA_loops_PSI_v2(PSI,CRR,param_fix,param_in)


for i=4 %1:size(CRR,2)

Curr_meas = CRR{i};
PSI_meas = PSI{i};

M_sim = evaluate_JA_loops_PSI_v2(PSI_meas,Curr_meas,param_fix,param_in);


end

end