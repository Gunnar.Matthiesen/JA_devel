 function [sse_sum] = eval_invJaDatasetMAT_PSI_SSE(PSI_meas,Curr_meas,M_sim,H_sim,nmbr_sets,weights,param,param_opt)
        sse_sum = 0;

        
        
        for i = 1:nmbr_sets  
            [Curr_sim{i},M_sim{i}] = evaluate_invJA_loops_PSI(PSI_meas{i},Curr_meas{i},H_sim{i},M_sim{i},param.*param_opt); 

            sse1 = sum((Curr_meas{i} - Curr_sim{i}).^2);  
%             sse1 = sum(weights{i}.*abs(Curr_meas{i} - Curr_sim{i}));  
%             sse2 = sum((Curr_meas{i} - Curr_sim{i}).^2); 
            sse_sum = sse_sum + sse1*1e6;
        end
 end
