       

% % select Data
% select_dataMAT
% load('test_data_sh')
%% parameter set working quite well

        my0 = 4.*pi.*1e-7;
% Fval:  77867.1406
% 4.2886      8.9191      2.3068      1.5412    0.065521      2.7746      8.5262           1           0
% 1419475.81426      24.2809571226  8.86562551069e-06      5.29750441763    0.0580672000142      3.32304272568      256.651374465  

        ja_Ms     = 1419475.81426;
        ja_a      =  24.2809571226;
        ja_alpha  = 0.086562551069 ;
        ja_k      =    5.29750441763;
        ja_c      =  0.0580672000142 ;
        
        param(1) = ja_Ms;
        param(2) = ja_a;
        param(3) = ja_alpha;
        param(4) = ja_k;
        param(5) = ja_c;
        param(6) =  3.32304272568 ; %psi
        param(7) = 2560.651374465  ;%i
        param(8) = 300000;
        param(9) = 10;
        bestParam = ones(size(param));
        
%        param = [1248145.54219593,0.0485382948605262,6.68428000725425e-09,3.55161871939975e-08,1.41149009717994,3.85519935908942,388379.779026173];
%     

% set up initial cell struct for sim output        
        nmbr_sets =size(B_meas,2);
        clear PSI_sim M_sim H_sim
        for i = 1:nmbr_sets
            PSI_sim{i} = zeros(size(B_meas{i}));
            M_sim{i} = zeros(size(B_meas{i}));
            H_sim{i} = zeros(size(B_meas{i}));            
        end
       
        

        call_optimJa = @(param_opt) eval_invJaDatasetMAT_PSI_SSE(B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,param,param_opt);
       

%         
        lb = [0.5,0.05,0.005,0.0005 ,0.00001,0.005      ,1    ,0.1  ,0.1];
        ub = [1.5  ,10 ,10   ,10    ,   1.5,      10 ,10    ,2  ,10];
% 

        
%         lb = [0.2,0.0075,0.0075,0.00075,0.075,0.5,0.5,0.05];
%         ub = [2,5,5,10,2,1.5,1.5,2];
        
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,9,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam));
        disp(num2str(param.*bestParam));
        
% plot optimization vs measurement
 %%
%  figure
        nmbr_sets =size(B_meas,2);
        clear M_sim PSI_sim
        for i = 1:nmbr_sets
            PSI_sim{i} = zeros(size(B_meas{i}));
            M_sim{i} = zeros(size(B_meas{i}));
            H_sim{i} = zeros(size(B_meas{i}));            
        end
       
        [Curr_sim_out,M_sim1,sse] = eval_invJaDatasetMAT_PSI(B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,param.*bestParam);  
 
        update_figure('results');
        clf
    for i = 1:1:nmbr_sets
        hold on   
        plot(H_meas{i}(:),B_meas{i}(:),'-k');
        hold on
        plot((Curr_sim_out{i}),(B_meas{i}),'-r'); 

    end
 grid on
 box on
 
 % plotten der anhyst curve
%%
% factors = param.*bestParam;
% fi = factors(7);
% fpsi = factors(6);
% 
% x =( 0:0.01:max([H_meas{:}]))*fi;
% 
% % solve equation
% Ms1  = factors(1);
% a1 = factors(2);
% alpha1   = factors(3);

%%
% syms y
% eqn =@(xi) y - Ms1*(coth((xi+alpha1*y)/a1)-a1./(xi+alpha1*y));
% yi = zeros(size(x));
%  for i = 1:length(x)
% yi(i)= solve(eqn(x(i)),y);
%  end
%  %%
%  my0 = 4.*pi.*1e-7;
%  figure
%  yi_psi = my0*((x+yi))/fpsi;
%  plot(x/fi,yi_psi)
%  
% xlim([0,0.6]) 
% ylim([0,0.4])
%  
%  
%  %%
%  
%  PSI_meas_ramp_up = linspace(0,0.3,600);
%  
%  PSI_meas = [PSI_meas_ramp_up,flip(PSI_meas_ramp_up(1:end-1))];
%  PSI_meas = [PSI_meas,-flip(PSI_meas(1:end-1))];
%    PSI_meas_1 = [PSI_meas(1:end-1)];
% % PSI_meas = PSI_meas(1:end-1);
% 
% PSI_meas = [PSI_meas_1,PSI_meas_1,PSI_meas_1.*linspace(1,0.8,length(PSI_meas_1)),PSI_meas_1.*linspace(0.8,0.3,length(PSI_meas_1))];
% 
%  Curr_meas = zeros(size(PSI_meas));
%  Curr_sim = zeros(size(PSI_meas));
%  PSI_sim = zeros(size(PSI_meas));
%  Curr_meas(1) = 0.01;
%  
%  [Curr_sim,PSI_sim] = evaluate_invJA_loops_PSI(PSI_meas,Curr_meas,Curr_sim,PSI_sim,param.*bestParam);
%  
% 
% 
% figure
% plot(Curr_sim(1:end-600),PSI_sim(1:end-600))
% hold on;
% plot(Curr_sim(end-600:end),PSI_sim(end-600:end),'-r')
% grid on
