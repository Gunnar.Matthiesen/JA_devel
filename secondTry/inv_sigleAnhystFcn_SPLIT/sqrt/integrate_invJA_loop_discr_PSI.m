function [M_sim,H_sim,dM_dB] = integrate_invJA_loop_discr_PSI(B_int,M_sim,H_sim,param)

        my0 = 4.*pi.*1e-7;

        Ms     = param(1);
        a      = param(2);
        alpha  = param(3);
        ja_k      = param(4);
        c      = param(5);
        He_sw = param(8);
        f = param(9);
        
        dM_dB = zeros(size(B_int));   
        
       
        
        b =  Ms*(1/He_sw - (He_sw*(coth(He_sw/a)^2 - 1))/a^2) /He_sw^(1/f - 1)/f; 
       
        k_sw =  Ms.*(coth(He_sw./a)-a./(He_sw))-b*(He_sw).^(1/f);
        
        
        for k = 1:length(B_int)-1    

            delta = sign(B_int(k+1)-B_int(k));
            He = H_sim(k)+alpha*M_sim(k);           
       
% % % %                 M_an = Ms*(coth(He/a)-a/He);    
% % % %                 dMan_dHe = Ms/a*( 1 - coth(He/a)^2 + (a/He)^2);  
% % % 
%            if abs(He) <= abs(He_sw)
                % calculate M_an
                M_an =  Ms.*(coth((He)./a)-a./(He));
                % calculate deM_andHe   
                dMan_dHe = Ms*(a/(He - He_sw)^2 - (coth((He - He_sw)/a)^2 - 1)/a);
%            elseif He>He_sw
%                 % calculate M_an
%                 M_an =   b*(He).^(1/f) + k_sw;
%                 % calculate deM_andHe   
%                 dMan_dHe = He_sw^(1/f - 1)/f;
%                 
%            elseif He<-He_sw
%                  dMan_dHe = -abs(He_sw)^(1/f - 1)/f;                
%                  M_an =  -He_sw^(1/f - 1)/f;
%            else
%                error('Fehler');
%                
%            end
            
 
            Mirr = (M_sim(k)-c*M_an)/(1-c);
            dMirr_dBe = (M_an - Mirr)/(my0*delta*ja_k);
            
            dM_dB(k) = ( (1-c)*dMirr_dBe +c/my0*dMan_dHe )/ ...
                    ( 1+ my0*(1-alpha)*(1-c)*dMirr_dBe + c*(1-alpha)*dMan_dHe); 
                
            M_sim(k+1) = M_sim(k) + dM_dB(k)*(B_int(k+1)-B_int(k));
            H_sim(k+1) = B_int(k+1)/my0 - M_sim(k+1);
        end
     

        
end



