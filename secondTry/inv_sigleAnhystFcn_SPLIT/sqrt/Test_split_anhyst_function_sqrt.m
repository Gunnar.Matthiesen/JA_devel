

% New Equation
% M_an = a*x/sqrt(1+b*x^2)+c
% eqn = a*x/sqrt(1+b*x^2)+1/sqrt(c*x^3)+

%%



figure
x = abs(H_meas{end}); 
y = abs(B_meas{end});
ax = axes();
hold on
plot(ax,x,y  )


%%
% optimale Anhyst Curve ohne Gradenstück

    param = [0.3, 1, 0.2,1];
     call_optimJa = @(param_opt) min_anhyst(x,y,param,param_opt);           
     lb = [0.001,0.001,0.01,1];
     ub = [2,100,2,10];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,4,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
       disp(num2str(bestParam));
       disp(num2str(param.*bestParam));

% estimate = calculate_M_an(x,bestParam);
% plot(ax,x,estimate)
% % optimale Anhyst Curve mit Gradenstück
% lb = [0.01,0.000001,0];
%  ub = [1,5,0.3];
%         options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
%         [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,3,lb,ub,options);
%         disp(['Fval:  ', num2str(fval)]);
%        disp(num2str(bestParam));
% 
%%
estimate = calculate_M_an(x,param,bestParam);
plot(ax,x,estimate)

legend(ax,'Messdaten','ohne Grade','mit Grade');

%%


function SSEmin = min_anhyst(x,y,param,param_opt)
    
%   SSEmin = sum(x.^2.*(y - calculate_M_an(x,param,param_opt)).^2);
    SSEmin = sum((y - calculate_M_an(x,param,param_opt)).^2);
end

function [M_an_S] = calculate_M_an(He,param,param_opt)

        param_lc = param.*param_opt;

        Ms = param_lc(1);
        a = param_lc(2);
        He_sw = param_lc(3);
        f =  param_lc(4);
        
        b =  Ms*(1/He_sw - (He_sw*(coth(He_sw/a)^2 - 1))/a^2) /He_sw^(1/f - 1)/f;% ((He_sw^(1/f)*log(He_sw))/f^2 )  ;     
       
        M_an_s1 =  Ms.*(coth(He./a)-a./(He));
        M_an_s2 = b*(He).^(1/f)-b*(He_sw).^(1/f) + Ms.*(coth(He_sw./a)-a./(He_sw));
        M_an_S = M_an_s1.*(He_sw>=He)+ M_an_s2.*(He_sw<He);
end

