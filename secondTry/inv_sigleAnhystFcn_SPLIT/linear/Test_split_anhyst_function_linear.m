

figure
x = abs(H_meas{end}); 
y = abs(B_meas{end});
ax = axes();
hold on
plot(ax,x,y  )




%%
% optimale Anhyst Curve ohne Gradenstück
 call_optimJa = @(param_opt) min_anhyst(x,y,param_opt);           
 lb = [0.01,0.000001,0];
 ub = [1,5,0];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,3,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
       disp(num2str(bestParam));

estimate = calculate_M_an(x,bestParam);
plot(ax,x,estimate)
% optimale Anhyst Curve mit Gradenstück
lb = [0.01,0.000001,0];
 ub = [1,5,0.3];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,3,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
       disp(num2str(bestParam));

estimate = calculate_M_an(x,bestParam);
plot(ax,x,estimate)

legend(ax,'Messdaten','ohne Grade','mit Grade');

%%


function SSEmin = min_anhyst(x,y,param)
    
  SSEmin = sum(x.^2.*(y - calculate_M_an(x,param)).^2);
    
end

function [M_an_S] = calculate_M_an(He,param)


        Ms = param(1);
        dM_an_dHe = param(2);
        He_sw = param(3);
        a = Ms/(3*dM_an_dHe); 
        M_an_s1 = dM_an_dHe*He ;
        M_an_s2 =  Ms.*(coth((He-He_sw)./a)-a./(He-He_sw)) + dM_an_dHe*He_sw;
        M_an_S = M_an_s1.*(He_sw>=He)+M_an_s2.*(He_sw<He);
        
end

