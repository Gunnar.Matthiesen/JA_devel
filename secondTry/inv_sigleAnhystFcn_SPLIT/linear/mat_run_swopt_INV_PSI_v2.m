       

% % select Data
% select_dataMAT
% load('test_data_sh')
%% parameter set working quite well

        my0 = 4.*pi.*1e-7;

%         ja_Ms     = 250437.862814894;
%         ja_a      = 0.5;
%         ja_alpha  = 1.0e-05;
%         ja_k      = 5*my0^2*100000000;
%         ja_c      = 0.7;

%         ja_Ms     = 1000000;
%         ja_a      = 2;
%         ja_alpha  = 1.0e-05;
%         ja_k      = 5*my0^2*1000;
%         ja_c      = 0.7;

% 
% Fval:  8041.9536
% 1.4226      1.9114      1.3179     0.29922     0.96573      1.4187      1.8924
% 296314.819      2.368424304  1.760719894e-10      1.969597202     0.8515576932     0.9070268112      27.73076973

% Fval:  10675.4277
% 1.117      1.1494      2.1828      1.7452      1.0407      1.3204      1.0855     0.26247
% 330987.0699      2.722365166  3.843257097e-06      3.437256353     0.8862406567      1.197648427      30.10147062      1.312325093


        ja_Ms     = 330987.0699 ;
        ja_a      = 2.722365166;
        ja_alpha  = 3.843257097e-06;
        ja_k      =    3.437256353;
        ja_c      = 0.8862406567 ;
        
        param(1) = ja_Ms;
        param(2) = ja_a;
        param(3) = ja_alpha;
        param(4) = ja_k;
        param(5) = ja_c;
        param(6) =  1.197648427 ;
        param(7) = 30.10147062  ;
        param(8) = 10;
        bestParam = ones(size(param));
        
%        param = [1248145.54219593,0.0485382948605262,6.68428000725425e-09,3.55161871939975e-08,1.41149009717994,3.85519935908942,388379.779026173];
%%      

% set up initial cell struct for sim output        
        nmbr_sets =size(B_meas,2);
        clear PSI_sim M_sim H_sim
        for i = 1:nmbr_sets
            PSI_sim{i} = zeros(size(B_meas{i}));
            M_sim{i} = zeros(size(B_meas{i}));
            H_sim{i} = zeros(size(B_meas{i}));            
        end
        
        
%           param = [18039688.4411349,3357.60893420928,0.000369962813185498,1942.94289820142,0.762985016540044,39.5235201952689,18421.4106985863];
%    param =  [90198.4422056745,2893.88410175598,0.000554944219778247,4873.38001386503,0.541204995357702,0.333570878398525,60188.6437731815];

% split
%    param = [45118.9708      14.4694205  0.000832308972      2428.66163     0.541124143   0.00166785439      300.943219];

% set up optimization
%          param(1) = param(1);
%          param(3) = param(3)*1000;
%          param(4) = param(4)*0.5;
%         param(5) = param(5)*0.8;

        call_optimJa = @(param_opt) eval_invJaDatasetMAT_PSI_SSE(B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,param,param_opt);
       

%         lb = [0.005,0.005,0.0005,0.0005,0.0005,0.000005,0.5];
%         ub = [5,50,50,50,50,5,5];
%         
%         lb = [0.8,0.05,0.0005,0.05,0.05,1,1,1];
%         ub = [1.2,100,100,500,500,1,1,1];
% 

%         lb = [1,1,1,1,1,1,0.95];
%         ub = [1,1,1,1,1,1,1.05];
        
        lb = [0.2,0.0075,0.0075,0.00075,0.075,0.5,0.5,0.05];
        ub = [2,5,5,10,2,1.5,1.5,2];
        
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',2000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,8,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam));
        disp(num2str(param.*bestParam));
        
% plot optimization vs measurement
 %%
%  figure
        nmbr_sets =size(B_meas,2);
        clear M_sim PSI_sim
        for i = 1:nmbr_sets
            PSI_sim{i} = zeros(size(B_meas{i}));
            M_sim{i} = zeros(size(B_meas{i}));
            H_sim{i} = zeros(size(B_meas{i}));            
        end
       
        [Curr_sim_out,M_sim1,sse] = eval_invJaDatasetMAT_PSI(B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,param.*bestParam);  
 
        update_figure('results');
        clf
    for i = 1:1:nmbr_sets
        hold on   
        plot(H_meas{i}(:),B_meas{i}(:),'-k');
        hold on
        plot((Curr_sim_out{i}),(B_meas{i}),'-r'); 

    end
 grid on
 box on
 
 % plotten der anhyst curve
%%
% factors = param.*bestParam;
% fi = factors(7);
% fpsi = factors(6);
% 
% x =( 0:0.01:max([H_meas{:}]))*fi;
% 
% % solve equation
% Ms1  = factors(1);
% a1 = factors(2);
% alpha1   = factors(3);

%%
% syms y
% eqn =@(xi) y - Ms1*(coth((xi+alpha1*y)/a1)-a1./(xi+alpha1*y));
% yi = zeros(size(x));
%  for i = 1:length(x)
% yi(i)= solve(eqn(x(i)),y);
%  end
%  %%
%  my0 = 4.*pi.*1e-7;
%  figure
%  yi_psi = my0*((x+yi))/fpsi;
%  plot(x/fi,yi_psi)
%  
% xlim([0,0.6]) 
% ylim([0,0.4])
%  
%  
%  %%
%  
%  PSI_meas_ramp_up = linspace(0,0.3,600);
%  
%  PSI_meas = [PSI_meas_ramp_up,flip(PSI_meas_ramp_up(1:end-1))];
%  PSI_meas = [PSI_meas,-flip(PSI_meas(1:end-1))];
%    PSI_meas_1 = [PSI_meas(1:end-1)];
% % PSI_meas = PSI_meas(1:end-1);
% 
% PSI_meas = [PSI_meas_1,PSI_meas_1,PSI_meas_1.*linspace(1,0.8,length(PSI_meas_1)),PSI_meas_1.*linspace(0.8,0.3,length(PSI_meas_1))];
% 
%  Curr_meas = zeros(size(PSI_meas));
%  Curr_sim = zeros(size(PSI_meas));
%  PSI_sim = zeros(size(PSI_meas));
%  Curr_meas(1) = 0.01;
%  
%  [Curr_sim,PSI_sim] = evaluate_invJA_loops_PSI(PSI_meas,Curr_meas,Curr_sim,PSI_sim,param.*bestParam);
%  
% 
% 
% figure
% plot(Curr_sim(1:end-600),PSI_sim(1:end-600))
% hold on;
% plot(Curr_sim(end-600:end),PSI_sim(end-600:end),'-r')
% grid on
