



j=1
k=5

% i=1
% 
%
ha = axes();
hold on
results_R_model=struct();
cum_curve_psi = 0;
cum_curve_crr= 0;
idx_res = 2
for k = 4:4
    for i =1:17
        display(i);
        try
        this_meas = meas_raw(k).freq(j).current(i);
        if i <=10
            this_meas.get_R_n_ramps_heated_full_crr_meas_cycle('skip_R_meas');  
            this_meas.calc_psi();
        else
           this_meas.get_R_n_ramps_heated_full_crr_meas_cycle();  
           this_meas.calc_psi('Rmodel','exp&sin'); 
        end
        this_meas.evaluate_psi_ramps();
%         this_meas.plot_psi(ha,2);

        
        this_res = meas_raw(k).freq(j).current(i).results(idx_res).flux_curve; %        
        current = -[smoothdata(this_res.current_decrease,'movmean',1),smoothdata(this_res.current_increase,'movmean',1)];
        psi = -[smoothdata(this_res.psi_decrease,'movmean',1),smoothdata(this_res.psi_increase,'movmean',1)];
        [max_curr,idx_max_curr] = max(current);
        max_psi =psi( idx_max_curr);
        results_R_model(i).current_sm = -[smoothdata(this_res.current_decrease,'movmean',50),smoothdata(this_res.current_increase,'movmean',50)];
        results_R_model(i).psi_sm = -[smoothdata(this_res.psi_decrease,'movmean',50),smoothdata(this_res.psi_increase,'movmean',50)];
        cum_curve_crr = [cum_curve_crr,max_curr];
        cum_curve_psi = [cum_curve_psi,max_psi];
        
        catch 
        disp('failed')
        cum_curve_crr = [cum_curve_crr,NaN];
        cum_curve_psi = [cum_curve_psi,NaN];
        end
        
    end
%     close all
end
results_R_model(1).cum_curve_crr=cum_curve_crr;
results_R_model(1).cum_curve_psi=cum_curve_psi;
%%
cum_curve_psi = 0;
cum_curve_crr= 0;
results_R_model_skip=struct();
for k = 4:4
    for i =1:17
        display(i);
        try
        this_meas = meas_raw(k).freq(j).current(i);
        if i <=18
            this_meas.get_R_n_ramps_heated_full_crr_meas_cycle('skip_R_meas');  
            this_meas.calc_psi();
        else
           this_meas.get_R_n_ramps_heated_full_crr_meas_cycle();  
           this_meas.calc_psi('Rmodel','exp&sin'); 
        end
        this_meas.evaluate_psi_ramps();
%         this_meas.plot_psi(ha,2);
        catch 
        disp('failed')
        end
        
        this_res = meas_raw(k).freq(j).current(i).results(idx_res).flux_curve; %        
        current = -[smoothdata(this_res.current_decrease,'movmean',1),smoothdata(this_res.current_increase,'movmean',1)];
        psi = -[smoothdata(this_res.psi_decrease,'movmean',1),smoothdata(this_res.psi_increase,'movmean',1)];
        [max_curr,idx_max_curr] = max(current);
        max_psi =psi( idx_max_curr);
        results_R_model_skip(i).current_sm = -[smoothdata(this_res.current_decrease,'movmean',50),smoothdata(this_res.current_increase,'movmean',50)];
        results_R_model_skip(i).psi_sm = -[smoothdata(this_res.psi_decrease,'movmean',50),smoothdata(this_res.psi_increase,'movmean',50)];
        cum_curve_crr = [cum_curve_crr,max_curr];
        cum_curve_psi = [cum_curve_psi,max_psi];
        
    end
%     close all
end
results_R_model_skip(1).cum_curve_crr=cum_curve_crr;
results_R_model_skip(1).cum_curve_psi=cum_curve_psi;

%%
idx_res = 3;
%%
figure
hold on
%
colors = gray(30);
cum_curve_crr = 0;
cum_curve_psi = 0;

for i =1:17  
        display(i)
     
        this_res = results_R_model(i);
        scatter(results_R_model(1).cum_curve_crr(i),results_R_model(1).cum_curve_psi(i),'s','MarkerEdgeColor','k');  

    if mod(i,3)==0  ||  i==17     
       plot(this_res.current_sm,this_res.psi_sm,'color',colors(ceil(i+3/1.5),:)); 
    end

end
i=i+1;
scatter(results_R_model(1).cum_curve_crr(i),results_R_model(1).cum_curve_psi(i),'s','MarkerEdgeColor','k'); 
box on;
grid on
xlim([0,0.7])
ylim([0,0.5])
crr_pl = 0:0.001:max(results_R_model(1).cum_curve_crr);
plot(crr_pl,interp1(results_R_model(1).cum_curve_crr,results_R_model(1).cum_curve_psi,crr_pl,'spline'),'-k','LineWidth',1.5)


for i =1:17 
        display(i)
   
        this_res = results_R_model_skip(i);
        scatter(results_R_model_skip(1).cum_curve_crr(i),results_R_model_skip(1).cum_curve_psi(i),'d','MarkerEdgeColor','k');  

    if mod(i,3)==0   || i==17      
       plot(this_res.current_sm,this_res.psi_sm,'color','k','LineStyle','--');  %[100+i*7,0,0]/255
    end

end
i=i+1;
scatter(results_R_model_skip(1).cum_curve_crr(i),results_R_model_skip(1).cum_curve_psi(i),'d','MarkerEdgeColor','k'); 
box on;
grid on
xlim([0,0.7])
ylim([0,0.5])
crr_pl = 0:0.001:max(results_R_model_skip(1).cum_curve_crr);
plot(crr_pl,interp1(results_R_model_skip(1).cum_curve_crr,results_R_model_skip(1).cum_curve_psi,crr_pl,'spline'),'-r','LineWidth',1.5)
