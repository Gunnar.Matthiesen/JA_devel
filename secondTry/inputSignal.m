classdef inputSignal < handle
    % The inputSignal class constructor returns the class object and if
    % if signalOptions are supplied,the complete input signal and duration
    % [obj] = inputSignal(options)
    % [obj,inputsignal,duration] = inputSignal(options)
      
   properties(GetAccess =   public)        
            daq_Rate;
            T_meas_resistance_start;
            T_meas_psi_sgnl_ramp;
            frequency;
            nmbr_cycles;
            ratio_T_meas_current_high_start;
            ratio_T_break_start;
            T_meas_resistanc_end;
            i_meas_current;
            i_max;
            Duration;
            Type; 
            T_zeros_end; 
            idx_signal_start;
            signal_ref_const_psi;
            signal_ref_voltage_ind_const_psi;
            scale_factor_ni;
            i_heat;            
            crr_hold_min = 0.1; % sec
            crr_hold_max = 0.2;
            nmbr_incr_hold_steps = 15;
            payload;
    end
   
    
    properties
           DataMapping = [  "daq_Rate","daq_Rate", "4000";   % {name in this object, name in base workspace, default value'}
                            "T_meas_resistance_start","T_meas_resistance_start", "1";...
                            "frequency","frequency", "2";...
                            "nmbr_cycles","nmbr_cycles" ,"4";...
                            "ratio_T_meas_current_high_start" ,"ratio_T_meas_current_high_start","0.4" ;...
                            "ratio_T_break_start","ratio_T_break_start","0.3";...
                            "T_meas_resistanc_end","T_meas_resistanc_end","1";...
                            "T_meas_psi_sgnl_ramp","T_meas_psi_sgnl_ramp","0.3";...
                            "i_meas_current","i_meas_current","0.2";...
                            "i_max","i_max","0.7";...
                            "ratio_T_meas_current_high_start","ratio_T_meas_current_high_start","0.4";...
                            "T_meas_resistance_start","T_meas_resistance_start","1";...
                            "Type","Type","'ramps'";...
                            "T_zeros_end","T_zeros_end","0.2"];                         
        
    end
    
    methods  
    % CONSTRUCTOR:    
        function [obj, varargout] = inputSignal(varargin) 
        %ADD CHECKS HERE
            switch(nargin)

            case 0 % if no signal shall be generated

                switch(nargout)
                case 1 % return obj itself
                     varargout = {[]} ; 
                otherwise % if more outputs are requestet but signalOptions not supplied as input
                    error("An input signal was requestet but no SignalOptions supplied"); 
                end
             
            case 1 % if signalOptions is passed an signal to be generated               
    
                signalOptions = varargin{1};
                
                if ~isa(signalOptions, 'inputSignalOptions') 
                    error("The input is not an inputSignalOtpions opbject");
                end
                % check if the supplied object is an inputsignaloptions
                % object
                
                obj = obj.parseSignalInputOptions(signalOptions);                
                signalData = obj.gen_InputSignal();
                varargout{1} = signalData ; 
                varargout{2} = obj.Duration ; 

            otherwise
                error("There were " + num2str(nargout) +" ouputs requested but only 3 are supplied"); 
            end
        end
      
        function obj = parseSignalInputOptions(obj,signalOptions)
            
            fields = fieldnames(signalOptions);
            
            % ADD CHECKS HERE
            for i = 1:size(fields,1)
                
                if isprop(obj,fields{i})
                    
                    % find the corresponding row in Datamapping      
                    
                    if isnumeric(signalOptions.(fields{i})) || strcmp(fields{i},'Type')
                        obj.(fields{i}) = signalOptions.(fields{i});
                    else
                       error( fields{i} + " must be a numeric value");
                    end
                    
                else
                    tmp_stack = dbstack;
                    error( fields{i} +" is not valid  property of " + tmp_stack(1).file);
                end
                
            end
            
        end
       
        function signalData = gen_InputSignal(obj)
                      
            % ADDCHECKS HERE 1. IF options_struct has been passend                      
            
            obj.T_meas_psi_sgnl_ramp = 1/obj.frequency/4;
            
             % ------- define resistance measurement start -------------       
            
%             obj.T_meas_resistance_start= 1; % [s]
%             obj.ratio_T_meas_current_high_start = 0.4; % in percent
%             obj.ratio_T_break_start = 0.3; % [s] time between current measurement and start of ramp

            % ------- define resistance measurement end ------------- 

%             obj.T_meas_resistanc_end= 1; % [s]
%             T_zeros_end = 0.1; % [s] time between current measurement and start of ramp
            % --------------- setup measureent signal
            ramp_sign = 1;
            
            switch(obj.Type)
                
                case 'rand_psi_ramps'
                    this_quarter_period = ramp_sign*linspace(0,obj.i_max,obj.daq_Rate*obj.T_meas_psi_sgnl_ramp); 
                    signalData = complete_signalType_1(obj,[],this_quarter_period)';
                    signalData = [signalData,zeros(1,obj.daq_Rate*obj.T_zeros_end)];
                case 'ramps'
                    this_quarter_period = ramp_sign*linspace(0,obj.i_max,obj.daq_Rate*obj.T_meas_psi_sgnl_ramp); 
                    signalData = [this_quarter_period,flip(this_quarter_period(1:end-1))]; 
                    signalData = complete_signalType_1(obj,signalData,this_quarter_period)'; 
                    signalData = [signalData,zeros(1,obj.daq_Rate*obj.T_zeros_end)];
                case 'sin'                    
                    this_quarter_period = ramp_sign*obj.i_max*sin(pi/2*linspace(0,1,obj.daq_Rate*obj.T_meas_psi_sgnl_ramp));
                    signalData = [this_quarter_period,flip(this_quarter_period(1:end-1))]; 
                    signalData = complete_signalType_1(obj,signalData,this_quarter_period)';
                    signalData = [signalData,zeros(1,obj.daq_Rate*obj.T_zeros_end)];
                    
                case 'ramps_crr_meas'
                                        
                    [signalData,this_quarter_period] = obj.ramps_with_crr_meas_hold_phase();                    
                    signalData = complete_signalType_1(obj,signalData,this_quarter_period)';
                    signalData(end+1-obj.T_meas_resistanc_end*obj.daq_Rate:end)=[]; % WE HAVE TO REMOVE SOME PART CREATED BY complete_signalType_1
                    signalData = [signalData,flip(this_quarter_period(1:end-1))];                    
%                     signalData = obj.addCurrentMeasurement(signalData,'start');   DONE BY     complete_signalType_1
                    signalData = obj.addCurrentMeasurement(signalData,'end');
                    
                    init_ramp = linspace(0,obj.i_heat,round(obj.T_meas_psi_sgnl_ramp*obj.i_heat/obj.i_max*obj.daq_Rate));                    
                    signalData = [flip(init_ramp) , signalData ,init_ramp];
                    signalData = [ones(1,obj.daq_Rate*obj.T_zeros_end)*obj.i_heat,signalData,ones(1,obj.daq_Rate*obj.T_zeros_end)*obj.i_heat];
                    
                case 'ramps_heated'                     

                    this_quarter_period = ramp_sign*linspace(0,obj.i_max,obj.daq_Rate*obj.T_meas_psi_sgnl_ramp); 
                    signalData = [this_quarter_period,flip(this_quarter_period(1:end-1))]; 
                    signalData_ramp = [signalData,-signalData(2:end)];
                    signalData = repmat(signalData_ramp(1:end-1),1,obj.nmbr_cycles); 
                    signalData =  [signalData,this_quarter_period,flip(this_quarter_period(1:end-1))];
                    signalData = obj.addCurrentMeasurement(signalData,'start');        
                    signalData = obj.addCurrentMeasurement(signalData,'end');                     
                    init_ramp = linspace(0,obj.i_heat,round(obj.T_meas_psi_sgnl_ramp*obj.i_heat/obj.i_max*obj.daq_Rate));                    
                    signalData = [flip(init_ramp) , signalData ,init_ramp];
                    signalData = [ones(1,obj.daq_Rate*obj.T_zeros_end)*obj.i_heat,signalData,ones(1,obj.daq_Rate*obj.T_zeros_end)*obj.i_heat];
                                        
                case 'ramps_heated_payload'
                    
                    
                    
                    this_quarter_period = ramp_sign*linspace(0,obj.i_max,obj.daq_Rate*obj.T_meas_psi_sgnl_ramp); 
                    signalData = [this_quarter_period,flip(this_quarter_period(1:end-1))]; 
                    signalData_ramp = [signalData,-signalData(2:end)];
                    signalData = repmat(signalData_ramp(1:end-1),1,obj.nmbr_cycles); 
%                     signalData =  [signalData,this_quarter_period,flip(this_quarter_period(1:end-1))];
                      signalData =  [signalData,this_quarter_period];
                    signalData = [signalData,obj.payload];
                    
                    signalData = obj.addCurrentMeasurement(signalData,'start');        
                    signalData = obj.addCurrentMeasurement(signalData,'end');                     
                    init_ramp = linspace(0,obj.i_heat,round(obj.T_meas_psi_sgnl_ramp*obj.i_heat/obj.i_max*obj.daq_Rate));                    
                    signalData = [flip(init_ramp) , signalData ,init_ramp];
                    signalData = [ones(1,obj.daq_Rate*obj.T_zeros_end)*obj.i_heat,signalData,ones(1,obj.daq_Rate*obj.T_zeros_end)*obj.i_heat];
                           
                    
                    
                    
                case 'ramps_heated_full_crr_meas_cycle'     
                    nmbr_crr_meas_ramps =2;
                    this_quarter_period = ramp_sign*linspace(0,obj.i_max,obj.daq_Rate*obj.T_meas_psi_sgnl_ramp); 
                    signalData = [this_quarter_period,flip(this_quarter_period(1:end-1))]; 
                    signalData_ramp = [signalData,-signalData(2:end)];
                    signalData = repmat(signalData_ramp(1:end-1),1,obj.nmbr_cycles); 
                    signalData =  [signalData,this_quarter_period,flip(this_quarter_period(1:end-1))];                                      
                    signalData = obj.addCurrentMeasurement(signalData,'start');        
                    signalData = obj.addCurrentMeasurement(signalData,'end');  
                    [signal_meas_ramp,~] = obj.ramps_with_crr_meas_hold_phase();
                    
                    init_ramp = linspace(0,obj.i_heat,round(obj.T_meas_psi_sgnl_ramp*obj.i_heat/obj.i_max*obj.daq_Rate));                    
                    signalData = [flip(init_ramp), repmat(signal_meas_ramp,1,nmbr_crr_meas_ramps) , signalData,repmat(signal_meas_ramp,1,nmbr_crr_meas_ramps) ,init_ramp];
                    signalData = [ones(1,obj.daq_Rate*obj.T_zeros_end)*obj.i_heat,signalData,ones(1,obj.daq_Rate*obj.T_zeros_end)*obj.i_heat];
                    
            otherwise
                error("unkown signal type, only ramps and sin implemented yet");
                    
            end
            
            
            

        end
        
        function signalData_full = complete_signalType_1(obj,signalData,this_quarter_period)
       
                  if ~isempty(signalData)
                    signalData_ramp = [signalData,-signalData(2:end)];  
                  end
                signalData_full = repmat(signalData_ramp(1:end-1),1,obj.nmbr_cycles);    

                % add plateau at the end
                signalData_full = [signalData_full,this_quarter_period(2:end)];

                signalData_full = obj.addCurrentMeasurement(signalData_full,'start');            
                                
                signalData_full = obj.addCurrentMeasurement_plateu(signalData_full,'end');          
                
                obj.Duration = obj.T_meas_resistance_start + obj.T_meas_psi_sgnl_ramp*(1+4*  ...
                                obj.nmbr_cycles) + obj.T_meas_resistanc_end + obj.T_zeros_end;            
              
            end
        
        function [signalData] = const_psi_base_profile(obj,this_result,dpsi_dt,psi_max)
            
                x = this_result.flux_curve.psi_mean_sm;
                y = this_result.flux_curve.current_grid_sm;

                %% Fit: 'untitled fit 1'.
                [xData, yData] = prepareCurveData(  x ,y);

                % Set up fittype and options.
                ft = fittype( 'poly9' );
                opts = fitoptions( 'Method', 'LinearLeastSquares' );
                opts.Lower = [-Inf -Inf -Inf -Inf -Inf -Inf -Inf -Inf -Inf 0];
                opts.Robust = 'Bisquare';
                opts.Upper = [Inf Inf Inf Inf Inf Inf Inf Inf Inf 0];

                % Fit model to data.
                [fitresult, ~] = fit( xData, yData, ft, opts );

                %%             
                T_4 = psi_max/dpsi_dt;
                obj.T_meas_psi_sgnl_ramp = T_4;
                daq_rate = obj.daq_Rate;               
                psi_vec = dpsi_dt*[1/daq_rate:1/daq_rate:T_4];

                signal_quart = feval(fitresult,psi_vec); 
                
                obj.i_max = signal_quart(end);
                
                signalData = [signal_quart;flip(signal_quart);-signal_quart;-flip(signal_quart)];            
                signalData = [signalData;repmat(signalData(2:end),obj.nmbr_cycles-1,1);signal_quart(2:end)]; 
                
                signalData = obj.addCurrentMeasurement(signalData);
                obj.signal_ref_const_psi=signalData;
                
                
                               
                dpsi_vec = ones(size(psi_vec))'*dpsi_dt;
                signal_volt_ind = [dpsi_vec;-dpsi_vec;-dpsi_vec;+dpsi_vec];     
                obj.signal_ref_voltage_ind_const_psi = [signal_volt_ind;repmat(signal_volt_ind(2:end),obj.nmbr_cycles-1,1);dpsi_vec(2:end)];
             
                
        end           
        
        function signalData = addCurrentMeasurement(obj,signalData,where)
                    
            
            %---------------- setup offset calib -------------------
%             T_offset_calib = 0.5;
            if obj.i_max <= 0.1
                obj.i_meas_current = 0.5*obj.i_max;
            else
                obj.i_meas_current = 0.2;
            end
            ramp_sign   = 1;
            i_meas_current_end = signalData(end);

                       
            % add leading measurement phase
            signalData_step = [zeros(round(obj.daq_Rate*obj.T_meas_resistance_start*(1-obj.ratio_T_meas_current_high_start-obj.ratio_T_break_start)),1);...
            ramp_sign*ones(obj.daq_Rate*obj.T_meas_resistance_start*obj.ratio_T_meas_current_high_start,1)*obj.i_meas_current; zeros(obj.daq_Rate*obj.T_meas_resistance_start*obj.ratio_T_break_start,1)];    
            obj.idx_signal_start = length(signalData_step)+1;
%             nmbr_pnts_signalData_step = length(signalData_step);
%             signalData_offset_calib = zeros(1,T_offset_calib*obj.daq_Rate);    

            % construct complete input signal for this run
%             signalData = [signalData_offset_calib,signalData_step,signalData,signalData_offset_calib]; 
            if strcmp(where,'start')
                signalData = [signalData_step',signalData];        
            elseif strcmp(where,'end')
                signalData = [signalData,signalData_step'];  
            else
                error("Position unkown");
            end
                
        end
        
        function signalData = addCurrentMeasurement_plateu(obj,signalData,where)
            ramp_sign = 1;
            i_meas_current_end = signalData(end);
            %add measurement plateu at the end
            signalData_current_meas_end = ramp_sign*ones(obj.daq_Rate*obj.T_meas_resistanc_end,1)*i_meas_current_end;
            signalData= signalData(:);
            
             if strcmp(where,'start')
                signalData = [signalData_current_meas_end;signalData];       
            elseif strcmp(where,'end')
                signalData = [signalData;signalData_current_meas_end]; 
            else
                error("Position unkown");
             end
        end
        
        function [signalData,this_quarter_period] = ramps_with_crr_meas_hold_phase(obj)
                    ramp_sign = 1;
%                     obj.crr_hold_min = 0.1; % sec
%                     obj.crr_hold_max = 0.2;
%                     obj.nmbr_incr_hold_steps = 15;
                    this_quarter_period =[];
                    for i = 1:   obj.nmbr_incr_hold_steps                          
                    this_quarter_period = [this_quarter_period , ramp_sign*linspace(obj.i_max*(i-1)/   obj.nmbr_incr_hold_steps,obj.i_max*i/   obj.nmbr_incr_hold_steps, ...
                                        obj.daq_Rate*obj.T_meas_psi_sgnl_ramp/   obj.nmbr_incr_hold_steps)];        
                        if i<   obj.nmbr_incr_hold_steps
                            if obj.i_max*(i-1)/   obj.nmbr_incr_hold_steps<0.175
                             crr_hold=   obj.crr_hold_max;
                            else
                                crr_hold=    obj.crr_hold_min;
                            end
                                
                        this_quarter_period = [this_quarter_period,obj.i_max*i/   obj.nmbr_incr_hold_steps*ones(1,obj.daq_Rate*crr_hold)];
                        end
                        
                    end                       
                    signalData = [this_quarter_period,obj.i_max*ones(1,obj.daq_Rate*crr_hold),flip(this_quarter_period(1:end-1))]; 
            
        end
        
    end
      
    methods (Access = public)  
       
          
        function [signalData,Duration] = set_inputsignal_opts(obj,signalOptions)
                           
                obj.parseSignalInputOptions(signalOptions);                
                signalData = obj.gen_InputSignal();
                Duration = obj.Duration ; 
            
        end
       
        
        function giveInputSignalOptions(obj)
            
            str_out = "% Input Singal options" +newline+"	" ;
            for i = 1:size(obj.DataMapping,1)
                str_out = str_out + "signalOptions."+ obj.DataMapping{i,1} + " = " + obj.DataMapping{i,3} + ";" + newline + "	";
            end
                    
            disp(str_out);
        end
        
    end
end