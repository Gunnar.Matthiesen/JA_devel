function [M_sim1,M_sim2] = integrate_JA_loop_discr_PSI_v2(H_int,M_sim1,M_sim2,param_fix,param)


        Ms1     = param_fix(1);
        Ms2     = param_fix(2);
        a1      = param_fix(3);
        a2      = param_fix(4);
        alpha1  = param_fix(5);
        alpha2  = param_fix(6);
        
        k1      = param(1);
        k2      = param(2);
        c1      = param(3);
        c2      = param(4);
        
        dM_dH1 = zeros(size(H_int));
        dM_dH2 = zeros(size(H_int));
        delta = zeros(size(H_int));


        for k = 1:length(H_int)-1    

            delta(k) = sign(H_int(k+1)-H_int(k));
            He1 = H_int(k)+alpha1*M_sim1(k);
            He2 = H_int(k)+alpha2*M_sim2(k);  
            
            M_an1 = Ms1*(coth(He1/a1)-a1/He1); 
            M_an2 = Ms2*(coth(He2/a1)-a1/He2); 
            
            dMan_dH1 =-Ms1*((coth((H_int(k) + M_sim1(k)*alpha1)/a1)^2 - 1)/a1 - a1/(H_int(k) + M_sim1(k)*alpha1)^2); 
            dMan_dH2 =-Ms2*((coth((H_int(k) + M_sim2(k)*alpha2)/a2)^2 - 1)/a2 - a2/(H_int(k) + M_sim2(k)*alpha2)^2);
            
            dM_dH1(k) = (1/(1+ c1)) * ( M_an1 - M_sim1(k)) /(   k1*delta(k) - alpha1*(M_an1 - M_sim1(k))) + c1/(1+c1)*dMan_dH1;
            dM_dH2(k) = (1/(1+ c2)) * ( M_an2 - M_sim2(k)) /(   k2*delta(k) - alpha2*(M_an2 - M_sim2(k))) + c2/(1+c2)*dMan_dH2;
            
                
            M_sim1(k+1) = M_sim1(k) + dM_dH1(k)*(H_int(k+1)-H_int(k));
            M_sim2(k+1) = M_sim2(k) + dM_dH2(k)*(H_int(k+1)-H_int(k));

        end
        
end