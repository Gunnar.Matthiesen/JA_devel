


this_an_curve = meas_raw(1).freq(1).anhyst_curve_max;
close all
figure
plot(this_an_curve.current,this_an_curve.psi)
hold on;


for i = 4:4:16
  this_curve = meas_raw(1).freq(1).current(i).results(3).flux_curve ;   
  
  plot(this_curve.current_increase_sm,this_curve.psi_increase_sm);
  plot(this_curve.current_decrease_sm,this_curve.psi_decrease_sm);
end
%%

        x = [0.01:0.01:0.6]';

        y = feval(Fit_Sigmoid,x);

        
        param(1) = 4.027000000000000e-04;% Ms1
        param(2) = 4.839465567811011;% Ms2 
        param(3) = 52.213462928582220;% a1   
        param(4) = 342.4795525502266;% a2
        param(5) = 77.844623182935550;% alpha1
        param(6) = 212.2224098592300;% alpha2 
        

     
        eval_opt_anhyst = @(param_opt) opt_anhyst(x,y,param,param_opt);
 
                
        lb = [0.001,0.0001,0.0001,0.00001,0.0001,0.0001];
        ub = [1000,1000,1000,1000,100000,100000];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(eval_opt_anhyst,6,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam));
        disp(num2str(param.*bestParam));
        
        %%
                
        param = param.*bestParam;

        Ms1  = param(1);
        Ms2 = param(2);
        a1   = param(3);
        a2  = param(4);
        alpha1 = param(5);
        alpha2 = param(6);

        x1 = x+alpha1*y; 
        x2 = x+alpha2*y;

        y_dach = Ms1*(coth(x1/a1)-a1./x1)  + Ms2*(coth(x2/a2)-a2./x2);
        figure
        plot(x,y_dach)
        
        hold on
        plot(x,y,'k-')
        
        %%
        
        
        
        
        param_fix = param; % ???
        
        param_in(1) = 1e-2;% k1
        param_in(2) = 1e-2; % k2
        param_in(3) = 0.7; % c1
        param_in(4) = 0.7; % c2
        
        eval_opt_anhyst = @(param_opt) opt_JA_loops_PSI_v2(PSI_meas,Curr_meas,param_fix,param_in,param_opt);
                
        lb = [0.001,0.0001,0.001,0.001];
        ub = [1000,1000,1.2,1.2];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(eval_opt_anhyst,4,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam));
        disp(num2str(param_in.*bestParam));
%%
        [M_sim] = eval_JA_loops_PSI_v2(PSI_meas,Curr_meas,param_fix,param_in.*bestParam) ;
        figure
        plot(Curr_meas{1},M_sim)
  
  
  