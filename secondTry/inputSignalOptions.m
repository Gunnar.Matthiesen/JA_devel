classdef inputSignalOptions < handle
    % The inputSignal options

      
   properties(GetAccess =   public)        
            daq_Rate;
            T_meas_resistance_start;
            T_meas_psi_sgnl_ramp;
            frequency;
            nmbr_cycles;
            ratio_T_meas_current_high_start;
            ratio_T_break_start;
            T_meas_resistanc_end;
            i_meas_current;
            i_max;
            Type; 
            T_zeros_end;    
            scale_factor_ni;
            i_heat; 
            crr_hold_min ; % sec
            crr_hold_max;
            nmbr_incr_hold_steps;
            payload;
    end
   
    
    properties (GetAccess =   private)   
           DataMapping = [  "daq_Rate","daq_Rate", "4000";   % {name in this object, name in base workspace, default value'}
                            "T_meas_resistance_start","T_meas_resistance_start", "1";...
                            "frequency","frequency", "2";...
                            "nmbr_cycles","nmbr_cycles" ,"4";...
                            "ratio_T_meas_current_high_start" ,"ratio_T_meas_current_high_start","0.4" ;...
                            "ratio_T_break_start","ratio_T_break_start","0.3";...
                            "T_meas_resistanc_end","T_meas_resistanc_end","1";...
                            "T_meas_psi_sgnl_ramp","T_meas_psi_sgnl_ramp","0.3";...
                            "i_meas_current","i_meas_current","0.2";...
                            "i_max","i_max","0.7";...
                            "ratio_T_meas_current_high_start","ratio_T_meas_current_high_start","0.4";...
                            "T_meas_resistance_start","T_meas_resistance_start","1";...
                            "Type","Type","'ramps'";...
                            "T_zeros_end","T_zeros_end","0.2"];                         
        
    end
    
    methods  
    % CONSTRUCTOR:    
        function obj= inputSignalOptions() 
      
        end
    end 
      
    methods (Access = public) 
     
        function giveInputSignalOptions(obj)
            
            str_out = "% Input Singal options" +newline+"	" ;
            for i = 1:size(obj.DataMapping,1)
                str_out = str_out + "signalOptions."+ obj.DataMapping{i,1} + " = " + obj.DataMapping{i,3} + ";" + newline + "	";
            end
                    
            disp(str_out);
        end
        
    end
end