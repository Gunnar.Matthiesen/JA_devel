function  [PSI_meas,Curr_meas,POS_meas,weights,PSI_meas_an,Curr_meas_an] =  auto_select(meas_raw,idx_sel,idx_meas)

    idx_result = 3;
    idx_freq = 1;
    
    nmbr_meas = size(meas_raw(idx_meas).freq(idx_freq).current,2) ;    
    nmbr_meas_sel = length(idx_sel);
    PSI_meas = cell(1,nmbr_meas_sel);
    Curr_meas = cell(1,nmbr_meas_sel);
    PSI_meas_an = cell(1,nmbr_meas_sel);
    Curr_meas_an = cell(1,nmbr_meas_sel);
    
    weights = cell(1,nmbr_meas_sel);
    PSI_an = [];
    CRR_an = []; 
    nmbr_grid_pnts = 400;    
    x = linspace(0,1,nmbr_grid_pnts/2);
    x = [-flip(x),x];
    this_weights = normpdf(x,0,2*2); 
    this_weights = this_weights/min(this_weights);

for i = 1:nmbr_meas_sel
        
        flag_const_grid = false;
        psi_grid_inc = 0.001;
           
        % we reduce the number of datapoints using interpolation
        % we do not check wether datapoints are unique or in ascending
        % order because we use preprocessed data and this has already been
        % done in other routines
        this_meas = meas_raw(idx_meas).freq(idx_freq).current(idx_sel(i)).results(idx_result).flux_curve;    
        
        PSI_meas_an{i} = this_meas.psi_mean_sm;
        Curr_meas_an{i} = this_meas.current_grid_sm ;
        
        % increase current 
        c_meas_tmp_up = this_meas.current_increase_sm;
        psi_meas_tmp_up = this_meas.psi_increase_sm;
           
        % decreasing current       
        c_meas_tmp_dwn = this_meas.current_decrease_sm;
        psi_meas_tmp_dwn = this_meas.psi_decrease_sm;   
        
%         update_figure('Psi Data selected');
%         plot(abs(c_meas_tmp_up),abs(psi_meas_tmp_up),'-b');
%         hold on
%         plot(abs(c_meas_tmp_dwn),abs(psi_meas_tmp_dwn),'-k');
%         plot(c_meas_tmp_up,psi_meas_tmp_up,'-b');
%         hold on
%         plot(c_meas_tmp_dwn,psi_meas_tmp_dwn,'-k');
        
       try 
        if psi_meas_tmp_up(end)>psi_meas_tmp_dwn(1)       
        % get intersection in first quadrant        
            [c_meas_tmp_up,psi_meas_tmp_up,c_meas_tmp_dwn,psi_meas_tmp_dwn] = ...
                cut_off_intersection(c_meas_tmp_up,psi_meas_tmp_up,c_meas_tmp_dwn,psi_meas_tmp_dwn);           
        end  
       catch
       end
        
% %         % get intersection in third quadrant    
% %         if psi_meas_tmp_up(1)> psi_meas_tmp_dwn(end) 
% %             warning("Intersection in quadrat 3, which should not be possible");
% %                     [c_meas_tmp_dwn,psi_meas_tmp_dwn,c_meas_tmp_up,psi_meas_tmp_up] = ...
% %                 cut_off_intersection(-flip(c_meas_tmp_dwn),-flip(psi_meas_tmp_dwn),-flip(c_meas_tmp_up),-flip(psi_meas_tmp_up));            
% %             c_meas_tmp_up = flip(c_meas_tmp_up);
% %             psi_meas_tmp_up = flip(psi_meas_tmp_up);
% %             c_meas_tmp_dwn = flip(c_meas_tmp_dwn);
% %             psi_meas_tmp_dwn = flip(psi_meas_tmp_dwn);            
% %         end         
        
        min_psi = min(min(this_meas.psi_increase_sm),min(this_meas.psi_decrease_sm));
        max_psi = max(max(this_meas.psi_increase_sm),max(this_meas.psi_decrease_sm));    
       
        c_meas_tmp_up_zero =  currentZeroCrossing(c_meas_tmp_up,psi_meas_tmp_up);    
        c_meas_tmp_dwn_zero =  -currentZeroCrossing(-c_meas_tmp_dwn,-psi_meas_tmp_dwn);        
        c_meas_tmp_shift_each = (c_meas_tmp_up_zero+c_meas_tmp_dwn_zero)/2;
        
%           Use these lines to check the shift        
%         c_meas_tmp_up_zero =  currentZeroCrossing([c_meas_tmp_up-c_meas_tmp_shift_each],psi_meas_tmp_up);
%         c_meas_tmp_dwn_zero =  -currentZeroCrossing(-[c_meas_tmp_dwn-c_meas_tmp_shift_each],-psi_meas_tmp_dwn);
       
        c_meas_tmp_up = c_meas_tmp_up-c_meas_tmp_shift_each;
        c_meas_tmp_dwn = c_meas_tmp_dwn-c_meas_tmp_shift_each;
        
%         update_figure('Psi Data selected');
%         plot(abs(c_meas_tmp_up),abs(psi_meas_tmp_up),'-k');
%         plot(c_meas_tmp_up,psi_meas_tmp_up,'-k');
%         hold on
%         plot(abs(c_meas_tmp_dwn),abs(psi_meas_tmp_dwn),'-b');
%         plot(c_meas_tmp_dwn,psi_meas_tmp_dwn,'-b');
        
        % now we make a grid for each single line from min to max. For the
        % decreasing line we include the zero, which will be our starting
        % point. The grid is setup along psi
%           plot(c_meas_tmp_dwn,psi_meas_tmp_dwn,'-r');
        % measurements decreasing psi
%         [psi_tmp_dwn,idx_sort_dwn] = sort(psi_meas_tmp_dwn);        
%         if idx_sort_dwn(1)>idx_sort_dwn(end)
%             idx_sort_dwn(idx_sort_dwn>idx_sort_dwn(1))=[];
%             idx_sort_dwn(idx_sort_dwn<idx_sort_dwn(end))=[];
%         else
%            error('not implemented yet') 
%         end        
        % Remove all points at the ends beeing shifted         
%         [psi_tmp_dwn_unique,idx_sort_dwn_unique]=unique(psi_meas_tmp_dwn(idx_sort_dwn));        
       

        psi_zero_meas_tmp_dwn = interp1(c_meas_tmp_dwn,psi_meas_tmp_dwn,0);        
        current_grid_meas_tmp_dwn = linspace(min(c_meas_tmp_dwn),max(c_meas_tmp_dwn),nmbr_grid_pnts);  
        psi_grid_meas_tmp_dwn = interp1(c_meas_tmp_dwn,psi_meas_tmp_dwn,current_grid_meas_tmp_dwn);  
        [idx_last_negative]=find(current_grid_meas_tmp_dwn<0,1,'last');
  
        
         % measurements increasing psi       
%         [psi_tmp_up,idx_sort_up] = sort(psi_meas_tmp_up);        
%         if idx_sort_up(1)<idx_sort_up(end)
%             idx_sort_up(idx_sort_up<idx_sort_up(1))=[];
%             idx_sort_up(idx_sort_up>idx_sort_up(end))=[];
%         else
%            error('not implemented yet') 
%         end   
%         
        % Remove all points at the ends beeing shifted         
%         [psi_tmp_up_unique,idx_sort_up_unique]=unique(psi_meas_tmp_up(idx_sort_up));   
%         current_grid_meas_tmp_up = linspace(min(psi_meas_tmp_up),max(psi_meas_tmp_up),nmbr_grid_pnts);  
%         psi_interp1_grid_meas_tmp_up= interp1(psi_tmp_up_unique,c_meas_tmp_up(idx_sort_up(idx_sort_up_unique)),current_grid_meas_tmp_up);  

%         [psi_tmp_up_unique,idx_sort_up_unique]=unique(psi_meas_tmp_up(idx_sort_up));   
        current_grid_meas_tmp_up = linspace(min(c_meas_tmp_up),max(c_meas_tmp_up),nmbr_grid_pnts);  
        psi_interp1_grid_meas_tmp_up= interp1(c_meas_tmp_up,psi_meas_tmp_up,current_grid_meas_tmp_up); 

        % store the data
        
        PSI_meas{i} = [psi_zero_meas_tmp_dwn,flip(psi_grid_meas_tmp_dwn(1:idx_last_negative)), ....
            psi_interp1_grid_meas_tmp_up,flip(psi_grid_meas_tmp_dwn(idx_last_negative+1:end))];
        Curr_meas{i} = [0,flip(current_grid_meas_tmp_dwn(1:idx_last_negative)), ...
            current_grid_meas_tmp_up,flip(current_grid_meas_tmp_dwn(idx_last_negative+1:end))];
        
        weights{i} = [this_weights(idx_last_negative),flip(this_weights(1:idx_last_negative)), ....
            this_weights,flip(this_weights(idx_last_negative+1:end))];
        
        POS_meas{i} =mean(meas_raw(idx_meas).freq(idx_freq).current(idx_sel(i)).position);
        
end
 

end
%%
function param = initParamJa(this_meas)
    

    l = 0.11; %[m]
    N = 1400; %[-]
    A = 0.0064^2*pi;        
    f_i = 1/l*N; % [1/m]    
    f_psi = 1/(A*N); % [1/m²]
    my0 = 4.*pi.*1e-7;
    
       

    % increase current 
    c_meas_tmp_up = this_meas.current_increase_sm;
    psi_meas_tmp_up = this_meas.psi_increase_sm;

    % decreasing current       
    c_meas_tmp_dwn = this_meas.current_decrease_sm;
    psi_meas_tmp_dwn = this_meas.psi_decrease_sm;   
    
    c_meas_tmp_up_zero =  currentZeroCrossing(c_meas_tmp_up,psi_meas_tmp_up);    
    c_meas_tmp_dwn_zero =  -currentZeroCrossing(-c_meas_tmp_dwn,-psi_meas_tmp_dwn);        
    c_meas_tmp_shift_each = (c_meas_tmp_up_zero+c_meas_tmp_dwn_zero)/2;
    c_meas_tmp_dwn = c_meas_tmp_dwn-c_meas_tmp_shift_each;
    
    i_c = - currentZeroCrossing(-c_meas_tmp_dwn,-psi_meas_tmp_dwn);
    psi_r =  -PsiZeroCrossing(-c_meas_tmp_dwn,-psi_meas_tmp_dwn);
    
    alpha = - f_i*i_c/(psi_r*f_psi/my0); % Einheiten checken!!!! VORZEICHEN CHECKEN
    c_inc = 0.01;    
    c_grid = max([min(c_meas_tmp_up ),min(c_meas_tmp_dwn)]) ...
            :c_inc:min([max(c_meas_tmp_up),max( c_meas_tmp_dwn)]);
    
    psi_mean = (interp1(c_meas_tmp_up,psi_meas_tmp_up,c_grid) ...
        +interp1(c_meas_tmp_dwn,psi_meas_tmp_dwn,c_grid))/2;
    
    B_mean =  f_psi*psi_mean; 
    H_grid =  f_i*c_grid;
    M_an_mean = B_mean/my0-H_grid;
    He = H_grid  +alpha*M_an_mean;
    
    [xData, yData] = prepareCurveData( double(He), double(M_an_mean) );

    % Set up fittype and options.
    ft = fittype( 'Ms*(coth(x/a)-a/x)', 'independent', 'x', 'dependent', 'y' );
    opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
    opts.Display = 'Off';
    opts.MaxFunEvals = 1000;
    opts.MaxIter = 1000;
    opts.StartPoint = [max(M_an_mean) 1000];

    % Fit model to data.
    [fitresult, gof] = fit( xData, yData, ft, opts );

    Ms = fitresult.Ms;
    a = fitresult.a;
    
    % initiale suzpenzibilität aus neukurve
    susz_in = 480;
    c = 3*a*susz_in*0.15/Ms;
    k =  - f_i*i_c; % Hc    
 
    param = [Ms,a,alpha,k,c,f_psi,f_i];
end
%%

             
function [c_meas_tmp_up,psi_meas_tmp_up,c_meas_tmp_dwn,psi_meas_tmp_dwn] = cut_off_intersection(c_meas_tmp_up,psi_meas_tmp_up,c_meas_tmp_dwn,psi_meas_tmp_dwn)   
             
            len = round(length(c_meas_tmp_up)*0.01);
            
            [current_intersect,psi_intersect]= polyxpoly(c_meas_tmp_up(end-len:end) ,psi_meas_tmp_up(end-len:end),c_meas_tmp_dwn(1:len) ,psi_meas_tmp_dwn(1:len));

            [~,row,~] = find(psi_meas_tmp_up>psi_intersect,1,'first');
            c_meas_tmp_up(row:end) = [];
            psi_meas_tmp_up(row:end) = [];
        
            [~,row,~] = find(psi_meas_tmp_dwn>psi_intersect,1,'last');
            c_meas_tmp_dwn(1:row) = [];
            psi_meas_tmp_dwn(1:row) = [];
            
%       now we add the intersection point to the end of the measurement
%       with increasing current
            c_meas_tmp_up(end+1) = current_intersect;
            psi_meas_tmp_up(end+1) =  psi_intersect;
  
 end
             
function c_meas_tmp_up_zero =  currentZeroCrossing(c_meas_tmp_up,psi_meas_tmp_up)
             
                [~, idx_min_psi_positiv] = find(psi_meas_tmp_up>0,1,'first');
        
                if psi_meas_tmp_up(idx_min_psi_positiv-1)>psi_meas_tmp_up(idx_min_psi_positiv)
                   error(['This algorithm requires psi_meas_tmp_up(idx_min_psi_positiv-1) to be smaller than psi_meas_tmp_up(idx_min_psi_positiv)!', ...
                           'Ensure current and psi to be in ascending  order, otherwise input them as (-current,-psi) and multiply output by -1']); 
                end

                m = (psi_meas_tmp_up(idx_min_psi_positiv)-psi_meas_tmp_up(idx_min_psi_positiv-1))/...
                    (c_meas_tmp_up(idx_min_psi_positiv)-c_meas_tmp_up(idx_min_psi_positiv-1));

                c_meas_tmp_up_zero = -psi_meas_tmp_up(idx_min_psi_positiv)/m+c_meas_tmp_up(idx_min_psi_positiv);

end
    
function psi_meas_tmp_up_zero =  PsiZeroCrossing(c_meas_tmp_up,psi_meas_tmp_up)
             
                [~, idx_min_curr_positiv] = find(c_meas_tmp_up>0,1,'first');
        
                if c_meas_tmp_up(idx_min_curr_positiv-1)>c_meas_tmp_up(idx_min_curr_positiv)
                   error(['This algorithm requires psi_meas_tmp_up(idx_min_psi_positiv-1) to be smaller than psi_meas_tmp_up(idx_min_psi_positiv)!', ...
                           'Ensure current and psi to be in ascending  order, otherwise input them as (-current,-psi) and multiply output by -1']); 
                end

                m = (psi_meas_tmp_up(idx_min_curr_positiv)-psi_meas_tmp_up(idx_min_curr_positiv-1))/...
                    (c_meas_tmp_up(idx_min_curr_positiv)-c_meas_tmp_up(idx_min_curr_positiv-1));

                psi_meas_tmp_up_zero = +psi_meas_tmp_up(idx_min_curr_positiv)-m*c_meas_tmp_up(idx_min_curr_positiv);

end