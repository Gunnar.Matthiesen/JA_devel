       
%%
idx_sel = [1,2,4,7,9,11];
idx_result_input = 3;
idx_meas = 3;

[B_meas,H_meas,position] = autoselect_data_inv_model(meas_raw,idx_meas,idx_result_input,idx_sel,true);

%% 
clear idx_sel_vec
idx_meas(1,:) = 8;
idx_sel_vec(1,:) = [1,2,4,6,9,11];
idx_result_input(1,:) = 3;

idx_meas(2,:) = 7;
idx_sel_vec(2,:) = [1,2,4,6,9,11];
idx_result_input(2,:) = 3;

idx_meas(3,:) = 6;
idx_sel_vec(3,:) = [1,2,4,6,9,11];
idx_result_input(3,:) = 3;

idx_meas(4,:) = 5;
idx_sel_vec(4,:) = [1,2,4,6,9,11];
idx_result_input(4,:) = 2;

idx_meas(5,:) = 4;
idx_sel_vec(5,:) = [1,2,4,5,8,11];
idx_result_input(5,:) = 2;

idx_meas(6,:) = 3;
idx_sel_vec(6,:) = [1,2,4,7,9,11];
idx_result_input(6,:) = 2;


idx_meas(7,:) = 1;
idx_sel_vec(7,:) = [1,2,4,5,8,11];
idx_result_input(7,:) = 2;

% parameter set working quite well

        my0 = 4.*pi.*1e-7;

        ja_Ms     = 296314.819;
        ja_a      = 2.368424304;
        ja_alpha  = 1.760719894e-10;
        ja_k      =  1.969597202;
        ja_c      = 0.8515576932;

 nmbr_runs = size(idx_meas,1);
 nmbr_params = 8;
param_save = zeros(nmbr_runs,size(nmbr_params,2));       
 
% 
% %%
% 
% for k = nmbr_runs:-1:1    
%    [B_meas,H_meas,weights,position] = autoselect_data_inv_model(meas_raw,idx_meas(k,:),idx_result_input(k,:),idx_sel_vec(k,:),false); 
% 
%     POSITION(k) = mean(position)
% end
        
%%    

for k = nmbr_runs:-1:1
    
   [B_meas,H_meas,weights,position] = autoselect_data_inv_model(meas_raw,idx_meas(k,:),idx_result_input(k,:),idx_sel_vec(k,:),false); 

if k < nmbr_runs
    param = param_save(k+1,:);
else
        clear param
%         param(1) = ja_Ms*1;
%         param(2) = ja_a*10;
%         param(3) = ja_alpha*1000;
%         param(4) = ja_k*0.1;
%         param(5) = ja_c;
%         param(6) =  0.9070268112;
%         param(7) = 27.73076973;
%         param(8) = 10000;
param = param_save_start(end,:);
end

disp(['Start param:  ',num2str(param)]);

% set up initial cell struct for sim output        
        nmbr_sets =size(B_meas,2);
        clear PSI_sim M_sim H_sim
        for i = 1:nmbr_sets
            PSI_sim{i} = zeros(size(B_meas{i}));
            M_sim{i} = zeros(size(B_meas{i}));
            H_sim{i} = zeros(size(B_meas{i}));            
        end
        
        call_optimJa = @(param_opt) eval_invJaDatasetMAT_PSI_SSE(B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,param,param_opt);
       
    if k == nmbr_runs
        lb = [0.005,0.001,0.0001,0.0005,0.0005,0.01,0.01,0.001];
        ub = [3,50,500,1000,2,10,30,100];    
    else
        lb = [0.75,0.5,0.5,0.75,0.75,0.75,0.9,0.09];
        ub = [1,1.5,1.5,1.5,1.1,1.2,1.1,1.1];
    end
disp(['Start LB:  ',num2str(lb)]);
disp(['Start UB:  ',num2str(ub)]);  

        options = optimoptions('particleswarm','FunctionTolerance',1e-6,'UseParallel',true,'SwarmSize',10000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,8,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        % param = [Ms,a,alpha,k,c,f_psi,f_i];
        disp(num2str(bestParam));
        disp(num2str(param.*bestParam));
%      
        param_save(k,:) = param.*bestParam;

        % plot optimization vs measurement
        nmbr_sets =size(B_meas,2);
        clear M_sim PSI_sim H_sim
        for i = 1:nmbr_sets 
            PSI_sim{i} = zeros(size(B_meas{i}));
            M_sim{i} = zeros(size(B_meas{i}));
            H_sim{i} = zeros(size(B_meas{i}));            
        end       
        [Curr_sim_out,M_sim1,sse,di_dpsi] = eval_invJaDatasetMAT_PSI(B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,param.*bestParam);   
        update_figure(['results',num2str(k),'  sse: ',num2str(sse)]);
        for i = 1:1:nmbr_sets
            hold on   
            plot(H_meas{i}(:),B_meas{i}(:),'-k');
            hold on
            plot((Curr_sim_out{i}),(B_meas{i}),'-r'); 

        end
        grid on
        box on
 end
 % plotten der anhyst curve
%%
factors = param.*bestParam;
fi = factors(7);
fpsi = factors(6);

x =( 0:0.02:max([H_meas{:}])*1.5)*fi;

% solve equation
Ms1  = factors(1);
a1 = factors(2);
alpha1   = factors(3);

%
syms y
eqn =@(xi) y - Ms1*(coth((xi+alpha1*y)/a1)-a1./(xi+alpha1*y));
yi = zeros(size(x));
 for i = 1:length(x)
yi(i)= solve(eqn(x(i)),y);
 end
 %
 my0 = 4.*pi.*1e-7;
%  figure
 yi_psi = my0*((x+yi))/fpsi;
 plot(x/fi,yi_psi,'-b')
 
xlim([0,0.6]) 
ylim([0,0.4])
 
 
 %%
 
 PSI_meas_ramp_up = linspace(0,0.3,600);
 
 PSI_meas = [PSI_meas_ramp_up,flip(PSI_meas_ramp_up(1:end-1))];
 PSI_meas = [PSI_meas,-flip(PSI_meas(1:end-1))];
   PSI_meas_1 = [PSI_meas(1:end-1)];
% PSI_meas = PSI_meas(1:end-1);

PSI_meas = [PSI_meas_1,PSI_meas_1,PSI_meas_1.*linspace(1,0.8,length(PSI_meas_1)),PSI_meas_1.*linspace(0.8,0.3,length(PSI_meas_1))];

 Curr_meas = zeros(size(PSI_meas));
 Curr_sim = zeros(size(PSI_meas));
 PSI_sim = zeros(size(PSI_meas));
 Curr_meas(1) = 0.01;
 
 [Curr_sim,PSI_sim] = evaluate_invJA_loops_PSI(PSI_meas,Curr_meas,Curr_sim,PSI_sim,param.*bestParam);
 


figure
plot(Curr_sim(1:end-600),PSI_sim(1:end-600))
hold on;
plot(Curr_sim(end-600:end),PSI_sim(end-600:end),'-r')
grid on
