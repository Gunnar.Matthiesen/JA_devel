 function [Curr_sim,PSI_sim,sse_sum,di_dpsi] = eval_invJaDatasetMAT_PSI(PSI_meas,Curr_meas,PSI_sim,Curr_sim,nmbr_sets,weights,param)
        sse_sum = 0; 
        flg_continous = false; 
%         figure
        for i = 1:nmbr_sets  
            
%             disp(['Curr_meas{i}(1):',num2str(Curr_meas{i}(1))])
            
            if flg_continous == false
                 [Curr_sim{i},PSI_sim{i},di_dpsi{i}] = evaluate_invJA_loops_PSI(PSI_meas{i},Curr_meas{i},Curr_sim{i},PSI_sim{i},param); 
            else
%                  [Curr_sim_tmp,PSI_sim{i}] = evaluate_invJA_loops_PSI(PSI_meas{i},Curr_meas{i},Curr_sim{i},PSI_sim{i},param); %                  
%                  
%                  Curr_sim{i} = interp1(    ,    ,PSI_meas);
            end
            
            
            di_dpsi_meas = gradient(Curr_meas{i}(2:end))./gradient(PSI_meas{i}(2:end));
  
%             figure
%             plot(Curr_sim{i}(2:end),di_dpsi{i}(2:end)) ;
%             hold on;
%             plot(Curr_meas{i}(2:end),di_dpsi_meas)
            
            sse1 = sum(((Curr_meas{i} - Curr_sim{i}).^2)) + 0*sum((di_dpsi_meas-di_dpsi{i}(2:end)).^2);  

            sse_sum = sse_sum + sse1*1e6;%+sse2*1e6;
            
        end
 end
