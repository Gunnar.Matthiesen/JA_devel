

% New Equation
% M_an = a*x/sqrt(1+b*x^2)+c
% eqn = a*x/sqrt(1+b*x^2)+1/sqrt(c*x^3)+

%%

figure
x = abs(H_meas{end}); 
y = abs(B_meas{end});
ax = axes();
hold on
plot(ax,x,y  )


%%
% optimale Anhyst Curve ohne Gradenstück

    param = [24, 2, 0.13];
     call_optimJa = @(param_opt) min_anhyst(x,y,param,param_opt);           
     lb = [0.1,0.01,0.01];
     ub = [10,10,2];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,3,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
       disp(num2str(bestParam));

% estimate = calculate_M_an(x,bestParam);
% plot(ax,x,estimate)
% % optimale Anhyst Curve mit Gradenstück
% lb = [0.01,0.000001,0];
%  ub = [1,5,0.3];
%         options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
%         [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,3,lb,ub,options);
%         disp(['Fval:  ', num2str(fval)]);
%        disp(num2str(bestParam));
% 
estimate = calculate_M_an(x,param,bestParam);
plot(ax,x,estimate)

legend(ax,'Messdaten','ohne Grade','mit Grade');

%%


function SSEmin = min_anhyst(x,y,param,param_opt)
    
  SSEmin = sum(x.^2.*(y - calculate_M_an(x,param,param_opt)).^2);
    
end

function [M_an_S] = calculate_M_an(He,param,param_opt)

        param_lc = param.*param_opt;

        b = param_lc(1);
        dM_an_dHe = param_lc(2);
        He_sw = param_lc(3);
      
        
        a = dM_an_dHe;  

        M_an_s1 = dM_an_dHe*He ;

        M_an_s2 =  a*(He-He_sw)./sqrt(1+b*(He-He_sw).^2) + dM_an_dHe*He_sw;

        M_an_S = M_an_s1.*(He_sw>=He)+ M_an_s2.*(He_sw<He);
end

