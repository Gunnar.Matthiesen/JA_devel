
load('U:\!01DissIt\Messungen\Gunnar\EraSib\hyst_frei_Loops\20210822_pr_secondshot.mat');
%%
clear idx_sel_vec
idx_meas(1,:) = 8;
idx_sel_vec(1,:) = [1,2,4,6,9,11];
idx_result_input(1,:) = 3;

idx_meas(2,:) = 7;
idx_sel_vec(2,:) = [1,2,4,6,9,11];
idx_result_input(2,:) = 3;

idx_meas(3,:) = 6;
idx_sel_vec(3,:) = [1,2,4,6,9,11];
idx_result_input(3,:) = 3;

idx_meas(4,:) = 5;
idx_sel_vec(4,:) = [1,2,4,6,9,11];
idx_result_input(4,:) = 2;

idx_meas(5,:) = 4;
idx_sel_vec(5,:) = [1,2,4,5,8,11];
idx_result_input(5,:) = 2;

idx_meas(6,:) = 3;
idx_sel_vec(6,:) = [1,2,4,7,9,11];
idx_result_input(6,:) = 2;

idx_meas(7,:) = 1;
idx_sel_vec(7,:) = [1,2,4,5,8,11];
idx_result_input(7,:) = 2;
%%
update_figure('Coefficients initial')
clf
x = double(POSITION);
[residuals,fitvalues] = get_residuals(x,param_save);
for i = 1:size(param_save,1)

h_sc = scatter(x,param_save(:,i)./max(param_save(:,i)));
h_sc.Annotation.LegendInformation.IconDisplayStyle = 'off';
hold on;
plot(x,fitvalues(:,i)./max(fitvalues(:,i)),'color',h_sc.CData)
end
grid on
legend('Ms','a','alpha','k','c','fpsi','fi','Hsw')
        
%%    


param_save(:,1) = fitvalues(:,1);
param_save(:,3) = fitvalues(:,3);
param_save(:,6) = fitvalues(:,6);
param_save(:,8) = fitvalues(:,8);
%%
% i_loop_max = 7;
% for i_loop = 1:i_loop_max    

i_loop = 1;
idx_next_opt = i_loop;
%      disp(['Optimization Run: ',num2str(i_loop),' of ',num2str(i_loop_max)]);
    
    
    
        update_figure('Coefficients');
        clf
        plot(param_save./max(param_save))
        legend('Ms','a','alpha','k','c','fpsi','fi','Hsw')
        grid on
        %

%         residuals = param_save-fitvalues;
%         
%         [max_sum_res,idx_next_opt] = max(sum(abs(residuals)./max(residuals),2)');
%         disp(['Optimiting dataset ',num2str(idx_next_opt)]);
%         
%         update_figure('Residuals');
%         clf
%         plot(x,residuals./max(residuals));
%         grid on
%         legend('Ms','a','alpha','k','c','fpsi','fi','Hsw')
%     
        param = fitvalues(idx_next_opt,:);
    
%         param = param_save(i_loop,:);

        k = i_loop;
        
        [B_meas,H_meas,weights,position] = autoselect_data_inv_model(meas_raw,idx_meas(k,:),idx_result_input(k,:),idx_sel_vec(k,:),false); 

        disp(['Start param:  ',num2str(param)]);

    % set up initial cell struct for sim output        
        nmbr_sets =size(B_meas,2);
        clear PSI_sim M_sim H_sim
        for i = 1:nmbr_sets
            PSI_sim{i} = zeros(size(B_meas{i}));
            M_sim{i} = zeros(size(B_meas{i}));
            H_sim{i} = zeros(size(B_meas{i}));            
        end
        
        call_optimJa = @(param_opt) eval_invJaDatasetMAT_PSI_SSE(B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,param,param_opt);
       
        lb = [0.9,0.9,0.9,0.75,0.75,0.75,0.9,0.9];
        ub = [1.1,1.1,1.1,1.5,1.1,1.2,1.1,1.1];

%         lb = [1,1,0.05,0.075,0.075,1,0.09,0.75];
%         ub = [1,1,15,15,10,1,10,1.5];

        disp(['Start LB:  ',num2str(lb)]);
        disp(['Start UB:  ',num2str(ub)]);  

        options = optimoptions('particleswarm','FunctionTolerance',1e-6,'UseParallel',true,'SwarmSize',6000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,8,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);

        disp(num2str(bestParam));
        disp(num2str(param.*bestParam));
      
        param_save(idx_next_opt,:) = param.*bestParam;


% end
%% compare 
k = 1

        nmbr_sets =size(B_meas,2);
        clear PSI_sim M_sim H_sim
        for i = 1:nmbr_sets
            PSI_sim{i} = zeros(size(B_meas{i}));
            M_sim{i} = zeros(size(B_meas{i}));
            H_sim{i} = zeros(size(B_meas{i}));            
        end
[B_meas,H_meas,weights,position] = autoselect_data_inv_model(meas_raw,idx_meas(k,:),idx_result_input(k,:),idx_sel_vec(k,:),false); 
nmbr_sets =size(B_meas,2);

plot_results(k,B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,param_save(k,:))
hold on;
plot_results(k,B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,fitvalues(k,:))

%%

function plot_results(nmbr_dataset,B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,param)

        % plot optimization vs measurement
        [Curr_sim_out,M_sim1,sse,di_dpsi] = eval_invJaDatasetMAT_PSI(B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,param);   
        update_figure(['results',num2str(nmbr_dataset),'  sse: ',num2str(sse)]);
        for i = 1:1:nmbr_sets
            hold on   
            plot(H_meas{i}(:),B_meas{i}(:),'-k');
            hold on
            plot((Curr_sim_out{i}),(B_meas{i}),'-r'); 

        end
        grid on
        box on

end

%%
function [residuals,fitvalues] = get_residuals(x,Y)

    
    fitvalues =zeros(size(Y));
    for i = 1:size(Y,2)
     this_y = Y(:,i);
%     [xData, yData] = prepareCurveData( x, y );
    % Set up fittype and options.
    ft = fittype( 'poly2' );
    opts = fitoptions( 'Method', 'LinearLeastSquares' ,'Normalize', 'on' );
    opts.Robust = 'LAR';
    % Fit model to data.
    [fitresult, ~] = fit( x', this_y, ft, opts );
        if 1==0
        % Plot fit with data.
        figure( 'Name', 'untitled fit 1' );
        h = plot( fitresult, x', this_y);
        legend( h, 'y vs. x', 'untitled fit 1', 'Location', 'NorthEast', 'Interpreter', 'none' );
        % Label axes
        xlabel( 'x', 'Interpreter', 'none' );
        ylabel( 'y', 'Interpreter', 'none' );
        grid on
        end
        
        fitvalues(:,i) = feval(fitresult, x');
    end
    
    residuals = fitvalues - Y;
    
end