classdef raw_flux_curve_measurement < handle
      
    properties(GetAccess =   public)
        time =[];
        current=[];
        voltage=[];
        current_input=[];
        time_position=[];
        position=[];
        h_plot_psi_curves =[];
        h_plot_u_ind_curves = [];
        h_plot_u_ind_psi_curves = [];
        h_plot_psi_curr_ind_curves = [];
        i_dot = [];
        inputSignal = [];
        userData =[];
        R_heat_model=[];
    end
   
  
    properties
        remove_offset_flag = true;
        compensate_drift_flag = true;
        index_ramps;
        idx_begin_hold_ramps_start ;
        idx_end_hold_ramps_start ;
        idx_diag_intersecction_start ;
        idx_begin_hold_ramps_end ;
        idx_end_hold_ramps_end ;
        idx_diag_intersecction_end;
        index_begin_current_hold_ramps_start;
        index_begin_current_hold_ramps_end;
        idx_start_curr_meas_begin;
        idx_start_curr_meas_end;
        
        % Here is the example Mapping to plot the flux linkage curves
        % We always need the increasing and decreasing curve
        % 1. current_increase 2. psi_increase 3. current_decrease 4. psi_decrease 5. psi_mean
        plot_mappings = {'current_increase','psi_increase','current_decrease','psi_decrease','current_grid_sm','psi_mean_sm','h_plot_psi_curves';
                          'current_increase','voltage_ind_increase','current_decrease','voltage_ind_decrease','current_grid_sm','voltage_ind_mean','h_plot_u_ind_curves';
                          'voltage_ind_increase','psi_increase','voltage_ind_decrease','psi_decrease','voltage_ind_mean','psi_mean_sm','h_plot_u_ind_psi_curves';                          
                          'psi_mean_sm','current_dissp','psi_mean_sm','current_dissp','','','h_plot_psi_curr_ind_curves'};
        
        plot_lable_mappings = {'Flux Linkage Curves','Current [A]','Flux Linkage [Wb]';
                                'Induced Voltage','Current [A]','Voltage [V]';
                                'Induced Voltage and Flux Linkage','Voltage [V]','Flux Linkage [Wb]';
                                'Flux Linkage and Dissipating Current','Flux Linkage [Wb]','Dissipating Current[A]'};
        
        % extract full dataset for voltage, current, force and position
        voltage_ind;
        psi;
        results;
        R_valve_end;
        R_valve_start;
        R_valve_end_idx;
        R_valve_start_idx;
        R_vec;
        
        mcc_wiring = {'current',1;   % do not change names only the ports 1 to 4
                      'voltage',2;
                      'current_input',3};       

        
        DataMapping = [ "daq_Rate","daq_Rate", "4000";   % {name in this object, name in base workspace, default value'}
                        "T_meas_resistance_start","T_meas_resistance_start", "1";...
                        "T_meas_psi_sgnl_ramp","T_meas_psi_sgnl_ramp", "0.3";...
                        "nmbr_cycles","nmbr_cycles" ,"4";...
                        "ratio_T_meas_current_high_start" ,"ratio_T_meas_current_high_start","0.4" ;...
                        "ratio_T_break_start","ratio_T_break_start","0.3";...
                        "T_meas_resistanc_end","T_meas_resistanc_end","1";...
                        "T_meas_psi_sgnl_ramp","T_meas_psi_sgnl_ramp","0.3";...
                        "i_meas_current","i_meas_current","0.2";...
                        "i_max","i_max","0.7";...
                        "ratio_T_meas_current_high_start","ratio_T_meas_current_high_start","0.4";...
                        "T_meas_resistance_start","T_meas_resistance_start","1"]; 
          
        
    end
    
    methods  
    % CONSTRUCTOR:    
        function [obj,varargout] = raw_flux_curve_measurement(varargin) 

        %ADD CHECKS HERE

        %ADD PROCESSING OF INPUTS HERE
        % if not supplied, try grab the data from the base workspace
%             obj = obj.grabDatafromWorkspace();


        %ADD CHECKS HERE
            switch(nargin)

            case 0 % if no signal shall be generated
                obj.inputSignal = inputSignal();
                varargout ={[]};
            case 1                
                signalOptions = varargin{1};                
                if isa(signalOptions, 'inputSignalOptions') 
                   [obj.inputSignal,output_signal,output_duration] = inputSignal(signalOptions); 
                   varargout{1} = output_signal;
                   varargout{2} = output_duration;
                else
                    obj.inputSignal = inputSignal(); 
                    varargout = {[]};
                end
                % check if the supplied object is an inputsignaloptions
                % object                 
            otherwise
                varargout ={[]};
            end
            
        end

    end    
  
    methods (Access = public)              
        
        function obj = plot_psi(varargin)
            obj = varargin{1};
            idx_plot_type_mapping = 1;
            [i_plt,h_axes]= obj.plot_results_parse(varargin);           
            obj = obj.plotThisResults(i_plt,h_axes,idx_plot_type_mapping);            
        end
        
        function obj = plot_voltage_ind(varargin)
            obj = varargin{1};
            idx_plot_type_mapping = 2;
            [i_plt,h_axes]= obj.plot_results_parse(varargin);           
            obj = obj.plotThisResults(i_plt,h_axes,idx_plot_type_mapping);            
        end
        
        function obj = plot_voltage_ind_dissp_current(varargin)
            obj = varargin{1};
            idx_plot_type_mapping = 5;
            [i_plt,h_axes]= obj.plot_results_parse(varargin);           
            obj = obj.plotThisResults(i_plt,h_axes,idx_plot_type_mapping);            
        end
        
        function obj = plot_voltage_ind_psi(varargin)
            obj = varargin{1};
            idx_plot_type_mapping = 3;
            [i_plt,h_axes]= obj.plot_results_parse(varargin);           
            obj = obj.plotThisResults(i_plt,h_axes,idx_plot_type_mapping);            
        end
        
        function obj = plot_dissp_current(varargin)
            obj = varargin{1};
            idx_plot_type_mapping = 4;
            [i_plt,h_axes]= obj.plot_results_parse(varargin);           
            obj = obj.plotThisResults(i_plt,h_axes,idx_plot_type_mapping);
            snapnow;
        end        
        
        function obj = plotThisResults(obj,i_plt,h_axes,idx_plot_type_mapping)   
            % DIRTY IMPLEMENTATION without input parser
            % SECOND INPUT CAN BE HANDLE TO AXES SYSTEM
            % LAST INPUT MUST BE VECTOR 1x2 for fluxes curves 2 plot
                                    
          
% %             flg_plot_internal = true;
% % %             idx_mapping = varargin{2};
            
                        
            % Execute plot
            if isempty(h_axes)
                plot2internalFig();   
            else                
                plot2externalFig(); % this is the handle to the external figure
            end
            
            %----------supporting nested functions -----------------
            function plot2externalFig()
                h_axes.NextPlot = 'add';
                plot2figure(false,false,false);   
            end
            
            function plot2internalFig()
                
                h_axes = obj.(obj.plot_mappings{idx_plot_type_mapping,7});    
                
                if isempty(h_axes)
                   flg_new_fig = true;                
                else
                    % chick if figure still exists
                    if ishandle(h_axes)
                        flg_new_fig = false;
                    else
                        flg_new_fig = true;
                    end

                end

                if flg_new_fig
                   hf = figure('name', obj.plot_lable_mappings{idx_plot_type_mapping,1});
                   h_axes = axes(hf);
                   obj.(obj.plot_mappings{idx_plot_type_mapping,7}) = h_axes;
                   xlabel(obj.plot_lable_mappings{idx_plot_type_mapping,2});
                   ylabel(obj.plot_lable_mappings{idx_plot_type_mapping,3})
                   grid on;
                   grid minor;
                   hold on;
                   box on;
                   set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
                   set(groot, 'defaultTextInterpreter','latex'); 
                   set(groot, 'defaultLegendInterpreter','latex');
                   title(['Position: ',num2str(mean(obj.position)),' mm']);
                else
                   delete(findobj(h_axes, 'type', 'Line'));
                   h_axes.NextPlot = 'add';

                end 
                plot2figure(true,true,true);   
            end
            
            function plot2figure(flag_legend,flag_resize,flag_title)               
                           
                legend_txt= {};  
                y_max_val = 0;
                y_min_val = 0;
                x_max_val = 0;
                x_min_val = 0;
                if flag_title == true
                    h_axes.Title.String = ['Position: ',num2str(mean(obj.position)),' mm'];
                end
                count_i_legend = 0;
                
                for i = i_plt  
                    count_i_legend =count_i_legend+1;
                    h_this_curve = obj.results(i).flux_curve;                    
                    x_max_val = max([ x_max_val,h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,1}),h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,3})]);
                    x_min_val = min([ x_min_val,h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,1}),h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,3})]);
                    y_max_val = max([ y_max_val,h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,2}),h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,4})]);
                    y_min_val = min([ y_min_val,h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,2}),h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,4})]);
                    hp = plot(h_axes,h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,1}),h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,2}));
                    if flag_legend == false
                        hp.Annotation.LegendInformation.IconDisplayStyle = 'off';    
                    end
                    hp = plot(h_axes,h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,3}),h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,4}),'Color',hp.Color);
                    hp.Annotation.LegendInformation.IconDisplayStyle = 'off';
                    if ~isempty(obj.plot_mappings{idx_plot_type_mapping,5}) && ~isempty(h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,5})) &&...
                        ~isempty(obj.plot_mappings{idx_plot_type_mapping,6}) && ~isempty(h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,6}))
                        hp = plot(h_axes,h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,5}),h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,6}),'--','Color',hp.Color);
                        hp.Annotation.LegendInformation.IconDisplayStyle = 'off';
                    end
                    legend_txt{count_i_legend}="Loop " + num2str(i);
                end  
                if flag_legend == true
                    legend(h_axes,legend_txt);
                end
                if flag_resize==true
                    xlim([-0.1*ceil(-10*x_min_val),0.1*ceil(10*x_max_val)]);
                    ylim([-0.1*ceil(-10*y_min_val),0.1*ceil(10*y_max_val)]);
                end
            end
                        
        end
        
        function obj = evaluate_measurement(obj)
            
            switch(obj.inputSignal.Type)
                
                case 'rand_psi_ramps'
                    obj = obj.evaluate_rand_psi_ramps_measurement();
                case 'ramps'
                    obj = obj.evaluate_ramps_measurement();
                case 'ramps_heated'
                    obj = obj.evaluate_ramps_heated_measurement();                    
                case 'ramps_heated_payload'
                    obj = obj.evaluate_ramps_heated_payload_measurement();
                case 'ramps_crr_meas'
                    obj = obj.evaluate_ramps_crr_meas_measurement();
                case 'sin'
                    obj = obj.evaluate_sin_measurement();
                case 'const_psi'
                    obj = evaluate_const_psi_measurement(obj);
                case 'ramps_heated_full_crr_meas_cycle'
                    obj = obj.evaluate_ramps_heated_full_crr_meas_cycle();
                otherwise
                    error("For the signal type of this measurement no evaluation is implemented yet");
            end
            
        end
        
        function obj = evaluate_rand_psi_ramps_measurement(obj)
            
            if obj.remove_offset_flag == true            
                obj.remove_offset();
            end
            
            
            [~,idx_end] = min(diff(obj.current));
            
             idx_end =  idx_end +  obj.inputSignal.T_zeros_end*obj.inputSignal.daq_Rate;
             
            current_rest = obj.current(idx_end:end);
            voltage_rest = obj.voltage(idx_end:end); 
            time_rest    = obj.time(idx_end:end);
             
            obj.current = obj.current(1:idx_end);
            obj.voltage = obj.voltage(1:idx_end);
            obj.time    = obj.time(1:idx_end);
             
            f_ratio_time =  length(time_rest)/length(obj.time);  
            % split the dataset             


            % extract resitance begin and end
            obj.extract_resictance_n_ramps();           
            obj.calc_psi();            
           [obj,psi_drift_mean_per_step] = obj.evaluate_psi_ramps();



            % DRITY IMPLEMENTED AN NOT CORRECT!!!!! assumend same losses as
            % during psi measurement phase, but this is not correct
            % Calculate Resitance Correction
            f_R = trapz(time_rest,current_rest.^2);
            cum_f_R = cumtrapz(time_rest,current_rest.^2);
            R_vec_rest = obj.R_valve_end + (obj.R_valve_end-obj.R_valve_start)*f_ratio_time*cum_f_R./f_R;

            % ------------------- calculate flux linkage ---------------------
            voltage_ind_rest = voltage_rest - R_vec_rest.*current_rest;            
            % calculate integral 
            psi_rest = cumtrapz(time_rest,voltage_ind_rest); 
            psi_rest = psi_rest - [1:length(psi_rest)]*psi_drift_mean_per_step;

            obj.userData.psi_rest = psi_rest;
            obj.userData.current_rest = current_rest;
            obj.userData.voltage_rest = voltage_rest;
            obj.userData.time_rest = time_rest;
           
           
           
        end 
        
        function obj = evaluate_sin_measurement(obj)
            
            if obj.remove_offset_flag == true            
                obj.remove_offset();
            end
            
            % extract resitance begin and end
            obj.extract_resictance_n_sinwaves();
%             obj.extract_resictance_n_periodical();
            obj.calc_psi();
            obj.evaluate_psi_sin();
            
        end
        
        function obj = evaluate_ramps_measurement(obj)
            
            if obj.remove_offset_flag == true            
                obj.remove_offset();
            end
            
            % extract resitance begin and end
            obj.extract_resictance_n_periodical();
            obj.calc_psi();
            obj.evaluate_psi_ramps();
            
        end

        function obj = evaluate_ramps_heated_full_crr_meas_cycle(obj)
            
            if obj.remove_offset_flag == true            
                obj.remove_offset();
            end            
            % extract resitance begin and end
            obj.extract_resictance_n_ramps_heated_full_crr_meas_cycle();  
            obj.calc_psi('Rmodel','exp&sin');
%             obj.calc_psi();
            obj.evaluate_psi_ramps();
            
        end
        
        function obj = calc_psi(varargin)

            %PERFORM CHECKS HERE
                obj = varargin{1};
                % extrct data requiered for resictance and the integration
                current_int = obj.current(obj.index_ramps);
                voltage_int = obj.voltage(obj.index_ramps);
                
                if nargin ==1 
                    sel_R_model = 1;
                else
                    if ischar(varargin{2}) && ischar(varargin{3})                      
                        if strcmp(varargin{2},'Rmodel') && strcmp(varargin{3},'exp&sin')
                            sel_R_model = 2;
                        else
                            sel_R_model = -1;
                        end
                    else
                        sel_R_model = -1;
                    end
               
                end
                
                switch(sel_R_model)
                    
                    case 1
                    % Calculate Resitance Correction
                    f_R = trapz(obj.time(obj.index_ramps),obj.current(obj.index_ramps).^2);
                    cum_f_R = cumtrapz(obj.time(obj.index_ramps),obj.current(obj.index_ramps).^2);
                    obj.R_vec = obj.R_valve_start + (obj.R_valve_end-obj.R_valve_start)*cum_f_R./f_R;
                    case 2
                        time = obj.time(obj.index_ramps);
                        time = time-time(1);
                        obj.R_vec = obj.R_heat_model.model_eval(time)';                        
                    otherwise
                        error("Error during selection fo resistance compensation");
                end
                    
                
                % ------------------- calculate flux linkage ---------------------
                obj.voltage_ind = voltage_int-obj.R_vec.*current_int;            
                % calculate integral 
                obj.psi = cumtrapz(obj.time(obj.index_ramps),obj.voltage_ind); 
                % calc max and min to get the offset
                % this function outputs the MEAN of all maximum and minimum points
                % of the flux linkeage BEFORE the offset correction.        


        end        
       
        function [obj,psi_drift_mean_per_step] = evaluate_psi_ramps(obj)

            % Variablen definieren für Anzahl Messpunkte in den
            % Zeiträumen der einzelnen Messungsabschnitte
            T_meas_psi_sgnl_ramp = obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp;
            
            % here find peaks is used to identify the max and min values
            % and indices in psi and current measurements; to find min
            % values the measurement curves are inverted as findpeaks() can
            % only locate max values
            [max_psi,idx_max_psi,~,prom_max_psi] =  findpeaks(obj.psi,'MinPeakDistance',round(T_meas_psi_sgnl_ramp*4*0.9));
            [min_psi,idx_min_psi,~,prom_min_psi] =  findpeaks(-obj.psi,'MinPeakDistance',round(T_meas_psi_sgnl_ramp*4*0.9));
             min_psi = -min_psi;
            [max_curr,idx_max_current,~,prom_max_current] = findpeaks(obj.current(obj.index_ramps),'MinPeakDistance',round(T_meas_psi_sgnl_ramp*4*0.9));
            [min_curr,idx_min_current,~,prom_min_current] = findpeaks(-obj.current(obj.index_ramps),'MinPeakDistance',round(T_meas_psi_sgnl_ramp*4*0.9));
            
                        
            %#########################################
            %######### ADDED PROMINENCE TO FIND OUTLINERS
            %###########################################
            %# NOT TESTED MUCH!!!!            
            max_psi_idx_remove = prom_max_psi/mean(prom_max_psi)<0.1;
            max_psi(max_psi_idx_remove)=[];
            idx_max_psi(max_psi_idx_remove)=[];
            min_psi_idx_remove =prom_min_psi/mean(prom_min_psi)<0.1;
            min_psi(min_psi_idx_remove)=[];
            idx_min_psi(min_psi_idx_remove)=[];
            max_curr_idx_remove = prom_max_current/mean(prom_max_current)<0.1;
            max_curr(max_curr_idx_remove)=[];
            idx_max_current(max_curr_idx_remove)=[];
            min_curr_idx_remove = prom_min_current/mean(prom_min_current)<0.1;
            min_curr(min_curr_idx_remove)=[];
            idx_min_current(min_curr_idx_remove)=[];
            
            
            % somtimes the slope is deteced aswell, then the last max current is excluded
            if length(idx_max_psi)>length(idx_min_psi) % the min index is used for detection because it is always right
                idx_max_current(end) = [];
            end
            
            if length(idx_max_current)>length(idx_min_current) % the min index is used for detection because it is always right
                idx_max_current(end) = [];
            end
            
            min_curr = -min_curr;

            % check to see if number of calculated max and min values
            % matches the number of cycles run during the measurement
            if ( mean(length(idx_max_psi),length(idx_min_psi)) ~= obj.inputSignal.nmbr_cycles  ) || (mean(length(idx_max_current),length(idx_min_current)) ~= obj.inputSignal.nmbr_cycles  ) 
                error("get_point_max_psi_n_offset: the number of detected closed loops differs from" + num2str(obj.inputSignal.nmbr_cycles));            
            end
            
            % calculate drift of psi during measurement cycles to get
            % average psi offset
            if obj.compensate_drift_flag ==true                
                % ADD CHECK FOR SUFFICIENT LOOPS HERE
                % calculation of drift's mean and std only for more than 2 full loop possible                    
                psi_at_max_current = obj.psi(idx_max_current);
                psi_at_min_current = obj.psi(idx_min_current);                
                psi_drift_max2max_mean = mean(diff(psi_at_max_current));
                psi_drift_min2min_mean = mean(diff(psi_at_min_current));
                psi_drift_mean = (psi_drift_max2max_mean+psi_drift_min2min_mean)/2;
                psi_drift_mean_per_step = psi_drift_mean/(mean(diff(idx_max_current)));
                psi_drift_max2max_std = std(diff(psi_at_max_current));
                psi_drift_min2min_std = std(psi_at_min_current);
                max_psi_drift_compensated = psi_at_max_current - psi_drift_mean*[0:length(psi_at_max_current)-1] ; 
                min_psi_drift_compensated = psi_at_min_current - psi_drift_mean*[0.5:length(psi_at_min_current)-0.5] ; 

                psi_offset =  mean(max_psi_drift_compensated + min_psi_drift_compensated)/2;
            end
                     
            psi_idx_offset = obj.index_ramps(1)-1; % needed because obj.psi and obj.current differ in size
            sum_idx_drift = 0;
        
            
            
            
            for i = 1:length(idx_max_current)-1
                
             %create new empty flux_curve object                
             obj.results(i).flux_curve =flux_curve(1/obj.inputSignal.daq_Rate,[],[]);
             
             %create handle
             h_this_curve = obj.results(i).flux_curve;             
%              h_this_curve.psi_decrease = obj.psi(idx_max_current(i):idx_min_current(i));
%              h_this_curve.voltage_ind_decrease = obj.voltage_ind(idx_max_current(i):idx_min_current(i));
             psi_decrease_tmp = obj.psi(idx_max_current(i):idx_min_current(i));
%              voltage_ind_decrease_tmp = obj.current(psi_idx_offset + (idx_max_current(i):idx_min_current(i)));
             voltage_ind_decrease_tmp = obj.voltage_ind(idx_max_current(i):idx_min_current(i));
             if obj.compensate_drift_flag ==true
                %add the drift
%              h_this_curve.psi_decrease = h_this_curve.psi_decrease -psi_offset  -(sum_idx_drift + [1:(idx_min_current(i)-idx_max_current(i)+1)])*psi_drift_mean_per_step;              
                psi_decrease_tmp = psi_decrease_tmp -psi_offset  -(sum_idx_drift + [1:(idx_min_current(i)-idx_max_current(i)+1)])*psi_drift_mean_per_step;                
                  sum_idx_dirft_last_curve_decrease = (idx_min_current(i)-idx_max_current(i)+1);
             end
              % now set it to the object
              h_this_curve.psi_decrease = psi_decrease_tmp;
              h_this_curve.voltage_ind_decrease = voltage_ind_decrease_tmp;
              h_this_curve.current_decrease = obj.current(psi_idx_offset + (idx_max_current(i):idx_min_current(i)));
              %###
              
             if i <=length(idx_max_current)-1
%              	  h_this_curve.psi_increase  = obj.psi(idx_min_current(i):idx_max_current(i+1));
%                 h_this_curve.current_increase = obj.current(psi_idx_offset + (idx_min_current(i):idx_max_current(i+1)));
%                 h_this_curve.voltage_ind_increase = obj.voltage_ind((idx_min_current(i):idx_max_current(i+1))); 
                psi_increase_tmp  = obj.psi(idx_min_current(i):idx_max_current(i+1));
                current_increase_tmp = obj.current(psi_idx_offset + (idx_min_current(i):idx_max_current(i+1)));
                voltage_ind_increase_tmp = obj.voltage_ind((idx_min_current(i):idx_max_current(i+1))); 
                
                if obj.compensate_drift_flag ==true
                    %add the drift
%                     h_this_curve.psi_increase = h_this_curve.psi_increase -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease ...
%                     + [1:(idx_max_current(i+1)-idx_min_current(i)+1)])*psi_drift_mean_per_step;  
                    psi_increase_tmp = psi_increase_tmp -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease ...
                    + [1:(idx_max_current(i+1)-idx_min_current(i)+1)])*psi_drift_mean_per_step;  
                 end
             else
%                 h_this_curve.psi_increase = obj.psi(idx_min_current(i):end);
%                 h_this_curve.current_increase = obj.current(psi_idx_offset + (idx_min_current(i):length(obj.current)));
%                 h_this_curve.voltage_ind_increase = obj.voltage_ind((idx_min_current(i):length(obj.current)));
                psi_increase_tmp = obj.psi(idx_min_current(i):end);
                voltage_ind_increase_tmp = obj.voltage_ind((idx_min_current(i):length(obj.current)));
                current_increase_tmp = obj.current(psi_idx_offset + (idx_min_current(i):length(obj.current)));
                                
                 if obj.compensate_drift_flag ==true
                    %add the drift
%                     h_this_curve.psi_increase = h_this_curve.psi_increase -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease + ...
%                     [1:(length(obj.psi)-idx_min_current(i)+1)])*psi_drift_mean_per_step;
                    psi_increase_tmp = psi_increase_tmp -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease + ...
                    [1:(length(obj.psi)-idx_min_current(i)+1)])*psi_drift_mean_per_step;
                 end
             end
             
              h_this_curve.psi_increase = psi_increase_tmp;
              h_this_curve.voltage_ind_increase = voltage_ind_increase_tmp;
              h_this_curve.current_increase = current_increase_tmp;
              
             
             
             
             
                % we calculate it each loop even if just required when
                % drift is compensated for
                sum_idx_drift = idx_max_current(i+1) - idx_max_current(1);
                
%                 obj.results(i).flux_curve.calc_mean_flux_curve;
%                 obj.results(i).flux_curve.calc_dissp_current;
            end
            idx_here = 1:idx_max_current(1)-1;
            
%             
%             figure
%             plot(obj.results(1).flux_curve.current_decrease,obj.results(1).flux_curve.psi_decrease)
%             hold on
%             plot(obj.results(1).flux_curve.current_increase,obj.results(1).flux_curve.psi_increase)
%             
            psi_offset = obj.results(1).flux_curve.psi_decrease(1) - obj.psi(idx_here(end));
            
            % here the drift must be substracted because we are moving
            % backwards through the channel
            psi_0_tmp = obj.psi(idx_here) + psi_offset - flip([idx_here-1]*psi_drift_mean_per_step);  
            current_0_tmp = obj.current(obj.index_ramps(idx_here));

%             plot(current_0_tmp,psi_0_tmp)
            
            
            obj.results(1).init_curve.psi = psi_0_tmp;
            obj.results(1).init_curve.current = current_0_tmp;
            obj.results(1).psi_drift_mean_per_step = psi_drift_mean_per_step;
            obj.results(1).psi_offset = psi_offset;
                    if 1==0
                    % only used for debuging
                    for j=1
                    clf
                        sum_idx_drift = 0;
                        legend_txt= {};
                        for i = 1:length(idx_max_current)-1
                            this_pl_psi = obj.psi(idx_max_current(i):idx_max_current(i+1)); 
                            this_pl_psi = this_pl_psi-(sum_idx_drift + [1:(idx_max_current(i+1)-idx_max_current(i)+1)])*psi_drift_mean_per_step;
                            plot(obj.current(psi_idx_offset + (idx_max_current(i):idx_max_current(i+1))),-psi_offset+this_pl_psi )
                            hold on; 
                            sum_idx_drift = idx_max_current(i+1) - idx_max_current(1);
                            legend_txt{i}="Loop " + num2str(i);
                        end
                        legend(legend_txt)
                        grid on
                    end  
                    end  
           
 
        end       
        
        function obj = evaluate_ramps_heated_measurement(obj)
            
            if obj.remove_offset_flag == true            
                obj.remove_offset();
            end
            
            % extract resitance begin and end
            obj.extract_resictance_n_ramps_heated();
            obj.calc_psi();
            obj.evaluate_psi_ramps();
            
        end
        
        function obj = evaluate_ramps_heated_payload_measurement(obj)           
            if obj.remove_offset_flag == true            
                obj.remove_offset();
            end            
            % extract resitance begin and end
            obj.extract_resictance_n_ramps_heated();
            index_ramps_incl_payload = obj.index_ramps;
                        
            idx_end = index_ramps_incl_payload(end) -1 +find(abs(obj.current(index_ramps_incl_payload(end):end)-0.0005)<=0.0005,1,'first');
            index_ramps_incl_payload = [index_ramps_incl_payload,index_ramps_incl_payload(end):1:idx_end];
            % only works with ramp signals!!!!!!!!!!!!!
            % because  'obj.extract_resictance_n_ramps_heated()' will
            % identify the wrond end dor the ramps pahse, we have to corret
            % it
%             idx_1=find(obj.current(1:obj.index_ramps(end))>obj.current(obj.index_ramps(end))*1.05,1,'last');
%             idx_2=find(obj.current(1:idx_1)<obj.current(obj.index_ramps(end)),1,'last');
%             [~,idx]= max(obj.current(idx_2:idx_1));
            [~,idx_peaks,~,p]=findpeaks(obj.current(obj.index_ramps(1):obj.index_ramps(end)));
            idx_peaks = idx_peaks(p>mean(p)*3)+obj.index_ramps(1)-1;
            idx_end_plus_one = idx_peaks(obj.inputSignal.nmbr_cycles+1) +1;
            obj.index_ramps( obj.index_ramps(:)>=idx_end_plus_one) = [];            
            obj.calc_psi();
            obj.evaluate_psi_ramps(); 
            obj.index_ramps = index_ramps_incl_payload;
            obj.calc_psi();
            
           
        end
        
        function obj = evaluate_ramps_crr_meas_measurement(obj)
            
            if obj.remove_offset_flag == true            
                obj.remove_offset();
            end
            
            % extract resitance begin and end
            obj.extract_resictance_n_periodical();
            obj.calc_psi();
%             obj.evaluate_psi_ramps();
            
        end
                
        function obj = evaluate_const_psi_measurement(obj)            
            if obj.remove_offset_flag == true            
                obj.remove_offset();
            end
            
            % extract resitance begin and end
            obj.extract_resictance_n_const_psi_ramps();
            obj.calc_psi();
             obj.evaluate_psi_ramps();
            
        end
   
        function obj = extract_resictance_n_ramps_heated_full_crr_meas_cycle(varargin)
        
            obj = varargin{1};
            [obj,idx_R_start,idx_R_end,abs_crr_dot_steps_idx] = obj.extract_idxs();

            flag_calc_R_meas = true;

            if nargin >1
                if strcmp(varargin{2},'skip_R_meas')
                    flag_calc_R_meas = false;
                end
            end

%             range_idx_check = round(length(obj.current)*0.75);  
% 
%             [idx_begin_R_meas_start,idx_end_R_meas_start,idx_begin_hold_ramps_start,idx_end_hold_ramps_start,idx_ramps_start,idx_diag_intersecction_start,index_begin_current_hold_ramps_start,idx_start_curr_meas_begin] ...
%             =  obj.get_meas_idx_with_ramps(1,range_idx_check);
% 
%             nmbr_meas_pnts = size(obj.current,2);
% 
%             [~,idx]=find(abs(obj.current-obj.inputSignal.i_heat)>0.001,1,'last');
% 
% 
%             range_idx_check = (nmbr_meas_pnts-idx)+range_idx_check;
%             if range_idx_check>nmbr_meas_pnts
%                 range_idx_check = nmbr_meas_pnts-1;
%             end
% 
%             [idx_begin_R_meas_end_tmp,idx_end_R_meas_end_tmp,idx_end_hold_ramps_end,idx_begin_hold_ramps_end,idx_ramps_end,idx_diag_intersecction_end,index_begin_current_hold_ramps_end,idx_start_curr_meas_end] ...
%                 =  obj.get_meas_idx_with_ramps(nmbr_meas_pnts,nmbr_meas_pnts-range_idx_check);
% 
%             idx_end_R_meas_end = nmbr_meas_pnts-idx_begin_R_meas_end_tmp;
%             idx_begin_R_meas_end = nmbr_meas_pnts-idx_end_R_meas_end_tmp;
%             idx_begin_hold_ramps_end = nmbr_meas_pnts-idx_begin_hold_ramps_end;
%             idx_end_hold_ramps_end = nmbr_meas_pnts-idx_end_hold_ramps_end;
%             idx_ramps_end = nmbr_meas_pnts-idx_ramps_end;
%             idx_diag_intersecction_end = nmbr_meas_pnts-idx_diag_intersecction_end;
%             index_begin_current_hold_ramps_end = nmbr_meas_pnts-index_begin_current_hold_ramps_end;
%             idx_start_curr_meas_end = nmbr_meas_pnts-idx_start_curr_meas_end;
% 
%            % obj.index_ramps = idx_ramps_start:idx_ramps_end; !!!!! These are
%            % all ramps but we want the last ramp to end at the peak
%             obj.index_ramps = idx_ramps_start:idx_diag_intersecction_end;
% 
% 
%             obj.idx_begin_hold_ramps_start =idx_begin_hold_ramps_start;
%             obj.idx_end_hold_ramps_start = idx_end_hold_ramps_start;
%             obj.idx_diag_intersecction_start = idx_diag_intersecction_start;
%             obj.idx_begin_hold_ramps_end = idx_begin_hold_ramps_end;
%             obj.idx_end_hold_ramps_end = idx_end_hold_ramps_end;
%             obj.idx_diag_intersecction_end = idx_diag_intersecction_end;
%             obj.index_begin_current_hold_ramps_start = index_begin_current_hold_ramps_start;
%             obj.index_begin_current_hold_ramps_end = index_begin_current_hold_ramps_end;
%             obj.idx_start_curr_meas_begin = idx_start_curr_meas_begin;
%             obj.idx_start_curr_meas_end = idx_start_curr_meas_end;
% 
%             idx_R_start = idx_begin_R_meas_start:idx_end_R_meas_start;
%             idx_R_end = idx_begin_R_meas_end:idx_end_R_meas_end;

            [obj.R_valve_start,R_valve_start_std,idx_start_end]= obj.calc_R_from_steady_state_v2(obj.voltage(idx_R_start),obj.current(idx_R_start));        
            obj.R_valve_start_idx = idx_start_end + idx_R_end(1);
            [obj.R_valve_end,R_valve_end_std,idx_start_end] = obj.calc_R_from_steady_state_v2(obj.voltage(idx_R_end),obj.current(idx_R_end));
            obj.R_valve_end_idx = idx_start_end + idx_R_start(1);

            if flag_calc_R_meas        
                results_start_ramps = obj.evaluate_heat_crr_meas_ramps(obj.index_begin_current_hold_ramps_start,obj.idx_begin_hold_ramps_start,obj.idx_end_hold_ramps_start);
                results_end_ramps = obj.evaluate_heat_crr_meas_ramps(obj.index_begin_current_hold_ramps_end,obj.idx_begin_hold_ramps_end,obj.idx_end_hold_ramps_end);        
                obj.R_heat_model = obj.fit_heating(results_start_ramps,results_end_ramps);
            end

%             figure
%             plot(obj.time(obj.index_ramps)-obj.time(obj.index_ramps(1)),abs(obj.current(obj.index_ramps)));
%             hold on;
%             plot(results_start_ramps.time_Rmeas_without_holdphase_zeroed_ramps_start,results_start_ramps.c_mean);
%             plot(results_end_ramps.time_Rmeas_without_holdphase_zeroed_ramps_start,results_end_ramps.c_mean);


        end
        
        
        function [model_fits]= fit_heating(obj,results_start_ramps,results_end_ramps)
%           
            
%             time_offset = time(1)-results_start_ramps.time_Rmeas_without_holdphase_zeroed_ramps_start(1);
%             time = time-time_offset;
            R = [results_start_ramps.R_mean;results_end_ramps.R_mean];
            R_valve_std = [results_start_ramps.R_valve_std;results_end_ramps.R_valve_std];
            c_mean =  [results_start_ramps.c_mean;results_end_ramps.c_mean];
            
            time_offset = results_start_ramps.time_Rmeas_without_holdphase(1)-results_start_ramps.time_Rmeas_without_holdphase_zeroed_ramps_start(1);
            [time_pnts_1,weights_1,R_fit_1]  = extract_peaks(results_start_ramps,time_offset,0);
            [time_pnts_2,weights_2,R_fit_2]  = extract_peaks(results_end_ramps,time_offset,results_start_ramps.holdphase_duration);
            
             duration_crr_meas_1 = obj.time(obj.index_ramps(1)) - obj.time(obj.idx_start_curr_meas_begin) ;
             duration_crr_meas_2 =   obj.time(obj. index_begin_current_hold_ramps_end )   -    obj.time(obj.idx_start_curr_meas_end );
             time_shift_total = results_start_ramps.holdphase_duration +duration_crr_meas_1+duration_crr_meas_2 ;
             
             time =[results_start_ramps.time_Rmeas_without_holdphase;results_end_ramps.time_Rmeas_without_holdphase-time_shift_total];
             time = time-time_offset;
             
            R_fit = [R_fit_1; R_fit_2];

            weights = [ weights_1; weights_2];
            time_pnts = [time_pnts_1;time_pnts_2];   
             
%              time = results.time_Rmeas_without_holdphase;
%              R = results.R_mean;

                % sort out some values
                R_m = mean(R_valve_std);
                R_std = std(R_valve_std);
                idx_sel = and(R_valve_std <(R_m+0.25*R_std) , c_mean >0.05);
% 
%                  R_range = max(R(idx_sel))-min(R(idx_sel));
%                  [~,locs,~,p] = findpeaks(R(idx_sel)); 
%                  index = find(p>mean(p));
%                  
%                  index_tmp = [1:length(idx_sel)];
%                  index_tmp(index_tmp==0)=[];                 
%                  
% %                  index = index_tmp(index);
%                  index_peaks = index_tmp(locs(index));
% %                  index_peaks = idx_sel(index_peaks);
%                  R_fit = [];
%                  weights = [];
%                  time_pnts = [];
%                  R_mean = 0;
%              
%              
% 
%              for i = 1:length(index)
%                 index =  find(abs(R(index_peaks(i)-5:index_peaks(i)+5)-R(index_peaks(i)))<R_range*0.01); 
%                 index_sel = index_peaks(i)-6+index;
%                 R_fit = [R_fit; R(index_sel)];
%                 nmbr_pnts = length(index_sel);
%                 weights = [ weights; ones(nmbr_pnts,1)*1/nmbr_pnts];
%                 time_pnts = [time_pnts;time(index_sel)];        
%              end


             

             [xData, yData, weights] = prepareCurveData( double(time_pnts), double(R_fit), weights );
            % Set up fittype and options.
            ft = fittype( 'e*(1-1/exp(a*x+b))', 'independent', 'x', 'dependent', 'y' );
            opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
            opts.Algorithm = 'Levenberg-Marquardt';
            opts.Display = 'Off';
            opts.Normalize = 'on';
            opts.Robust = 'Bisquare';
            opts.StartPoint = [0 7 25];
            opts.Weights = weights;
            % Fit model to data.
            [model_fits.model_heating, ~] = fit( xData, yData, ft, opts );
               figure( 'Name', 'untitled fit 1' );
            h = plot( model_fits.model_heating, xData, yData );
            legend( h, 'R_zeroed vs. time_zeroed', 'untitled fit 1', 'Location', 'NorthEast', 'Interpreter', 'none' );
            % Label axes
            xlabel( 'time_zeroed', 'Interpreter', 'none' );
            ylabel( 'R_zeroed', 'Interpreter', 'none' );
            grid on
            hold on;
%             plot(time,R);
%             1+1
                        
             % we want to get the signals period by fiding the current peaks
             [~,idx_peaks]=findpeaks(c_mean);
             % because we have the flux measurement ramps included, we sort
             % them out dirty
             dt = gradient(time(idx_peaks));
             mean_dt = mean(dt);
             % use later to guess the sin's period
             T_mean = mean(dt(dt<mean_dt));
            
           
            shift_fit_data =  - feval(model_fits.model_heating,time(idx_sel))+R(idx_sel);
            shift_fit_data = shift_fit_data-mean(shift_fit_data);
            
            vec_tmp = shift_fit_data-min(shift_fit_data);
            weights = 0.05+ones(size(shift_fit_data)).*(vec_tmp./max(vec_tmp)).^2*0.95;
            
            %first sin fit
            [xData, yData] = prepareCurveData( double(time(idx_sel)), double(shift_fit_data));
            % Set up fittype and options.
            ft = fittype( 'sin1' );
            opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
            opts.Display = 'Off';
            opts.StartPoint = [max(yData) 2*pi/T_mean 2*pi*0.8];
            opts.upper = [max(yData) 2*pi/T_mean*1.01 2*pi];
            opts.Lower = [0.95*max(yData) 2*pi/T_mean*0.99 0];
            opts.Weights = weights;
            % Fit model to data.
            [model_fits.model_sin, ~] = fit( xData, yData, ft, opts );
% %             a1 = fitresult.a1;
% %             b1 = fitresult.b1;
% %             c1 = fitresult.c1;
            % % Plot fit with data.
            figure( 'Name', 'untitled fit 1' );
            h = plot( model_fits.model_sin, xData, yData );
            legend( h, 'R_zeroed vs. time_zeroed', 'untitled fit 1', 'Location', 'NorthEast', 'Interpreter', 'none' );
            % Label axes
            xlabel( 'time_zeroed', 'Interpreter', 'none' );
            ylabel( 'R_zeroed', 'Interpreter', 'none' );
            grid on

% % %             % second fit to improve phase, everything else locked
% % %             % Set up fittype and options.
% % %             ft = fittype( 'a*sin(b*x+c)+d', 'independent', 'x', 'dependent', 'y' );
% % %             opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
% % %             opts.Display = 'Off';    
% % %             opts.Robust = 'Bisquare';
% % % 
% % %             opts.Lower = [a1 b1 0 0];
% % %             opts.StartPoint = [a1 b1 c1 0];
% % %             opts.Upper = [a1 b1 2*pi 0];
% % %             % Fit model to data.
% % %             [model_fits.model_sin, ~] = fit( xData(yData>0), yData(yData>0), ft, opts );        

            % plot(xData, yData);
            % hold on
            % yf = feval(fitresult,xData);
            % plot(xData,yf);
            % plot(  xData,feval(results.model_sin, xData));

            % third fit to optimize offset
            % % Set up fittype and options.
%             ft = fittype( 'a*sin(b*x+c)+d', 'independent', 'x', 'dependent', 'y' );
%             opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
%             opts.Display = 'Off';
%             opts.Lower = [a1 b1 c1 0];
%             opts.Robust = 'Bisquare';
%             opts.StartPoint = [a1 b1 c1 0];
%             opts.Upper = [a1 b1 c1 1];
%             % Fit model to data.
%             [model_sin, ~] = fit( xData, yData, ft, opts );

%             % Plot fit with data.
%             figure( 'Name', 'untitled fit 1' );
%             h = plot( model_sin, xData, yData );
%             legend( h, 'y vs. x', 'untitled fit 1', 'Location', 'NorthEast', 'Interpreter', 'none' );
%             % Label axes
%             xlabel( 'x', 'Interpreter', 'none' );
%             ylabel( 'y', 'Interpreter', 'none' );
%             grid on



            model_fits.offset = model_fits.model_sin.a1;
            model_fits.model_eval = @(this_time) feval(model_fits.model_heating,this_time)...
                                + feval(model_fits.model_sin,this_time)-model_fits.offset;
            model_fits.R_model = model_fits.model_eval(time);
            model_fits.time_R_model = time;
            model_fits.R_zeroed = shift_fit_data;
            model_fits.time_zeroed = time(idx_sel);
            model_fits.R = R;
            %%
            
            
            %%
            
            function [time_pnts,weights,R_fit]  = extract_peaks(this_results,time_offset,time_duration_offset)
                
%                  time =[results_start_ramps.time_Rmeas_without_holdphase;results_end_ramps.time_Rmeas_without_holdphase-results_start_ramps.holdphase_duration];
                
                time_local = this_results.time_Rmeas_without_holdphase-time_offset-time_duration_offset;
                R_loc = this_results.R_mean;
                R_valve_std_loc = this_results.R_valve_std;
                c_mean_loc =  this_results.c_mean;
                 % sort out some values
                R_m_loc = mean(R_valve_std_loc);
                R_std_loc = std(R_valve_std_loc);
                idx_sel_loc = and(R_valve_std_loc <(R_m_loc+0.25*R_std_loc) , c_mean_loc >0.05);

                 R_range = max(R_loc(idx_sel_loc))-min(R_loc(idx_sel_loc));
                 [~,locs,w,p] = findpeaks(R_loc(idx_sel_loc)); 
                 
                 
                 if length(locs)>2 %!!!!! NOT GOOD !!! WE ASUME HERE TO HAVE 2 HEAT RAMPS
                    index = find(p>mean(p)-0.25*std(p));     
                    % WE KNOW THERE MUST BE TO PEAKS SO WE SPLIT THE SET IN
                    % TWO AND LOOK FOR THE MOST PROMINET PEAK IN EACH SET
                    if length(index)>2  
                        index=zeros(1,2);
                     idx_end_first_block = find(diff(idx_sel_loc)<0,1,'first');                                         
                     [~,index(1)]=max(p(locs<=idx_end_first_block));
                     
%                      idx_begin_second_block = find(diff(idx_sel_loc(idx_end_first_block:end)>0,1,'first');                                         
                     [~,index(2)]=max(p(locs>idx_end_first_block));
                     
                     index(2) = index(2)+sum(locs<=idx_end_first_block);
                     
                    end
                 else
                     index = 1:length(locs);
                 end

                 index_tmp = [1:length(idx_sel_loc)].*idx_sel_loc';
                 index_tmp(index_tmp==0)=[];                 

%                  index = index_tmp(index);
                 index_peaks = index_tmp(locs(index));
%                  index_peaks = idx_sel(index_peaks);
                 R_fit = [];
                 weights = [];
                 time_pnts = [];
                 R_mean = 0;
                 
                 for ii = 1:length(index)
                    index =  find(abs(R_loc(index_peaks(ii)-5:index_peaks(ii)+5)-R_loc(index_peaks(ii)))<R_range*0.01); 
                    index_sel = index_peaks(ii)-6+index;
                    R_fit = [R_fit; R_loc(index_sel)];
                    nmbr_pnts = length(index_sel);
                    weights = [ weights; ones(nmbr_pnts,1)*1/nmbr_pnts];
                    time_pnts = [time_pnts;time_local(index_sel)];        
                 end
            
            end
        end
        
       
%         function [model_sin} = fit_model_sin(~,
        
        
        function results = evaluate_heat_crr_meas_ramps(obj,idx_ramp_start,idx_begin_hold_ramps_end,idx_end_hold_ramps_end)
            
            daq_rate = obj.inputSignal.daq_Rate;  
            filter_current = @(data)  smoothdata(data,'gaussian',round(daq_rate*0.005));
            filter_current_dot = @(data) smoothdata(gradient(data),'movmean',round(daq_rate*0.025));      
            curr_ramps = obj.current(idx_begin_hold_ramps_end:idx_end_hold_ramps_end);
            vlt_ramps = obj.voltage(idx_begin_hold_ramps_end:idx_end_hold_ramps_end);
            time = obj.time(idx_begin_hold_ramps_end:idx_end_hold_ramps_end);
            crr_sm_ramps = filter_current(curr_ramps);
            crr_dot_sm_ramps = filter_current_dot(crr_sm_ramps);

            crr_dot_limit = max((crr_dot_sm_ramps));

            crr_dot_sm_ramps_zero = abs(crr_dot_sm_ramps)>crr_dot_limit*0.5;
            %#################################################EDIT
            crr_dot_sm_ramps_zero= 1-round(smoothdata(crr_dot_sm_ramps_zero,'movmean',200)); %%%%%%%%%% !!! for higher i_dot the filter might by less than 200 in order to remove only the change around i = 0

            crr_dot_sm_ramps_zero_changes = crr_dot_sm_ramps_zero(2:end)-crr_dot_sm_ramps_zero(1:end-1);

            zones_start = find(crr_dot_sm_ramps_zero_changes>0);
            zones_end = find(crr_dot_sm_ramps_zero_changes<0);

            zones_start = [1,zones_start]; % we have to add first and last value
            zones_end = [zones_end,length(crr_dot_sm_ramps)];

            nmbr_Zones = length(zones_start);
            R_mean = zeros(nmbr_Zones,1);
            R_valve_std = zeros(nmbr_Zones,1);
            index_R_mean = zeros(nmbr_Zones,2);
            duration = zeros(nmbr_Zones,1);
            time_middle = zeros(nmbr_Zones,1);
            c_mean = zeros(nmbr_Zones,1);
            v_mean = zeros(nmbr_Zones,1);
            idx_holdphase_start = zeros(nmbr_Zones,1);
            idx_holdphase_end = zeros(nmbr_Zones,1);

            for i = 1:nmbr_Zones
                try
                    
% % %                     if i==30
% % %                         1+1                        
% % %                     end
                    
                    idx = zones_start(i):zones_end(i);
                    [R_mean(i),R_valve_std(i), idx_range] = obj.calc_R_from_steady_state_v2(vlt_ramps(idx),curr_ramps(idx));
                    index_R_mean(i,:) = idx(idx_range);
                    time_R_meas = time(index_R_mean(i,:));
                    duration(i) = time_R_meas(end)-time_R_meas(1);
                    time_middle(i) = time_R_meas(1)+duration(i)/2;   
                    c_mean(i) = mean(curr_ramps(idx(idx_range(1):idx_range(2))));
                    v_mean(i) = mean(vlt_ramps(idx(idx_range(1):idx_range(2))));
                    % we need to find the index where measurement phase starts/ends

                    if i == 1            
                        idx_holdphase_start(i) = 1;
%                         idx_diag_search_end = zones_start(i);         
                    else
                        if mod(i-1,29) == 0
                            [~,idx_min]=min(curr_ramps(zones_start(i-1):zones_start(i)));
                            idx_diag_search_start = zones_start(i-1)-1+idx_min;
                        else
                            idx_diag_search_start =  zones_start(i-1);                    
                        end
                        idx_diag_search_end =  zones_start(i);
                        
                        [~,index_start] = find(abs(curr_ramps(idx_diag_search_start:idx_diag_search_end) - c_mean(i))<0.001 ,1,'first');  
                        dig_delta = round((idx_diag_search_end-idx_diag_search_start)*0.5);   
                        idx_diag_search_start = idx_diag_search_start +index_start-1;   
                        idx_diag_search_start = round(idx_diag_search_start-dig_delta:idx_diag_search_start);     
                        
% %                         if i==29
% %                            1+1 
% %                         end
                        
                        idx_holdphase_start(i) = obj.getIntersection_diag_hor(idx_diag_search_start(1),curr_ramps(idx_diag_search_start),curr_ramps(idx(idx_range(1):idx_range(2))));
                       
                    end      
                    
                    
                    if i < length(zones_end)  
                        idx_diag_search_start = zones_end(i);                
                        if mod(i,29) == 0                
                            [~,idx_min]=min(curr_ramps(zones_end(i):zones_end(i+1)));
                            idx_diag_search_end = zones_end(i)-1+idx_min;
                        else
                            idx_diag_search_end = zones_end(i+1); 
                        end
                        [~,index_start] = find(abs(curr_ramps(idx_diag_search_start:idx_diag_search_end) -c_mean(i))<0.001 ,1,'last');  
                        dig_delta = round((idx_diag_search_end-idx_diag_search_start)*0.5);   
                        idx_diag_search_start = idx_diag_search_start +index_start-1;   
                        idx_diag_search_start = round(idx_diag_search_start:idx_diag_search_start+dig_delta);          
                        idx_holdphase_end(i) = obj.getIntersection_diag_hor(idx_diag_search_start(1),curr_ramps(idx_diag_search_start),curr_ramps(idx(idx_range(1):idx_range(2))));
                    else         
%                             idx_diag_search_start =  zones_end(i);
                             idx_holdphase_end(i)  =  length(curr_ramps);
                    end      
                    
                catch
                 error('Error while evaluating heat ramps');  
                end

            end 
            results=struct();
    
            duration_holdphase = time(idx_holdphase_start)-time(idx_holdphase_end);        
            time_Rmeas_without_holdphase=[];
            time_tmp = time;   
            for k = 1:length(idx_holdphase_start)
                time_Rmeas_without_holdphase(k) = time_tmp(idx_holdphase_start(k));
                time_tmp = time_tmp+duration_holdphase(k);
            end 
            time_Rmeas_without_holdphase_zeroed_ramps_start = time_Rmeas_without_holdphase-time_Rmeas_without_holdphase(1);
            % we want the time_  vector to be zero when the current ramp
            % start. Because whe started with the first hold phase, we have
            % to add the duration of the first ramp.! ATTENTION: when the
            % apply the function to the ramps at the and of the full
            % measurement cycle, we have to use the last ramp because the
            % signal ist flipped.


            time_offset = obj.time(idx_begin_hold_ramps_end)- obj.time(idx_ramp_start);            
            time_Rmeas_without_holdphase_zeroed_ramps_start = time_Rmeas_without_holdphase_zeroed_ramps_start+time_offset;
            
            % we substract this_Data.time(idx_ramps_steps_start) so at t=0 the
            % ramps start
            results.time_Rmeas_without_holdphase = time_Rmeas_without_holdphase';% - this_Data.time(idx_ramps_steps_start); % <----- Hauptzeitkanal
            results.time_middle = time_middle;% -this_Data.time(idx_ramps_steps_start) ;
            results.time_Rmeas_without_holdphase_zeroed_ramps_start = time_Rmeas_without_holdphase_zeroed_ramps_start;
            results.R_mean = R_mean;
            results.index_R_mean = index_R_mean;
            results.duration = duration;    
            results.R_valve_std = R_valve_std;
            results.c_mean = c_mean;
            results.v_mean = v_mean;              
            results.idx_holdphase_end = idx_holdphase_end;
            results.idx_holdphase_start = idx_holdphase_start;        
            results.i_max = max(c_mean);
            results.holdphase_duration = (time(end)-time(1))-(time_Rmeas_without_holdphase(end)-time_Rmeas_without_holdphase(1));

    
        end
        
                 
        function [idx_begin_R_meas_start,idx_end_R_meas_start,idx_begin_hold_ramps_start,idx_begin_hold_ramps_end,idx_ramps_steps_start ,idx_diag_intersecction,index_start_current_hold_ramps,index_start_curr_meas] ...
                    =  get_meas_idx_with_ramps(obj,idx_search_start,idx_search_end)
        % ----------------- get start and end of the ramps for integration -
                        % THIS IMPLEMENTATION ONLY WORKS FOR SIGNAL STARTING A SOME
                        % POSITVE CURRENT, lowering torwards zero, apply a step for
                        % current measumrements followed by ramps, last ramp has to
                        % end at zero. Then again a step for current measurmet is
                        % apply and the current raised to the initial value!

                        % the saved index 'idx_saved_ramp marks' the region, where the ramp
                        % stars, so we search here. We look for a intersection of
                        % horizontal line with an increasing/decreasing.
                        % This is done in 'getIntersection_diag_hor()'.                      
                        daq_rate = obj.inputSignal.daq_Rate;
                        nmbr_cycles = obj.inputSignal.nmbr_cycles;

                        if idx_search_start < idx_search_end  
                            flg_signl_order_reversed = false;
                        else
                            flg_signl_order_reversed = true;
                        end

                        if ~flg_signl_order_reversed                               
                            current =  obj.current(idx_search_start:idx_search_end);
                            voltage =  obj.voltage(idx_search_start:idx_search_end);                
                        else                    
                            current =  obj.current(idx_search_start:-1:idx_search_end);
                            voltage =  obj.voltage(idx_search_start:-1:idx_search_end); 
                        end

                        nmbr_data_pnts = length(current);
                        i_meas = obj.inputSignal.i_meas_current;
%                         T_heating = obj.inputSignal.T_meas_psi_sgnl_ramp/sqrt(3);
                        T_heating = obj.inputSignal.T_meas_psi_sgnl_ramp*obj.inputSignal.i_heat/obj.inputSignal.i_max;
                        T_meas_res_start = obj.inputSignal.T_meas_resistance_start;
                        T_sgnl_ramp = obj.inputSignal.T_meas_psi_sgnl_ramp;

                        % now we use a filterd signal of the current change in
                        % order to find the regions where the signal changes
                        % ATTENTION: due to filters the signal is blurred and is 
                        % only sufficient to find the regions, the exact edges must
                        % then be searched for separately

                        current2 = smoothdata(current,'gaussian',round(daq_rate*0.001)); % #gm set from 0.01 to 0.001
                        crr_dot = smoothdata(gradient(current2),'movmean',round(daq_rate*0.025));
                        abs_crr_dot = abs(crr_dot); 

                        mean_abs_crr_dot = mean(abs_crr_dot(abs_crr_dot>3e-6)); %  #gm  set from 1e-6 to 3e-6 we asume 1e-6 to be the threshold for noise
                        std_abs_crr_dot = std(abs_crr_dot(abs_crr_dot>1e-6));
                        mean_abs_crr_dot = mean(abs_crr_dot(and(abs_crr_dot>1e-6,abs_crr_dot<mean_abs_crr_dot+std_abs_crr_dot)));
                        std_abs_crr_dot = std(abs_crr_dot(and(abs_crr_dot>1e-6,abs_crr_dot<mean_abs_crr_dot+std_abs_crr_dot)));

                        abs_crr_dot_above_mean =  abs_crr_dot>mean_abs_crr_dot*0.5; % <---- because of filters 
                        % this id blurred so we assume everything above
                        % mean_abs_crr_dot*0.5 to be current_dot not zero! We don't
                        % need it exactly in the following
%!!!!!!!!!!!!! Attention, this filter adjustment for low currents is only a hot fix                      
                        if obj.inputSignal.i_max < 0.06
                            abs_crr_dot_above_mean = round(smoothdata(abs_crr_dot_above_mean,'movmean',25)); 
                        else
                            abs_crr_dot_above_mean = round(smoothdata(abs_crr_dot_above_mean,'movmean',50)); 
                        end

                        abs_crr_dot_steps = abs_crr_dot_above_mean(2:end)-abs_crr_dot_above_mean(1:end-1);         

                        % now get changes at beginning             
                        % first step: falling current goes to zero
                        % second step: current starts to rise to measruement current
                        % third step: current has reached  measruement current
                        % fourth step: current starts to fall to zero current
                        % fifth step: current has reached  zero  current
                        % sixth step: current starts with first ramp

                        abs_crr_dot_steps_idx = find(abs_crr_dot_steps);

                        flag_initial_zero_phase = false;
                        %due to lags we don't know, if we start with a ramp or
                        %constant current -> we check it                
                        if mean(abs_crr_dot(1:abs_crr_dot_steps_idx(1))) < mean_abs_crr_dot-std_abs_crr_dot
                            % if we start with a constant phase there will be an
                            % additional step at the beginning! The rest of the
                            % function is implemented to start with a decreasing
                            % current, therefore we remove the first step
                            if ~flg_signl_order_reversed
                                first_idx_2_calc_start_holdphase = abs_crr_dot_steps_idx(1);
                            else
                                %otherwise calculated later
                            end
                            abs_crr_dot_steps_idx(1) = [];
                            flag_initial_zero_phase = true;                            
                        else
                            if ~flg_signl_order_reversed
                                first_idx_2_calc_start_holdphase = 1;
                            else
                                %otherwise calculated later
                            end
                            
                        end

                        % due to the filter our idx_crr_steps_start are 'behind'
                        % the real measured signal
                        nmbr_hold_steps_per_ramp = obj.inputSignal.nmbr_incr_hold_steps;
                        
                        nmbr_hold_steps_cycles = 2;
                        
                       
                        idx_begin_hold_ramps_start = 3;
                        idx_begin_hold_ramps_start=abs_crr_dot_steps_idx(idx_begin_hold_ramps_start);
                        
                        
                        switch(obj.inputSignal.Type) 
                        
                            case 'ramps_heated_full_crr_meas_cycle'
                                idx_begin_hold_ramps_end = nmbr_hold_steps_per_ramp*4*nmbr_hold_steps_cycles;                        
                                idx_start_search = idx_begin_hold_ramps_end +3;
                                idx_begin_hold_ramps_end =abs_crr_dot_steps_idx(idx_begin_hold_ramps_end);
                            case 'ramps_heated'                             
                                idx_start_search = 3;
                            otherwise
                                error("unkown signal type for this function");
                        end
                        
                        [~,idx_begin_R_meas_start]=find(abs(current(abs_crr_dot_steps_idx(idx_start_search):abs_crr_dot_steps_idx(idx_start_search+1))-i_meas)<=0.002,1,'first');
                        
                        [~,idx_end_R_meas_start]=find(abs(current(abs_crr_dot_steps_idx(idx_start_search):abs_crr_dot_steps_idx(idx_start_search+1))-i_meas)<=0.002,1,'last');

                        idx_begin_R_meas_start = idx_begin_R_meas_start +abs_crr_dot_steps_idx(idx_start_search)-1;
                        idx_end_R_meas_start =  idx_end_R_meas_start +abs_crr_dot_steps_idx(idx_start_search)-1;
                        
                         
                        %%%%%%%%% ramp begin
                        [~,idx_tmp]=min(abs(abs_crr_dot_steps_idx-idx_end_R_meas_start));
                        idx_Rmeas_end = abs_crr_dot_steps_idx(idx_tmp+1); % measurement end
                        idx_Rramp_start = abs_crr_dot_steps_idx(idx_tmp+2); % first ramp start
                        
                        if length(abs_crr_dot_steps_idx)>= idx_tmp+3 %some times the last velocity step is missing
                            idx_Rramp_end = abs_crr_dot_steps_idx(idx_tmp+3); % first ramp end
                        else
                            idx_Rramp_end = length(current); 
                        end
                        
                        len_hor = idx_Rramp_start - idx_Rmeas_end;
                        len_diag =idx_Rramp_end - idx_Rramp_start;
                        index_hor_search = idx_Rmeas_end+ round(len_hor*0.1):idx_Rmeas_end+ round(len_hor*0.8);
                        index_diag_search = idx_Rramp_start + round(len_diag*0.1):idx_Rramp_start + round(len_diag*0.8);

                        curr_diag_vals = current(index_diag_search); 
                        curr_hor_vals = current(index_hor_search);     
                        idx_ramps_steps_start = obj.getIntersection_diag_hor(index_diag_search(1),curr_diag_vals,curr_hor_vals);
                        
                        idx_Ramp2_start = abs_crr_dot_steps_idx(idx_tmp+4); % first ramp start
                        
                        if length(abs_crr_dot_steps_idx)< idx_tmp+5
                            idx_Ramp2_end = length(current);
                        else
                            idx_Ramp2_end = abs_crr_dot_steps_idx(idx_tmp+5); % first ramp end 
                        end
                        len_diag =idx_Ramp2_end - idx_Ramp2_start;
                        index_diag2_search = idx_Ramp2_start + round(len_diag*0.1):idx_Ramp2_start + round(len_diag*0.8);
                        curr_diag2_vals = current(index_diag2_search);   
                        idx_diag_intersecction = obj.getIntersection_diag_diag(index_diag_search(1),curr_diag_vals,index_diag2_search(1),curr_diag2_vals);
           
                        
                         switch(obj.inputSignal.Type) 
                        
                            case 'ramps_heated_full_crr_meas_cycle'
                                 % calculate start of the R meas ramps 
                                if ~flg_signl_order_reversed  
                                    % find start of hold ramps
                                    delta_len = abs_crr_dot_steps_idx(1)- first_idx_2_calc_start_holdphase;                        
                                    idx_ramp1 = abs_crr_dot_steps_idx(1)-round(0.7*delta_len):abs_crr_dot_steps_idx(1)-round(0.2*delta_len);
                                    delta_len = abs_crr_dot_steps_idx(3)- abs_crr_dot_steps_idx(2);
                                    idx_ramp2 = abs_crr_dot_steps_idx(2)+round(0.2*delta_len):abs_crr_dot_steps_idx(2)+round(0.7*delta_len);
                                    index_start_current_hold_ramps = obj.getIntersection_diag_diag(idx_ramp1(1),current(idx_ramp1),idx_ramp2(1),current(idx_ramp2));
                                    % find end oh hold ramps
                                    idx_crr_m_start = (obj.inputSignal.nmbr_incr_hold_steps*2)*2*2+1; %!!!!!!!!!!!!!!! ATTENTION!!!!! HARD CODED FOR TWO RAMPS!!!!!! 
                                    delta_len = abs_crr_dot_steps_idx(idx_crr_m_start+1)-abs_crr_dot_steps_idx(idx_crr_m_start);
                                    idx_hor = abs_crr_dot_steps_idx(idx_crr_m_start)+round(0.2*delta_len):abs_crr_dot_steps_idx(idx_crr_m_start)+round(0.7*delta_len);                            
                                    delta_len = abs_crr_dot_steps_idx(idx_crr_m_start)-abs_crr_dot_steps_idx(idx_crr_m_start-1);
                                    idx_ramp1= abs_crr_dot_steps_idx(idx_crr_m_start)-round(0.8*delta_len):abs_crr_dot_steps_idx(idx_crr_m_start)-round(0.2*delta_len); 
                                    index_start_curr_meas = obj.getIntersection_diag_hor(idx_ramp1(1),current(idx_ramp1),current(idx_hor));

                                else 
                                    % case for reversed signal
                                    idx_begin_hold_ramps_end_tmp = nmbr_hold_steps_per_ramp*4*nmbr_hold_steps_cycles;   

                                    delta_len = abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp+1)-abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp);
                                    idx_ramp1 = abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp+1)-round(0.7*delta_len):abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp+1)-round(0.2*delta_len);                            
                                    delta_len = abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp+2)-abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp+1);
                                    idx_hor= abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp+1)+round(0.2*delta_len):abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp+1)+round(0.7*delta_len); 
                                    index_start_current_hold_ramps = obj.getIntersection_diag_hor(idx_ramp1(1),current(idx_ramp1),current(idx_hor));
                                    % find start of current 
                                    index_start_curr_meas = idx_Rramp_start;
                                end
                            case 'ramps_heated'                             
                                %nothing
                                index_start_current_hold_ramps = [];
                                index_start_curr_meas =[];
                                idx_begin_hold_ramps_end=[];
                            otherwise
                                error("unkown signal type for this function");
                        end
                        
                        
                        
                        
                       
                        
        end
                  
        function [idx_begin_R_meas_start,idx_end_R_meas_start] =  get_meas_idx(obj,idx_search_start,idx_search_end)
        % ----------------- get start and end of the ramps for integration -
                        % THIS IMPLEMENTATION ONLY WORKS FOR SIGNAL STARTING A SOME
                        % POSITVE CURRENT, lowering torwards zero, apply a step for
                        % current measumrements followed by ramps, last ramp has to
                        % end at zero. Then again a step for current measurmet is
                        % apply and the current raised to the initial value!

                        % the saved index 'idx_saved_ramp marks' the region, where the ramp
                        % stars, so we search here. We look for a intersection of
                        % horizontal line with an increasing/decreasing.
                        % This is done in 'getIntersection_diag_hor()'.                      
                        daq_rate = obj.inputSignal.daq_Rate;
                        nmbr_cycles = obj.inputSignal.nmbr_cycles;

                        if idx_search_start < idx_search_end  
                            flg_signl_order_reversed = false;
                        else
                            flg_signl_order_reversed = true;
                        end

                        if ~flg_signl_order_reversed                               
                            current =  obj.current(idx_search_start:idx_search_end);
                            voltage =  obj.voltage(idx_search_start:idx_search_end);                
                        else                    
                            current =  obj.current(idx_search_start:-1:idx_search_end);
                            voltage =  obj.voltage(idx_search_start:-1:idx_search_end); 
                        end

                        nmbr_data_pnts = length(current);
                        i_meas = obj.inputSignal.i_meas_current;
                        T_heating = obj.inputSignal.T_meas_psi_sgnl_ramp*obj.inputSignal.i_heat/obj.inputSignal.i_max;
                        T_meas_res_start = obj.inputSignal.T_meas_resistance_start;
                        T_sgnl_ramp = obj.inputSignal.T_meas_psi_sgnl_ramp;

                        % now we use a filterd signal of the current change in
                        % order to find the regions where the signal changes
                        % ATTENTION: due to filters the signal is blurred and is 
                        % only sufficient to find the regions, the exact edges must
                        % then be searched for separately

                        current2 = smoothdata(current,'gaussian',round(daq_rate*0.01));
                        crr_dot = smoothdata(gradient(current2),'movmean',round(daq_rate*0.025));
                        abs_crr_dot = abs(crr_dot); 

                        mean_abs_crr_dot = mean(abs_crr_dot(abs_crr_dot>1e-6)); % we asume 1e-6 to be the thershold for noise
                        std_abs_crr_dot = std(abs_crr_dot(abs_crr_dot>1e-6));
                        mean_abs_crr_dot = mean(abs_crr_dot(and(abs_crr_dot>1e-6,abs_crr_dot<mean_abs_crr_dot+std_abs_crr_dot)));
                        std_abs_crr_dot = std(abs_crr_dot(and(abs_crr_dot>1e-6,abs_crr_dot<mean_abs_crr_dot+std_abs_crr_dot)));

                        abs_crr_dot_above_mean =  abs_crr_dot>mean_abs_crr_dot*0.5; % <---- because of filters 
                        % this id blurred so we assume everything above
                        % mean_abs_crr_dot*0.5 to be current_dot not zero! We don't
                        % need it exactly in the following
                        abs_crr_dot_above_mean = round(smoothdata(abs_crr_dot_above_mean,'movmean',50)); 
                        abs_crr_dot_steps = abs_crr_dot_above_mean(2:end)-abs_crr_dot_above_mean(1:end-1);         

                        % now get changes at beginning             
                        % first step: falling current goes to zero
                        % second step: current starts to rise to measruement current
                        % third step: current has reached  measruement current
                        % fourth step: current starts to fall to zero current
                        % fifth step: current has reached  zero  current
                        % sixth step: current starts with first ramp

                        abs_crr_dot_steps_idx = find(abs_crr_dot_steps);

                        flag_initial_zero_phase = false;
                        %due to lags we don't know, if we start with a ramp or
                        %constant current -> we check it                
                        if mean(abs_crr_dot(1:abs_crr_dot_steps_idx(1))) < mean_abs_crr_dot-std_abs_crr_dot
                            % if we start with a constant phase there will be an
                            % additional step at the beginning! The rest of the
                            % function is implemented to start with a decreasing
                            % current, therefore we remove the first step
                            abs_crr_dot_steps_idx(1) = [];
                            flag_initial_zero_phase = true;
                        end

                        % due to the filter our idx_crr_steps_start are 'behind'
                        % the real measured signal

        %                   if ~flg_signl_order_reversed    
                                [~,idx_begin_R_meas_start]=find(abs(current(abs_crr_dot_steps_idx(3):abs_crr_dot_steps_idx(4))-i_meas)<=0.0025,1,'first');
                                [~,idx_end_R_meas_start]=find(abs(current(abs_crr_dot_steps_idx(3):abs_crr_dot_steps_idx(4))-i_meas)<=0.0025,1,'last');
                                % R measure phase must be within this region
                                idx_begin_R_meas_start = idx_begin_R_meas_start +abs_crr_dot_steps_idx(3)-1;
                                idx_end_R_meas_start =  idx_end_R_meas_start +abs_crr_dot_steps_idx(3)-1;
        %                   else
        %                         [~,idx_begin_R_meas_start]=find(abs(current(abs_crr_dot_steps_idx(2):abs_crr_dot_steps_idx(3))-i_meas)<=0.0025,1,'first');
        %                         [~,idx_end_R_meas_start]=find(abs(current(abs_crr_dot_steps_idx(2):abs_crr_dot_steps_idx(3))-i_meas)<=0.0025,1,'last');
        %                         % R measure phase must be within this region
        %                         idx_begin_R_meas_start = idx_begin_R_meas_start +abs_crr_dot_steps_idx(2)-1;
        %                         idx_end_R_meas_start =  idx_end_R_meas_start +abs_crr_dot_steps_idx(2)-1;
        %                   end

         end

        
        function new_input_signal = update_const_psi_run(obj)            
            if obj.remove_offset_flag == true            
                obj.remove_offset();
            end
            
            % extract resitance begin and end
            obj.extract_resictance_n_const_psi_ramps();
            
            % Messbereich soll/ist identifizieren
            obj.calc_psi();
            %du_dt soll  - du_dt ist
            
            % store input signal for next run
            
            % idle full input signal           
            [~,idx,~,prom_crr_ref]=findpeaks(obj.inputSignal.signal_ref_const_psi);            
            idx_peaks_current_input_ref = idx(prom_crr_ref>max(prom_crr_ref)*0.1);            
            idx_peaks_current_input_ref = idx_peaks_current_input_ref(idx_peaks_current_input_ref > obj.inputSignal.T_meas_resistance_start*obj.inputSignal.daq_Rate);
            idx_peaks_current_input_ref = idx_peaks_current_input_ref(1: obj.inputSignal.nmbr_cycles);  
           
            % measured input signal
            [~,idx,~,prom_crr_ipt]=findpeaks(obj.current_input);            
            idx_peaks_current_input = idx(prom_crr_ipt>max(prom_crr_ipt)*0.1);            
            idx_peaks_current_input = idx_peaks_current_input(idx_peaks_current_input > obj.inputSignal.T_meas_resistance_start*obj.inputSignal.daq_Rate);
            idx_peaks_current_input = idx_peaks_current_input(1: obj.inputSignal.nmbr_cycles);            
            
            % measured current
            [~,idx,~,prom_crr]=findpeaks(obj.current);            
            idx_peaks_current = idx(prom_crr>max(prom_crr)*0.1);            
            idx_peaks_current = idx_peaks_current(idx_peaks_current > obj.inputSignal.T_meas_resistance_start*obj.inputSignal.daq_Rate);
            idx_peaks_current = idx_peaks_current(1: obj.inputSignal.nmbr_cycles);
            
            if  obj.inputSignal.nmbr_cycles<3
               error("This update routine is implemented for 3 or more cycles yet"); 
            end

 
%            window = round(mean(diff(idx_peaks_current)));           
%             x_start(1) = double(max(obj.current)/max(obj.inputSignal.signal_ref_const_psi));
%             x_start(2) = 0;
%             x_start(3) = -1;
%             scale_shift_current_input_ref = fmincon(@min_shift_current_input_ref,x_start);
%            function delta =  min_shift_current_input_ref(x)
%             a=x(1);
%             b=x(2);
%             c=round(x(3));
%             delta = sum(abs(double(obj.current(idx_peaks_current(2)-window:idx_peaks_current(2)+window)-...
%                         (obj.inputSignal.signal_ref_const_psi(idx_peaks_current_input_ref(2)-window-c:idx_peaks_current_input_ref(2)+window-c)'*a+b))));
%            end
%             
%             x_start(1) = double(max(obj.current)/max(obj.current_input));
%             x_start(2) = 0;
%             x_start(3) = -1;
%             scale_shift_current_input = fmincon(@min_shift_current_input,x_start);
%            function delta =  min_shift_current_input(x)
%             a=x(1);
%             b=x(2);
%             c=round(x(3));
%             delta = sum(abs(double(obj.current(idx_peaks_current(2)-window:idx_peaks_current(2)+window)-...
%                         (obj.current_input(idx_peaks_current_input(2)-window-c:idx_peaks_current_input(2)+window-c)*a+b))));
%            end 
%            figure
%            plot(obj.inputSignal.signal_ref_const_psi(idx_peaks_current_input_ref(1)-scale_shift_current_input_ref(3):idx_peaks_current_input_ref(3)-scale_shift_current_input_ref(3))*...
%             scale_shift_current_input_ref(1)+scale_shift_current_input_ref(2));   
%            hold on;
%            plot(obj.current_input(idx_peaks_current_input(1)-scale_shift_current_input(3):idx_peaks_current_input(3)-scale_shift_current_input(3))*...
%             scale_shift_current_input(1)+scale_shift_current_input(2));   
%            hold on;           
%            plot(obj.current(idx_peaks_current(1):idx_peaks_current(3)));
            
%             delta_volt_in = obj.inputSignal.signal_ref_voltage_ind_const_psi(1:min_idx)'-obj.voltage_ind(1:min_idx);
           
            % we try to find the sections with differing induced voltage in
            % the signal in order to detect a missalignment between desired
            % input signal an measured output signal.
            
            min_idx = min([length(obj.inputSignal.signal_ref_voltage_ind_const_psi'),length(obj.voltage_ind)]);
%             obj.voltage_ind(1:min_idx); 
            delta = diff(obj.voltage_ind(1:min_idx));
            min_hight = max(obj.inputSignal.signal_ref_voltage_ind_const_psi*0.3);
            
            [~,idx_max,~,~] =findpeaks(delta,'MinPeakHeight',min_hight);
            [~,idx_min,~,~] =findpeaks(-delta,'MinPeakHeight',min_hight);
            idx_peaks = sort([idx_max,idx_min]);
              
            % corrent first estimation
            for i = 1:length(idx_peaks)
                idx_peaks(i) = find_step(obj,idx_peaks(i));
            end
            idx_peak_set_input = find(diff(obj.inputSignal.signal_ref_voltage_ind_const_psi)~=0);
            
            
            
            if ~isequal(idx_peaks,idx_peak_set_input')
                warning("Updating the input signal, there was a missalignment  between input signal and measured sigal!");
            end
%             figure
%             plot(obj.voltage_ind)
%             hold on
%             scatter(idx_peaks,obj.voltage_ind(idx_peaks))
            % because the powersupply needs certain time to invert the
            % voltage we use weights. The error for the inital values will
            % always be large due to the power supply's limitations an the
            % corretion algorithm will result in unreasonable input
            % currents after some iteration loops
            
%%
%             new_signal = zeros(1,idx_peaks(3));
%             for i = 1:3
%              
%                                 
%                 % we apply a svitzky-golay filter to the signal because
%                 % this won't add a delay to the signal an smooth out high
%                 % frequent oscillation
%                 
%                 if i==1
%                     here = 1: idx_peaks(i);                    
%                 else
%                     here = idx_peaks(i-1)+1: idx_peaks(i);
%                 end
%                 
%                 delta_volt_in = obj.inputSignal.signal_ref_voltage_ind_const_psi(here)' - obj.voltage_ind(here);
%                 
%                 delta_volt_in = double(delta_volt_in);
%                 order = 3;
%                 framelen = 165;
%                 delta_volt_in = sgolayfilt(delta_volt_in,order,framelen); 
% 
%                 kp = 0.1;
%                 
%                     if i==1
%                         new_signal(here) = obj.inputSignal.signal_ref_const_psi(here)'+delta_volt_in*kp;
%                     else
% %                         weights = ones(size(delta_volt_in));
% %                         T = 40; %<----- adjust this value to change the sigmoids shape!!
% %                         c = 1/T;
% %                         f = @(x) 2./(1+exp(-c.*(x-T)));
% %                         weights(1:T)=f(1:T);
% 
% 
%                         new_signal(here) = obj.inputSignal.signal_ref_const_psi(here)';
% %                         new_signal(here(1):here(1)+T) =  new_signal(here(1)-1:-1:here(1)-1-T);
%                         new_signal(here) = new_signal(here)+delta_volt_in*kp ;%.*weights;
%                     end
%             end
%                 plot(new_signal)
%%
%             new_signal = zeros(1,idx_peaks(6));
%             for i = 2:3             
%                                 
%                 % we apply a svitzky-golay filter to the signal because
%                 % this won't add a delay to the signal an smooth out high
%                 % frequent oscillation    
%                 here = idx_peaks(i-1)+1: idx_peaks(i);    
%                 delta_volt_in = obj.inputSignal.signal_ref_voltage_ind_const_psi(here)' - obj.voltage_ind(here);                
%                 delta_volt_in = double(delta_volt_in);
% %                 order = 3;
% %                 framelen = 165;
% %                 delta_volt_in = sgolayfilt(delta_volt_in,order,framelen); 
%                  kp = 0.075;
%                 
%                delta_volt_in= smoothdata(delta_volt_in,'gaussian');
% %                plot(delta_volt_in)
% 
% %                         weights = ones(size(delta_volt_in));
% %                         T = 40; %<----- adjust this value to change the sigmoids shape!!
% %                         c = 1/T;
% %                         f = @(x) 2./(1+exp(-c.*(x-T)));
% %                         weights(1:T)=f(1:T);
% 
%                 new_signal(here) = obj.inputSignal.signal_ref_const_psi(here)';
%     %                         new_signal(here(1):here(1)+T) =  new_signal(here(1)-1:-1:here(1)-1-T);
%                 new_signal(here) = new_signal(here)+delta_volt_in*kp ;%.*weights;
%                 dT = 40;
%                 here = [idx_peaks(i-1),idx_peaks(i-1)+dT,idx_peaks(i-1)+dT+1,idx_peaks(i-1)+dT+2];
%                 here_now = idx_peaks(i-1)+1:idx_peaks(i-1)+dT;
%                 new_signal(here_now) = interp1(here,new_signal(here),here_now,'spline');
%             end


%                 new_signal = zeros(1,idx_peaks(6));
                
                % we apply a svitzky-golay filter to the signal because
                % this won't add a delay to the signal an smooth out high
                % frequent oscillation    
                
                shift = round(mean(idx_peaks_current_input_ref-idx_peaks_current'));
                
%                 idx_peaks_current(1)
%
                here = [idx_peaks_current_input_ref(1)+1:idx_peaks_current_input_ref(1)+4000]-shift - obj.index_ramps(1);
                
                delta_volt_in = obj.voltage_ind(here) - ones(size(obj.voltage_ind(here)))*min(obj.inputSignal.signal_ref_voltage_ind_const_psi(here)');
                
                here = [idx_peaks_current_input_ref(1)+4001:idx_peaks_current_input_ref(2)]- shift  - obj.index_ramps(1);
                
                delta_volt_in = [delta_volt_in , obj.voltage_ind(here) - ...
                    ones(size(obj.voltage_ind(here)))*max(obj.inputSignal.signal_ref_voltage_ind_const_psi(here)')];
                
                
%                 delta_volt_in = obj.inputSignal.signal_ref_voltage_ind_const_psi(here)' - obj.voltage_ind(here);                
%                 delta_volt_in = double(delta_volt_in);
%                 order = 3;
%                 framelen = 165;
%                 delta_volt_in = sgolayfilt(delta_volt_in,order,framelen); 

               kp = 0.025;
                
               delta_volt_in = smoothdata(delta_volt_in,'gaussian',300);
               
               
%                plot(delta_volt_in)

%                         weights = ones(size(delta_volt_in));
%                         T = 40; %<----- adjust this value to change the sigmoids shape!!
%                         c = 1/T;
%                         f = @(x) 2./(1+exp(-c.*(x-T)));
%                         weights(1:T)=f(1:T);

                here = idx_peaks_current(1)+1:idx_peaks_current(2);   

                new_signal = obj.inputSignal.signal_ref_const_psi(idx_peaks_current_input_ref(1)+1:idx_peaks_current_input_ref(2))';
%               new_signal(here(1):here(1)+T) =  new_signal(here(1)-1:-1:here(1)-1-T);
                new_signal = new_signal-delta_volt_in*kp ;%.*weights;
                
                new_signal_complete = repmat(new_signal,1,3);
                new_signal_complete =  [flip(new_signal_complete(1:2000)),new_signal_complete];
                
%                 dT = 40;
%                 i=2
%                 here = [idx_peaks(i-1),idx_peaks(i-1)+dT,idx_peaks(i-1)+dT+1,idx_peaks(i-1)+dT+2];
%                 here_now = idx_peaks(i-1)+1:idx_peaks(i-1)+dT;
%                 new_signal(here_now) = interp1(here,new_signal(here),here_now,'spline');
%                 new_signal_complete = repmat(new_signal(here_now),1,3);
%                 new_signal_complete =  [flip(new_signal_complete(1:2000)),new_signal_complete];
%             end


%             
%             new_signal_loop = new_signal(idx_peaks(3)+1:idx_peaks(5));
%             
%             new_signal_complete = repmat(new_signal_loop,1,3);
%             new_signal_complete =  [flip(new_signal_complete(1:2000)),new_signal_complete];

             new_signal_complete(1:100) = interp1([1,100],[0,new_signal_complete(100)],1:100,'spline');

            new_input_signal = obj.inputSignal.addCurrentMeasurement(new_signal_complete);
            
              obj.inputSignal.i_max =new_signal_complete(end);
            
    %%        
            % correct each section 1 to 3
            
            
            
             
            % we repeat the sequence for section 2 and 3 
            
            
            
            
            % for the first part we man the current to ramp up from zero so
            % the correct the signal
            
            
        
    end
 
        function idx_step = find_step(obj,idx_peak)
                span = 50;
                v_ind_loc = obj.voltage_ind(idx_peak-span:idx_peak);
                idx_interp = 1:1:length(v_ind_loc);

                [xData, yData] = prepareCurveData(double(idx_interp),double(v_ind_loc));
                % Set up fittype and options.
                ft = fittype( 'poly1' );
                opts = fitoptions( 'Method', 'LinearLeastSquares' );
                opts.Robust = 'Bisquare';
                % Fit model to data.
                [fitresult, ~] = fit( xData, yData, ft, opts );
                y_fit = feval(fitresult,idx_interp);            
                delta_fit = v_ind_loc-y_fit';            
                idx_fnd=find(abs(delta_fit)<=mean(abs(delta_fit)),1,'last');            
%                 v_ind_loc-feval(fitresult,idx)+mean(abs(v_ind_loc-y_fit'));                         
                idx_step = idx_peak-(span+1)+idx_fnd;
        end
         
        function obj = loadData(obj,daq_time,daq_data,serial_time,serial_data)
            % this function is for lazy people applying the mapping. You can
            % also pass the data to the properties step by step yourself
            if ~isequal(class(obj),'raw_flux_curve_measurement')
               error('First input must be the object itself and must be of type raw_flux_curve_measurement'); 
            end
            % check dimensions of the input
            if size(daq_data,1)<2
                error("The object daq_data passed has less than " + num2str(size(obj.mcc_wiring,1)) +" channels, inside mcc_wiring there are more declared"); 
            end        
            if size(daq_time,1)>1
                error("The object daq_time passed has more than one channel"); 
            end
            if size(serial_time,1)>1
                error("The object serial_time passed has more than one channel"); 
            end
            if size(serial_data,1)>1
                error("The object serial_data passed has more than one channel"); 
            end        
            % copy the data
            for i = 1:size(obj.mcc_wiring,1)            
                try 
                    obj.(obj.mcc_wiring{i,1}) = daq_data(obj.mcc_wiring{i,2},:);
                catch
                    error(['The object ', obj.mcc_wiring{i,1}, 'does not exist in this object']) 
                end
            end
             obj.time = daq_time;
             obj.time_position = serial_time;
             obj.position = serial_data; 
        end   
    
        function       plot_output_vs_input(obj)
            
            signalData_unscaled = obj.inputSignal.gen_InputSignal;
            figure            
            plot(signalData_unscaled(obj.inputSignal.idx_signal_start:end));
            hold on;
            plot(obj.current(1,obj.index_ramps(1):end));
            legend('signal','meas');  
            grid on
            
        end 
        
    end % end Methods(Access = public)    
    
    methods  (Access = private)
        
        function [i_plt,h_axes]= plot_results_parse(obj,args_passed)
            if size(args_passed,2)>4
                error("too many input arguments");
            end            
            nargin =length(args_passed);               
            if nargin>0 && isnumeric(args_passed{end})              
                
                if length(args_passed{end})==1
                    i_plt  = args_passed{end};
                else                
                    i_plt  = min([args_passed{end}]):max([args_passed{end}]);    
                    if i_plt(1)<1
                        i_plt(1) = 1;
                    end                
                    if i_plt(2)>size(obj.results,2)
                       i_plt(2) = size(obj.results,2);
                    end
                end
                
            else
                i_plt = 1:size(obj.results,2);
            end 
            
            if nargin>2 && isa(args_passed{2},'matlab.graphics.axis.Axes')
               h_axes = args_passed{2};
                % plot2externalFig(varargin{2});   
            else
               h_axes = [];
                %plot2internalFig();
            end
        end
       
       
        function obj = evaluate_psi_sin(obj)

            % here find peaks is used
            [max_psi,idx_max_psi,~,prom_max_psi] =  findpeaks(obj.psi,'MinPeakDistance',round(obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*4*0.9));
            [min_psi,idx_min_psi,~,prom_min_psi] =  findpeaks(-obj.psi,'MinPeakDistance',round(obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*4*0.9));
             min_psi = -min_psi;
            [max_curr,idx_max_current,~,prom_max_current] = findpeaks(obj.current(obj.index_ramps),'MinPeakDistance',round(obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*4*0.9));
            [min_curr,idx_min_current,~,prom_min_current] = findpeaks(-obj.current(obj.index_ramps),'MinPeakDistance',round(obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*4*0.9));
            
            
            %#########################################
            %######### ADDED PROMINENCE TO FIND OUTLINERS
            %###########################################
            %# NOT TESTED MUCH!!!!            
            max_psi_idx_remove = prom_max_psi/mean(prom_max_psi)<0.1;
            max_psi(max_psi_idx_remove)=[];
            idx_max_psi(max_psi_idx_remove)=[];
            min_psi_idx_remove =prom_min_psi/mean(prom_min_psi)<0.1;
            min_psi(min_psi_idx_remove)=[];
            idx_min_psi(min_psi_idx_remove)=[];
            max_curr_idx_remove = prom_max_current/mean(prom_max_current)<0.1;
            max_curr(max_curr_idx_remove)=[];
            idx_max_current(max_curr_idx_remove)=[];
            min_curr_idx_remove = prom_min_current/mean(prom_min_current)<0.1;
            min_curr(min_curr_idx_remove)=[];
            idx_min_current(min_curr_idx_remove)=[];
            
            
            % somtimes the slope is deteced aswell, then the last max current is excluded
            if length(idx_max_psi)>length(idx_min_psi) % the min index is used for detection because it is always right
                idx_max_current(end) = [];
            end
            
            if length(idx_max_current)>length(idx_min_current) % the min index is used for detection because it is always right
                idx_max_current(end) = [];
            end
            min_curr = -min_curr;

            if ( mean(length(idx_max_psi),length(idx_min_psi)) ~= obj.inputSignal.nmbr_cycles  ) || (mean(length(idx_max_current),length(idx_min_current)) ~= obj.inputSignal.nmbr_cycles  ) 
                error("get_point_max_psi_n_offset: the number of detected closed loops differs from" + num2str(obj.inputSignal.nmbr_cycles));            
            end
            

            if obj.compensate_drift_flag ==true                
                % ADD CHECK FOR SUFFICIENT LOOPS HERE
                % calculation of drift's mean and std only for more than 2 full loop possible                    
                psi_at_max_current = obj.psi(idx_max_current);
                psi_at_min_current = obj.psi(idx_min_current);                
                psi_drift_max2max_mean = mean(diff(psi_at_max_current));
                psi_drift_min2min_mean = mean(diff(psi_at_min_current));
                psi_drift_mean = (psi_drift_max2max_mean+psi_drift_min2min_mean)/2;
                psi_drift_mean_per_step = psi_drift_mean/(mean(diff(idx_max_current)));
                psi_drift_max2max_std = std(diff(psi_at_max_current));
                psi_drift_min2min_std = std(psi_at_min_current);
                max_psi_drift_compensated = psi_at_max_current - psi_drift_mean*[0:length(psi_at_max_current)-1] ; 
                min_psi_drift_compensated = psi_at_min_current - psi_drift_mean*[0.5:length(psi_at_min_current)-0.5] ; 

                psi_offset =  mean(max_psi_drift_compensated + min_psi_drift_compensated)/2;
            end
                     
            psi_idx_offset = obj.index_ramps(1)-1; % needed because obj.psi and obj.current differ in size
            sum_idx_drift = 0;
        
            for i = 1:length(idx_max_current)-1
                
             %create new empty flux_curve object                
             obj.results(i).flux_curve =flux_curve(1/obj.inputSignal.daq_Rate,[],[]);
             
             %create handle
             h_this_curve = obj.results(i).flux_curve;             
%              h_this_curve.psi_decrease = obj.psi(idx_max_current(i):idx_min_current(i));
%              h_this_curve.voltage_ind_decrease = obj.voltage_ind(idx_max_current(i):idx_min_current(i));
             psi_decrease_tmp = obj.psi(idx_max_current(i):idx_min_current(i));
%              voltage_ind_decrease_tmp = obj.current(psi_idx_offset + (idx_max_current(i):idx_min_current(i)));
             voltage_ind_decrease_tmp = obj.voltage_ind(idx_max_current(i):idx_min_current(i));
             if obj.compensate_drift_flag ==true
                %add the drift
%              h_this_curve.psi_decrease = h_this_curve.psi_decrease -psi_offset  -(sum_idx_drift + [1:(idx_min_current(i)-idx_max_current(i)+1)])*psi_drift_mean_per_step;              
                psi_decrease_tmp = psi_decrease_tmp -psi_offset  -(sum_idx_drift + [1:(idx_min_current(i)-idx_max_current(i)+1)])*psi_drift_mean_per_step;                
                  sum_idx_dirft_last_curve_decrease = (idx_min_current(i)-idx_max_current(i)+1);
             end
              % now set it to the object
              h_this_curve.psi_decrease = psi_decrease_tmp;
              h_this_curve.voltage_ind_decrease = voltage_ind_decrease_tmp;
              h_this_curve.current_decrease = obj.current(psi_idx_offset + (idx_max_current(i):idx_min_current(i)));
              %###
              
             if i <=length(idx_max_current)-1
%              	  h_this_curve.psi_increase  = obj.psi(idx_min_current(i):idx_max_current(i+1));
%                 h_this_curve.current_increase = obj.current(psi_idx_offset + (idx_min_current(i):idx_max_current(i+1)));
%                 h_this_curve.voltage_ind_increase = obj.voltage_ind((idx_min_current(i):idx_max_current(i+1))); 
                psi_increase_tmp  = obj.psi(idx_min_current(i):idx_max_current(i+1));
                current_increase_tmp = obj.current(psi_idx_offset + (idx_min_current(i):idx_max_current(i+1)));
                voltage_ind_increase_tmp = obj.voltage_ind((idx_min_current(i):idx_max_current(i+1))); 
                
                if obj.compensate_drift_flag ==true
                    %add the drift
%                     h_this_curve.psi_increase = h_this_curve.psi_increase -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease ...
%                     + [1:(idx_max_current(i+1)-idx_min_current(i)+1)])*psi_drift_mean_per_step;  
                    psi_increase_tmp = psi_increase_tmp -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease ...
                    + [1:(idx_max_current(i+1)-idx_min_current(i)+1)])*psi_drift_mean_per_step;  
                 end
             else
%                 h_this_curve.psi_increase = obj.psi(idx_min_current(i):end);
%                 h_this_curve.current_increase = obj.current(psi_idx_offset + (idx_min_current(i):length(obj.current)));
%                 h_this_curve.voltage_ind_increase = obj.voltage_ind((idx_min_current(i):length(obj.current)));
                psi_increase_tmp = obj.psi(idx_min_current(i):end);
                voltage_ind_increase_tmp = obj.voltage_ind((idx_min_current(i):length(obj.current)));
                current_increase_tmp = obj.current(psi_idx_offset + (idx_min_current(i):length(obj.current)));
                                
                 if obj.compensate_drift_flag ==true
                    %add the drift
%                     h_this_curve.psi_increase = h_this_curve.psi_increase -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease + ...
%                     [1:(length(obj.psi)-idx_min_current(i)+1)])*psi_drift_mean_per_step;
                    psi_increase_tmp = psi_increase_tmp -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease + ...
                    [1:(length(obj.psi)-idx_min_current(i)+1)])*psi_drift_mean_per_step;
                 end
             end
             
              h_this_curve.psi_increase = psi_increase_tmp;
              h_this_curve.voltage_ind_increase = voltage_ind_increase_tmp;
              h_this_curve.current_increase = current_increase_tmp;
              
             
             
             
             
                % we calculate it each loop even if just required when
                % drift is compensated for
                sum_idx_drift = idx_max_current(i+1) - idx_max_current(1);
                
%                 obj.results(i).flux_curve.calc_mean_flux_curve;
%                 obj.results(i).flux_curve.calc_dissp_current;
            end
        
                    if 1==0
                    % only used for debuging
                    for j=1
                    clf
                        sum_idx_drift = 0;
                        legend_txt= {};
                        for i = 1:length(idx_max_current)-1
                            this_pl_psi = obj.psi(idx_max_current(i):idx_max_current(i+1)); 
                            this_pl_psi = this_pl_psi-(sum_idx_drift + [1:(idx_max_current(i+1)-idx_max_current(i)+1)])*psi_drift_mean_per_step;
                            plot(obj.current(psi_idx_offset + (idx_max_current(i):idx_max_current(i+1))),-psi_offset+this_pl_psi )
                            hold on; 
                            sum_idx_drift = idx_max_current(i+1) - idx_max_current(1);
                            legend_txt{i}="Loop " + num2str(i);
                        end
                        legend(legend_txt)
                        grid on
                    end  
                    end  
           
 
        end
        
        function obj = extract_resictance_n_sinwaves(obj)   
                % ----------------- get start and end of the ramps for integration -
                % get the index where the ramp starts
                % the saved index 'idx_saved_ramp marks' the region, where the ramp
                % stars, so we search here. We look for a intersection of
                % horizontal line with an increasing/decreasing.
                % This is done in 'getIntersection_diag_hor()'.                      

                [~,idx_i_meas_start_begins]=find(abs(obj.current-obj.inputSignal.i_meas_current)<=0.0025,1,'first');
                idx_begin_R_meas_start =idx_i_meas_start_begins;

                [~,idx_end_curr_meas]=find(obj.current(idx_i_meas_start_begins+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*0.5:idx_i_meas_start_begins+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*1.5)-obj.inputSignal.i_meas_current*0.5< 0 ,1,'first');

                idx_end_curr_meas = idx_i_meas_start_begins+idx_end_curr_meas+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*0.5;

                
                
                idx_hor_start = idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.3;
                idx_hor_end = idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.8;             

                idx_search_start = round(idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.5);
                idx_search_end = round(idx_search_start+ obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start+obj.inputSignal.T_meas_psi_sgnl_ramp));

                [~,idx_diag_start]=find(abs(obj.current(idx_search_start:idx_search_end)-obj.inputSignal.i_max*0.2)<=0.01,1,'first');
                idx_diag_start = round(idx_diag_start+idx_search_start);
                idx_diag_end    =  round(idx_diag_start+obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.3));

                % use curve fitting to find the intersection
                idx_sin_start = obj.getIntersection_diag_horSin(idx_end_curr_meas,idx_diag_start,obj.current(idx_diag_start:idx_diag_end),obj.current(idx_hor_start:idx_hor_end));

                % To find the end of the ramp we try to find the last value closed i max.
                % first geuss for the end

                idx_int_end =  round(idx_sin_start+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*(1+4*obj.inputSignal.nmbr_cycles));            
                idx_end_R_meas_end = idx_int_end + find(obj.current(idx_int_end:end)>obj.inputSignal.i_max*0.98,1,'last');

                idx_hor_end_start = round(idx_end_R_meas_end-obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end*0.8);
                idx_hor_end_end     = round(idx_end_R_meas_end-obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end*0.2);            

                [~,idx_sin_end_est]=find(abs(obj.current(idx_sin_start:idx_hor_end_start)-obj.inputSignal.i_max)>=0.01,1,'last');
                idx_sin_end_est = idx_sin_start + idx_sin_end_est;

                idx_diag_end_start  = round(idx_sin_end_est-obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.7));
                idx_diag_end_end    = round(idx_sin_end_est-obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.2));                          

                % use curve fitting to find the intersection        
                idx_ramp_end = obj.getIntersection_diag_horSin(idx_hor_end_start,idx_diag_end_start,obj.current(idx_diag_end_start:idx_diag_end_end),obj.current(idx_hor_end_start:idx_hor_end_end));

                obj.index_ramps = idx_sin_start:idx_ramp_end;    

                % ----------------- get initial offset and noise from data --------       
                this_offset = [mean(obj.current(round(1:idx_begin_R_meas_start*0.9))); ...
                               mean(obj.voltage(round(1:idx_begin_R_meas_start*0.9)))] ;

                % ----------------- get resictance from data ----------------------
                % calculate Resitance at beginning
                ixd_step_meas_R_start = round(idx_begin_R_meas_start:idx_begin_R_meas_start+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start);

                % calculate Resitance at and
                ixd_step_meas_R_end = round(idx_end_R_meas_end - obj.inputSignal.T_meas_resistanc_end*obj.inputSignal.daq_Rate:idx_end_R_meas_end);

                [obj.R_valve_start,std_out] = obj.calc_R_from_Step('start',obj.current(ixd_step_meas_R_start),obj.voltage(ixd_step_meas_R_start),[0;0]);
                [obj.R_valve_end,std_out_end] = obj.calc_R_from_plateau_v2(obj.current(ixd_step_meas_R_end),obj.voltage(ixd_step_meas_R_end),[0;0]); 
    
        end
        
        function obj = extract_resictance_n_periodical(obj)   
                % ----------------- get start and end of the ramps for integration -
                % get the index where the ramp starts
                % the saved index 'idx_saved_ramp marks' the region, where the ramp
                % starts, so we search here. We look for a intersection of
                % horizontal line with an increasing/decreasing.
                % This is done in 'getIntersection()'.                      

                % Variablen definieren für Anzahl Messpunkte in den
                % Zeiträumen der einzelnen Messungsabschnitte
                T_meas_resist_start = obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start;
                T_meas_current_high_start = T_meas_resist_start*obj.inputSignal.ratio_T_meas_current_high_start;
                T_break_start = T_meas_resist_start*obj.inputSignal.ratio_T_break_start;
                T_meas_psi_sgnl_ramp = obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp;
                T_meas_resist_end = obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end;
                
                
                % finding index to start the measurement cycle at the first
                % step in current measurement
                [~,idx_i_meas_start_begins]=find(abs(obj.current-obj.inputSignal.i_meas_current)<=0.0025,1,'first');
                idx_begin_R_meas_start =idx_i_meas_start_begins;
                
                
                % Auffinden der abfallenden Flanke der statischen
                % Strommessung zu Beginn des Messzyklus
                % Durchführung in zwei Schritten: 1.Suchbereich eingrenzen
                % durch Betrachtung eines kleineren Wertebereichs und
                % Mittelpunkt der Flanke ermitteln, 2.auf den Beginn des
                % Wertebereichs aufaddieren um absoluten Messpunkt zu
                % erhalten
                rel_curr = obj.current(idx_i_meas_start_begins+T_meas_current_high_start*0.5:idx_i_meas_start_begins+T_meas_current_high_start*1.5);
                [~,rel_idx_end_curr_meas]=find(rel_curr-obj.inputSignal.i_meas_current*0.5< 0 ,1,'first');

                idx_end_curr_meas = idx_i_meas_start_begins+rel_idx_end_curr_meas+T_meas_current_high_start*0.5;

                
                % horizontale Linie hinter der ersten stationären 
                % Strommessung definieren um Kreuzung mit der ersten Rampe 
                % zu finden
                idx_hor_start = idx_end_curr_meas + T_break_start*0.3;
                idx_hor_end = idx_end_curr_meas + T_break_start*0.8;             
                
                % Start und Ende des Bereichs zur Suche nach der Schrägen der
                % ersten Rampe definieren
                idx_search_start = round(idx_end_curr_meas + T_break_start*0.5);
                idx_search_end = round(idx_search_start+ T_break_start+T_meas_psi_sgnl_ramp);
                
                % Beginn der Rampe ermittteln, auf Absolutwerte
                % aufaddieren, Endpunkt der betrachteten Rampe bestimmen
                [~,idx_diag_start]=find(abs(obj.current(idx_search_start:idx_search_end)-obj.inputSignal.i_max*0.2)<=0.01,1,'first');
                idx_diag_start = round(idx_diag_start+idx_search_start);
                idx_diag_end    =  round(idx_diag_start+T_meas_psi_sgnl_ramp*0.3);

                
                
                % use curve fitting to find the intersection
                % use switch/case to define the curve fitting/intersection
                % method required depending on the type of periodical
                % signal
                switch(obj.inputSignal.Type)
                
                case 'ramps'
                    % use curve fitting to find the intersection
                    idx_period_start = obj.getIntersection_diag_hor(idx_diag_start,obj.current(idx_diag_start:idx_diag_end),obj.current(idx_hor_start:idx_hor_end));

                    % To find the end of the ramp we try to find the last value closed i max.
                    % first geuss for the end
                    % Index zur approximation der Rampe am Ende des Messzyklus
                    idx_int_end =  round(idx_period_start+T_meas_psi_sgnl_ramp*(1+4*(obj.inputSignal.nmbr_cycles-1)));            
                case 'sin'
                    % use curve fitting to find the intersection
                    idx_period_start = obj.getIntersection_diag_horSin(idx_end_curr_meas,idx_diag_start,obj.current(idx_diag_start:idx_diag_end),obj.current(idx_hor_start:idx_hor_end));

                    % To find the end of the ramp we try to find the last value closed i max.
                    % first guess for the end
                    % Index zur approximation der Rampe am Ende des Messzyklus
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %Bitte mit sin Daten überprüfen ob auch hier eine -1 in
                    %die Klammer muss, sonst besteht hier großes Fehler
                    %Potenzial!!!!!!!!!!!!!!!!!!!!!
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    idx_int_end =  round(idx_period_start+T_meas_psi_sgnl_ramp*(1+4*obj.inputSignal.nmbr_cycles)); 
                case 'ramps_crr_meas'
                    % identical to case 'ramps'
                    idx_period_start = obj.getIntersection_diag_hor(idx_diag_start,obj.current(idx_diag_start:idx_diag_end),obj.current(idx_hor_start:idx_hor_end));
                    idx_int_end =  round(idx_period_start+T_meas_psi_sgnl_ramp*(1+4*(obj.inputSignal.nmbr_cycles-1)));   
                otherwise
                    error("For the signal type of this measurement no evaluation is implemented yet");
                end
                                
                % Index am Ende des gesamten Messzyklus
                idx_end_R_meas_end = idx_int_end + find(obj.current(idx_int_end:end)>obj.inputSignal.i_max*0.98,1,'last');
                
                % Startpunkt Horizontale des Endplateaus zur Bestimmung der
                % letzten Rampe
                idx_hor_end_start = round(idx_end_R_meas_end-T_meas_resist_end*0.8);
                % Endpunkt der Horizontalen
                idx_hor_end_end     = round(idx_end_R_meas_end-T_meas_resist_end*0.2);            
                
                % estimated relative index to find end of periodical
                % section
                [~,idx_period_end_rel_est]=find(abs(obj.current(idx_period_start:idx_hor_end_start)-obj.inputSignal.i_max)>=0.01,1,'last');
                idx_period_end_est = idx_period_start + idx_period_end_rel_est;
                
                % index of diagonal at the end of periodical section 
                idx_diag_end_start  = round(idx_period_end_est-T_meas_psi_sgnl_ramp*0.7);
                idx_diag_end_end    = round(idx_period_end_est-T_meas_psi_sgnl_ramp*0.2);                          

                
                switch(obj.inputSignal.Type)
                
                case 'ramps'
                    % use curve fitting to find the intersection        
                    idx_period_end = obj.getIntersection_diag_hor(idx_diag_end_start,obj.current(idx_diag_end_start:idx_diag_end_end),obj.current(idx_hor_end_start:idx_hor_end_end));
                case 'sin'
                    % use curve fitting to find the intersection        
                    idx_period_end = obj.getIntersection_diag_horSin(idx_hor_end_start,idx_diag_end_start,obj.current(idx_diag_end_start:idx_diag_end_end),obj.current(idx_hor_end_start:idx_hor_end_end));
                case 'ramps_crr_meas'
                    idx_period_end = obj.getIntersection_diag_hor(idx_diag_end_start,obj.current(idx_diag_end_start:idx_diag_end_end),obj.current(idx_hor_end_start:idx_hor_end_end));
                otherwise
                    error("For the signal type of this measurement no evaluation is implemented yet");
                end
                
                
                obj.index_ramps = idx_period_start:idx_period_end;    

                
                % ----------------- get initial offset and noise from data --------       
                this_offset = [mean(obj.current(round(1:idx_begin_R_meas_start*0.9))); ...
                               mean(obj.voltage(round(1:idx_begin_R_meas_start*0.9)))] ;

                % ----------------- get resictance from data ----------------------
                % calculate Resitance at beginning
                ixd_step_meas_R_start = round(idx_begin_R_meas_start:idx_begin_R_meas_start+T_meas_current_high_start);

                % calculate Resitance at and
                ixd_step_meas_R_end = round(idx_end_R_meas_end - T_meas_resist_end:idx_end_R_meas_end);

                [obj.R_valve_start,std_out] = obj.calc_R_from_Step('start',obj.current(ixd_step_meas_R_start),obj.voltage(ixd_step_meas_R_start),[0;0]);
                [obj.R_valve_end,std_out_end] = obj.calc_R_from_plateau_v2(obj.current(ixd_step_meas_R_end),obj.voltage(ixd_step_meas_R_end),[0;0]); 
    
        end
    
        function obj = extract_resictance_n_ramps_heated(obj)   
            % ----------------- get start and end of the ramps for integration -
            % THIS IMPLEMENTATION ONLY WORKS FOR SIGNAL STARTING A SOME
            % POSITVE CURRENT, lowering torwards zero, apply a step for
            % current measurements followed by ramps, last ramp has to
            % end at zero. Then again a step for current measurement is
            % apply and the current raised to the initial value!

            [obj,idxs_R_start,idxs_R_end,abs_crr_dot_steps_idx] = obj.extract_idxs();
            current =  obj.current;
            voltage =  obj.voltage;
            
            
%             range_idx_check = round(length(obj.current)*0.75);  
% 
%             [idx_begin_R_meas_start,idx_end_R_meas_start,idx_begin_hold_ramps_start,idx_end_hold_ramps_start,idx_ramps_start,idx_diag_intersecction_start,index_begin_current_hold_ramps_start,idx_start_curr_meas_begin] ...
%             =  obj.get_meas_idx_with_ramps(1,range_idx_check);
% 
%             nmbr_meas_pnts = size(obj.current,2);
% 
%             i_heat = obj.inputSignal.i_max/sqrt(3);                    
%             [~,idx]=find(abs(obj.current-i_heat)>0.001,1,'last');
% 
% 
%             range_idx_check = (nmbr_meas_pnts-idx)+range_idx_check;
%             if range_idx_check>nmbr_meas_pnts
%                 range_idx_check = nmbr_meas_pnts-1;
%             end
% 
%             [idx_begin_R_meas_end_tmp,idx_end_R_meas_end_tmp,idx_end_hold_ramps_end,idx_begin_hold_ramps_end,idx_ramps_end,idx_diag_intersecction_end,index_begin_current_hold_ramps_end,idx_start_curr_meas_end] ...
%                 =  obj.get_meas_idx_with_ramps(nmbr_meas_pnts,nmbr_meas_pnts-range_idx_check);
% 
%             idx_end_R_meas_end = nmbr_meas_pnts-idx_begin_R_meas_end_tmp;
%             idx_begin_R_meas_end = nmbr_meas_pnts-idx_end_R_meas_end_tmp;
%             idx_ramps_end = nmbr_meas_pnts-idx_ramps_end;
%             idx_diag_intersecction_end = nmbr_meas_pnts-idx_diag_intersecction_end;
%             index_begin_current_hold_ramps_end = nmbr_meas_pnts-index_begin_current_hold_ramps_end;
%             idx_start_curr_meas_end = nmbr_meas_pnts-idx_start_curr_meas_end;
% 
% 
% 
%             % the saved index 'idx_saved_ramp marks' the region, where the ramp
%             % stars, so we search here. We look for a intersection of
%             % horizontal line with an increasing/decreasing.
%             % This is done in 'getIntersection_diag_hor()'.                      
%             daq_rate = obj.inputSignal.daq_Rate;
%             nmbr_cycles = obj.inputSignal.nmbr_cycles;
%            
%             nmbr_data_pnts = length(current);
%             i_meas = obj.inputSignal.i_meas_current;
%             T_heating = obj.inputSignal.T_meas_psi_sgnl_ramp/sqrt(3);
% %             T_heating = obj.inputSignal.T_meas_psi_sgnl_ramp*obj.inputSignal.i_heat/obj.inputSignal.i_max;
%             T_meas_res_start = obj.inputSignal.T_meas_resistance_start;
%             T_sgnl_ramp = obj.inputSignal.T_meas_psi_sgnl_ramp;
% 
%             % now we use a filterd signal of the current change in
%             % order to find the regions where the signal changes
%             % ATTENTION: due to filters the signal is blurred and is 
%             % only sufficient to find the regions, the exact edges must
%             % then be searched for separately
% 
%             current2 = smoothdata(current,'gaussian',round(daq_rate*0.001)); % #gm set from 0.01 to 0.001
%             crr_dot = smoothdata(gradient(current2),'movmean',round(daq_rate*0.025));
%             abs_crr_dot = abs(crr_dot); 
% 
%             mean_abs_crr_dot = mean(abs_crr_dot(abs_crr_dot>3e-6)); %  #gm  set from 1e-6 to 3e-6 we asume 1e-6 to be the threshold for noise                
%             abs_crr_dot_above_mean =  abs_crr_dot>mean_abs_crr_dot*0.5; % <---- because of filters 
%             % this id blurred so we assume everything above
%             % mean_abs_crr_dot*0.5 to be current_dot not zero! We don't
%             % need it exactly in the following
%             abs_crr_dot_above_mean = round(smoothdata(abs_crr_dot_above_mean,'movmean',50)); 
%             abs_crr_dot_steps = abs_crr_dot_above_mean(2:end)-abs_crr_dot_above_mean(1:end-1);         
% 
%             % now get changes at beginning             
%             % first step: falling current goes to zero
%             % second step: current starts to rise to measruement current
%             % third step: current has reached  measruement current
%             % fourth step: current starts to fall to zero current
%             % fifth step: current has reached  zero  current
%             % sixth step: current starts with first ramp
% 
%             abs_crr_dot_steps_idx = find(abs_crr_dot_steps);
% 
%             flag_initial_zero_phase = false;
%             %due to lags we don't know, if we start with a ramp or
%             %constant current -> we check it                
%             if mean(abs_crr_dot(1:abs_crr_dot_steps_idx(1))) < mean_abs_crr_dot
%                 % if we start with a constant phase there will be an
%                 % additional step at the beginning! The rest of the
%                 % function is implemented to start with a decreasing
%                 % current, therefore we remove the first step
%                 abs_crr_dot_steps_idx(1) = [];
%                 flag_initial_zero_phase = true;
%             end
% 
%             % due to the filter our idx_crr_steps_start are 'behind'
%             % the real measured signal
%             [~,idx_begin_R_meas_start]=find(abs(current(abs_crr_dot_steps_idx(3):abs_crr_dot_steps_idx(5))-i_meas)<=0.0025,1,'first');
%             [~,idx_end_R_meas_start]=find(abs(current(abs_crr_dot_steps_idx(3):abs_crr_dot_steps_idx(5))-i_meas)<=0.0025,1,'last');
% 
%             % R measure phase must be within this region
%             idx_begin_R_meas_start = idx_begin_R_meas_start +abs_crr_dot_steps_idx(3)-1;
%             idx_end_R_meas_start =  idx_end_R_meas_start +abs_crr_dot_steps_idx(3)-1;
% 
%             % due to filter setting the first two "zero change" zones
%             % are the current measuremt zones but the start before the current step end afterwards
%             % therefore we                 
% 
%             T_start_high = obj.inputSignal.ratio_T_meas_current_high_start;
%             T_start_end_low = obj.inputSignal.ratio_T_break_start;
%             min_nmbr_sampels_zero = min([1-T_start_high-T_start_end_low,T_start_end_low])*daq_rate;
%             index_crr_dot_zero =  round(smoothdata( abs_crr_dot<0.0000015,'movmean',min_nmbr_sampels_zero*0.5));  
% %                 index_crr_dot_zero =  round(smoothdata( abs_crr_dot<0.00001,'movmean',200)); %!!!!!!!!! Assume to smooth steps away by 300 
%             idx_zero_zones =find(index_crr_dot_zero(2:end)-index_crr_dot_zero(1:end-1));
% 
%             % if there was an intial constant current phase, we have to
%             %  remove the first entry in idx_zero_zones
% 
%             if flag_initial_zero_phase
%                 idx_zero_zones(1)=[];
%             end
% 
%             
%             idx_steps_crr_meas_end = abs_crr_dot_steps_idx(and(abs_crr_dot_steps_idx > idx_zero_zones(3) , abs_crr_dot_steps_idx < idx_zero_zones(4)));             
% 
%             [~,idx_begin_R_meas_end]=find(abs(current(idx_steps_crr_meas_end(2):idx_steps_crr_meas_end(4))-i_meas)<=0.0025,1,'first');
%             [~,idx_end_R_meas_end]=find(abs(current(idx_steps_crr_meas_end(2):idx_steps_crr_meas_end(4))-i_meas)<=0.0025,1,'last');
% 
%             idx_begin_R_meas_end = idx_begin_R_meas_end + idx_steps_crr_meas_end(2)-1;
%             idx_end_R_meas_end = idx_end_R_meas_end + idx_steps_crr_meas_end(2)-1;
% 
%             idxs_R_start = idx_begin_R_meas_start:idx_end_R_meas_start;
%             idxs_R_end = idx_begin_R_meas_end:idx_end_R_meas_end;                   


            


            [obj.R_valve_start,idx_start_end]= calc_R_from_steady_state(obj,voltage(idxs_R_start),current(idxs_R_start));
            obj.R_valve_end_idx = idx_start_end + idxs_R_start(1);
            [obj.R_valve_end,idx_start_end] = calc_R_from_steady_state(obj,voltage(idxs_R_end),current(idxs_R_end));
            obj.R_valve_start_idx = idx_start_end + idxs_R_end(1);


            
            

%             % now we need to find beginning and end of the ramps                
%             idx_hor = abs_crr_dot_steps_idx(5): abs_crr_dot_steps_idx(5)+round((abs_crr_dot_steps_idx(6)-abs_crr_dot_steps_idx(5))*0.7);                
%             idx_diag =  abs_crr_dot_steps_idx(6): abs_crr_dot_steps_idx(6)+round((abs_crr_dot_steps_idx(7)-abs_crr_dot_steps_idx(6))*0.7);                
%             index_ramps_start = obj.getIntersection_diag_hor(idx_diag(1),current(idx_diag),obj.current(idx_hor));
% 
% 
%             idx_crr_meas_end_start = find(abs_crr_dot_steps_idx==idx_steps_crr_meas_end(1));
% 
% % USE THIS CODE IF YOU WANT THE LAST POINT OF THE LAST FALLING RAMP                 
% %                 delta_idx = (abs_crr_dot_steps_idx(idx_crr_meas_end_start)-abs_crr_dot_steps_idx(idx_crr_meas_end_start-1));
% %                 idx_hor_start = abs_crr_dot_steps_idx(idx_crr_meas_end_start-1)+ round(delta_idx)*0.2;
% %                 idx_hor = idx_hor_start:idx_hor_start+round(delta_idx*0.7);                
% %                 idx_diag = abs_crr_dot_steps_idx(idx_crr_meas_end_start-1)-round((abs_crr_dot_steps_idx(idx_crr_meas_end_start-1)-abs_crr_dot_steps_idx(idx_crr_meas_end_start-2))*0.7): abs_crr_dot_steps_idx(idx_crr_meas_end_start-1); 
% %                 index_ramps_end = obj.getIntersection_diag_hor(idx_diag(1),current(idx_diag),obj.current(idx_hor));
% 
%             idx_crr_meas_peak_diag1 = idx_crr_meas_end_start-3;
%             idx_crr_meas_peak_diag2 = idx_crr_meas_end_start-2;
% 
%             delta_diag1 = abs_crr_dot_steps_idx(idx_crr_meas_peak_diag1)-abs_crr_dot_steps_idx(idx_crr_meas_peak_diag1-1);
%             idx_end_diag1 = abs_crr_dot_steps_idx(idx_crr_meas_peak_diag1)-round(delta_diag1*0.1);
%             idx_diag1 =  idx_end_diag1-round(delta_diag1*0.7):idx_end_diag1;
% 
%             delta_diag2 = abs_crr_dot_steps_idx(idx_crr_meas_peak_diag2+1)-abs_crr_dot_steps_idx(idx_crr_meas_peak_diag2);
%             idx_start_diag2 = abs_crr_dot_steps_idx(idx_crr_meas_peak_diag2)+round(delta_diag2*0.1);
%             idx_diag2 =  idx_start_diag2:idx_start_diag2+round(delta_diag2*0.7);
% 
%             index_ramps_end = obj.getIntersection_diag_diag(idx_diag1(1),current(idx_diag1),idx_diag2(1),current(idx_diag2));               
% 
%             obj.index_ramps = index_ramps_start:index_ramps_end;   

        end
        
        function obj = extract_resictance_n_const_psi_ramps(obj)   
            % ----------------- get start and end of the ramps for integration -
            % get the index where the ramp starts
            % the saved index 'idx_saved_ramp marks' the region, where the ramp
            % stars, so we search here. We look for a intersection of
            % horizontal line with an increasing/decreasing.
            % This is done in 'getIntersection_diag_hor()'.                      

            [~,idx_i_meas_start_begins]=find(abs(obj.current-obj.inputSignal.i_meas_current)<=0.0025,1,'first');
            idx_begin_R_meas_start =idx_i_meas_start_begins;

            [~,idx_end_curr_meas]=find(obj.current(idx_i_meas_start_begins+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*0.5:idx_i_meas_start_begins+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*1.5)-obj.inputSignal.i_meas_current*0.5< 0 ,1,'first');

            idx_end_curr_meas = idx_i_meas_start_begins+idx_end_curr_meas+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*0.5;



            idx_hor_start = idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.3;
            idx_hor_end = idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.8;             

            zero_curr_mean = mean(obj.current(idx_hor_start:idx_hor_end));
            zero_curr_std  = std(obj.current(idx_hor_start:idx_hor_end));
            
            [~,idx1]=find(obj.current(idx_hor_end:end)>=(zero_curr_mean+5*zero_curr_std),1,'first');
            [~,idx2]=find(obj.current(idx_hor_end:end)>=(zero_curr_mean+50*zero_curr_std),1,'first');
            
            idx_diag_start = idx_hor_end + idx1;
            idx_diag_end =  idx_hor_end+ idx2;
            
%             idx_search_start = round(idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.5);
%             idx_search_end = round(idx_search_start+ obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start+obj.inputSignal.T_meas_psi_sgnl_ramp));

%             [~,idx_diag_start]=find(abs(obj.current(idx_search_start:idx_search_end)-obj.inputSignal.i_max*0.2)<=0.01,1,'first');
%             idx_diag_start = round(idx_diag_start+idx_search_start);
%             idx_diag_end    =  round(idx_diag_start+obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.3));

            % use curve fitting to find the intersection
            idx_ramp_start = obj.getIntersection_diag_hor(idx_diag_start,obj.current(idx_diag_start:idx_diag_end),obj.current(idx_hor_start:idx_hor_end));

            % To find the end of the ramp we try to find the last value closed i max.
            % first geuss for the end

            
            
            
            idx_int_end =  round(idx_ramp_start+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*(1+4*(obj.inputSignal.nmbr_cycles)));            
            idx_end_R_meas_end = idx_int_end + find(obj.current(idx_int_end:end)>obj.inputSignal.i_max*0.98,1,'last');

            idx_hor_end_start = round(idx_end_R_meas_end-obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end*0.8);
            idx_hor_end_end     = round(idx_end_R_meas_end-obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end*0.2);            
% 

            zero_curr_mean = mean(obj.current(idx_hor_end_start:idx_hor_end_end));
            zero_curr_std  = std(obj.current(idx_hor_end_start:idx_hor_end_end));
            

            
            [~,idx_ramp_end_est]=find(abs(obj.current(idx_ramp_start:idx_hor_end_start)-obj.inputSignal.i_max)>=0.01,1,'last');
            
            
            [~,idx1]=find(obj.current(idx_ramp_end_est:idx_hor_end_start)<(zero_curr_mean-5*zero_curr_std),1,'last');
            [~,idx2]=find(obj.current(idx_ramp_end_est:idx_hor_end_start)<(zero_curr_mean-500*zero_curr_std),1,'last');
            
            
            idx_diag_end_start = idx_ramp_end_est + idx2;
            idx_diag_end_end =  idx_ramp_end_est + idx1;
            


 
%             idx_ramp_end_est = idx_ramp_start + idx_ramp_end_est;
% 
%             idx_diag_end_start  = round(idx_ramp_end_est-obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.7));
%             idx_diag_end_end    = round(idx_ramp_end_est-obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.2));                          

            % use curve fitting to find the intersection        
            idx_ramp_end = obj.getIntersection_diag_hor(idx_diag_end_start,obj.current(idx_diag_end_start:idx_diag_end_end),obj.current(idx_hor_end_start:idx_hor_end_end));

            obj.index_ramps = idx_ramp_start:idx_ramp_end;    

            % ----------------- get initial offset and noise from data --------       
            this_offset = [mean(obj.current(round(1:idx_begin_R_meas_start*0.9))); ...
                           mean(obj.voltage(round(1:idx_begin_R_meas_start*0.9)))] ;

            % ----------------- get resictance from data ----------------------
            % calculate Resitance at beginning
            ixd_step_meas_R_start = round(idx_begin_R_meas_start:idx_begin_R_meas_start+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start);

            % calculate Resitance at and
            ixd_step_meas_R_end = round(idx_end_R_meas_end - obj.inputSignal.T_meas_resistanc_end*obj.inputSignal.daq_Rate:idx_end_R_meas_end);

            [obj.R_valve_start,std_out] = obj.calc_R_from_Step('start',obj.current(ixd_step_meas_R_start),obj.voltage(ixd_step_meas_R_start),[0;0]);
            [obj.R_valve_end,std_out_end] = obj.calc_R_from_plateau_v2(obj.current(ixd_step_meas_R_end),obj.voltage(ixd_step_meas_R_end),[0;0]); 

        end
        
        
        
        
        
        function [obj,idxs_R_meas_start,idxs_R_meas_end,abs_crr_dot_steps_idx_forward] = extract_idxs(obj)
            % ----------------- get start and end of the ramps for integration -
            % THIS IMPLEMENTATION ONLY WORKS FOR SIGNAL STARTING A SOME
            % POSITVE CURRENT, lowering torwards zero, apply a step for
            % current measumrements followed by ramps, last ramp has to
            % end at zero. Then again a step for current measurmet is
            % apply and the current raised to the initial value!

            % the saved index 'idx_saved_ramp marks' the region, where the ramp
            % stars, so we search here. We look for a intersection of
            % horizontal line with an increasing/decreasing.
            % This is done in 'getIntersection_diag_hor()'.
            daq_rate = obj.inputSignal.daq_Rate;
            i_meas = obj.inputSignal.i_meas_current;
            
            range_idx_check = length(obj.current);
            [current_forward,abs_crr_dot_steps_idx_forward,first_idx_2_calc_start_holdphase] = currentidx(1,range_idx_check);
            
            nmbr_meas_pnts = size(obj.current,2);
            i_heat = obj.inputSignal.i_heat; %obj.inputSignal.i_max/sqrt(3);
            [~,idx]=find(abs(obj.current-i_heat)>0.001,1,'last');
            range_idx_check = (nmbr_meas_pnts-idx)+range_idx_check;
            if range_idx_check>=nmbr_meas_pnts
                range_idx_check = nmbr_meas_pnts-1;
            end
            [current_reverse,abs_crr_dot_steps_idx_reverse] = currentidx(nmbr_meas_pnts,nmbr_meas_pnts-range_idx_check);
            
            function[current,abs_crr_dot_steps_idx,first_idx_2_calc_start_holdphase] = currentidx(idx_search_start,idx_search_end)
                if idx_search_start < idx_search_end  
                    flg_signl_order_reversed = false;
                else
                    flg_signl_order_reversed = true;
                end

                if ~flg_signl_order_reversed                               
                    current =  obj.current(idx_search_start:idx_search_end);           
                else                    
                    current =  obj.current(idx_search_start:-1:idx_search_end); 
                end

                % now we use a filterd signal of the current change in
                % order to find the regions where the signal changes
                % ATTENTION: due to filters the signal is blurred and is 
                % only sufficient to find the regions, the exact edges must
                % then be searched for separately

                current2 = smoothdata(current,'gaussian',round(daq_rate*0.001)); % #gm set from 0.01 to 0.001
                crr_dot = smoothdata(gradient(current2),'movmean',round(daq_rate*0.025));
                abs_crr_dot = abs(crr_dot); 

                mean_abs_crr_dot = mean(abs_crr_dot(abs_crr_dot>3e-6)); %  #gm  set from 1e-6 to 3e-6 we asume 1e-6 to be the threshold for noise
                std_abs_crr_dot = std(abs_crr_dot(abs_crr_dot>1e-6));
                mean_abs_crr_dot = mean(abs_crr_dot(and(abs_crr_dot>1e-6,abs_crr_dot<mean_abs_crr_dot+std_abs_crr_dot)));
                std_abs_crr_dot = std(abs_crr_dot(and(abs_crr_dot>1e-6,abs_crr_dot<mean_abs_crr_dot+std_abs_crr_dot)));

                abs_crr_dot_above_mean =  abs_crr_dot>mean_abs_crr_dot*0.5; % <---- because of filters 
                % this id blurred so we assume everything above
                % mean_abs_crr_dot*0.5 to be current_dot not zero! We don't
                % need it exactly in the following
%!!!!!!!!!!!!! Attention, this filter adjustment for low currents is only a hot fix                      
                if obj.inputSignal.i_max < 0.06
                    abs_crr_dot_above_mean = round(smoothdata(abs_crr_dot_above_mean,'movmean',25)); 
                else
                    abs_crr_dot_above_mean = round(smoothdata(abs_crr_dot_above_mean,'movmean',50)); 
                end

                abs_crr_dot_steps = abs_crr_dot_above_mean(2:end)-abs_crr_dot_above_mean(1:end-1);         

                % now get changes at beginning             
                % first step: falling current goes to zero
                % second step: current starts to rise to measruement current
                % third step: current has reached  measruement current
                % fourth step: current starts to fall to zero current
                % fifth step: current has reached  zero  current
                % sixth step: current starts with first ramp

                abs_crr_dot_steps_idx = find(abs_crr_dot_steps);
                
                flag_initial_zero_phase = false;
                %due to lags we don't know, if we start with a ramp or
                %constant current -> we check it                
                if mean(abs_crr_dot(1:abs_crr_dot_steps_idx(1))) < mean_abs_crr_dot-std_abs_crr_dot
                    % if we start with a constant phase there will be an
                    % additional step at the beginning! The rest of the
                    % function is implemented to start with a decreasing
                    % current, therefore we remove the first step
                    if ~flg_signl_order_reversed
                        first_idx_2_calc_start_holdphase = abs_crr_dot_steps_idx(1);
                    else
                        %otherwise calculated later
                    end
                    abs_crr_dot_steps_idx(1) = [];
                    flag_initial_zero_phase = true;                            
                else
                    if ~flg_signl_order_reversed
                        first_idx_2_calc_start_holdphase = 1;
                    else
                        %otherwise calculated later
                    end

                end
                
            end
            
            switch(obj.inputSignal.Type)
                case {'ramps_heated','ramps_heated_payload'}
                    
                    flg_signl_order_reversed = false;
                    [idx_begin_R_meas_start,idx_end_R_meas_start] = get_idx_meas_R_static(current_forward,abs_crr_dot_steps_idx_forward);
                    
                    flg_signl_order_reversed = true;
                    [idx_begin_R_meas_end_tmp,idx_end_R_meas_end_tmp] = get_idx_meas_R_static(current_reverse,abs_crr_dot_steps_idx_reverse);
                    
                    idx_end_R_meas_end = nmbr_meas_pnts-idx_begin_R_meas_end_tmp;
                    idx_begin_R_meas_end = nmbr_meas_pnts-idx_end_R_meas_end_tmp;
                                        
                    idxs_R_meas_start = idx_begin_R_meas_start:idx_end_R_meas_start;
                    idxs_R_meas_end = idx_begin_R_meas_end:idx_end_R_meas_end;
                    
                    %find indexes for dynamic middle part of the
                    %measurement cycle
                    [index_ramps_start,index_ramps_end] = get_meas_idxs_crr_dynmc;
                    obj.index_ramps = index_ramps_start:index_ramps_end;
                    
                case 'ramps_heated_full_crr_meas_cycle'
                    nmbr_hold_steps_cycles = 2;     %!!!!!!!!!!!still hard coded, needs to be included into object inputSignal!!!!!!!!!
                    nmbr_hold_steps_per_ramp = obj.inputSignal.nmbr_incr_hold_steps;
                    
                    flg_signl_order_reversed = false;
                    [idx_begin_R_meas_start,idx_end_R_meas_start,idx_begin_hold_ramps_start2,idx_end_hold_ramps_start2] = get_idx_meas_R_static(current_forward,abs_crr_dot_steps_idx_forward);
                    flg_signl_order_reversed = true;
                    [idx_begin_R_meas_end_tmp,idx_end_R_meas_end_tmp,idx_end_hold_ramps_end2,idx_begin_hold_ramps_end2] = get_idx_meas_R_static(current_reverse,abs_crr_dot_steps_idx_reverse);

                    idx_end_R_meas_end = nmbr_meas_pnts-idx_begin_R_meas_end_tmp;
                    idx_begin_R_meas_end = nmbr_meas_pnts-idx_end_R_meas_end_tmp;
                    
                    idxs_R_meas_start = idx_begin_R_meas_start:idx_end_R_meas_start;
                    idxs_R_meas_end = idx_begin_R_meas_end:idx_end_R_meas_end;
                    
                    
                    [index_ramps_start,index_ramps_end,idx_diag_intersecction_start2] = get_meas_idxs_crr_dynmc;
                    obj.index_ramps = index_ramps_start:index_ramps_end;

                    
                    flg_signl_order_reversed = false;
                    [index_begin_current_hold_ramps_start2,idx_start_curr_meas_begin2] = get_idx_meas_R_stepramps(current_forward,abs_crr_dot_steps_idx_forward,idx_end_R_meas_start);
                    flg_signl_order_reversed = true;
                    [index_begin_current_hold_ramps_end2,idx_start_curr_meas_end2] = get_idx_meas_R_stepramps(current_reverse,abs_crr_dot_steps_idx_reverse,idx_end_R_meas_end_tmp);
                    
                    obj.idx_begin_hold_ramps_start = idx_begin_hold_ramps_start2;
                    obj.idx_end_hold_ramps_start = idx_end_hold_ramps_start2;
                    obj.idx_diag_intersecction_start = idx_diag_intersecction_start2;
                    obj.idx_begin_hold_ramps_end = nmbr_meas_pnts-idx_begin_hold_ramps_end2;
                    obj.idx_end_hold_ramps_end = nmbr_meas_pnts-idx_end_hold_ramps_end2;
                    obj.idx_diag_intersecction_end = index_ramps_end;
                    obj.index_begin_current_hold_ramps_start = index_begin_current_hold_ramps_start2;
                    obj.index_begin_current_hold_ramps_end = nmbr_meas_pnts-index_begin_current_hold_ramps_end2;
                    obj.idx_start_curr_meas_begin = idx_start_curr_meas_begin2;
                    obj.idx_start_curr_meas_end = nmbr_meas_pnts-idx_start_curr_meas_end2;
                    
                    otherwise
                                error("unkown signal type for this function");
            end
            
            
            function [idx_start_R_meas,idx_fin_R_meas,idx_begin_hold_ramps_start,idx_begin_hold_ramps_end] = get_idx_meas_R_static(rel_current,abs_crr_dot_steps_idx)
                        % due to the filter our idx_crr_steps_start are 'behind'
                        % the real measured signal
%                         nmbr_hold_steps_per_ramp = obj.inputSignal.nmbr_incr_hold_steps;

                        idx_begin_hold_ramps_start = 3;
                        idx_begin_hold_ramps_start=abs_crr_dot_steps_idx(idx_begin_hold_ramps_start);
                        
                        
                        switch(obj.inputSignal.Type) 
                        
                            case 'ramps_heated_full_crr_meas_cycle'
                                idx_begin_hold_ramps_end = nmbr_hold_steps_per_ramp*4*nmbr_hold_steps_cycles;                        
                                idx_start_search = idx_begin_hold_ramps_end +3;
                                idx_begin_hold_ramps_end =abs_crr_dot_steps_idx(idx_begin_hold_ramps_end);
                            case 'ramps_heated'                             
                                idx_start_search = 3;
                            otherwise
                                error("unkown signal type for this function");
                        end
                        
                        [~,idx_start_R_meas]=find(abs(rel_current(abs_crr_dot_steps_idx(idx_start_search):abs_crr_dot_steps_idx(idx_start_search+1))-i_meas)<=0.002,1,'first');
                        
                        [~,idx_fin_R_meas]=find(abs(rel_current(abs_crr_dot_steps_idx(idx_start_search):abs_crr_dot_steps_idx(idx_start_search+1))-i_meas)<=0.002,1,'last');

                        idx_start_R_meas = idx_start_R_meas +abs_crr_dot_steps_idx(idx_start_search)-1;
                        idx_fin_R_meas =  idx_fin_R_meas +abs_crr_dot_steps_idx(idx_start_search)-1;
                        
            end
            
            function [index_ramps_start,index_ramps_end,idx_firstpeak] = get_meas_idxs_crr_dynmc
                %This function finds the start and end index for a dynamic
                %ramp middle part of the measurement cycle
                idx_start_search = abs_crr_dot_steps_idx_forward(find(abs_crr_dot_steps_idx_forward>idx_end_R_meas_start,1,'first'));
                idx_end_search = abs_crr_dot_steps_idx_forward(find(abs_crr_dot_steps_idx_forward<idx_begin_R_meas_end,1,'last'));
                max_current = obj.inputSignal.i_max;
                
                [~,idx_peaks,~,p]=findpeaks(obj.current(idx_start_search:idx_end_search));
                idx_peaks = idx_peaks(p>mean(p)*3)+idx_start_search-1;
                [~,x]=find(abs(current_forward(idx_peaks)/max_current)>=0.9);
                idx_peaks = idx_peaks(x);
                
                
                % now we need to find beginning and end of the ramps                
                idx_hor = idx_start_search: idx_start_search+500;        %!!!!!!!!!!!!!!!!hard coded, a better endpoint for the search index should be found        
                [~,y]=find(abs(current_forward(idx_start_search:idx_peaks(1))/max_current)>=0.3,1,'first');
                Y = y +idx_start_search-1;
                idx_diag =  Y: idx_peaks(1);                
                index_ramps_start = obj.getIntersection_diag_hor(idx_diag(1),current_forward(idx_diag),current_forward(idx_hor));
                
                idx_firstpeak = idx_peaks(1);
                index_ramps_end = idx_peaks(end); 
            end
            
            function [index_start_current_hold_ramps,index_start_curr_meas]  =  get_idx_meas_R_stepramps(current,abs_crr_dot_steps_idx,idx_end_R_meas)
                %index for optional warm up or R measurement cycles

                %%%%%%%%% ramp begin
                [~,idx_tmp]=min(abs(abs_crr_dot_steps_idx-idx_end_R_meas));
%                 idx_Rmeas_end = abs_crr_dot_steps_idx(idx_tmp+1); % measurement end
                idx_Rramp_start = abs_crr_dot_steps_idx(idx_tmp+2); % first ramp start

                switch(obj.inputSignal.Type) 
                    case 'ramps_heated_full_crr_meas_cycle'
                         % calculate start of the R meas ramps 
                        if ~flg_signl_order_reversed  
                            % find start of hold ramps
                            delta_len = abs_crr_dot_steps_idx(1)- first_idx_2_calc_start_holdphase;                        
                            idx_ramp1 = abs_crr_dot_steps_idx(1)-round(0.7*delta_len):abs_crr_dot_steps_idx(1)-round(0.2*delta_len);
                            delta_len = abs_crr_dot_steps_idx(3)- abs_crr_dot_steps_idx(2);
                            idx_ramp2 = abs_crr_dot_steps_idx(2)+round(0.2*delta_len):abs_crr_dot_steps_idx(2)+round(0.7*delta_len);
                            index_start_current_hold_ramps = obj.getIntersection_diag_diag(idx_ramp1(1),current(idx_ramp1),idx_ramp2(1),current(idx_ramp2));
                            % find end oh hold ramps
                            idx_crr_m_start = (obj.inputSignal.nmbr_incr_hold_steps*2)*2*2+1; %!!!!!!!!!!!!!!! ATTENTION!!!!! HARD CODED FOR TWO RAMPS!!!!!! 
                            delta_len = abs_crr_dot_steps_idx(idx_crr_m_start+1)-abs_crr_dot_steps_idx(idx_crr_m_start);
                            idx_hor = abs_crr_dot_steps_idx(idx_crr_m_start)+round(0.2*delta_len):abs_crr_dot_steps_idx(idx_crr_m_start)+round(0.7*delta_len);                            
                            delta_len = abs_crr_dot_steps_idx(idx_crr_m_start)-abs_crr_dot_steps_idx(idx_crr_m_start-1);
                            idx_ramp1= abs_crr_dot_steps_idx(idx_crr_m_start)-round(0.8*delta_len):abs_crr_dot_steps_idx(idx_crr_m_start)-round(0.2*delta_len); 
                            index_start_curr_meas = obj.getIntersection_diag_hor(idx_ramp1(1),current(idx_ramp1),current(idx_hor));

                        else 
                            % case for reversed signal
                            idx_begin_hold_ramps_end_tmp = nmbr_hold_steps_per_ramp*4*nmbr_hold_steps_cycles;   

                            delta_len = abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp+1)-abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp);
                            idx_ramp1 = abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp+1)-round(0.7*delta_len):abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp+1)-round(0.2*delta_len);                            
                            delta_len = abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp+2)-abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp+1);
                            idx_hor= abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp+1)+round(0.2*delta_len):abs_crr_dot_steps_idx(idx_begin_hold_ramps_end_tmp+1)+round(0.7*delta_len); 
                            index_start_current_hold_ramps = obj.getIntersection_diag_hor(idx_ramp1(1),current(idx_ramp1),current(idx_hor));
                            % find start of current 
                            index_start_curr_meas = idx_Rramp_start;
                        end
                    otherwise
                        error("unkown signal type for this function");
                end
                          
            end
            
            
        end
        
        
        
        
        
        function obj = grabDatafromWorkspace(obj)
        % this function is for lazy people. If no data about the input signal is passed to the constructor, 
        % this function looks for data in the base workspace. Pay attention the
        % the DataMapping property
            for i = 1:size(obj.DataMapping,1)
                try
                obj.(obj.DataMapping{i,1}) = evalin('base', obj.DataMapping{i,2});
                catch 
                    error("Could not find the variabel " + obj.DataMapping{i,2} + " inside base workspace! " + newline...
                        + "Either rename name it to  " + obj.DataMapping{i,2} + ...
                        ",or supply an new mapping or all variables directly to the constructor")
                end
            end        

        end       

        function obj = remove_offset(obj)      

%                 end_offset_zone = round(0.9*obj.daq_Rate*obj.T_meas_resistance_start);
%                 start_offset_zone = round(0.5*obj.daq_Rate*obj.T_meas_resistance_start);

                start_offset_zone = 1;
                
                %find first peak
               [~,end_offset_zone_curr] = max(diff(obj.current(1:obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start)   ));
%                [~,end_offset_zone_vol] = max(diff(obj.voltage(1:obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start)   ));
               %                 end_offset_zone = round(0.8*obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*(1-obj.inputSignal.ratio_T_meas_current_high_start));
                
               end_offset_zone_curr = round(end_offset_zone_curr*0.9);
                offset_voltage = mean(obj.voltage(start_offset_zone:end_offset_zone_curr));
                offset_current = mean(obj.current(start_offset_zone:end_offset_zone_curr));

                obj.current = obj.current - offset_current;
                obj.voltage = obj.voltage - offset_voltage;

        end
        
        function idx_intersecction = getIntersection_diag_horSin(~,idx_end_curr_meas,idx_diag_start,increasing_vals,horizontal_val)
            % This function calculates the intersection of a horizontal and an
            % increasing/decreasing sinwave!!!
            % THIS SHOULD BE IMPLEMENTED USING ANALYTICAL CALCULUS AND
            % PAYING ATTATION TO THE BAD CONDITION DUE TO SMALL NUMBERS OF THE FITTED SINWAVE
            % DIRTY IMPLEMENTED!!!!!!!!!!!!!!!
            hor_val = mean(horizontal_val);
            % Fit model to data
            [fitresult, ~] = fit( double(idx_diag_start:idx_diag_start+length(increasing_vals)-1)',  double(increasing_vals)', 'sin1' );

            if idx_end_curr_meas < idx_diag_start % case if begin of sinwave block is estimated
            
            [~,idx_min]=min(abs(feval(fitresult,idx_end_curr_meas:idx_diag_start)-hor_val));
            idx_intersecction = idx_end_curr_meas+idx_min;
            else % case if end of sinwave block is estimated
            [~,idx_min]=min(abs(feval(fitresult,idx_diag_start:idx_end_curr_meas)-hor_val));
            idx_intersecction = idx_diag_start+idx_min;
            end
            
            %             c_val = coeffvalues(fitresult);
            %             idx_intersecction = round(  (   asin(hor_val/c_val(1))  - c_val(3))/c_val(2)  );
        end
         
        function idx_intersecction = getIntersection_diag_diag(~,idx_start_diag1,val_diag_1,idx_start_diag2,val_diag_2)
            % This function calculates the intersection of a horizontal and an
            % increasing/decreasing sinwave!!!
            % THIS SHOULD BE IMPLEMENTED USING ANALYTICAL CALCULUS AND
            % PAYING ATTATION TO THE BAD CONDITION DUE TO SMALL NUMBERS OF THE FITTED SINWAVE
            % DIRTY IMPLEMENTED!!!!!!!!!!!!!!!

            % Fit model to data
            [fit_diag1, ~] = fit( double(idx_start_diag1:idx_start_diag1+length(val_diag_1)-1)',  double(val_diag_1)', 'poly1', fitoptions( 'Method', 'LinearLeastSquares' ) );
            [fit_diag2, ~] = fit( double(idx_start_diag2:idx_start_diag2+length(val_diag_2)-1)',  double(val_diag_2)', 'poly1', fitoptions( 'Method', 'LinearLeastSquares' ) );            
            
           
            idx_intersecction = round((fit_diag2.p2 -fit_diag1.p2)/(fit_diag1.p1 -fit_diag2.p1));
         
        end            
        
        function idx_intersecction = getIntersection_diag_hor(~,idx_diag_start,increasing_vals,horizontal_val)
            % This function calculates the intersection of a horizontal and an
            % increasing/decreasing line

            hor_val = mean(horizontal_val);
            % Fit model to data
            [fitresult, ~] = fit( double(idx_diag_start:idx_diag_start+length(increasing_vals)-1)',  double(increasing_vals)', 'poly1', fitoptions( 'Method', 'LinearLeastSquares' ) );
            c_val = coeffvalues(fitresult);
            idx_intersecction = round((hor_val-c_val(2))/c_val(1));
        end

        function[R_valve,std_out]= calc_R_from_Step(obj,where,current,voltage,offset)

            if strcmp(where,'start')

                   idx_max_curr_change = 1;
                   idx_start= idx_max_curr_change + 0.55*obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start;  
                   idx_end = idx_max_curr_change + 0.85*obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start;  

                   elseif strcmp(where,'end')
                       here = round([0.5,1.5]*obj.inputSignal.T_meas_resistance_start*obj.daq_Rate);
                       idx_here = here(1):here(2);
                       if mean(current(idx_here))>0           
                             [~,idx_max_volt_change]=min(voltage(here(1):here(2)));  
                       else
                            [~,idx_max_volt_change]=max(voltage(here(1):here(2)));  
                       end
                         idx_end= idx_here(idx_max_volt_change) - 0.1*obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start;  
                         idx_start = idx_here(idx_max_volt_change) - 0.6*obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start;    
                   elseif strcmp(where,'end_plateau')


                   else
                       error('Unkown input! use ''start'' or ''end'' to determine if you want to extract resitance before or after the ramp'); 
                   end

                   R_valve = mean((voltage(idx_start:idx_end)-offset(2))./(current(idx_start:idx_end)-offset(1)));
                   std_out = std((voltage(idx_start:idx_end)-offset(2))./(current(idx_start:idx_end)-offset(1)));
        end

        function[R_valve,std_out]= calc_R_from_plateau_v2(obj,current,voltage,offset)
 
        %  detect falling voltate at the end of hold phase
            idx_end = length(current);
           % calc start and end of measuring (move a little away from real ends)
           idx_start  = idx_end - obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end*0.5;
           idx_end   = idx_end-0.05*obj.inputSignal.daq_Rate;
        % Fit model to data.
            [fitresult, ~] = fit( [idx_start:idx_end]', double([(voltage(idx_start:idx_end)-offset(2))./(current(idx_start:idx_end)-offset(1))]'), 'poly1'  );
              idx_eval_fit = - obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end*0.05;
                   R_valve = abs(fitresult.p1*idx_eval_fit+fitresult.p2);
                   std_out = std((fitresult.p1*[idx_start:idx_end]+fitresult.p2) - voltage(idx_start:idx_end)./current(idx_start:idx_end));

        end
        
        function[R_valve,idx_start_end]= calc_R_from_steady_state(obj,volt,current) 
                
                idx_volt_steady_begin_R_meas_start = idx_channel_steady(volt,obj.inputSignal.daq_Rate);
                idx_current_steady_begin_R_meas_start = idx_channel_steady(current,obj.inputSignal.daq_Rate);                
                idx_R_steady = intersect(idx_volt_steady_begin_R_meas_start,idx_current_steady_begin_R_meas_start);
                
                R_valve = mean(volt(idx_R_steady)./current(idx_R_steady));
                idx_start_end = [idx_R_steady(1),idx_R_steady(end)];
                function idx = idx_channel_steady(signal,rate)                
                    len = length(signal);   
                    signal = smoothdata(signal,'movmean',round(rate*0.025));
                    signal_mean_end = mean(signal(round(0.75*len):end));
                    signal_mean_std = std(signal(round(0.75*len):end));                
                    idx_start = find(abs(signal-signal_mean_end)<signal_mean_std,1,'first');
                    idx_end = find(abs(signal-signal_mean_end)<signal_mean_std,1,'last');
                    idx = idx_start:idx_end;
                end               
                
        end
            
        function[R_valve,R_valve_std,idx_start_end]= calc_R_from_steady_state_v2(this_Data,volt,current) 
                
                idx_volt_steady_begin_R_meas_start = idx_channel_steady(volt,this_Data.inputSignal.daq_Rate);
                idx_current_steady_begin_R_meas_start = idx_channel_steady(current,this_Data.inputSignal.daq_Rate);                
                idx_R_steady = intersect(idx_volt_steady_begin_R_meas_start,idx_current_steady_begin_R_meas_start);
                
                R_valve = mean(volt(idx_R_steady)./current(idx_R_steady));
                R_valve_std  = std(volt(idx_R_steady)./current(idx_R_steady));
                idx_start_end = [idx_R_steady(1),idx_R_steady(end)];
                function idx = idx_channel_steady(signal,rate)                
                    len = length(signal);   
                    signal = smoothdata(signal,'movmean',round(rate*0.025));
                    signal_mean_end = mean(signal(round(0.75*len):end));
                    signal_mean_std = std(signal(round(0.75*len):end));                
                    idx_start = find(abs(signal-signal_mean_end)<signal_mean_std,1,'first');
                    idx_end = find(abs(signal-signal_mean_end)<signal_mean_std,1,'last');
                    idx = idx_start:idx_end;
                end               
                
 end
        
        
        
    end % end Methods(Access = private)
    
end