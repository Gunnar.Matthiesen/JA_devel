function [M_sim,H_sim,dM_dB] = integrate_invJA_loop_discr_PSI(B_int,M_sim,H_sim,param)

        my0 = 4.*pi.*1e-7;
        Ms     = param(1);
        a      = param(2);
        alpha  = param(3);
        ja_k      = param(4);
        c      = param(5);
%         alpha2  = param(8); 
        He_sw = param(8); 
        
        dM_an_dHe = Ms/(3*a);
         dM_dB = zeros(size(B_int));
        M_an = zeros(size(B_int));
        dMan_dHe= zeros(size(B_int));
        dMirr_dBe= zeros(size(B_int));
        Mirr= zeros(size(B_int));
        
        use_split_M_an = false;
        
        for k = 2:length(B_int)    

                        
            
            delta = sign(B_int(k)-B_int(k-1));
            He = H_sim(k-1)+alpha*M_sim(k-1);
            
            
            if abs(He) <= abs(He_sw)
                % calculate M_an
                M_an(k) = dM_an_dHe*He ;
                % calculate deM_andHe    
                dMan_dHe(k) =    dM_an_dHe ;
           elseif He>He_sw
                % calculate M_an
                M_an(k) =  Ms.*(coth((He-He_sw)./a)-a./(He-He_sw)) + dM_an_dHe*He_sw;
                % calculate deM_andHe   
                dMan_dHe(k) = Ms*(a/(He - He_sw)^2 - (coth((He - He_sw)/a)^2 - 1)/a);
                
           elseif He<-He_sw
                 dMan_dHe(k) = Ms*(a/(He + He_sw)^2 - (coth((He + He_sw)/a)^2 - 1)/a);                
                 M_an(k) =  Ms.*(coth((He+He_sw)./a)-a./(He+He_sw)) - dM_an_dHe*He_sw;
           else
               error();
               
           end
            
%             if use_split_M_an == false  
% %                    M_an = Ms*(coth(He/a)-a/He);    
% %                 dMan_dHe = Ms/a*( 1 - coth(He/a)^2 + (a/He)^2);  
%                 M_an(k) = Ms*(coth(He/a)-a/He);    
%                 dMan_dHe(k) = Ms/a*( 1 - coth(He/a)^2 + (a/He)^2);  
%             else
%                 [M_an, dMan_dHe]=calc_M_ans_split(He,He_sw);  
%             end     
%             Mirr = (M_sim(k)-c*M_an)/(1-c);
%             dMirr_dBe = (M_an - Mirr)/(my0*delta*ja_k);
%             dM_dB(k) = ( (1-c)*dMirr_dBe +c/my0*dMan_dHe )/ ...
%                     ( 1+ my0*(1-alpha)*(1-c)*dMirr_dBe + c*(1-alpha)*dMan_dHe);

            Mirr(k) = (M_sim(k-1)-c*M_an(k))/(1-c);
            dMirr_dBe(k) = (M_an(k) - Mirr(k))/(my0*delta*ja_k);
            dM_dB(k) = ( (1-c)*dMirr_dBe(k) +c/my0*dMan_dHe(k) )/ ...
                    ( 1+ my0*(1-alpha)*(1-c)*dMirr_dBe(k) + c*(1-alpha)*dMan_dHe(k));
                
            M_sim(k) = M_sim(k-1) + dM_dB(k)*(B_int(k)-B_int(k-1));
            H_sim(k) = B_int(k)/my0 - M_sim(k);
        end
            
            
            
            
%             delta = sign(B_int(k+1)-B_int(k));
%             He = H_sim(k)+alpha*M_sim(k);
%             
%             if use_split_M_an == false  
% %                    M_an = Ms*(coth(He/a)-a/He);    
% %                 dMan_dHe = Ms/a*( 1 - coth(He/a)^2 + (a/He)^2);  
%                 M_an(k) = Ms*(coth(He/a)-a/He);    
%                 dMan_dHe(k) = Ms/a*( 1 - coth(He/a)^2 + (a/He)^2);  
%             else
%                 [M_an, dMan_dHe]=calc_M_ans_split(He,He_sw);  
%             end             
%             
% %             Mirr = (M_sim(k)-c*M_an)/(1-c);
% %             dMirr_dBe = (M_an - Mirr)/(my0*delta*ja_k);
% 
%             
% %             dM_dB(k) = ( (1-c)*dMirr_dBe +c/my0*dMan_dHe )/ ...
% %                     ( 1+ my0*(1-alpha)*(1-c)*dMirr_dBe + c*(1-alpha)*dMan_dHe);
% 
%             Mirr(k) = (M_sim(k)-c*M_an(k))/(1-c);
%             dMirr_dBe(k) = (M_an(k) - Mirr(k))/(my0*delta*ja_k);
%             dM_dB(k) = ( (1-c)*dMirr_dBe(k) +c/my0*dMan_dHe(k) )/ ...
%                     ( 1+ my0*(1-alpha)*(1-c)*dMirr_dBe(k) + c*(1-alpha)*dMan_dHe(k));
%                 
%             M_sim(k+1) = M_sim(k) + dM_dB(k)*(B_int(k+1)-B_int(k));
%             H_sim(k+1) = B_int(k+1)/my0 - M_sim(k+1);
%         end
     
        
%     function [M_an_s, dM_an_dHe_s]=calc_M_ans_split(He)  
%    
%         if He <= He_sw
%         % calculate M_an
%         M_an_s = 1 ;
%         % calculate deM_andHe    
%         dM_an_dHe_s =    1 ;
%         else
%         % calculate M_an
%         M_an_s =  Ms*(coth(He/a)-a/He);
%         % calculate deM_andHe    
%         dM_an_dHe_s =   Ms/a*( 1 - coth(He/a)^2 + (a/He)^2);     
%         end

%     end    
        
        
end



