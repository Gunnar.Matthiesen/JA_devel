function  sse = opt_anhyst_2_fcn(x,y,param,param_opt)

param = param.*param_opt;

Ms1  = param(1);
Ms2 = param(2);
a1   = param(3);
a2  = param(4);
alpha1 = param(5);
alpha2 = param(6);


x1 = x+alpha1*y; 
x2 = x+alpha2*y;

y_dach = Ms1*(coth(x1/a1)-a1./x1)  + Ms2*(coth(x2/a2)-a2./x2);

sse = sum((y-y_dach).^2);
end
