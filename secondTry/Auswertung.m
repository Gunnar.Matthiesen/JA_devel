
results_R_model=struct();
cum_curve_psi = 0;
cum_curve_crr= 0;
idx_res = 2;
j=1;
for k = 1:13
    cum_curve_crr = [];
    cum_curve_psi = [];
    for i =1:18
        display(i);
        try
        this_meas = meas_raw(k).freq(j).current(i);
        if i <=10
            this_meas.get_R_n_ramps_heated_full_crr_meas_cycle('skip_R_meas');  
            this_meas.calc_psi();
        else
           this_meas.get_R_n_ramps_heated_full_crr_meas_cycle();  
           this_meas.calc_psi('Rmodel','exp&sin'); 
        end
        this_meas.evaluate_psi_ramps();      
        this_res = meas_raw(k).freq(j).current(i).results(idx_res).flux_curve; %        
        current = -[smoothdata(this_res.current_decrease,'movmean',1),smoothdata(this_res.current_increase,'movmean',1)];
        psi = -[smoothdata(this_res.psi_decrease,'movmean',1),smoothdata(this_res.psi_increase,'movmean',1)];
        [max_curr,idx_max_curr] = max(current);
        max_psi =psi( idx_max_curr);
%         results_R_model(i).current_sm = -[smoothdata(this_res.current_decrease,'movmean',50),smoothdata(this_res.current_increase,'movmean',50)];
%         results_R_model(i).psi_sm = -[smoothdata(this_res.psi_decrease,'movmean',50),smoothdata(this_res.psi_increase,'movmean',50)];
        cum_curve_crr = [cum_curve_crr,max_curr];
        cum_curve_psi = [cum_curve_psi,max_psi];
        
        catch 
        disp('failed')
        cum_curve_crr = [cum_curve_crr,NaN];
        cum_curve_psi = [cum_curve_psi,NaN];
        end
        
    end
     close all
     meas_raw(k).freq(j).cum_curve(1).current = cum_curve_crr;
     meas_raw(k).freq(j).cum_curve(1).psi = cum_curve_psi;
     meas_raw(k).freq(j).anhyst_curve_max(1).current = this_res.current_grid_sm;
     meas_raw(k).freq(j).anhyst_curve_max(1).psi = this_res.psi_mean_sm;
end
%%
for k = 1: size(meas_raw,2)
    position = [];
    for i = 1 : size(meas_raw(k).freq(j).current,2)
        position = [ position, max(meas_raw(k).freq(j).current(i).position)];
    end  
    meas_raw(k).freq(j).cum_curve(1).position = position;
end
%% Neukurve in Array stecken
    position = [];
    current =[];
    psi = [];
for k = 1: size(meas_raw,2)
     position = [meas_raw(k).freq(j).cum_curve(1).position(1),meas_raw(k).freq(j).cum_curve(1).position,position];
     current =[0,meas_raw(k).freq(j).cum_curve(1).current,current];
     psi = [0,meas_raw(k).freq(j).cum_curve(1).psi,psi];
end
%% Anhysteritische Kurve
nmbr_samples = 75;
POSITION = [];
CURRENT = []; 
PSI = [];
j=1;
idx_res=3;
for k = 1:size(meas_raw,2)

    i_max_dim = size(meas_raw(k).freq(j).current,2);
    idx_res = 3;
    
    current_grid = meas_raw(k).freq(j).current(i_max_dim).results(idx_res).flux_curve.current_grid_sm;
    psi_grid = meas_raw(k).freq(j).current(i_max_dim).results(idx_res).flux_curve.psi_mean_sm;
    
    this_position = meas_raw(k).freq(j).current(i_max_dim).position;
    pos_max = max(this_position);
    pos_min = min(this_position);
    
    psi_grid_grad = smoothdata(gradient(psi_grid),'gaussian',1000);
    [~,idx] = max(psi_grid_grad);
    % get data points for negative part
    [psi_sort,idx_sort] = sort(psi_grid_grad(1:idx));
    current_sort =  current_grid(1:idx);
    current_sort = current_sort(idx_sort);
    [psi_sort, ia ] = unique(psi_sort);
    current_sort = current_sort(ia);
    current_grid_new_negative = interp1(psi_sort,current_sort,linspace(min(psi_grid_grad),max(psi_grid_grad),nmbr_samples));
    psi_grid_new_negative = interp1(current_grid,psi_grid,current_grid_new_negative);
    % get data points for positive part   
     [psi_sort,idx_sort] = sort(psi_grid_grad(idx:end));
    current_sort =  current_grid(idx:end);
    current_sort = current_sort(idx_sort);
    [psi_sort, ia ] = unique(psi_sort);
    current_sort = current_sort(ia);
    
    
    current_grid_new_positive = interp1(psi_sort,current_sort,linspace(max(psi_grid_grad),min(psi_grid_grad),nmbr_samples),'linear');
    psi_grid_new_positive = interp1(current_grid,psi_grid,current_grid_new_positive,'linear');
    
    % set up the new vecotrs
    
	current_grid_reduced = [current_grid_new_negative,current_grid_new_positive];
    psi_grid_reduced =   [psi_grid_new_negative,psi_grid_new_positive];
    position = interp1([0, max(current_grid)],[  pos_min ,pos_max],abs(current_grid_reduced),'linear');
    
    % create position vector
    POSITION = [POSITION,position];
    CURRENT = [CURRENT, current_grid_reduced];
    PSI = [PSI, psi_grid_reduced ];
end
x = abs(CURRENT);
y = abs(POSITION);
z = abs(PSI);
[xData, yData, zData] = prepareSurfaceData( x, y, z );
% Set up fittype and options.
ft = fittype( 'poly53' );
opts = fitoptions( 'Method', 'LinearLeastSquares' );
opts.Lower = [0 -Inf 0 -Inf -Inf 0 -Inf -Inf -Inf 0 -Inf -Inf -Inf -Inf -Inf -Inf -Inf -Inf];
opts.Robust = 'Bisquare';
opts.Upper = [0 Inf 0 Inf Inf 0 Inf Inf Inf 0 Inf Inf Inf Inf Inf Inf Inf Inf];

% Fit model to data.
[poly_psi_an_max, gof] = fit( [xData, yData], zData, ft, opts );

% Create a figure for the plots.
figure( 'Name', 'Fit PSI An' );

% Plot fit with data.
subplot( 2, 1, 1 );
h = plot( poly_psi_an_max, [xData, yData], zData );
legend( h, 'untitled fit 1', 'z vs. x, y', 'Location', 'NorthEast', 'Interpreter', 'none' );
% Label axes
xlabel( 'current [A]', 'Interpreter', 'none' );
ylabel( 'position [mm]', 'Interpreter', 'none' );
zlabel( 'flux linkage ', 'Interpreter', 'none' );
grid on
view( -2.6, 3.7 );

% Plot residuals.
subplot( 2, 1, 2 );
h = plot( poly_psi_an_max, [xData, yData], zData, 'Style', 'Residual' );
legend( h, 'untitled fit 1 - residuals', 'Location', 'NorthEast', 'Interpreter', 'none' );
% Label axes
xlabel( 'x', 'Interpreter', 'none' );
ylabel( 'y', 'Interpreter', 'none' );
zlabel( 'z', 'Interpreter', 'none' );
grid on
view( -2.6, 3.7 );

save('poly_psi_an_max.mat','poly_psi_an_max','x','y','z')
    %%
%     this_position = meas_raw(k).freq(j).current(i_max_dim).position;
%     [~,idx]=min(abs(current_grid));
%     interp_pos = [min(this_position),max(this_position)];
%     position = interp1([0,max(current_grid)],interp_pos,linsa
%     
% end   
    
    
%%
function c_meas_tmp_up_zero =  currentZeroCrossing(c_meas_tmp_up,psi_meas_tmp_up)
             
                [~, idx_min_psi_positiv] = find(psi_meas_tmp_up>0,1,'first');
        
                if psi_meas_tmp_up(idx_min_psi_positiv-1)>psi_meas_tmp_up(idx_min_psi_positiv)
                   error(['This algorithm requires psi_meas_tmp_up(idx_min_psi_positiv-1) to be smaller than psi_meas_tmp_up(idx_min_psi_positiv)!', ...
                           'Ensure current and psi to be in ascending  order, otherwise input them as (-current,-psi) and multiply output by -1']); 
                end

                m = (psi_meas_tmp_up(idx_min_psi_positiv)-psi_meas_tmp_up(idx_min_psi_positiv-1))/...
                    (c_meas_tmp_up(idx_min_psi_positiv)-c_meas_tmp_up(idx_min_psi_positiv-1));

                c_meas_tmp_up_zero = -psi_meas_tmp_up(idx_min_psi_positiv)/m+c_meas_tmp_up(idx_min_psi_positiv);

end
    
function psi_meas_tmp_up_zero =  PsiZeroCrossing(c_meas_tmp_up,psi_meas_tmp_up)
             
                [~, idx_min_curr_positiv] = find(c_meas_tmp_up>0,1,'first');
        
                if c_meas_tmp_up(idx_min_curr_positiv-1)>c_meas_tmp_up(idx_min_curr_positiv)
                   error(['This algorithm requires psi_meas_tmp_up(idx_min_psi_positiv-1) to be smaller than psi_meas_tmp_up(idx_min_psi_positiv)!', ...
                           'Ensure current and psi to be in ascending  order, otherwise input them as (-current,-psi) and multiply output by -1']); 
                end

                m = (psi_meas_tmp_up(idx_min_curr_positiv)-psi_meas_tmp_up(idx_min_curr_positiv-1))/...
                    (c_meas_tmp_up(idx_min_curr_positiv)-c_meas_tmp_up(idx_min_curr_positiv-1));

                psi_meas_tmp_up_zero = +psi_meas_tmp_up(idx_min_curr_positiv)-m*c_meas_tmp_up(idx_min_curr_positiv);

end
    