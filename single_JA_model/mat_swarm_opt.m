        



        call_optimJa = @(param_opt) optimJA2(M_meas,H_meas,M_sim,nmbr_sets,param,param_opt);
         
%         fun = @(bestParam) call_optimJa(HmeasT,BmeasT,bestParam);
tic

        lb = [0.001,0.001,0.001,0.001,0.001];
        ub = [1.5,1.5,1.5,1.5,5];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',6000,'PlotFcn',@pswplotbestf);
        [x,fval,exitflag,output] = particleswarm(call_optimJa,5,lb,ub,options);
toc
        
        %%
        
        
        tic      
        [M_sim_out,sse] = evalJaDatasetMAT(M_meas,H_meas,M_sim,nmbr_sets,param.*bestParam)  
        toc
        
        figure
    for i = 1:4
        hold on
        plot(H_meas{i}(:),M_sim_out{i}(:),'-r');
           plot(H_meas{i}(:),M_meas{i}(:),'-k');
    end
 