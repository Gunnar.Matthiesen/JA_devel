function fit_JA_model_devel(CURR_meas,PSI_meas,CRR_an,PSI_an)


% use the eanhysteretic curve of largest set for inital estimation of
% ja_Ms,ja_a,ja_alpha, fsc_psi and fsc_curr.



%% collect all anhyteretic curves
    CRR_an_collected = [];
    PSI_an_collected = [];
    for i =1:size(PSI_an,2)
        PSI_an_collected  = [PSI_an_collected , PSI_an{i}];
        CRR_an_collected  = [CRR_an_collected , CRR_an{i}];
    end
% setup optimization

%%
     % prepare initial vecotr
     % scale I and PSI       
     fsc_psi_opt_start     = 1;
     fsc_curr_opt_start    = 1;

     HE_start = fsc_curr_opt_start*CRR_an_collected ;         
     M_an_start = fsc_psi_opt_start*PSI_an_collected;
     
%      x = HE_start
%       y = M_an_start
     start = [0.5*fsc_psi_opt_start,fsc_curr_opt_start*0.06];
     % applying initial fsc_psi and fsc_curr
%     [Ms_start,a_start]= estimate_Ms_n(HE_start,M_an_start,start);
    Ms_start = 0.4931;
    a_start = 0.0662;
% 
% 
%      param_start;   
%     
%      @optimJA param_start
%      
%     % for measurement run the calculation  
%      nmbr_sets = size(PSI_meas,2);

        param(1) = Ms_start; % ja_Ms
        param(2) = a_start; %ja_a
        param(3) = 2e-7*2; % ja_alpha
        param(4) = 5; % ja_k
        param(5) = 0.9; %ja_c
%         param(6) = 1; % fsc_psi
%         param(7) = 1; % fsc_curr       
        
        nmbr_sets = size(PSI_meas,2);
        
        sse = optimJA(PSI_meas,CURR_meas,nmbr_sets,param) ;
        
        call_optimJa = @(param) optimJA(PSI_meas,CURR_meas,nmbr_sets,param);
         
        fun = @(bestParam) call_optimJa(HmeasT,BmeasT,bestParam);


        lb = [0.45,0.001,1e-8,0.5,0];
        ub = [0.5,0.01,1e-7,120,1];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',1500,'PlotFcn',@pswplotbestf);
        bestParam = particleswarm(call_optimJa,5,lb,ub,options);



        plotresults(PSI_meas,CURR_meas,nmbr_sets,param)

    function sse_sum = optimJA(PSI_meas,CURR_meas,nmbr_sets,param)      

        % unpack parameter set    
        my0 = 4.*pi.*1e-7;
        ja_Ms = param(1);
        ja_a =  param(2);
        ja_alpha  = param(3);
        ja_k  = param(4);
        ja_c  = param(5);
        fsc_psi     = 1;%param(6);
        fsc_curr    = 1;%param(7);
        
        H_sim =  cell(size(PSI_meas));
        M_sim =  cell(size(PSI_meas));
        sse =  size(PSI_meas,2);
        sse_sum = 0;
        
        for ii = 1:nmbr_sets             
            HE_meas = fsc_curr*CURR_meas{ii};  
            M0 = PSI_meas{1}(1);

%              [H_sim{ii},M_sim{ii}] = integrate_JA_loop(HE_meas(1),HE_meas(end),M0);  
            [M_sim{ii}] = integrate_JA_loop_discr(HE_meas,M0); 
%              plot(HE_meas,M_sim{ii})
%              hold on
            sse(ii) = sum((fsc_psi*PSI_meas{ii} - M_sim{ii}).^2);     
            sse_sum = sse_sum + sse(ii);
        end

        
        function M_sim_loc = integrate_JA_loop_discr(HE_meas,M0)
            if HE_meas(1)<HE_meas(end)
                delta = 1; 
            elseif HE_meas(1)==HE_meas(end)
                error(" H_start equals H_end");
            else
                delta=-1; 
            end
            
            M_sim_loc = zeros(size(HE_meas));
            M_sim_loc(1) = M0;
            
            for k = 1:length(HE_meas)-1                  
                He = HE_meas(k)+ja_alpha*M_sim_loc(k);    
                M_an = ja_Ms*(coth(He/ja_a)-ja_a/He);
                
                He_up = HE_meas(k)+1e-6+ja_alpha*M_sim_loc(k);
                He_low = HE_meas(k)-1e-6+ja_alpha*M_sim_loc(k);
                dMan_dH = ( ja_Ms*(coth(He_up/ja_a)-ja_a/He_up) -  ja_Ms*(coth(He_low/ja_a)-ja_a/He_low))./2e-6;         
                
                dM_dH = (1/(1+ ja_c)) * ( M_an - M_sim_loc(k)) /(   ja_k*delta/my0 - ja_alpha*(M_an - M_sim_loc(k))) + ja_c/(1+ja_c)*dMan_dH;
                
                M_sim_loc(k+1)= M_sim_loc(k) + dM_dH*(HE_meas(k+1)-HE_meas(k));
            end
            
        end
        

    
        % calc single slope of hysteresis loop    
        function [H,M] = integrate_JA_loop(Hstart,Hend,M0)
            if Hstart<Hend
                delta = 1; 
            elseif Hstart==Hend
                error(" H_start equals H_end");
            else
                delta=-1; 
            end    

            % J-A model's ode
            He =@(H,M) H+ja_alpha*M;
            M_an = @(H,M) ja_Ms*(coth(He(H,M)/ja_a)-ja_a/He(H,M));            
%             dMan_dH = @(H,M) ja_Ms*(   ja_a/((He(H,M))^2) -   (1/((sin((He(H,M))/ja_alpha))^2))/ja_a );
            
            dMan_dH=@(H,M)(M_an(H+1e-6,M) - M_an(H-1e-6,M))./2e-6;  
            
            dM_dH = @(H,M) (1/(1+ ja_c)) * ( M_an(H,M) - M) /(   ja_k*delta/my0 - ja_alpha*(M_an(H,M) - M)) + ja_c/(1+ja_c)*dMan_dH(H,M);
            options=odeset('RelTol',1e-4,'AbsTol',1e-6,'MaxStep',abs(Hend-Hstart)./10,'InitialStep',(Hend-Hstart)./100);   

            
            M(n+1)=M(n)+dM_dB*dH(n);
            H(n+1)=(B(n+1)/Mu0)-M(n+1);

            
            [H,M] = ode45(dM_dH,[Hstart Hend],M0,options);
        end
        
        


    end

    function plotresults(PSI_meas,CURR_meas,nmbr_sets,param) 
             % unpack parameter set
             figure
        my0 = 1;
        ja_Ms = param(1);
        ja_a =  param(2);
        ja_alpha  = param(3);
        ja_k  = param(4);
        ja_c  = param(5);
        fsc_psi     = 1;%param(6);
        fsc_curr    = 1;%param(7);
        

        for iii = 1:nmbr_sets             
            HE_meas_pl = fsc_curr*CURR_meas{iii};  
            M0_pl = PSI_meas{iii}(1);
            [H_sim_pl,M_sim_pl] = integrate_JA_loop(HE_meas_pl(1),HE_meas_pl(end),M0_pl);  
            plot(H_sim_pl,M_sim_pl);
            hold on;
            plot(CURR_meas{iii},PSI_meas{iii},'-k');

        end
        
        function [H,M] = integrate_JA_loop(Hstart,Hend,M0)
            if Hstart<Hend
                delta = 1; 
            elseif Hstart==Hend
                error(" H_start equals H_end");
            else
                delta=-1; 
            end    

            % J-A model's ode
            He =@(H,M) H+ja_alpha*M;
            M_an = @(H,M) ja_Ms*(coth(He(H,M)/ja_a)-ja_a/He(H,M));            
%             dMan_dH = @(H,M) ja_Ms*(   ja_a/((He(H,M))^2) -   (1/((sin((He(H,M))/ja_alpha))^2))/ja_a );
            
            dMan_dH=@(H,M)(M_an(H+1e-6,M) - M_an(H-1e-6,M))./2e-6;  
            
            dM_dH = @(H,M) (1/(1+ ja_c)) * ( M_an(H,M) - M) /(   ja_k*delta/my0 - ja_alpha*(M_an(H,M) - M)) + ja_c/(1+ja_c)*dMan_dH(H,M);
            options=odeset('RelTol',1e-4,'AbsTol',1e-6,'MaxStep',abs(Hend-Hstart)./10,'InitialStep',(Hend-Hstart)./100);   

            [H,M] = ode45(dM_dH,[Hstart Hend],M0,options);
        end
 
    end

    % calc inital anhysteretic curve
    function [Ms,a]= estimate_Ms_n(HE,M_an,start)
        % Set up fittype and options.
        ft = fittype( 'Ms*(coth(x/a)-a/x)', 'independent', 'x', 'dependent', 'y' );
        opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
        opts.Algorithm = 'Levenberg-Marquardt';
        opts.Display = 'Off';
        opts.Robust = 'LAR';
        opts.StartPoint = [start(2) start(1)];
        % Fit model to data
        [fitresult, gof] = fit( double(HE'), double(M_an'), ft, opts );
        % Plot fit with data.
        % h = plot( fitresult, HE, PSI_an );

        Ms = fitresult.Ms;
        a  = fitresult.a;
    end

end

