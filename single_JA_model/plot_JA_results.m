 function plot_JA_results(PSI_meas,CURR_meas,param) 
             % unpack parameter set
             
            my0 = 1;
            ja_Ms = param(1);
            ja_a =  param(2);
            ja_alpha  = param(3);
            ja_k  = param(4);
            ja_c  = param(5);
            fsc_psi     = param(6);
            fsc_curr    = param(7);

        figure
        hold on
        
        for i = 1:size(  PSI_meas,2)           
            HE_meas_pl = fsc_curr*CURR_meas{i};  
            M0_pl = PSI_meas{i}(1);
           
            [M_sim_pl] =  integrate_JA_loop_discr(HE_meas_pl,M0_pl,param); 

            plot(HE_meas_pl*fsc_curr,M_sim_pl*fsc_psi);
            hold on;
            plot(CURR_meas{i},PSI_meas{i},'-k');

        end
 end