function M_sim = integrate_JA_loop_discr(H_in,M_sim,M0_in,param)

        my0 = 1;%4.*pi.*1e-7;
        ja_Ms = param(1);
        ja_a =  param(2);
        ja_alpha  = param(3);
        ja_k  = param(4);
        ja_c  = param(5);
        fsc_psi     =1;% param(6);
        fsc_curr    =1;% param(7);
        
        HE_meas=H_in*fsc_curr;

        if HE_meas(1)<HE_meas(end)
            delta = 1; 
        elseif HE_meas(1)==HE_meas(end)
            error(" H_start equals H_end");
        else
            delta=-1; 
        end

%         M_sim = zeros(size(HE_meas));
         M_sim(1) = M0_in*fsc_psi;

        for k = 1:length(HE_meas)-1                  
            He = HE_meas(k)+ja_alpha*M_sim(k);    
            M_an = ja_Ms*(coth(He/ja_a)-ja_a/He);
            He_up = HE_meas(k) + 1e-6 + ja_alpha*M_sim(k);
            He_low = HE_meas(k) - 1e-6 + ja_alpha*M_sim(k);
            dMan_dH = ( ja_Ms*(coth(He_up/ja_a)-ja_a/He_up) -  ja_Ms*(coth(He_low/ja_a)-ja_a/He_low))./2e-6; 
            dM_dH = (1/(1+ ja_c)) * ( M_an - M_sim(k)) /(   ja_k*delta/my0 - ja_alpha*(M_an - M_sim(k))) + ja_c/(1+ja_c)*dMan_dH;
            M_sim(k+1)= M_sim(k) + dM_dH*(HE_meas(k+1)-HE_meas(k));
        end

end
