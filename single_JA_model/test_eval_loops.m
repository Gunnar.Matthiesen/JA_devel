
idx =1
H_test = [H_meas{idx}{1},H_meas{idx}{2}]

H_test(end+1,1)=H_test(1,2);
H_test(end,2)=H_test(1,1);
M0 = M_meas{idx}{1}(1);

M_test = zeros(size(H_test));


param=JApoint0./JApoint_res;

% param = [34129.0201912656,65987.3529748788,22035.4839487496,41648.5111339774,67342.1063908468];

M_test = evaluate_JA_loops(H_test,M_test,M0,param)


figure
plot(H_test(:),M_test(:));
