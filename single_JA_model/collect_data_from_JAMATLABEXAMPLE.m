


B_meas={};
H_meas ={};

for j = 1:size(BmeasT,2)
    
    
[max_val_B,~]         =   max(BmeasT(:,j));     
[min_val_B,idx_min_B]   =   min(BmeasT(:,j));  
[max_val_H,~]         =   max(HmeasT(:,j));     
[min_val_H,idx_min_H]   =   min(HmeasT(:,j));  


grid_H = linspace(min_val_H,max_val_H,idx_min_H);
% grid_H = linspace(min_val_H,max_val_H,idx_min);

H_meas{j}(:,1)= flip(grid_H);

B_meas{j}(:,1) = interp1(HmeasT(1:idx_min_B,j),BmeasT(1:idx_min_B,j),H_meas{j}(:,1),'cubic')';

H_meas{j}(:,2) = grid_H;

B_meas{j}(:,2) = interp1(HmeasT(idx_min_H:end,j),BmeasT(idx_min_H:end,j),H_meas{j}(:,2),'cubic')'; 
end