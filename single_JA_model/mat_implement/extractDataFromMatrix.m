
% H = HmeasT(:,3);
% M = BmeasT(:,3)/mi0-H;

%%
a= 5.73929836246509;
Ms = 260226.162266034;
alpha = 8.17160489788839e-08;
mi0 = 4.*pi.*1e-7;

plot(H,Mah_iso(a,Ms,H+alpha*M))
%%
clear B_meas H_meas M_meas
for i = 1:4
    [~,idx] = min(HmeasT(:,i));
    B_meas{i}{1} = [BmeasT(1:idx,i)];    
    H_meas{i}{1} = HmeasT(1:idx,i);
    M_meas{i}{1} = BmeasT(1:idx,i)/mi0 - HmeasT(1:idx,i);
    B_meas{i}{2} = BmeasT(idx+1:end,i);    
    H_meas{i}{2} = HmeasT(idx+1:end,i);  
    M_meas{i}{2} = BmeasT(idx+1:end,i)/mi0 - HmeasT(idx+1:end,i)
end


%%
clear B_meas H_meas M_meas
for i = 1:4
    [~,idx] = min(HmeasT(:,i));
    
    
    min_H = min(HmeasT(:,i));
    max_H = max(HmeasT(:,i));
    
    inc_h = (max_H-min_H)/100;
    
    H1 = HmeasT(1:idx,i);
    H_meas{i}{1} = [max_H:-inc_h:min_H]';    
    B_meas{i}{1} = [interp1(H1,BmeasT(1:idx,i),H_meas{i}{1},'PCHIP')];    
    M_meas{i}{1} =  B_meas{i}{1}/mi0 -H_meas{i}{1};
    
    H2 = HmeasT(idx+1:end,i);  
    H_meas{i}{2} = [min_H:inc_h:max_H]';
    B_meas{i}{2} = interp1(H2,BmeasT(idx+1:end,i),H_meas{i}{2},'PCHIP');  
    M_meas{i}{2} =  B_meas{i}{2}/mi0 -H_meas{i}{2};
end

%%

for i = 1:4
   
H_meas{i} = cell2mat(H_meas{i});
B_meas{i} = cell2mat(B_meas{i});
M_meas{i} = cell2mat(M_meas{i});
    
end



%%
% for i = 1:4
%     [~,idx] = min(HmeasT(:,i));
%     B_meas{1+(i-1)*2} = BmeasT(1:idx,i);    
%     H_meas{1+(i-1)*2} = HmeasT(1:idx,i);
%     M_meas{1+(i-1)*2} = B_meas{1+(i-1)*2}/mi0 - HmeasT(1:idx,i);
%     B_meas{2+(i-1)*2} = BmeasT(idx+1:end,i);    
%     H_meas{2+(i-1)*2} = HmeasT(idx+1:end,i);  
%     M_meas{2+(i-1)*2} = B_meas{2+(i-1)*2}/mi0 - HmeasT(idx+1:end,i)
% end