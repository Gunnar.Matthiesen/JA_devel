classdef raw_flux_curve_measurement < handle
      
   properties(GetAccess =   public)
        time =[];
        current=[];
        voltage=[];
        current_input=[];
        time_position=[];
        position=[];
        h_plot_psi_curves =[];
        h_plot_u_ind_curves = [];
        h_plot_u_ind_psi_curves = [];
        h_plot_psi_curr_ind_curves = [];
        i_dot = [];
        inputSignal = [];
   end
   
%     properties(GetAccess =   private)
%         h_plot_psi_curves =[];
%     end
    
    properties
        remove_offset_flag = true;
        compensate_drift_flag = true;
%         daq_Rate;
%         T_meas_resistance_start;
%         i_max;
%         T_meas_psi_sgnl_ramp; 
%         nmbr_cycles ;
%         i_meas_current;
%         ratio_T_meas_current_high_start ;
%         ratio_T_break_start;
%         T_meas_resistanc_end;
%         Duration = [];
%         Type = [];
        index_ramps;
        
        % Here is the example Mapping to plot the flux linkage curves
        % We always need the increasing and decreasing curve
        % 1. current_increase 2. psi_increase 3. current_decrease 4. psi_decrease 5. psi_mean
        plot_mappings = {'current_increase','psi_increase','current_decrease','psi_decrease','current_grid_sm','psi_mean_sm','h_plot_psi_curves';
                          'current_increase','voltage_ind_increase','current_decrease','voltage_ind_decrease','current_grid_sm','voltage_ind_mean','h_plot_u_ind_curves';
                          'voltage_ind_increase','psi_increase','voltage_ind_decrease','psi_decrease','voltage_ind_mean','psi_mean_sm','h_plot_u_ind_psi_curves';                          
                          'psi_mean_sm','current_dissp','psi_mean_sm','current_dissp','','','h_plot_psi_curr_ind_curves'};
        
        plot_lable_mappings = {'Flux Linkage Curves','Current [A]','Flux Linkage [Wb]';
                                'Induced Voltage','Current [A]','Voltage [V]';
                                'Induced Voltage and Flux Linkage','Voltage [V]','Flux Linkage [Wb]';
                                'Flux Linkage and Dissipating Current','Flux Linkage [Wb]','Dissipating Current[A]'};
        
        % extract full dataset for voltage, current, force and position
        voltage_ind;
        psi;
        results;
        R_valve_end;
        R_valve_start;
        R_vec;
        
        mcc_wiring = {'current',1;   % do not change names only the ports 1 to 4
                      'voltage',2;
                      'current_input',3};       

        
        DataMapping = [ "daq_Rate","daq_Rate", "4000";   % {name in this object, name in base workspace, default value'}
                        "T_meas_resistance_start","T_meas_resistance_start", "1";...
                        "T_meas_psi_sgnl_ramp","T_meas_psi_sgnl_ramp", "0.3";...
                        "nmbr_cycles","nmbr_cycles" ,"4";...
                        "ratio_T_meas_current_high_start" ,"ratio_T_meas_current_high_start","0.4" ;...
                        "ratio_T_break_start","ratio_T_break_start","0.3";...
                        "T_meas_resistanc_end","T_meas_resistanc_end","1";...
                        "T_meas_psi_sgnl_ramp","T_meas_psi_sgnl_ramp","0.3";...
                        "i_meas_current","i_meas_current","0.2";...
                        "i_max","i_max","0.7";...
                        "ratio_T_meas_current_high_start","ratio_T_meas_current_high_start","0.4";...
                        "T_meas_resistance_start","T_meas_resistance_start","1"]; 
          
        
    end
    
    methods  
    % CONSTRUCTOR:    
        function [obj,varargout] = raw_flux_curve_measurement(varargin) 

        %ADD CHECKS HERE

        %ADD PROCESSING OF INPUTS HERE
        % if not supplied, try grab the data from the base workspace
%             obj = obj.grabDatafromWorkspace();


        %ADD CHECKS HERE
            switch(nargin)

            case 0 % if no signal shall be generated
                obj.inputSignal = inputSignal();
                varargout ={[]};
            case 1                
                signalOptions = varargin{1};                
                if isa(signalOptions, 'inputSignalOptions') 
                   [obj.inputSignal,output_signal,output_duration] = inputSignal(signalOptions); 
                   varargout{1} = output_signal;
                   varargout{2} = output_duration;
                else
                    obj.inputSignal = inputSignal(); 
                    varargout = {[]};
                end
                % check if the supplied object is an inputsignaloptions
                % object                 
            otherwise
                varargout ={[]};
            end
            
        end

    end
    
  
    methods (Access = public)              
        
        function obj = plot_psi(varargin)
            obj = varargin{1};
            idx_plot_type_mapping = 1;
            [i_plt,h_axes]= obj.plot_results_parse(varargin);           
            obj = obj.plotThisResults(i_plt,h_axes,idx_plot_type_mapping);            
        end
        
        function obj = plot_voltage_ind(varargin)
            obj = varargin{1};
            idx_plot_type_mapping = 2;
            [i_plt,h_axes]= obj.plot_results_parse(varargin);           
            obj = obj.plotThisResults(i_plt,h_axes,idx_plot_type_mapping);            
        end
        
        function obj = plot_voltage_ind_dissp_current(varargin)
            obj = varargin{1};
            idx_plot_type_mapping = 5;
            [i_plt,h_axes]= obj.plot_results_parse(varargin);           
            obj = obj.plotThisResults(i_plt,h_axes,idx_plot_type_mapping);            
        end
        
        function obj = plot_voltage_ind_psi(varargin)
            obj = varargin{1};
            idx_plot_type_mapping = 3;
            [i_plt,h_axes]= obj.plot_results_parse(varargin);           
            obj = obj.plotThisResults(i_plt,h_axes,idx_plot_type_mapping);            
        end
        
        function obj = plot_dissp_current(varargin)
            obj = varargin{1};
            idx_plot_type_mapping = 4;
            [i_plt,h_axes]= obj.plot_results_parse(varargin);           
            obj = obj.plotThisResults(i_plt,h_axes,idx_plot_type_mapping);
            snapnow;
        end        
        
        function obj = plotThisResults(obj,i_plt,h_axes,idx_plot_type_mapping)   
            % DIRTY IMPLEMENTATION without input parser
            % SECOND INPUT CAN BE HANDLE TO AXES SYSTEM
            % LAST INPUT MUST BE VECTOR 1x2 for fluxes curves 2 plot
                                    
          
% %             flg_plot_internal = true;
% % %             idx_mapping = varargin{2};
            
                        
            % Execute plot
            if isempty(h_axes)
                plot2internalFig();   
            else                
                plot2externalFig(); % this is the handle to the external figure
            end
            
            %----------supporting nested functions -----------------
            function plot2externalFig()
                h_axes.NextPlot = 'add';
                plot2figure(false,false,false);   
            end
            
            function plot2internalFig()
                
                h_axes = obj.(obj.plot_mappings{idx_plot_type_mapping,7});    
                
                if isempty(h_axes)
                   flg_new_fig = true;                
                else
                    % chick if figure still exists
                    if ishandle(h_axes)
                        flg_new_fig = false;
                    else
                        flg_new_fig = true;
                    end

                end

                if flg_new_fig
                   hf = figure('name', obj.plot_lable_mappings{idx_plot_type_mapping,1});
                   h_axes = axes(hf);
                   obj.(obj.plot_mappings{idx_plot_type_mapping,7}) = h_axes;
                   xlabel(obj.plot_lable_mappings{idx_plot_type_mapping,2});
                   ylabel(obj.plot_lable_mappings{idx_plot_type_mapping,3})
                   grid on;
                   grid minor;
                   hold on;
                   box on;
                   set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
                   set(groot, 'defaultTextInterpreter','latex'); 
                   set(groot, 'defaultLegendInterpreter','latex');
                   title(['Position: ',num2str(mean(obj.position)),' mm']);
                else
                   delete(findobj(h_axes, 'type', 'Line'));
                   h_axes.NextPlot = 'add';

                end 
                plot2figure(true,true,true);   
            end
            
            function plot2figure(flag_legend,flag_resize,flag_title)               
                           
                legend_txt= {};  
                y_max_val = 0;
                y_min_val = 0;
                x_max_val = 0;
                x_min_val = 0;
                if flag_title == true
                    h_axes.Title.String = ['Position: ',num2str(mean(obj.position)),' mm'];
                end
                count_i_legend = 0;
                
                for i = i_plt  
                    count_i_legend =count_i_legend+1;
                    h_this_curve = obj.results(i).flux_curve;                    
                    x_max_val = max([ x_max_val,h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,1}),h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,3})]);
                    x_min_val = min([ x_min_val,h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,1}),h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,3})]);
                    y_max_val = max([ y_max_val,h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,2}),h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,4})]);
                    y_min_val = min([ y_min_val,h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,2}),h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,4})]);
                    hp = plot(h_axes,h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,1}),h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,2}));
                    if flag_legend == false
                        hp.Annotation.LegendInformation.IconDisplayStyle = 'off';    
                    end
                    hp = plot(h_axes,h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,3}),h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,4}),'Color',hp.Color);
                    hp.Annotation.LegendInformation.IconDisplayStyle = 'off';
                    if ~isempty(obj.plot_mappings{idx_plot_type_mapping,5}) && ~isempty(h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,5})) &&...
                        ~isempty(obj.plot_mappings{idx_plot_type_mapping,6}) && ~isempty(h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,6}))
                        hp = plot(h_axes,h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,5}),h_this_curve.(obj.plot_mappings{idx_plot_type_mapping,6}),'--','Color',hp.Color);
                        hp.Annotation.LegendInformation.IconDisplayStyle = 'off';
                    end
                    legend_txt{count_i_legend}="Loop " + num2str(i);
                end  
                if flag_legend == true
                    legend(h_axes,legend_txt);
                end
                if flag_resize==true
                    xlim([-0.1*ceil(-10*x_min_val),0.1*ceil(10*x_max_val)]);
                    ylim([-0.1*ceil(-10*y_min_val),0.1*ceil(10*y_max_val)]);
                end
            end
                        
        end
        
        function obj = evaluate_measurement(obj)
            
            switch(obj.inputSignal.Type)
                
                case 'ramps'
                    obj = obj.evaluate_ramps_measurement();
                case 'sin'
                    obj = obj.evaluate_sin_measurement();
                case 'const_psi'
                    obj = evaluate_const_psi_measurement(obj);
                otherwise
                    error("For the signal type of this measurement no evaluation is implemented yet");
            end
            
        end
        
       function obj = evaluate_sin_measurement(obj)
            
            if obj.remove_offset_flag == true            
                obj.remove_offset();
            end
            
            % extract resitance begin and end
            obj.extract_resictance_n_sinwaves();            
            obj.calc_psi();
            
            obj.evaluate_psi_sin();
            
        end
        
       function obj = evaluate_ramps_measurement(obj)
            
            if obj.remove_offset_flag == true            
                obj.remove_offset();
            end
            
            % extract resitance begin and end
            obj.extract_resictance_n_ramps();
            obj.calc_psi();
            obj.evaluate_psi_ramps();
            
        end
        
       function obj = evaluate_const_psi_measurement(obj)            
            if obj.remove_offset_flag == true            
                obj.remove_offset();
            end
            
            % extract resitance begin and end
            obj.extract_resictance_n_const_psi_ramps();
            obj.calc_psi();
            obj.evaluate_psi_ramps();
            
       end
   
       function new_input_signal = update_const_psi_run(obj)            
            if obj.remove_offset_flag == true            
                obj.remove_offset();
            end
            
            % extract resitance begin and end
            obj.extract_resictance_n_const_psi_ramps();
            
            % Messbereich soll/ist identifizieren
            obj.calc_psi();
            %du_dt soll  - du_dt ist
            
            % store input signal for next run
            
            % idle full input signal           
            [~,idx,~,prom_crr_ref]=findpeaks(obj.inputSignal.signal_ref_const_psi);            
            idx_peaks_current_input_ref = idx(prom_crr_ref>max(prom_crr_ref)*0.1);            
            idx_peaks_current_input_ref = idx_peaks_current_input_ref(idx_peaks_current_input_ref > obj.inputSignal.T_meas_resistance_start*obj.inputSignal.daq_Rate);
            idx_peaks_current_input_ref = idx_peaks_current_input_ref(1: obj.inputSignal.nmbr_cycles);  
           
            % measured input signal
            [~,idx,~,prom_crr_ipt]=findpeaks(obj.current_input);            
            idx_peaks_current_input = idx(prom_crr_ipt>max(prom_crr_ipt)*0.1);            
            idx_peaks_current_input = idx_peaks_current_input(idx_peaks_current_input > obj.inputSignal.T_meas_resistance_start*obj.inputSignal.daq_Rate);
            idx_peaks_current_input = idx_peaks_current_input(1: obj.inputSignal.nmbr_cycles);            
            
            % measured current
            [~,idx,~,prom_crr]=findpeaks(obj.current);            
            idx_peaks_current = idx(prom_crr>max(prom_crr)*0.1);            
            idx_peaks_current = idx_peaks_current(idx_peaks_current > obj.inputSignal.T_meas_resistance_start*obj.inputSignal.daq_Rate);
            idx_peaks_current = idx_peaks_current(1: obj.inputSignal.nmbr_cycles);
            
            if  obj.inputSignal.nmbr_cycles<3
               error("This update routine is implemented for 3 or more cycles yet"); 
            end

 
%            window = round(mean(diff(idx_peaks_current)));           
%             x_start(1) = double(max(obj.current)/max(obj.inputSignal.signal_ref_const_psi));
%             x_start(2) = 0;
%             x_start(3) = -1;
%             scale_shift_current_input_ref = fmincon(@min_shift_current_input_ref,x_start);
%            function delta =  min_shift_current_input_ref(x)
%             a=x(1);
%             b=x(2);
%             c=round(x(3));
%             delta = sum(abs(double(obj.current(idx_peaks_current(2)-window:idx_peaks_current(2)+window)-...
%                         (obj.inputSignal.signal_ref_const_psi(idx_peaks_current_input_ref(2)-window-c:idx_peaks_current_input_ref(2)+window-c)'*a+b))));
%            end
%             
%             x_start(1) = double(max(obj.current)/max(obj.current_input));
%             x_start(2) = 0;
%             x_start(3) = -1;
%             scale_shift_current_input = fmincon(@min_shift_current_input,x_start);
%            function delta =  min_shift_current_input(x)
%             a=x(1);
%             b=x(2);
%             c=round(x(3));
%             delta = sum(abs(double(obj.current(idx_peaks_current(2)-window:idx_peaks_current(2)+window)-...
%                         (obj.current_input(idx_peaks_current_input(2)-window-c:idx_peaks_current_input(2)+window-c)*a+b))));
%            end 
%            figure
%            plot(obj.inputSignal.signal_ref_const_psi(idx_peaks_current_input_ref(1)-scale_shift_current_input_ref(3):idx_peaks_current_input_ref(3)-scale_shift_current_input_ref(3))*...
%             scale_shift_current_input_ref(1)+scale_shift_current_input_ref(2));   
%            hold on;
%            plot(obj.current_input(idx_peaks_current_input(1)-scale_shift_current_input(3):idx_peaks_current_input(3)-scale_shift_current_input(3))*...
%             scale_shift_current_input(1)+scale_shift_current_input(2));   
%            hold on;           
%            plot(obj.current(idx_peaks_current(1):idx_peaks_current(3)));
            
%             delta_volt_in = obj.inputSignal.signal_ref_voltage_ind_const_psi(1:min_idx)'-obj.voltage_ind(1:min_idx);
           
            % we try to find the sections with differing induced voltage in
            % the signal in order to detect a missalignment between desired
            % input signal an measured output signal.
            
            min_idx = min([length(obj.inputSignal.signal_ref_voltage_ind_const_psi'),length(obj.voltage_ind)]);
%             obj.voltage_ind(1:min_idx); 
            delta = diff(obj.voltage_ind(1:min_idx));
            min_hight = max(obj.inputSignal.signal_ref_voltage_ind_const_psi*0.3);
            
            [~,idx_max,~,~] =findpeaks(delta,'MinPeakHeight',min_hight);
            [~,idx_min,~,~] =findpeaks(-delta,'MinPeakHeight',min_hight);
            idx_peaks = sort([idx_max,idx_min]);
              
            % corrent first estimation
            for i = 1:length(idx_peaks)
                idx_peaks(i) = find_step(obj,idx_peaks(i));
            end
            idx_peak_set_input = find(diff(obj.inputSignal.signal_ref_voltage_ind_const_psi)~=0);
            
            
            
            if ~isequal(idx_peaks,idx_peak_set_input')
                warning("Updating the input signal, there was a missalignment  between input signal and measured sigal!");
            end
%             figure
%             plot(obj.voltage_ind)
%             hold on
%             scatter(idx_peaks,obj.voltage_ind(idx_peaks))
            % because the powersupply needs certain time to invert the
            % voltage we use weights. The error for the inital values will
            % always be large due to the power supply's limitations an the
            % corretion algorithm will result in unreasonable input
            % currents after some iteration loops
            
%%
%             new_signal = zeros(1,idx_peaks(3));
%             for i = 1:3
%              
%                                 
%                 % we apply a svitzky-golay filter to the signal because
%                 % this won't add a delay to the signal an smooth out high
%                 % frequent oscillation
%                 
%                 if i==1
%                     here = 1: idx_peaks(i);                    
%                 else
%                     here = idx_peaks(i-1)+1: idx_peaks(i);
%                 end
%                 
%                 delta_volt_in = obj.inputSignal.signal_ref_voltage_ind_const_psi(here)' - obj.voltage_ind(here);
%                 
%                 delta_volt_in = double(delta_volt_in);
%                 order = 3;
%                 framelen = 165;
%                 delta_volt_in = sgolayfilt(delta_volt_in,order,framelen); 
% 
%                 kp = 0.1;
%                 
%                     if i==1
%                         new_signal(here) = obj.inputSignal.signal_ref_const_psi(here)'+delta_volt_in*kp;
%                     else
% %                         weights = ones(size(delta_volt_in));
% %                         T = 40; %<----- adjust this value to change the sigmoids shape!!
% %                         c = 1/T;
% %                         f = @(x) 2./(1+exp(-c.*(x-T)));
% %                         weights(1:T)=f(1:T);
% 
% 
%                         new_signal(here) = obj.inputSignal.signal_ref_const_psi(here)';
% %                         new_signal(here(1):here(1)+T) =  new_signal(here(1)-1:-1:here(1)-1-T);
%                         new_signal(here) = new_signal(here)+delta_volt_in*kp ;%.*weights;
%                     end
%             end
%                 plot(new_signal)
%%
%             new_signal = zeros(1,idx_peaks(6));
%             for i = 2:3             
%                                 
%                 % we apply a svitzky-golay filter to the signal because
%                 % this won't add a delay to the signal an smooth out high
%                 % frequent oscillation    
%                 here = idx_peaks(i-1)+1: idx_peaks(i);    
%                 delta_volt_in = obj.inputSignal.signal_ref_voltage_ind_const_psi(here)' - obj.voltage_ind(here);                
%                 delta_volt_in = double(delta_volt_in);
% %                 order = 3;
% %                 framelen = 165;
% %                 delta_volt_in = sgolayfilt(delta_volt_in,order,framelen); 
%                  kp = 0.075;
%                 
%                delta_volt_in= smoothdata(delta_volt_in,'gaussian');
% %                plot(delta_volt_in)
% 
% %                         weights = ones(size(delta_volt_in));
% %                         T = 40; %<----- adjust this value to change the sigmoids shape!!
% %                         c = 1/T;
% %                         f = @(x) 2./(1+exp(-c.*(x-T)));
% %                         weights(1:T)=f(1:T);
% 
%                 new_signal(here) = obj.inputSignal.signal_ref_const_psi(here)';
%     %                         new_signal(here(1):here(1)+T) =  new_signal(here(1)-1:-1:here(1)-1-T);
%                 new_signal(here) = new_signal(here)+delta_volt_in*kp ;%.*weights;
%                 dT = 40;
%                 here = [idx_peaks(i-1),idx_peaks(i-1)+dT,idx_peaks(i-1)+dT+1,idx_peaks(i-1)+dT+2];
%                 here_now = idx_peaks(i-1)+1:idx_peaks(i-1)+dT;
%                 new_signal(here_now) = interp1(here,new_signal(here),here_now,'spline');
%             end


%                 new_signal = zeros(1,idx_peaks(6));
                
                % we apply a svitzky-golay filter to the signal because
                % this won't add a delay to the signal an smooth out high
                % frequent oscillation    
                
                shift = round(mean(idx_peaks_current_input_ref-idx_peaks_current'));
                
%                 idx_peaks_current(1)
%
                here = [idx_peaks_current_input_ref(1)+1:idx_peaks_current_input_ref(1)+4000]-shift - obj.index_ramps(1);
                
                delta_volt_in = obj.voltage_ind(here) - ones(size(obj.voltage_ind(here)))*min(obj.inputSignal.signal_ref_voltage_ind_const_psi(here)');
                
                here = [idx_peaks_current_input_ref(1)+4001:idx_peaks_current_input_ref(2)]- shift  - obj.index_ramps(1);
                
                delta_volt_in = [delta_volt_in , obj.voltage_ind(here) - ...
                    ones(size(obj.voltage_ind(here)))*max(obj.inputSignal.signal_ref_voltage_ind_const_psi(here)')];
                
                
%                 delta_volt_in = obj.inputSignal.signal_ref_voltage_ind_const_psi(here)' - obj.voltage_ind(here);                
%                 delta_volt_in = double(delta_volt_in);
%                 order = 3;
%                 framelen = 165;
%                 delta_volt_in = sgolayfilt(delta_volt_in,order,framelen); 

               kp = 0.025;
                
               delta_volt_in = smoothdata(delta_volt_in,'gaussian',300);
               
               
%                plot(delta_volt_in)

%                         weights = ones(size(delta_volt_in));
%                         T = 40; %<----- adjust this value to change the sigmoids shape!!
%                         c = 1/T;
%                         f = @(x) 2./(1+exp(-c.*(x-T)));
%                         weights(1:T)=f(1:T);

                here = idx_peaks_current(1)+1:idx_peaks_current(2);   

                new_signal = obj.inputSignal.signal_ref_const_psi(idx_peaks_current_input_ref(1)+1:idx_peaks_current_input_ref(2))';
%               new_signal(here(1):here(1)+T) =  new_signal(here(1)-1:-1:here(1)-1-T);
                new_signal = new_signal-delta_volt_in*kp ;%.*weights;
                
                new_signal_complete = repmat(new_signal,1,3);
                new_signal_complete =  [flip(new_signal_complete(1:2000)),new_signal_complete];
                
%                 dT = 40;
%                 i=2
%                 here = [idx_peaks(i-1),idx_peaks(i-1)+dT,idx_peaks(i-1)+dT+1,idx_peaks(i-1)+dT+2];
%                 here_now = idx_peaks(i-1)+1:idx_peaks(i-1)+dT;
%                 new_signal(here_now) = interp1(here,new_signal(here),here_now,'spline');
%                 new_signal_complete = repmat(new_signal(here_now),1,3);
%                 new_signal_complete =  [flip(new_signal_complete(1:2000)),new_signal_complete];
%             end


%             
%             new_signal_loop = new_signal(idx_peaks(3)+1:idx_peaks(5));
%             
%             new_signal_complete = repmat(new_signal_loop,1,3);
%             new_signal_complete =  [flip(new_signal_complete(1:2000)),new_signal_complete];

             new_signal_complete(1:100) = interp1([1,100],[0,new_signal_complete(100)],1:100,'spline');

            new_input_signal = obj.inputSignal.addCurrentMeasurement(new_signal_complete);
            
              obj.inputSignal.i_max =new_signal_complete(end);
            
    %%        
            % correct each section 1 to 3
            
            
            
             
            % we repeat the sequence for section 2 and 3 
            
            
            
            
            % for the first part we man the current to ramp up from zero so
            % the correct the signal
            
            
        
    end
 
%             obj.current_input

           function idx_step = find_step(obj,idx_peak)
                span = 50;
                v_ind_loc = obj.voltage_ind(idx_peak-span:idx_peak);
                idx_interp = 1:1:length(v_ind_loc);

                [xData, yData] = prepareCurveData(double(idx_interp),double(v_ind_loc));
                % Set up fittype and options.
                ft = fittype( 'poly1' );
                opts = fitoptions( 'Method', 'LinearLeastSquares' );
                opts.Robust = 'Bisquare';
                % Fit model to data.
                [fitresult, ~] = fit( xData, yData, ft, opts );
                y_fit = feval(fitresult,idx_interp);            
                delta_fit = v_ind_loc-y_fit';            
                idx_fnd=find(abs(delta_fit)<=mean(abs(delta_fit)),1,'last');            
%                 v_ind_loc-feval(fitresult,idx)+mean(abs(v_ind_loc-y_fit'));                         
                idx_step = idx_peak-(span+1)+idx_fnd;
            end

        
    
       
       function obj = loadData(obj,daq_time,daq_data,serial_time,serial_data)
            % this function is for lazy people applying the mapping. You can
            % also pass the data to the properties step by step yourself
            if ~isequal(class(obj),'raw_flux_curve_measurement')
               error('First input must be the object itself and must be of type raw_flux_curve_measurement'); 
            end
            % check dimensions of the input
            if size(daq_data,1)<2
                error("The object daq_data passed has less than " + num2str(size(obj.mcc_wiring,1)) +" channels, inside mcc_wiring there are more declared"); 
            end        
            if size(daq_time,1)>1
                error("The object daq_time passed has more than one channel"); 
            end
            if size(serial_time,1)>1
                error("The object serial_time passed has more than one channel"); 
            end
            if size(serial_data,1)>1
                error("The object serial_data passed has more than one channel"); 
            end        
            % copy the data
            for i = 1:size(obj.mcc_wiring,1)            
                try 
                    obj.(obj.mcc_wiring{i,1}) = daq_data(obj.mcc_wiring{i,2},:);
                catch
                    error(['The object ', obj.mcc_wiring{i,1}, 'does not exist in this object']) 
                end
            end
             obj.time = daq_time;
             obj.time_position = serial_time;
             obj.position = serial_data; 
       end   
    
       function       plot_output_vs_input(obj)
            
            signalData_unscaled = obj.inputSignal.gen_InputSignal;
            figure            
            plot(signalData_unscaled(obj.inputSignal.idx_signal_start:end));
            hold on;
            plot(obj.current(1,obj.index_ramps(1):end));
            legend('signal','meas');  
            grid on
            
        end 
        
    end % end Methods(Access = public)    
    
    methods  (Access = private)
        
        function [i_plt,h_axes]= plot_results_parse(obj,args_passed)
            if size(args_passed,2)>4
                error("too many input arguments");
            end            
            nargin =length(args_passed);               
            if nargin>0 && isnumeric(args_passed{end})              
                
                if length(args_passed{end})==1
                    i_plt  = args_passed{end};
                else                
                    i_plt  = min([args_passed{end}]):max([args_passed{end}]);    
                    if i_plt(1)<1
                        i_plt(1) = 1;
                    end                
                    if i_plt(2)>size(obj.results,2)
                       i_plt(2) = size(obj.results,2);
                    end
                end
                
            else
                i_plt = 1:size(obj.results,2);
            end 
            
            if nargin>2 && isa(args_passed{2},'matlab.graphics.axis.Axes')
               h_axes = args_passed{2};
                % plot2externalFig(varargin{2});   
            else
               h_axes = [];
                %plot2internalFig();
            end
        end
       
        function obj = evaluate_psi_ramps(obj)

            % here find peaks is used
            [max_psi,idx_max_psi,~,prom_max_psi] =  findpeaks(obj.psi,'MinPeakDistance',round(obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*4*0.9));
            [min_psi,idx_min_psi,~,prom_min_psi] =  findpeaks(-obj.psi,'MinPeakDistance',round(obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*4*0.9));
             min_psi = -min_psi;
            [max_curr,idx_max_current,~,prom_max_current] = findpeaks(obj.current(obj.index_ramps),'MinPeakDistance',round(obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*4*0.9));
            [min_curr,idx_min_current,~,prom_min_current] = findpeaks(-obj.current(obj.index_ramps),'MinPeakDistance',round(obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*4*0.9));
            
            
            %#########################################
            %######### ADDED PROMINENCE TO FIND OUTLINERS
            %###########################################
            %# NOT TESTED MUCH!!!!            
            max_psi_idx_remove = prom_max_psi/mean(prom_max_psi)<0.1;
            max_psi(max_psi_idx_remove)=[];
            idx_max_psi(max_psi_idx_remove)=[];
            min_psi_idx_remove =prom_min_psi/mean(prom_min_psi)<0.1;
            min_psi(min_psi_idx_remove)=[];
            idx_min_psi(min_psi_idx_remove)=[];
            max_curr_idx_remove = prom_max_current/mean(prom_max_current)<0.1;
            max_curr(max_curr_idx_remove)=[];
            idx_max_current(max_curr_idx_remove)=[];
            min_curr_idx_remove = prom_min_current/mean(prom_min_current)<0.1;
            min_curr(min_curr_idx_remove)=[];
            idx_min_current(min_curr_idx_remove)=[];
            
            
            % somtimes the slope is deteced aswell, then the last max current is excluded
            if length(idx_max_psi)>length(idx_min_psi) % the min index is used for detection because it is always right
                idx_max_current(end) = [];
            end
            
            if length(idx_max_current)>length(idx_min_current) % the min index is used for detection because it is always right
                idx_max_current(end) = [];
            end
            min_curr = -min_curr;

            if ( mean(length(idx_max_psi),length(idx_min_psi)) ~= obj.inputSignal.nmbr_cycles  ) || (mean(length(idx_max_current),length(idx_min_current)) ~= obj.inputSignal.nmbr_cycles  ) 
                error("get_point_max_psi_n_offset: the number of detected closed loops differs from" + num2str(obj.inputSignal.nmbr_cycles));            
            end
            

            if obj.compensate_drift_flag ==true                
                % ADD CHECK FOR SUFFICIENT LOOPS HERE
                % calculation of drift's mean and std only for more than 2 full loop possible                    
                psi_at_max_current = obj.psi(idx_max_current);
                psi_at_min_current = obj.psi(idx_min_current);                
                psi_drift_max2max_mean = mean(diff(psi_at_max_current));
                psi_drift_min2min_mean = mean(diff(psi_at_min_current));
                psi_drift_mean = (psi_drift_max2max_mean+psi_drift_min2min_mean)/2;
                psi_drift_mean_per_step = psi_drift_mean/(mean(diff(idx_max_current)));
                psi_drift_max2max_std = std(diff(psi_at_max_current));
                psi_drift_min2min_std = std(psi_at_min_current);
                max_psi_drift_compensated = psi_at_max_current - psi_drift_mean*[0:length(psi_at_max_current)-1] ; 
                min_psi_drift_compensated = psi_at_min_current - psi_drift_mean*[0.5:length(psi_at_min_current)-0.5] ; 

                psi_offset =  mean(max_psi_drift_compensated + min_psi_drift_compensated)/2;
            end
                     
            psi_idx_offset = obj.index_ramps(1)-1; % needed because obj.psi and obj.current differ in size
            sum_idx_drift = 0;
        
            for i = 1:length(idx_max_current)-1
                
             %create new empty flux_curve object                
             obj.results(i).flux_curve =flux_curve(1/obj.inputSignal.daq_Rate,[],[]);
             
             %create handle
             h_this_curve = obj.results(i).flux_curve;             
%              h_this_curve.psi_decrease = obj.psi(idx_max_current(i):idx_min_current(i));
%              h_this_curve.voltage_ind_decrease = obj.voltage_ind(idx_max_current(i):idx_min_current(i));
             psi_decrease_tmp = obj.psi(idx_max_current(i):idx_min_current(i));
%              voltage_ind_decrease_tmp = obj.current(psi_idx_offset + (idx_max_current(i):idx_min_current(i)));
             voltage_ind_decrease_tmp = obj.voltage_ind(idx_max_current(i):idx_min_current(i));
             if obj.compensate_drift_flag ==true
                %add the drift
%              h_this_curve.psi_decrease = h_this_curve.psi_decrease -psi_offset  -(sum_idx_drift + [1:(idx_min_current(i)-idx_max_current(i)+1)])*psi_drift_mean_per_step;              
                psi_decrease_tmp = psi_decrease_tmp -psi_offset  -(sum_idx_drift + [1:(idx_min_current(i)-idx_max_current(i)+1)])*psi_drift_mean_per_step;                
                  sum_idx_dirft_last_curve_decrease = (idx_min_current(i)-idx_max_current(i)+1);
             end
              % now set it to the object
              h_this_curve.psi_decrease = psi_decrease_tmp;
              h_this_curve.voltage_ind_decrease = voltage_ind_decrease_tmp;
              h_this_curve.current_decrease = obj.current(psi_idx_offset + (idx_max_current(i):idx_min_current(i)));
              %###
              
             if i <=length(idx_max_current)-1
%              	  h_this_curve.psi_increase  = obj.psi(idx_min_current(i):idx_max_current(i+1));
%                 h_this_curve.current_increase = obj.current(psi_idx_offset + (idx_min_current(i):idx_max_current(i+1)));
%                 h_this_curve.voltage_ind_increase = obj.voltage_ind((idx_min_current(i):idx_max_current(i+1))); 
                psi_increase_tmp  = obj.psi(idx_min_current(i):idx_max_current(i+1));
                current_increase_tmp = obj.current(psi_idx_offset + (idx_min_current(i):idx_max_current(i+1)));
                voltage_ind_increase_tmp = obj.voltage_ind((idx_min_current(i):idx_max_current(i+1))); 
                
                if obj.compensate_drift_flag ==true
                    %add the drift
%                     h_this_curve.psi_increase = h_this_curve.psi_increase -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease ...
%                     + [1:(idx_max_current(i+1)-idx_min_current(i)+1)])*psi_drift_mean_per_step;  
                    psi_increase_tmp = psi_increase_tmp -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease ...
                    + [1:(idx_max_current(i+1)-idx_min_current(i)+1)])*psi_drift_mean_per_step;  
                 end
             else
%                 h_this_curve.psi_increase = obj.psi(idx_min_current(i):end);
%                 h_this_curve.current_increase = obj.current(psi_idx_offset + (idx_min_current(i):length(obj.current)));
%                 h_this_curve.voltage_ind_increase = obj.voltage_ind((idx_min_current(i):length(obj.current)));
                psi_increase_tmp = obj.psi(idx_min_current(i):end);
                voltage_ind_increase_tmp = obj.voltage_ind((idx_min_current(i):length(obj.current)));
                current_increase_tmp = obj.current(psi_idx_offset + (idx_min_current(i):length(obj.current)));
                                
                 if obj.compensate_drift_flag ==true
                    %add the drift
%                     h_this_curve.psi_increase = h_this_curve.psi_increase -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease + ...
%                     [1:(length(obj.psi)-idx_min_current(i)+1)])*psi_drift_mean_per_step;
                    psi_increase_tmp = psi_increase_tmp -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease + ...
                    [1:(length(obj.psi)-idx_min_current(i)+1)])*psi_drift_mean_per_step;
                 end
             end
             
              h_this_curve.psi_increase = psi_increase_tmp;
              h_this_curve.voltage_ind_increase = voltage_ind_increase_tmp;
              h_this_curve.current_increase = current_increase_tmp;
              
             
             
             
             
                % we calculate it each loop even if just required when
                % drift is compensated for
                sum_idx_drift = idx_max_current(i+1) - idx_max_current(1);
                
%                 obj.results(i).flux_curve.calc_mean_flux_curve;
%                 obj.results(i).flux_curve.calc_dissp_current;
            end
        
                    if 1==0
                    % only used for debuging
                    for j=1
                    clf
                        sum_idx_drift = 0;
                        legend_txt= {};
                        for i = 1:length(idx_max_current)-1
                            this_pl_psi = obj.psi(idx_max_current(i):idx_max_current(i+1)); 
                            this_pl_psi = this_pl_psi-(sum_idx_drift + [1:(idx_max_current(i+1)-idx_max_current(i)+1)])*psi_drift_mean_per_step;
                            plot(obj.current(psi_idx_offset + (idx_max_current(i):idx_max_current(i+1))),-psi_offset+this_pl_psi )
                            hold on; 
                            sum_idx_drift = idx_max_current(i+1) - idx_max_current(1);
                            legend_txt{i}="Loop " + num2str(i);
                        end
                        legend(legend_txt)
                        grid on
                    end  
                    end  
           
 
        end       
        
        function obj = evaluate_psi_sin(obj)

            % here find peaks is used
            [max_psi,idx_max_psi,~,prom_max_psi] =  findpeaks(obj.psi,'MinPeakDistance',round(obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*4*0.9));
            [min_psi,idx_min_psi,~,prom_min_psi] =  findpeaks(-obj.psi,'MinPeakDistance',round(obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*4*0.9));
             min_psi = -min_psi;
            [max_curr,idx_max_current,~,prom_max_current] = findpeaks(obj.current(obj.index_ramps),'MinPeakDistance',round(obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*4*0.9));
            [min_curr,idx_min_current,~,prom_min_current] = findpeaks(-obj.current(obj.index_ramps),'MinPeakDistance',round(obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*4*0.9));
            
            
            %#########################################
            %######### ADDED PROMINENCE TO FIND OUTLINERS
            %###########################################
            %# NOT TESTED MUCH!!!!            
            max_psi_idx_remove = prom_max_psi/mean(prom_max_psi)<0.1;
            max_psi(max_psi_idx_remove)=[];
            idx_max_psi(max_psi_idx_remove)=[];
            min_psi_idx_remove =prom_min_psi/mean(prom_min_psi)<0.1;
            min_psi(min_psi_idx_remove)=[];
            idx_min_psi(min_psi_idx_remove)=[];
            max_curr_idx_remove = prom_max_current/mean(prom_max_current)<0.1;
            max_curr(max_curr_idx_remove)=[];
            idx_max_current(max_curr_idx_remove)=[];
            min_curr_idx_remove = prom_min_current/mean(prom_min_current)<0.1;
            min_curr(min_curr_idx_remove)=[];
            idx_min_current(min_curr_idx_remove)=[];
            
            
            % somtimes the slope is deteced aswell, then the last max current is excluded
            if length(idx_max_psi)>length(idx_min_psi) % the min index is used for detection because it is always right
                idx_max_current(end) = [];
            end
            
            if length(idx_max_current)>length(idx_min_current) % the min index is used for detection because it is always right
                idx_max_current(end) = [];
            end
            min_curr = -min_curr;

            if ( mean(length(idx_max_psi),length(idx_min_psi)) ~= obj.inputSignal.nmbr_cycles  ) || (mean(length(idx_max_current),length(idx_min_current)) ~= obj.inputSignal.nmbr_cycles  ) 
                error("get_point_max_psi_n_offset: the number of detected closed loops differs from" + num2str(obj.inputSignal.nmbr_cycles));            
            end
            

            if obj.compensate_drift_flag ==true                
                % ADD CHECK FOR SUFFICIENT LOOPS HERE
                % calculation of drift's mean and std only for more than 2 full loop possible                    
                psi_at_max_current = obj.psi(idx_max_current);
                psi_at_min_current = obj.psi(idx_min_current);                
                psi_drift_max2max_mean = mean(diff(psi_at_max_current));
                psi_drift_min2min_mean = mean(diff(psi_at_min_current));
                psi_drift_mean = (psi_drift_max2max_mean+psi_drift_min2min_mean)/2;
                psi_drift_mean_per_step = psi_drift_mean/(mean(diff(idx_max_current)));
                psi_drift_max2max_std = std(diff(psi_at_max_current));
                psi_drift_min2min_std = std(psi_at_min_current);
                max_psi_drift_compensated = psi_at_max_current - psi_drift_mean*[0:length(psi_at_max_current)-1] ; 
                min_psi_drift_compensated = psi_at_min_current - psi_drift_mean*[0.5:length(psi_at_min_current)-0.5] ; 

                psi_offset =  mean(max_psi_drift_compensated + min_psi_drift_compensated)/2;
            end
                     
            psi_idx_offset = obj.index_ramps(1)-1; % needed because obj.psi and obj.current differ in size
            sum_idx_drift = 0;
        
            for i = 1:length(idx_max_current)-1
                
             %create new empty flux_curve object                
             obj.results(i).flux_curve =flux_curve(1/obj.inputSignal.daq_Rate,[],[]);
             
             %create handle
             h_this_curve = obj.results(i).flux_curve;             
%              h_this_curve.psi_decrease = obj.psi(idx_max_current(i):idx_min_current(i));
%              h_this_curve.voltage_ind_decrease = obj.voltage_ind(idx_max_current(i):idx_min_current(i));
             psi_decrease_tmp = obj.psi(idx_max_current(i):idx_min_current(i));
%              voltage_ind_decrease_tmp = obj.current(psi_idx_offset + (idx_max_current(i):idx_min_current(i)));
             voltage_ind_decrease_tmp = obj.voltage_ind(idx_max_current(i):idx_min_current(i));
             if obj.compensate_drift_flag ==true
                %add the drift
%              h_this_curve.psi_decrease = h_this_curve.psi_decrease -psi_offset  -(sum_idx_drift + [1:(idx_min_current(i)-idx_max_current(i)+1)])*psi_drift_mean_per_step;              
                psi_decrease_tmp = psi_decrease_tmp -psi_offset  -(sum_idx_drift + [1:(idx_min_current(i)-idx_max_current(i)+1)])*psi_drift_mean_per_step;                
                  sum_idx_dirft_last_curve_decrease = (idx_min_current(i)-idx_max_current(i)+1);
             end
              % now set it to the object
              h_this_curve.psi_decrease = psi_decrease_tmp;
              h_this_curve.voltage_ind_decrease = voltage_ind_decrease_tmp;
              h_this_curve.current_decrease = obj.current(psi_idx_offset + (idx_max_current(i):idx_min_current(i)));
              %###
              
             if i <=length(idx_max_current)-1
%              	  h_this_curve.psi_increase  = obj.psi(idx_min_current(i):idx_max_current(i+1));
%                 h_this_curve.current_increase = obj.current(psi_idx_offset + (idx_min_current(i):idx_max_current(i+1)));
%                 h_this_curve.voltage_ind_increase = obj.voltage_ind((idx_min_current(i):idx_max_current(i+1))); 
                psi_increase_tmp  = obj.psi(idx_min_current(i):idx_max_current(i+1));
                current_increase_tmp = obj.current(psi_idx_offset + (idx_min_current(i):idx_max_current(i+1)));
                voltage_ind_increase_tmp = obj.voltage_ind((idx_min_current(i):idx_max_current(i+1))); 
                
                if obj.compensate_drift_flag ==true
                    %add the drift
%                     h_this_curve.psi_increase = h_this_curve.psi_increase -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease ...
%                     + [1:(idx_max_current(i+1)-idx_min_current(i)+1)])*psi_drift_mean_per_step;  
                    psi_increase_tmp = psi_increase_tmp -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease ...
                    + [1:(idx_max_current(i+1)-idx_min_current(i)+1)])*psi_drift_mean_per_step;  
                 end
             else
%                 h_this_curve.psi_increase = obj.psi(idx_min_current(i):end);
%                 h_this_curve.current_increase = obj.current(psi_idx_offset + (idx_min_current(i):length(obj.current)));
%                 h_this_curve.voltage_ind_increase = obj.voltage_ind((idx_min_current(i):length(obj.current)));
                psi_increase_tmp = obj.psi(idx_min_current(i):end);
                voltage_ind_increase_tmp = obj.voltage_ind((idx_min_current(i):length(obj.current)));
                current_increase_tmp = obj.current(psi_idx_offset + (idx_min_current(i):length(obj.current)));
                                
                 if obj.compensate_drift_flag ==true
                    %add the drift
%                     h_this_curve.psi_increase = h_this_curve.psi_increase -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease + ...
%                     [1:(length(obj.psi)-idx_min_current(i)+1)])*psi_drift_mean_per_step;
                    psi_increase_tmp = psi_increase_tmp -psi_offset -(sum_idx_drift + sum_idx_dirft_last_curve_decrease + ...
                    [1:(length(obj.psi)-idx_min_current(i)+1)])*psi_drift_mean_per_step;
                 end
             end
             
              h_this_curve.psi_increase = psi_increase_tmp;
              h_this_curve.voltage_ind_increase = voltage_ind_increase_tmp;
              h_this_curve.current_increase = current_increase_tmp;
              
             
             
             
             
                % we calculate it each loop even if just required when
                % drift is compensated for
                sum_idx_drift = idx_max_current(i+1) - idx_max_current(1);
                
%                 obj.results(i).flux_curve.calc_mean_flux_curve;
%                 obj.results(i).flux_curve.calc_dissp_current;
            end
        
                    if 1==0
                    % only used for debuging
                    for j=1
                    clf
                        sum_idx_drift = 0;
                        legend_txt= {};
                        for i = 1:length(idx_max_current)-1
                            this_pl_psi = obj.psi(idx_max_current(i):idx_max_current(i+1)); 
                            this_pl_psi = this_pl_psi-(sum_idx_drift + [1:(idx_max_current(i+1)-idx_max_current(i)+1)])*psi_drift_mean_per_step;
                            plot(obj.current(psi_idx_offset + (idx_max_current(i):idx_max_current(i+1))),-psi_offset+this_pl_psi )
                            hold on; 
                            sum_idx_drift = idx_max_current(i+1) - idx_max_current(1);
                            legend_txt{i}="Loop " + num2str(i);
                        end
                        legend(legend_txt)
                        grid on
                    end  
                    end  
           
 
        end
        
        function obj = extract_resictance_n_sinwaves(obj)   
                % ----------------- get start and end of the ramps for integration -
                % get the index where the ramp starts
                % the saved index 'idx_saved_ramp marks' the region, where the ramp
                % stars, so we search here. We look for a intersection of
                % horizontal line with an increasing/decreasing.
                % This is done in 'getIntersection()'.                      

                [~,idx_i_meas_start_begins]=find(abs(obj.current-obj.inputSignal.i_meas_current)<=0.0025,1,'first');
                idx_begin_R_meas_start =idx_i_meas_start_begins;

                [~,idx_end_curr_meas]=find(obj.current(idx_i_meas_start_begins+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*0.5:idx_i_meas_start_begins+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*1.5)-obj.inputSignal.i_meas_current*0.5< 0 ,1,'first');

                idx_end_curr_meas = idx_i_meas_start_begins+idx_end_curr_meas+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*0.5;

                
                
                idx_hor_start = idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.3;
                idx_hor_end = idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.8;             

                idx_search_start = round(idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.5);
                idx_search_end = round(idx_search_start+ obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start+obj.inputSignal.T_meas_psi_sgnl_ramp));

                [~,idx_diag_start]=find(abs(obj.current(idx_search_start:idx_search_end)-obj.inputSignal.i_max*0.2)<=0.01,1,'first');
                idx_diag_start = round(idx_diag_start+idx_search_start);
                idx_diag_end    =  round(idx_diag_start+obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.3));

                % use curve fitting to find the intersection
                idx_sin_start = obj.getIntersectionSin(idx_end_curr_meas,idx_diag_start,obj.current(idx_diag_start:idx_diag_end),obj.current(idx_hor_start:idx_hor_end));

                % To find the end of the ramp we try to find the last value closed i max.
                % first geuss for the end

                idx_int_end =  round(idx_sin_start+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*(1+4*obj.inputSignal.nmbr_cycles));            
                idx_end_R_meas_end = idx_int_end + find(obj.current(idx_int_end:end)>obj.inputSignal.i_max*0.98,1,'last');

                idx_hor_end_start = round(idx_end_R_meas_end-obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end*0.8);
                idx_hor_end_end     = round(idx_end_R_meas_end-obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end*0.2);            

                [~,idx_sin_end_est]=find(abs(obj.current(idx_sin_start:idx_hor_end_start)-obj.inputSignal.i_max)>=0.01,1,'last');
                idx_sin_end_est = idx_sin_start + idx_sin_end_est;

                idx_diag_end_start  = round(idx_sin_end_est-obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.7));
                idx_diag_end_end    = round(idx_sin_end_est-obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.2));                          

                % use curve fitting to find the intersection        
                idx_ramp_end = obj.getIntersectionSin(idx_hor_end_start,idx_diag_end_start,obj.current(idx_diag_end_start:idx_diag_end_end),obj.current(idx_hor_end_start:idx_hor_end_end));

                obj.index_ramps = idx_sin_start:idx_ramp_end;    

                % ----------------- get initial offset and noise from data --------       
                this_offset = [mean(obj.current(round(1:idx_begin_R_meas_start*0.9))); ...
                               mean(obj.voltage(round(1:idx_begin_R_meas_start*0.9)))] ;

                % ----------------- get resictance from data ----------------------
                % calculate Resitance at beginning
                ixd_step_meas_R_start = round(idx_begin_R_meas_start:idx_begin_R_meas_start+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start);

                % calculate Resitance at and
                ixd_step_meas_R_end = round(idx_end_R_meas_end - obj.inputSignal.T_meas_resistanc_end*obj.inputSignal.daq_Rate:idx_end_R_meas_end);

                [obj.R_valve_start,std_out] = obj.calc_R_from_Step('start',obj.current(ixd_step_meas_R_start),obj.voltage(ixd_step_meas_R_start),[0;0]);
                [obj.R_valve_end,std_out_end] = obj.calc_R_from_plateau_v2(obj.current(ixd_step_meas_R_end),obj.voltage(ixd_step_meas_R_end),[0;0]); 
    
        end
    
        function obj = extract_resictance_n_ramps(obj)   
                % ----------------- get start and end of the ramps for integration -
                % get the index where the ramp starts
                % the saved index 'idx_saved_ramp marks' the region, where the ramp
                % stars, so we search here. We look for a intersection of
                % horizontal line with an increasing/decreasing.
                % This is done in 'getIntersection()'.                      

                [~,idx_i_meas_start_begins]=find(abs(obj.current-obj.inputSignal.i_meas_current)<=0.0025,1,'first');
                idx_begin_R_meas_start =idx_i_meas_start_begins;

                [~,idx_end_curr_meas]=find(obj.current(idx_i_meas_start_begins+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*0.5:idx_i_meas_start_begins+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*1.5)-obj.inputSignal.i_meas_current*0.5< 0 ,1,'first');

                idx_end_curr_meas = idx_i_meas_start_begins+idx_end_curr_meas+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*0.5;

                
                
                idx_hor_start = idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.3;
                idx_hor_end = idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.8;             

                idx_search_start = round(idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.5);
                idx_search_end = round(idx_search_start+ obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start+obj.inputSignal.T_meas_psi_sgnl_ramp));

                [~,idx_diag_start]=find(abs(obj.current(idx_search_start:idx_search_end)-obj.inputSignal.i_max*0.2)<=0.01,1,'first');
                idx_diag_start = round(idx_diag_start+idx_search_start);
                idx_diag_end    =  round(idx_diag_start+obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.3));

                % use curve fitting to find the intersection
                idx_ramp_start = obj.getIntersection(idx_diag_start,obj.current(idx_diag_start:idx_diag_end),obj.current(idx_hor_start:idx_hor_end));

                % To find the end of the ramp we try to find the last value closed i max.
                % first geuss for the end

                idx_int_end =  round(idx_ramp_start+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*(1+4*(obj.inputSignal.nmbr_cycles-1)));            
                idx_end_R_meas_end = idx_int_end + find(obj.current(idx_int_end:end)>obj.inputSignal.i_max*0.98,1,'last');

                idx_hor_end_start = round(idx_end_R_meas_end-obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end*0.8);
                idx_hor_end_end     = round(idx_end_R_meas_end-obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end*0.2);            

                [~,idx_ramp_end_est]=find(abs(obj.current(idx_ramp_start:idx_hor_end_start)-obj.inputSignal.i_max)>=0.01,1,'last');
                idx_ramp_end_est = idx_ramp_start + idx_ramp_end_est;

                idx_diag_end_start  = round(idx_ramp_end_est-obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.7));
                idx_diag_end_end    = round(idx_ramp_end_est-obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.2));                          

                % use curve fitting to find the intersection        
                idx_ramp_end = obj.getIntersection(idx_diag_end_start,obj.current(idx_diag_end_start:idx_diag_end_end),obj.current(idx_hor_end_start:idx_hor_end_end));

                obj.index_ramps = idx_ramp_start:idx_ramp_end;    

                % ----------------- get initial offset and noise from data --------       
                this_offset = [mean(obj.current(round(1:idx_begin_R_meas_start*0.9))); ...
                               mean(obj.voltage(round(1:idx_begin_R_meas_start*0.9)))] ;

                % ----------------- get resictance from data ----------------------
                % calculate Resitance at beginning
                ixd_step_meas_R_start = round(idx_begin_R_meas_start:idx_begin_R_meas_start+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start);

                % calculate Resitance at and
                ixd_step_meas_R_end = round(idx_end_R_meas_end - obj.inputSignal.T_meas_resistanc_end*obj.inputSignal.daq_Rate:idx_end_R_meas_end);

                [obj.R_valve_start,std_out] = obj.calc_R_from_Step('start',obj.current(ixd_step_meas_R_start),obj.voltage(ixd_step_meas_R_start),[0;0]);
                [obj.R_valve_end,std_out_end] = obj.calc_R_from_plateau_v2(obj.current(ixd_step_meas_R_end),obj.voltage(ixd_step_meas_R_end),[0;0]); 
    
        end
    
        function obj = extract_resictance_n_const_psi_ramps(obj)   
            % ----------------- get start and end of the ramps for integration -
            % get the index where the ramp starts
            % the saved index 'idx_saved_ramp marks' the region, where the ramp
            % stars, so we search here. We look for a intersection of
            % horizontal line with an increasing/decreasing.
            % This is done in 'getIntersection()'.                      

            [~,idx_i_meas_start_begins]=find(abs(obj.current-obj.inputSignal.i_meas_current)<=0.0025,1,'first');
            idx_begin_R_meas_start =idx_i_meas_start_begins;

            [~,idx_end_curr_meas]=find(obj.current(idx_i_meas_start_begins+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*0.5:idx_i_meas_start_begins+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*1.5)-obj.inputSignal.i_meas_current*0.5< 0 ,1,'first');

            idx_end_curr_meas = idx_i_meas_start_begins+idx_end_curr_meas+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start*0.5;



            idx_hor_start = idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.3;
            idx_hor_end = idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.8;             

            zero_curr_mean = mean(obj.current(idx_hor_start:idx_hor_end));
            zero_curr_std  = std(obj.current(idx_hor_start:idx_hor_end));
            
            [~,idx1]=find(obj.current(idx_hor_end:end)>=(zero_curr_mean+5*zero_curr_std),1,'first');
            [~,idx2]=find(obj.current(idx_hor_end:end)>=(zero_curr_mean+50*zero_curr_std),1,'first');
            
            idx_diag_start = idx_hor_end + idx1;
            idx_diag_end =  idx_hor_end+ idx2;
            
%             idx_search_start = round(idx_end_curr_meas + obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start*0.5);
%             idx_search_end = round(idx_search_start+ obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_break_start+obj.inputSignal.T_meas_psi_sgnl_ramp));

%             [~,idx_diag_start]=find(abs(obj.current(idx_search_start:idx_search_end)-obj.inputSignal.i_max*0.2)<=0.01,1,'first');
%             idx_diag_start = round(idx_diag_start+idx_search_start);
%             idx_diag_end    =  round(idx_diag_start+obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.3));

            % use curve fitting to find the intersection
            idx_ramp_start = obj.getIntersection(idx_diag_start,obj.current(idx_diag_start:idx_diag_end),obj.current(idx_hor_start:idx_hor_end));

            % To find the end of the ramp we try to find the last value closed i max.
            % first geuss for the end

            
            
            
            idx_int_end =  round(idx_ramp_start+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_psi_sgnl_ramp*(1+4*(obj.inputSignal.nmbr_cycles)));            
            idx_end_R_meas_end = idx_int_end + find(obj.current(idx_int_end:end)>obj.inputSignal.i_max*0.98,1,'last');

            idx_hor_end_start = round(idx_end_R_meas_end-obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end*0.8);
            idx_hor_end_end     = round(idx_end_R_meas_end-obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end*0.2);            
% 

            zero_curr_mean = mean(obj.current(idx_hor_end_start:idx_hor_end_end));
            zero_curr_std  = std(obj.current(idx_hor_end_start:idx_hor_end_end));
            

            
            [~,idx_ramp_end_est]=find(abs(obj.current(idx_ramp_start:idx_hor_end_start)-obj.inputSignal.i_max)>=0.01,1,'last');
            
            
            [~,idx1]=find(obj.current(idx_ramp_end_est:idx_hor_end_start)<(zero_curr_mean-5*zero_curr_std),1,'last');
            [~,idx2]=find(obj.current(idx_ramp_end_est:idx_hor_end_start)<(zero_curr_mean-500*zero_curr_std),1,'last');
            
            
            idx_diag_end_start = idx_ramp_end_est + idx2;
            idx_diag_end_end =  idx_ramp_end_est + idx1;
            


 
%             idx_ramp_end_est = idx_ramp_start + idx_ramp_end_est;
% 
%             idx_diag_end_start  = round(idx_ramp_end_est-obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.7));
%             idx_diag_end_end    = round(idx_ramp_end_est-obj.inputSignal.daq_Rate*(obj.inputSignal.T_meas_psi_sgnl_ramp*0.2));                          

            % use curve fitting to find the intersection        
            idx_ramp_end = obj.getIntersection(idx_diag_end_start,obj.current(idx_diag_end_start:idx_diag_end_end),obj.current(idx_hor_end_start:idx_hor_end_end));

            obj.index_ramps = idx_ramp_start:idx_ramp_end;    

            % ----------------- get initial offset and noise from data --------       
            this_offset = [mean(obj.current(round(1:idx_begin_R_meas_start*0.9))); ...
                           mean(obj.voltage(round(1:idx_begin_R_meas_start*0.9)))] ;

            % ----------------- get resictance from data ----------------------
            % calculate Resitance at beginning
            ixd_step_meas_R_start = round(idx_begin_R_meas_start:idx_begin_R_meas_start+obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start);

            % calculate Resitance at and
            ixd_step_meas_R_end = round(idx_end_R_meas_end - obj.inputSignal.T_meas_resistanc_end*obj.inputSignal.daq_Rate:idx_end_R_meas_end);

            [obj.R_valve_start,std_out] = obj.calc_R_from_Step('start',obj.current(ixd_step_meas_R_start),obj.voltage(ixd_step_meas_R_start),[0;0]);
            [obj.R_valve_end,std_out_end] = obj.calc_R_from_plateau_v2(obj.current(ixd_step_meas_R_end),obj.voltage(ixd_step_meas_R_end),[0;0]); 

        end
      
        function obj = calc_psi(obj)

            %PERFORM CHECKS HERE

                % extrct data requiered for resictance and the integration
                current_int = obj.current(obj.index_ramps);
                voltage_int = obj.voltage(obj.index_ramps);

                % Calculate Resitance Correction
                f_R = trapz(obj.time(obj.index_ramps),obj.current(obj.index_ramps).^2);
                cum_f_R = cumtrapz(obj.time(obj.index_ramps),obj.current(obj.index_ramps).^2);
                obj.R_vec = obj.R_valve_start + (obj.R_valve_end-obj.R_valve_start)*cum_f_R./f_R;

                % ------------------- calculate flux linkage ---------------------
                obj.voltage_ind = voltage_int-obj.R_vec.*current_int;            
                % calculate integral 
                obj.psi = cumtrapz(obj.time(obj.index_ramps),obj.voltage_ind); 
                % calc max and min to get the offset
                % this function outputs the MEAN of all maximum and minimum points
                % of the flux linkeage BEFORE the offset correction.        


        end
        
        function obj = grabDatafromWorkspace(obj)
        % this function is for lazy people. If no data about the input signal is passed to the constructor, 
        % this function looks for data in the base workspace. Pay attention the
        % the DataMapping property
            for i = 1:size(obj.DataMapping,1)
                try
                obj.(obj.DataMapping{i,1}) = evalin('base', obj.DataMapping{i,2});
                catch 
                    error("Could not find the variabel " + obj.DataMapping{i,2} + " inside base workspace! " + newline...
                        + "Either rename name it to  " + obj.DataMapping{i,2} + ...
                        ",or supply an new mapping or all variables directly to the constructor")
                end
            end        

        end       

        function obj = remove_offset(obj)      

%                 end_offset_zone = round(0.9*obj.daq_Rate*obj.T_meas_resistance_start);
%                 start_offset_zone = round(0.5*obj.daq_Rate*obj.T_meas_resistance_start);

                start_offset_zone = 1;
                
                %find first peak
               [~,end_offset_zone_curr] = max(diff(obj.current(1:obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start)   ));
%                [~,end_offset_zone_vol] = max(diff(obj.voltage(1:obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start)   ));
               %                 end_offset_zone = round(0.8*obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*(1-obj.inputSignal.ratio_T_meas_current_high_start));
                
               end_offset_zone_curr = round(end_offset_zone_curr*0.9);
                offset_voltage = mean(obj.voltage(start_offset_zone:end_offset_zone_curr));
                offset_current = mean(obj.current(start_offset_zone:end_offset_zone_curr));

                obj.current = obj.current - offset_current;
                obj.voltage = obj.voltage - offset_voltage;

        end
        
        function idx_intersecction = getIntersectionSin(~,idx_end_curr_meas,idx_diag_start,increasing_vals,horizontal_val)
            % This function calculates the intersection of a horizontal and an
            % increasing/decreasing sinwave!!!
            % THIS SHOULD BE IMPLEMENTED USING ANALYTICAL CALCULUS AND
            % PAYING ATTATION TO THE BAD CONDITION DUE TO SMALL NUMBERS OF THE FITTED SINWAVE
            % DIRTY IMPLEMENTED!!!!!!!!!!!!!!!
            hor_val = mean(horizontal_val);
            % Fit model to data
            [fitresult, ~] = fit( double(idx_diag_start:idx_diag_start+length(increasing_vals)-1)',  double(increasing_vals)', 'sin1' );

            if idx_end_curr_meas < idx_diag_start % case if begin of sinwave block is estimated
            
            [~,idx_min]=min(abs(feval(fitresult,idx_end_curr_meas:idx_diag_start)-hor_val));
            idx_intersecction = idx_end_curr_meas+idx_min;
            else % case if end of sinwave block is estimated
            [~,idx_min]=min(abs(feval(fitresult,idx_diag_start:idx_end_curr_meas)-hor_val));
            idx_intersecction = idx_diag_start+idx_min;
            end
            
            %             c_val = coeffvalues(fitresult);
            %             idx_intersecction = round(  (   asin(hor_val/c_val(1))  - c_val(3))/c_val(2)  );
        end
                
        function idx_intersecction = getIntersection(~,idx_diag_start,increasing_vals,horizontal_val)
            % This function calculates the intersection of a horizontal and an
            % increasing/decreasing line

            hor_val = mean(horizontal_val);
            % Fit model to data
            [fitresult, ~] = fit( double(idx_diag_start:idx_diag_start+length(increasing_vals)-1)',  double(increasing_vals)', 'poly1', fitoptions( 'Method', 'LinearLeastSquares' ) );
            c_val = coeffvalues(fitresult);
            idx_intersecction = round((hor_val-c_val(2))/c_val(1));
        end

        function[R_valve,std_out]= calc_R_from_Step(obj,where,current,voltage,offset)

            if strcmp(where,'start')

                   idx_max_curr_change = 1;
                   idx_start= idx_max_curr_change + 0.55*obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start;  
                   idx_end = idx_max_curr_change + 0.85*obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start;  

                   elseif strcmp(where,'end')
                       here = round([0.5,1.5]*obj.inputSignal.T_meas_resistance_start*obj.daq_Rate);
                       idx_here = here(1):here(2);
                       if mean(current(idx_here))>0           
                             [~,idx_max_volt_change]=min(voltage(here(1):here(2)));  
                       else
                            [~,idx_max_volt_change]=max(voltage(here(1):here(2)));  
                       end
                         idx_end= idx_here(idx_max_volt_change) - 0.1*obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start;  
                         idx_start = idx_here(idx_max_volt_change) - 0.6*obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistance_start*obj.inputSignal.ratio_T_meas_current_high_start;    
                   elseif strcmp(where,'end_plateau')


                   else
                       error('Unkown input! use ''start'' or ''end'' to determine if you want to extract resitance before or after the ramp'); 
                   end

                   R_valve = mean((voltage(idx_start:idx_end)-offset(2))./(current(idx_start:idx_end)-offset(1)));
                   std_out = std((voltage(idx_start:idx_end)-offset(2))./(current(idx_start:idx_end)-offset(1)));
        end

        function[R_valve,std_out]= calc_R_from_plateau_v2(obj,current,voltage,offset)
 
        %  detect falling voltate at the end of hold phase
            idx_end = length(current);
           % calc start and end of measuring (move a little away from real ends)
           idx_start  = idx_end - obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end*0.5;
           idx_end   = idx_end-0.05*obj.inputSignal.daq_Rate;
        % Fit model to data.
            [fitresult, ~] = fit( [idx_start:idx_end]', double([(voltage(idx_start:idx_end)-offset(2))./(current(idx_start:idx_end)-offset(1))]'), 'poly1'  );
              idx_eval_fit = - obj.inputSignal.daq_Rate*obj.inputSignal.T_meas_resistanc_end*0.05;
                   R_valve = abs(fitresult.p1*idx_eval_fit+fitresult.p2);
                   std_out = std((fitresult.p1*[idx_start:idx_end]+fitresult.p2) - voltage(idx_start:idx_end)./current(idx_start:idx_end));

        end
        
    
    end % end Methods(Access = private)
    
end