 function [Curr_sim,PSI_sim,sse_sum] = eval_JaDatasetMAT_PSI(PSI_meas,Curr_meas,PSI_sim,Curr_sim,nmbr_sets,weights,param)
        sse_sum = 0; 
        for i = 1:nmbr_sets  

                 [Curr_sim{i},PSI_sim{i}] = evaluate_JA_loops_PSI(PSI_meas{i},Curr_meas{i},Curr_sim{i},PSI_sim{i},param); 
  
            
        end
 end
 
