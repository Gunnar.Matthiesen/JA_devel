function [M_sim,H_sim] = integrate_JA_loop_discr_PSI(H_int,M_sim,H_sim,param)

        my0 = 4.*pi.*1e-7;
        ja_Ms     = param(1);
        ja_a      = param(2);
        ja_alpha  = param(3);
        ja_k      = param(4);
        ja_c      = param(5);
        
        dM_dH = zeros(size(H_int));
        delta = zeros(size(H_int));

        for k = 1:length(H_int)-1    

            delta(k) = sign(H_int(k+1)-H_int(k));
            He = H_int(k)+ja_alpha*M_sim(k);            
            M_an = ja_Ms*(coth(He/ja_a)-ja_a/He);    
%             dMan_dHe = ja_Ms/ja_a*( 1 - coth(He/ja_a)^2 + (ja_a/He)^2);  
            
            dMan_dH =-ja_Ms*((coth((H_int(k) + M_sim(k)*ja_alpha)/ja_a)^2 - 1)/ja_a - ja_a/(H_int(k) + M_sim(k)*ja_alpha)^2);            
            
            dM_dH(k) = (1/(1+ ja_c)) * ( M_an - M_sim(k)) /(   ja_k*delta(k)/my0 - ja_alpha*(M_an - M_sim(k))) + ja_c/(1+ja_c)*dMan_dH;
            
            
%             Mirr = (M_sim(k)-ja_c*M_an)/(1-ja_c);
%             dMirr_dBe = (M_an - Mirr)/(my0*delta*ja_k);
%             dM_dB(k) = ( (1-ja_c)*dMirr_dBe +ja_c/my0*dMan_dHe )/ ...
%                     ( 1+ my0*(1-ja_alpha)*(1-ja_c)*dMirr_dBe + ja_c*(1-ja_alpha)*dMan_dHe);                 
            
                
                M_sim(k+1) = M_sim(k) + dM_dH(k)*(H_int(k+1)-H_int(k));
%             H_sim(k+1) = B_int(k+1)/my0 - M_sim(k+1);
        end

end