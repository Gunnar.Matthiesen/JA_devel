function [PSI_sim,M_sim] = evaluate_JA_loops_PSI(Curr_sim,PSI_sim,M_sim,PSI0,param)
    % an der Stelle PSI_sim(1,1) muss der Startwert M0 stehen und in Loops in H_sim müssen die gleiche Länge auf auf und absteigendem Ast haben 
    % H_sim is a matrix 
    
    % H_sim input as current!!!!!
    % PSI_sim input as PSI !!!!
    
    my0 = 4.*pi.*1e-7;
    f_psi  = param(6);
    f_i    = param(7);
    
    %convert from PSI to M
    M0 =   PSI0*f_psi/my0-Curr_sim(1)*f_i;
    
    for j = 1:2
    
        for i = 1:size(Curr_sim,2)  
            M_sim(1,i) = M0;
            M_sim(:,i) = integrate_JA_loop_discr_PSI(Curr_sim(:,i),M_sim(:,i),param); 
%             PSI_sim(:,i) = M_sim(:,i);
            M0 = M_sim(end,i);         
            % convert from M to PSI        
            PSI_sim(:,i) =   (M_sim(:,i)+Curr_sim(:,i)*f_i)/f_psi*my0; %!!!!!! Returned as PSI
        end
    
    end
end