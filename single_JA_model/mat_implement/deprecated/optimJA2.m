 function [sse_sum] = optimJA2(M_meas,H_meas,M_sim,nmbr_sets,param,param_opt)     

         sse_sum = 0;       
        
        for i = 1:nmbr_sets  
            M_sim{i} = evaluate_JA_loops(H_meas{i},M_sim{i},M_meas{i}(1),param.*param_opt); 
            sse1 = sum((M_meas{i}(:,1) - M_sim{i}(:,1)).^2);  
            sse2 = sum((M_meas{i}(:,2) - M_sim{i}(:,2)).^2); 
            sse_sum = sse_sum + sse1+sse2;
        end
 end