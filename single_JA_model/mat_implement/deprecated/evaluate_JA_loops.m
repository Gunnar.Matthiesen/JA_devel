function M_sim = evaluate_JA_loops(H_sim,M_sim,M0,param)
    % an der Stelle M_sim(1,1) muss der Startwert M0 stehen und in Loops in H_sim müssen die gleiche Länge auf auf und absteigendem Ast haben 
    % H_sim is a matrix 
    for i = 1:size(H_sim,2) 
        
        
        
        M_sim(1,i) = M0;
        M_sim(:,i) = integrate_JA_loop_discr2(H_sim(:,i),M_sim(:,i),param); 
        M0 = M_sim(end,i);   
    end
end