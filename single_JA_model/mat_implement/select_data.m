
    idx_result = 2;
    idx_meas = 1; 
    
    nmbr_meas = size(meas_raw(idx_meas).current,2) ;

    
    PSI_meas = cell(1,nmbr_meas*2 - 6*2);
    Curr_meas = cell(1,nmbr_meas*2 - 6*2 );
    PSI_an = [];%cell(1,nmbr_meas);
    CRR_an = []; % cell(1,nmbr_meas);

    idx_sel = 1:12;
    nmbr_meas_sel = idx_sel;
    
    for i = 1:nmbr_meas_sel  
                     
        current_grid_inc = 0.001;
        % we reduce the number of datapoints using interpolation
        % we do not check wether datapoints are unique or in ascending
        % order because we use preprocessed data and this has already been
        % done in other routines
        this_meas = meas_raw(idx_meas).current(idx_sel(i)).results(idx_result).flux_curve;    
        
        current_grid = current_grid_inc*(fix(min(this_meas.current_increase/current_grid_inc)): ...
                        1:max(this_meas.current_increase/current_grid_inc));                     

        PSI_meas{1+(i-1)*2} =  interp1(this_meas.current_increase_sm,...
                            this_meas.psi_increase_sm, ...
                                     current_grid,'PCHIP');                                  
        Curr_meas{1+(i-1)*2}   = current_grid;     
        
        % decreasing current
        current_grid = current_grid_inc*(fix(min(this_meas.current_decrease_sm/current_grid_inc)): ...
                        1:max(this_meas.current_decrease_sm/current_grid_inc));  
        
        PSI_meas{2+(i-1)*2} =   interp1(this_meas.current_decrease_sm, ...
                                     this_meas.psi_decrease_sm,current_grid,'PCHIP');  
        
        Curr_meas{2+(i-1)*2}   = current_grid;
        
        [Curr_meas{2+(i-1)*2},idx] = sort(Curr_meas{2+(i-1)*2},'descend');
        PSI_meas{2+(i-1)*2} = PSI_meas{2+(i-1)*2}(idx);
%         current_grid = current_grid_inc*(fix(min(this_meas.current_grid_sm/current_grid_inc)): ...
%                     1:max(this_meas.current_grid_sm/current_grid_inc));  
%         
%         PSI_an{i} =   interp1(this_meas.current_grid_sm, ...
%                                      this_meas.psi_mean_sm,current_grid);  
%         
%         CRR_an{i}   = current_grid;
PSI_an{i} = this_meas.psi_mean_sm;
CRR_an{i} = this_meas.current_grid_sm;
% PSI_an = [PSI_an,this_meas.psi_mean_sm];
% CRR_an = [CRR_an,this_meas.current_grid_sm];
        
    end