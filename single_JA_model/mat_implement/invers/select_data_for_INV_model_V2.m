
    idx_result = 2;
    idx_meas = 2;  
    idx_sel =2:1:12;

    
    nmbr_meas = size(meas_raw(idx_meas).current,2) ;    
    nmbr_meas_sel = length(idx_sel);
    PSI_meas = cell(1,nmbr_meas_sel);
    Curr_meas = cell(1,nmbr_meas_sel);
    PSI_an = [];%cell(1,nmbr_meas);
    CRR_an = []; % cell(1,nmbr_meas);

    

    
    for i = 1:nmbr_meas_sel
        
        flag_const_grid = false;
        psi_grid_inc = 0.001;
        nmbr_grid_pnts = 200;        
        % we reduce the number of datapoints using interpolation
        % we do not check wether datapoints are unique or in ascending
        % order because we use preprocessed data and this has already been
        % done in other routines
        this_meas = meas_raw(idx_meas).current(idx_sel(i)).results(idx_result).flux_curve;    
        
        min_psi = min(min(this_meas.psi_increase_sm),min(this_meas.psi_decrease_sm));
        max_psi = max(max(this_meas.psi_increase_sm),max(this_meas.psi_decrease_sm));    
        
        abs_psi = max(abs([min_psi,max_psi]));
        
        psi_grid = linspace(-abs_psi,...
             abs_psi,nmbr_grid_pnts)';   
        
        
%         if flag_const_grid==true
%         psi_grid = psi_grid_inc*(fix(min_psi./psi_grid_inc): ...
%                         1:ceil(max_psi./psi_grid_inc))';    
%         else
%         psi_grid = psi_grid_inc*linspace(fix(min_psi./psi_grid_inc),...
%              ceil(max_psi./psi_grid_inc),nmbr_grid_pnts)';  
%         end
                    
        % increase current 
        c_meas_tmp_up = this_meas.current_increase_sm;
        psi_meas_tmp_up = this_meas.psi_increase_sm;  
        
        % decreasing current       
%         PSI_meas{i}(:,2)   = flip(psi_grid);
        c_meas_tmp_dwn = this_meas.current_decrease_sm;
        psi_meas_tmp_dwn = this_meas.psi_decrease_sm;   
        
        
        x = [c_meas_tmp_up(:);-c_meas_tmp_dwn(:)];
        y = [psi_meas_tmp_up(:);-psi_meas_tmp_dwn(:)];
        
        [fitresult, gof] = fit( double(y), double(x), 'poly9',fitoptions('Method', 'LinearLeastSquares' , 'Normalize','on'));
        c_result = feval(fitresult,psi_grid);
        
%         [psi_meas_tmp,idx_unique] = unique(psi_meas_tmp);
% %         psi_meas_tmp = psi_meas_tmp(idx_unique);
%         c_meas_tmp = c_meas_tmp(idx_unique);  
%         
         update_figure('Psi Data selected');
        plot(c_result,psi_grid);        
        hold on;
        plot(c_meas_tmp_up,psi_meas_tmp_up,'k')
        plot(c_meas_tmp_dwn,psi_meas_tmp_dwn,'k')
        plot(-c_result,-psi_grid);
        xlabel('Current [A]');
        ylabel('Flux linkage [Wb]');
        
%         [psi_meas_tmp,sort_idx ] = sort(psi_meas_tmp);    
%         c_meas_tmp = c_meas_tmp(sort_idx);
%         psi_meas_tmp = psi_meas_tmp(sort_idx);      
%         Curr_meas{i}(:,1) =   interp1(psi_meas_tmp,c_meas_tmp,psi_grid,'PCHIP');      
%         PSI_meas{i}(:,1)   = psi_grid;
%         
% 
%         curr_start = Curr_meas{i}(1:10,1);        
%         if sum(abs(diff(curr_start))>3*mean(abs(diff(curr_start))) )>0 
%             nbr_fit = round(length(c_meas_tmp)*0.05);            
%             [fitresult, gof] = fit( psi_meas_tmp(1:nbr_fit)',c_meas_tmp(1:nbr_fit)', 'poly5' , fitoptions( 'Method', 'LinearLeastSquares' ) );
%             Curr_meas{i}(1:3,1) = feval(fitresult,PSI_meas{i}(1:3,1));
%         end
%         
%         curr_end = Curr_meas{i}(end-10:end,1);        
%         if sum(abs(diff(curr_end))>3*mean(abs(diff(curr_end))) )>0 
%             nbr_fit = round(length(c_meas_tmp)*0.05);            
%             [fitresult, gof] = fit( psi_meas_tmp(end-nbr_fit:end)',c_meas_tmp(end-nbr_fit:end)', 'poly5' , fitoptions( 'Method', 'LinearLeastSquares' ) );
%             Curr_meas{i}(end-3:end,1) = feval(fitresult,PSI_meas{i}(end-3:end,1));
%         end
%         
%         
%         decreasing current       
%         PSI_meas{i}(:,2)   = flip(psi_grid);
%         c_meas_tmp = this_meas.current_decrease_sm;
%         psi_meas_tmp = this_meas.psi_decrease_sm;            
%         [psi_meas_tmp,idx_unique] = unique(psi_meas_tmp);
%         c_meas_tmp = c_meas_tmp(idx_unique);
%         psi_meas_tmp = psi_meas_tmp(idx_unique);  
%         [psi_meas_tmp,sort_idx ] = sort(psi_meas_tmp);    
%         c_meas_tmp = c_meas_tmp(sort_idx);
%         psi_meas_tmp = psi_meas_tmp(sort_idx);        
%         Curr_meas{i}(:,2)   = flip(psi_grid);
%         Curr_meas{i}(:,2) =   interp1(psi_meas_tmp,c_meas_tmp,PSI_meas{i}(:,2),'PCHIP');  
%         
%         
%         
%         curr_start = Curr_meas{i}(1:10,2);        
%         if sum(abs(diff(curr_start))>1.5*mean(abs(diff(curr_start))))>0 
%             nbr_fit = round(length(c_meas_tmp)*0.05);            
%             [fitresult, gof] = fit( psi_meas_tmp(end-nbr_fit:end)',c_meas_tmp(end-nbr_fit:end)', 'poly5' , fitoptions( 'Normalize', 'on' , 'Method', 'LinearLeastSquares' ) );
%             Curr_meas{i}(1:3,2) = feval(fitresult,PSI_meas{i}(1:3,2));
% 
%         end
%                 
%         curr_end = Curr_meas{i}(end-10:end,2);        
%         if sum(abs(diff(curr_end))>3*mean(abs(diff(curr_end))) )>0 
%             nbr_fit = round(length(c_meas_tmp)*0.05);            
%             [fitresult, gof] = fit( psi_meas_tmp(end-nbr_fit:end)',c_meas_tmp(end-nbr_fit:end)', 'poly5' , fitoptions('Normalize', 'on' , 'Method', 'LinearLeastSquares' ) );
%             Curr_meas{i}(end-3:end,2) = feval(fitresult,PSI_meas{i}(end-3:end,2));
%         end
%          
%         update_figure('Psi Data selected');
%         plot(Curr_meas{i}(:),PSI_meas{i}(:));
%         xlabel('Current [A]');
%         ylabel('Flux linkage [Wb]');
%         hold on
%         grid on
    end
   

    