
    idx_result = 2;
    idx_meas = 1;  
    idx_sel = [2,3,4,5];

    
    nmbr_meas = size(meas_raw(idx_meas).current,2) ;    
    nmbr_meas_sel = length(idx_sel);
    PSI_meas = cell(1,nmbr_meas_sel);
    Curr_meas = cell(1,nmbr_meas_sel);
    PSI_an = [];%cell(1,nmbr_meas);
    CRR_an = []; % cell(1,nmbr_meas);
    

    
    for i = 1:nmbr_meas_sel
        
        flag_const_grid = false;
        psi_grid_inc = 0.001;
        nmbr_grid_pnts = 150;        
        % we reduce the number of datapoints using interpolation
        % we do not check wether datapoints are unique or in ascending
        % order because we use preprocessed data and this has already been
        % done in other routines
        this_meas = meas_raw(idx_meas).current(idx_sel(i)).results(idx_result).flux_curve;    
        
        % increase current 
        c_meas_tmp_up = this_meas.current_increase_sm;
        psi_meas_tmp_up = this_meas.psi_increase_sm;
 
                
        % decreasing current       
        c_meas_tmp_dwn = this_meas.current_decrease_sm;
        psi_meas_tmp_dwn = this_meas.psi_decrease_sm;  
        
        
        update_figure('Psi Data selected');
%         plot(abs(c_meas_tmp_up),abs(psi_meas_tmp_up),'-b');
%         hold on
%         plot(abs(c_meas_tmp_dwn),abs(psi_meas_tmp_dwn),'-k');
%         plot(c_meas_tmp_up,psi_meas_tmp_up,'-b');
        hold on
        plot(c_meas_tmp_dwn,psi_meas_tmp_dwn,'-k');
        
        
        if psi_meas_tmp_up(end)>psi_meas_tmp_dwn(1)       
        % get intersection in first quadrant        
            [c_meas_tmp_up,psi_meas_tmp_up,c_meas_tmp_dwn,psi_meas_tmp_dwn] = ...
                cut_off_intersection(c_meas_tmp_up,psi_meas_tmp_up,c_meas_tmp_dwn,psi_meas_tmp_dwn);           
        end  
        
% %         % get intersection in third quadrant    
% %         if psi_meas_tmp_up(1)> psi_meas_tmp_dwn(end) 
% %             warning("Intersection in quadrat 3, which should not be possible");
% %                     [c_meas_tmp_dwn,psi_meas_tmp_dwn,c_meas_tmp_up,psi_meas_tmp_up] = ...
% %                 cut_off_intersection(-flip(c_meas_tmp_dwn),-flip(psi_meas_tmp_dwn),-flip(c_meas_tmp_up),-flip(psi_meas_tmp_up));            
% %             c_meas_tmp_up = flip(c_meas_tmp_up);
% %             psi_meas_tmp_up = flip(psi_meas_tmp_up);
% %             c_meas_tmp_dwn = flip(c_meas_tmp_dwn);
% %             psi_meas_tmp_dwn = flip(psi_meas_tmp_dwn);            
% %         end         
        
        
        min_psi = min(min(this_meas.psi_increase_sm),min(this_meas.psi_decrease_sm));
        max_psi = max(max(this_meas.psi_increase_sm),max(this_meas.psi_decrease_sm));    
        
        
        c_meas_tmp_up_zero =  currentZeroCrossing(c_meas_tmp_up,psi_meas_tmp_up);    
        c_meas_tmp_dwn_zero =  -currentZeroCrossing(-c_meas_tmp_dwn,-psi_meas_tmp_dwn);        
        c_meas_tmp_shift_each = (c_meas_tmp_up_zero+c_meas_tmp_dwn_zero)/2;
        
% Use these lines to check the shift        
%         c_meas_tmp_up_zero =  currentZeroCrossing([c_meas_tmp_up-c_meas_tmp_shift_each],psi_meas_tmp_up);
%         c_meas_tmp_dwn_zero =  -currentZeroCrossing(-[c_meas_tmp_dwn-c_meas_tmp_shift_each],-psi_meas_tmp_dwn);
       
        c_meas_tmp_up = c_meas_tmp_up-c_meas_tmp_shift_each;
        c_meas_tmp_dwn = c_meas_tmp_dwn-c_meas_tmp_shift_each;
        
%         update_figure('Psi Data selected');
%         plot(abs(c_meas_tmp_up),abs(psi_meas_tmp_up),'-k');
%         plot(c_meas_tmp_up,psi_meas_tmp_up,'-k');
%         hold on
%         plot(abs(c_meas_tmp_dwn),abs(psi_meas_tmp_dwn),'-b');
%         plot(c_meas_tmp_dwn,psi_meas_tmp_dwn,'-b');
        
        
        % now we make a grid for each single line from min to max. For the
        % decreasing line we include the zero, which will be our starting
        % point. The grid is setup along psi
%           plot(c_meas_tmp_dwn,psi_meas_tmp_dwn,'-r');
        % measurements decreasing psi
        [psi_tmp_dwn,idx_sort_dwn] = sort(psi_meas_tmp_dwn);        
        if idx_sort_dwn(1)>idx_sort_dwn(end)
            idx_sort_dwn(idx_sort_dwn>idx_sort_dwn(1))=[];
            idx_sort_dwn(idx_sort_dwn<idx_sort_dwn(end))=[];
        else
           error('not implemented yet') 
        end        
        % Remove all points at the ends beeing shifted         
        [psi_tmp_dwn_unique,idx_sort_dwn_unique]=unique(psi_meas_tmp_dwn(idx_sort_dwn));        
        
        current_zero_meas_tmp_dwn = interp1(psi_tmp_dwn_unique,c_meas_tmp_dwn(idx_sort_dwn(idx_sort_dwn_unique)),0);        
        psi_grid_meas_tmp_dwn = linspace(min(psi_meas_tmp_dwn),max(psi_meas_tmp_dwn),nmbr_grid_pnts);  
        currnet_interp1_grid_meas_tmp_dwn = interp1(psi_tmp_dwn_unique,c_meas_tmp_dwn(idx_sort_dwn(idx_sort_dwn_unique)),psi_grid_meas_tmp_dwn);  
        [idx_last_negative]=find(psi_grid_meas_tmp_dwn<0,1,'last');
        
        
         % measurements increasing psi       
        [psi_tmp_up,idx_sort_up] = sort(psi_meas_tmp_up);        
        if idx_sort_up(1)<idx_sort_up(end)
            idx_sort_up(idx_sort_up<idx_sort_up(1))=[];
            idx_sort_up(idx_sort_up>idx_sort_up(end))=[];
        else
           error('not implemented yet') 
        end        
        % Remove all points at the ends beeing shifted         
        [psi_tmp_up_unique,idx_sort_up_unique]=unique(psi_meas_tmp_up(idx_sort_up));  
        psi_grid_meas_tmp_up = linspace(min(psi_meas_tmp_up),max(psi_meas_tmp_up),nmbr_grid_pnts);  
        currnet_interp1_grid_meas_tmp_up= interp1(psi_tmp_up_unique,c_meas_tmp_up(idx_sort_up(idx_sort_up_unique)),psi_grid_meas_tmp_up);  
        
        
        
       
        Curr_meas{i} = [current_zero_meas_tmp_dwn,flip(currnet_interp1_grid_meas_tmp_dwn(1:idx_last_negative)), ....
            currnet_interp1_grid_meas_tmp_up,flip(currnet_interp1_grid_meas_tmp_dwn(idx_last_negative+1:end))];
        PSI_meas{i} = [0,flip(psi_grid_meas_tmp_dwn(1:idx_last_negative)), ...
            psi_grid_meas_tmp_up,flip(psi_grid_meas_tmp_dwn(idx_last_negative+1:end))];
        
    end
%


for     i = 1:nmbr_meas_sel          
        update_figure('Psi Data selected');
        plot( Curr_meas{i},PSI_meas{i} ,'-k');
        hold on
end


B_meas = PSI_meas;
H_meas = Curr_meas;
%%
        if flag_const_grid==true
        psi_grid = psi_grid_inc*(fix(min_psi./psi_grid_inc): ...
                        1:ceil(max_psi./psi_grid_inc))';    
        else
        psi_grid = psi_grid_inc*linspace(fix(min_psi./psi_grid_inc),...
             ceil(max_psi./psi_grid_inc),nmbr_grid_pnts)';  
        end
                    

        
        update_figure('Psi Data selected');
        plot(c_meas_tmp,psi_meas_tmp,'-k');
        hold on
        [psi_meas_tmp,idx_unique] = unique(psi_meas_tmp);
%         psi_meas_tmp = psi_meas_tmp(idx_unique);
        c_meas_tmp = c_meas_tmp(idx_unique);  
        
        [psi_meas_tmp,sort_idx ] = sort(psi_meas_tmp);    
        c_meas_tmp = c_meas_tmp(sort_idx);
%         psi_meas_tmp = psi_meas_tmp(sort_idx);      
        Curr_meas{i}(:,1) =   interp1(psi_meas_tmp,c_meas_tmp,psi_grid,'PCHIP');      
        PSI_meas{i}(:,1)   = psi_grid;
        

        curr_start = Curr_meas{i}(1:20,1);        
        if sum(abs(diff(curr_start))>3*mean(abs(diff(curr_start))) )>0 
            nbr_fit = round(length(c_meas_tmp)*0.1);            
            [fitresult, gof] = fit( psi_meas_tmp(1:nbr_fit)',c_meas_tmp(1:nbr_fit)', 'poly5' , fitoptions( 'Method', 'LinearLeastSquares' ) );
            Curr_meas{i}(1:3,1) = feval(fitresult,PSI_meas{i}(1:3,1));
        end
        
        curr_end = Curr_meas{i}(end-20:end,1);        
        if sum(abs(diff(curr_end))>3*mean(abs(diff(curr_end))) )>0 
            nbr_fit = round(length(c_meas_tmp)*0.1);            
            [fitresult, gof] = fit( psi_meas_tmp(end-nbr_fit:end)',c_meas_tmp(end-nbr_fit:end)', 'poly5' , fitoptions( 'Method', 'LinearLeastSquares' ) );
            Curr_meas{i}(end-3:end,1) = feval(fitresult,PSI_meas{i}(end-3:end,1));
        end
        
        
        % decreasing current       
        PSI_meas{i}(:,2)   = flip(psi_grid);
   
        
        update_figure('Psi Data selected');
        plot(c_meas_tmp,psi_meas_tmp,'-k');
        
        [psi_meas_tmp,idx_unique] = unique(psi_meas_tmp);
        c_meas_tmp = c_meas_tmp(idx_unique);
%         psi_meas_tmp = psi_meas_tmp(idx_unique);  
        [psi_meas_tmp,sort_idx ] = sort(psi_meas_tmp);    
        c_meas_tmp = c_meas_tmp(sort_idx);
%         psi_meas_tmp = psi_meas_tmp(sort_idx);        
%         Curr_meas{i}(:,2)   = flip(psi_grid);
        Curr_meas{i}(:,2) =   interp1(psi_meas_tmp,c_meas_tmp,PSI_meas{i}(:,2),'PCHIP');  
        
        
        
        curr_start = Curr_meas{i}(1:20,2);        
        if sum(abs(diff(curr_start))>1.5*mean(abs(diff(curr_start))))>0 
            nbr_fit = round(length(c_meas_tmp)*0.1);            
            [fitresult, gof] = fit( psi_meas_tmp(end-nbr_fit:end)',c_meas_tmp(end-nbr_fit:end)', 'poly5' , fitoptions( 'Normalize', 'on' , 'Method', 'LinearLeastSquares' ) );
            Curr_meas{i}(1:3,2) = feval(fitresult,PSI_meas{i}(1:3,2));

        end
                
        curr_end = Curr_meas{i}(end-20:end,2);        
        if sum(abs(diff(curr_end))>3*mean(abs(diff(curr_end))) )>0 
            nbr_fit = round(length(c_meas_tmp)*0.1);            
            [fitresult, gof] = fit( psi_meas_tmp(end-nbr_fit:end)',c_meas_tmp(end-nbr_fit:end)', 'poly5' , fitoptions('Normalize', 'on' , 'Method', 'LinearLeastSquares' ) );
            Curr_meas{i}(end-3:end,2) = feval(fitresult,PSI_meas{i}(end-3:end,2));
        end
         
%         update_figure('Psi Data selected');
%         plot(Curr_meas{i}(:),PSI_meas{i}(:));
%         xlabel('Current [A]');
%         ylabel('Flux linkage [Wb]');
%         hold on
%         grid on
%     end
   

%     plot(x,y)
clear weights
% for i = 1:nmbr_meas_sel
%       x = PSI_meas{i}(:,1);
%     y = normpdf(x,0,max(PSI_meas{i})*0.5);    
%     for j = 1:2
%      weights{i}(:,j)= y(:,1).^2;
%     end
% end
for i = 1:nmbr_meas_sel
      weights{i}   = y(:,1);
      weights{i} = weights{i}.^2;
end     
for i = 1:nmbr_meas_sel
    
      Curr_meas{i} = [Curr_meas{i}(:,1);Curr_meas{i}(2:end-1,2)];
      PSI_meas{i}  = [PSI_meas{i}(:,1);PSI_meas{i}(2:end-1,2)];
             x = PSI_meas{i}(:,1);
     y = normpdf(x,0,max(PSI_meas{i})*0.5); 

        update_figure('Psi Data selected');
        plot(Curr_meas{i}(:),PSI_meas{i}(:));
        xlabel('Current [A]');
        ylabel('Flux linkage [Wb]');
        hold on
        grid on        
        
end
B_meas = PSI_meas;
H_meas = Curr_meas;
             
             function [c_meas_tmp_up,psi_meas_tmp_up,c_meas_tmp_dwn,psi_meas_tmp_dwn] = cut_off_intersection(c_meas_tmp_up,psi_meas_tmp_up,c_meas_tmp_dwn,psi_meas_tmp_dwn)   
             
             len = round(length(c_meas_tmp_up)*0.01);
            
            [current_intersect,psi_intersect]= polyxpoly(c_meas_tmp_up(end-len:end) ,psi_meas_tmp_up(end-len:end),c_meas_tmp_dwn(1:len) ,psi_meas_tmp_dwn(1:len));
%   
%             update_figure('Psi Data selected');
%             plot(c_meas_tmp_up,psi_meas_tmp_up,'-k');
%             hold on
%             plot(c_meas_tmp_dwn,psi_meas_tmp_dwn,'-b');
%             scatter(current_intersect,psi_intersect)

            [~,row,~] = find(psi_meas_tmp_up>psi_intersect,1,'first');
            c_meas_tmp_up(row:end) = [];
            psi_meas_tmp_up(row:end) = [];
%           update_figure('Psi Data selected');
%         plot(c_meas_tmp_up,psi_meas_tmp_up,'-r');
        
            [~,row,~] = find(psi_meas_tmp_dwn>psi_intersect,1,'last');
            c_meas_tmp_dwn(1:row) = [];
            psi_meas_tmp_dwn(1:row) = [];
%         plot(c_meas_tmp_dwn,psi_meas_tmp_dwn,'-g');
%         hold on
        
%       now we add the intersection point to the end of the measurement
%       with increasing current
            c_meas_tmp_up(end+1) = current_intersect;
            psi_meas_tmp_up(end+1) =  psi_intersect;
%             plot(c_meas_tmp_up,psi_meas_tmp_up,'-g');
             
             
             end
             
             function c_meas_tmp_up_zero =  currentZeroCrossing(c_meas_tmp_up,psi_meas_tmp_up)
             
                [~, idx_min_psi_positiv] = find(psi_meas_tmp_up>0,1,'first');
        
                if psi_meas_tmp_up(idx_min_psi_positiv-1)>psi_meas_tmp_up(idx_min_psi_positiv)
                   error(['This algorithm requires psi_meas_tmp_up(idx_min_psi_positiv-1) to be smaller than psi_meas_tmp_up(idx_min_psi_positiv)!', ...
                           'Ensure current and psi to be in ascending  order, otherwise input them as (-current,-psi) and multiply output by -1']); 
                end

                m = (psi_meas_tmp_up(idx_min_psi_positiv)-psi_meas_tmp_up(idx_min_psi_positiv-1))/...
                    (c_meas_tmp_up(idx_min_psi_positiv)-c_meas_tmp_up(idx_min_psi_positiv-1));

                c_meas_tmp_up_zero = -psi_meas_tmp_up(idx_min_psi_positiv)/m+c_meas_tmp_up(idx_min_psi_positiv);

             end
    