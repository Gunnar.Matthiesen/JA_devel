function [Curr_sim,PSI_sim] = evaluate_invJA_loops_PSI(PSI_meas,Curr_meas,Curr_sim,PSI_sim,param)
    % an der Stelle PSI_sim(1,1) muss der Startwert M0 stehen und in Loops in Curr_sim müssen die gleiche Länge auf auf und absteigendem Ast haben 
    % Curr_sim is a matrix 
    
    % Curr_sim input as current!!!!!
    % PSI_sim input as PSI !!!!
    
     my0 = 4.*pi.*1e-7;
    
    f_psi  = param(6);
    f_i    = param(7);
    
    %scale from Psi to B
    B_meas = PSI_meas*f_psi;  
    H_meas =  Curr_meas*f_i;    
    M_meas =  B_meas/my0-H_meas;
%     
    M0 =   M_meas(1);
    H0 =   H_meas(1);
%         figure
    for j = 1:2
    
%         for i = 1:size(PSI_meas,2)  

         
                % ATTENTION, PSI and Curr are the names, but it this point
                % they carry M and H as values
                PSI_sim(1) = M0;            
                Curr_sim(1) = H0;  
                [PSI_sim,Curr_sim] = integrate_invJA_loop_discr_PSI(B_meas,PSI_sim,Curr_sim,param);             
  
                
                [PSI_tmp,Curr_tmp] = integrate_invJA_loop_discr_PSI([B_meas(end),B_meas(1)],[PSI_sim(end),0],[Curr_sim(end),0],param);   
                
                M0 = PSI_tmp(end);
                H0 = Curr_tmp(end);   
                
%                 plot(Curr_sim(:,i),PSI_sim(:,i));
%                 hold on
%                 i
         
%         end    
    end
       
%     for i = 1:size(PSI_meas,2)
        Curr_sim =  Curr_sim/f_i;
        PSI_sim =  f_psi* my0*(PSI_sim+ Curr_sim);
%     end
    
    
end