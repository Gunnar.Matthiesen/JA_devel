 function [sse_sum] = eval_invJaDatasetMAT_PSI_SSE(PSI_meas,Curr_meas,M_sim,H_sim,nmbr_sets,weights,param,param_opt)
        sse_sum = 0;
%         f_psi  = param(6);
%         f_i    = param(7);
%         my0 = 4.*pi.*1e-7;
 
        
        
        for i = 1:nmbr_sets  
            [Curr_sim{i},M_sim{i}] = evaluate_invJA_loops_PSI(PSI_meas{i},Curr_meas{i},H_sim{i},M_sim{i},param.*param_opt); 
%             PSI_sim1 = (Curr_sim{i}(:,1)*f_i + M_sim{i}(:,1))/f_psi*my0;
%             PSI_sim2 = (Curr_sim{i}(:,2)*f_i + M_sim{i}(:,2))/f_psi*my0;
            
%             sse1 = sum((PSI_meas{i}(:,1) - PSI_sim1).^2);  
%             sse2 = sum((PSI_meas{i}(:,2) - PSI_sim2).^2); 
%             sse1 = sum(abs(Curr_meas{i} - Curr_sim{i}));  
            sse1 = sum(weights{i}.*abs(Curr_meas{i} - Curr_sim{i}));  
%             sse2 = sum((Curr_meas{i} - Curr_sim{i}).^2); 
            sse_sum = sse_sum + sse1*1e6;
        end
 end
 
%  (PSI_meas,M_sim,H_sim,PSI0,param)