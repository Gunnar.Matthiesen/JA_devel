classdef commutation_curve < handle
      
   properties(GetAccess =   public)
       
       cum_curve_current = [];
       cum_curve_psi = [];
       cum_curve_pos = [];
   end
   
   
   methods
       
       function obj = commutation_curve()
%            obj.calc_commutation_curve(1);
       end
       
       
       
       
       function psi0 = give_psi0(obj,current)
           
           psi0 = interp1(obj.cum_curve_current,obj.cum_curve_psi,current,'PCHIP');
       
       end
                 
       function obj = calc_commutation_curve(obj,pos_idx,T_idx,meas_data)
        % hier ein Mapping, im Feld 'current' steht der entsprechende Strom im Feld
        % T_meas an welcher stelle?         
        % Evaluate the data
        nmbr_current_meas_pnts = size(meas_data(pos_idx).current,2);
        obj.cum_curve_psi = zeros(1,nmbr_current_meas_pnts+1);
        obj.cum_curve_current = zeros(1,nmbr_current_meas_pnts+1);
        obj.cum_curve_current(1) = 0;
        obj.cum_curve_psi(1) = 0;
        cntr_i_array = 1;
        for i = 1:nmbr_current_meas_pnts
            cntr_i_array = cntr_i_array+1;
            nmbr_curves = size(meas_data(pos_idx).current(i).T_meas(T_idx).flux_curve.results,2);
            cum_curve_current_tmp = zeros(1,nmbr_curves);
            cum_curve_psi_tmp = zeros(1,nmbr_curves);
            for j = 1:nmbr_curves        
                idx_current_greater_zero = meas_data(pos_idx).current(i).T_meas(T_idx).flux_curve.results(j).flux_curve.current_increase>0;
                opts = fitoptions( 'Method', 'LinearLeastSquares' );
%                 opts.Robust = 'Bisquare'; % TOO EXPENSIVE
                [fitresult, ~] = fit( double(meas_data(pos_idx).current(i).T_meas(T_idx).flux_curve.results(j).flux_curve.current_increase(idx_current_greater_zero)'),...
                                        double(meas_data(pos_idx).current(i).T_meas(T_idx).flux_curve.results(j).flux_curve.psi_increase(idx_current_greater_zero)'), ...
                                        'poly3', opts );                            
                cum_curve_current_tmp(j) = max(meas_data(pos_idx).current(i).T_meas(T_idx).flux_curve.results(j).flux_curve.current_increase);
                cum_curve_psi_tmp(j) = feval(fitresult,cum_curve_current_tmp(j));  
             
            end  
            % now calc the position as mean over all measurements                
            position = meas_data(pos_idx).current(i).T_meas(T_idx).flux_curve.position;
            sampls_per_ramp = meas_data(pos_idx).current(i).T_meas(T_idx).flux_curve.i_max/...
            meas_data(pos_idx).current(i).T_meas(T_idx).flux_curve.i_dot*...  
            meas_data(pos_idx).current(i).T_meas(T_idx).flux_curve.daq_Rate;
            [peaks,loc] = findpeaks(position,'MinPeakDistance',sampls_per_ramp*0.5);
            med_peak=(max(peaks)+min(peaks))/2;
            obj.cum_curve_pos(cntr_i_array) = mean(position(loc(peaks>med_peak)));                
            obj.cum_curve_current(cntr_i_array) = mean(cum_curve_current_tmp);
            obj.cum_curve_psi(cntr_i_array) =  mean(cum_curve_psi_tmp);    
        end

       end
end

end
