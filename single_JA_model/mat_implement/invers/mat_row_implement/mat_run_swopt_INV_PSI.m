       

% % select Data
% select_dataMAT
load('test_data_sh')
%% parameter set working quite well

        my0 = 4.*pi.*1e-7;

        ja_Ms     = 250437.862814894;
        ja_a      = 1;
        ja_alpha  = 1.0e-05;
        ja_k      = 5*my0^2*100000000;
        ja_c      = 0.7;
        
        param(1) = ja_Ms;
        param(2) = ja_a;
        param(3) = ja_alpha;
        param(4) = ja_k;
        param(5) = ja_c;
        param(6) = 1;
        param(7) = 1;
        
%        param = [1248145.54219593,0.0485382948605262,6.68428000725425e-09,3.55161871939975e-08,1.41149009717994,3.85519935908942,388379.779026173];
%%      

% set up initial cell struct for sim output        
        nmbr_sets =size(B_meas,2);
        for i = 1:nmbr_sets
            PSI_sim{i} = zeros(size(B_meas{i}));
            M_sim{i} = zeros(size(B_meas{i}));
            H_sim{i} = zeros(size(B_meas{i}));            
        end
        
        
%           param = [18039688.4411349,3357.60893420928,0.000369962813185498,1942.94289820142,0.762985016540044,39.5235201952689,18421.4106985863];
   param =  [90198.4422056745,2893.88410175598,0.000554944219778247,4873.38001386503,0.541204995357702,0.333570878398525,60188.6437731815];

   % split
%    param = [45118.9708      14.4694205  0.000832308972      2428.66163     0.541124143   0.00166785439      300.943219];
   %           
%     param(1) = param(1)*1;
%           param(3) = param(3)*0.5;
%            param(4) = param(4)*0.5;
%            param(5) = param(5)*0.5;
%           param(7) = param(7)*0.5;
          %%
% set up optimization

param(5) = param(5)*0.5;

call_optimJa = @(param_opt) eval_invJaDatasetMAT_PSI_SSE(B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,param,param_opt);
       

        lb = [0.005,0.005,0.0005,0.0005,0.0005,0.0005,0.0005];
        ub = [2,10,10,20,2,2,50];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',1000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,7,lb,ub,options);
        disp(['Fval:  ', num2str(fval)]);
        disp(num2str(param.*bestParam));
        
%% plot optimization vs measurement
      
%         param = [18039688.4411349,3357.60893420928,0.000369962813185498,1942.94289820142,0.762985016540044,39.5235201952689,18421.4106985863];
    
        nmbr_sets =size(B_meas,2);
        for i = 1:nmbr_sets
            PSI_sim{i} = zeros(size(B_meas{i}));
            M_sim{i} = zeros(size(B_meas{i}));
            H_sim{i} = zeros(size(B_meas{i}));            
        end
        
tic
        [Curr_sim_out,M_sim1,sse] = eval_invJaDatasetMAT_PSI(B_meas,H_meas,M_sim,H_sim,nmbr_sets,weights,param.*bestParam);  
toc     
        
%      [Curr_sim_out,M_sim1,sse] = eval_invJaDatasetMAT_PSI(B_meas,H_meas,M_sim,H_sim,nmbr_sets,param);  
        figure
    for i = 1:1:nmbr_sets
        hold on
         plot(H_meas{i}(:),B_meas{i}(:),'-k');
%            scatter(H_meas{i}(:),B_meas{i}(:),'k');
%                     plot(abs(Curr_meas{i}(:)),abs(M_sim_out{i}(:)),'-r'); 
                    hold on
% 	              plot(abs(Curr_meas{i}(:)),abs(PSI_meas{i}(:)),'-k'); 
            plot((Curr_sim_out{i}),(B_meas{i}),'-r'); 
%         scatter(Curr_sim_out{i}(:),B_meas{i}(:));
    end
 grid on
 box on
 %%
 M_sim = {};
 H_sim = {};
         nmbr_sets =size(PSI_meas,2);
        for i = 1:nmbr_sets
            M_sim{i} = zeros(size(PSI_meas{i}));
            H_sim{i} = zeros(size(PSI_meas{i}));
        end
 
 [Curr_sim_out,M_sim2,sse] = eval_invJaDatasetMAT_PSI(PSI_meas,Curr_meas,M_sim,H_sim,nmbr_sets,weights,param.*bestParam);  
%  Curr_sim_out{1}(1:30)
%  M_sim1{3}(1:10)
%  M_sim2{3}(1:10)
        
%         figure
%     for i = 1:2:nmbr_sets
%         hold on
%          plot(Curr_meas{i}(:),PSI_sim_out{i}(:),'-r');
% %             plot(Curr_meas{i}(:),PSI_meas{i}(:),'-k');
% %                     plot(abs(Curr_meas{i}(:)),abs(M_sim_out{i}(:)),'-r'); 
%                     hold on
% % 	              plot(abs(Curr_meas{i}(:)),abs(PSI_meas{i}(:)),'-k'); 
%             plot((Curr_meas{i}(:)),(PSI_meas{i}(:)),'-k'); 
% %         scatter(Curr_meas{i}(:),PSI_meas{i}(:));
%     end

%%
x = [];
y=[];
for i = 1:size(Curr_meas,2)
x = [x;Curr_meas{i}];
y = [y;PSI_meas{i}];
end



