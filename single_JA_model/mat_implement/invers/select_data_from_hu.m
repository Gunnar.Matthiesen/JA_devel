

 

B_meas={};
H_meas ={};
% 
% for j = 1:size(BmeasT,2)
%     
%     
% [max_val_B,idx_max_B]         =   max(BmeasT(:,j));     
% [min_val_B,~]   =   min(BmeasT(:,j));  
% [max_val_H,idx_max_H]         =   max(HmeasT(:,j));     
% [min_val_H,~]   =   min(HmeasT(:,j));  
% 
% 
% grid_H = linspace(min_val_H,max_val_H,idx_max_H);
% % grid_H = linspace(min_val_H,max_val_H,idx_min);
% 
% H_meas{j}(:,1)= grid_H;
% H_tmp = HmeasT(1:idx_max_B,j);
% [~,sort_idx]=sort(H_tmp);
% B_tmp = BmeasT(1:idx_max_B,j)
% B_meas{j}(:,1) = interp1(H_tmp(sort_idx),B_tmp(sort_idx),H_meas{j}(:,1),'PCHIP')';
% H_meas{j}(:,2) = flip(grid_H);
% B_meas{j}(:,2) = interp1(HmeasT(idx_max_H:end,j),BmeasT(idx_max_H:end,j),H_meas{j}(:,2),'PCHIP')'; 
% end
    
nmbr_datapoints = 200
for j = 1:size(BmeasT,2)
    
    
[max_val_B,idx_max_B]         =   max(BmeasT(:,j));     
[min_val_B,~]   =   min(BmeasT(:,j));  
[max_val_H,idx_max_H]         =   max(HmeasT(:,j));     
[min_val_H,~]   =   min(HmeasT(:,j));  


% grid_H = linspace(min_val_H,max_val_H,idx_max_H);
grid_B = linspace(min_val_B,max_val_B,nmbr_datapoints);

B_meas{j}(:,1)= grid_B;
B_tmp = BmeasT(1:idx_max_B,j);
[~,sort_idx]=sort(B_tmp);
H_tmp = HmeasT(1:idx_max_B,j);
H_meas{j}(:,1) = interp1(B_tmp(sort_idx),H_tmp(sort_idx),B_meas{j}(:,1),'PCHIP')';
B_meas{j}(:,2) = flip(grid_B);
H_meas{j}(:,2) = interp1(BmeasT(idx_max_H:end,j),HmeasT(idx_max_H:end,j),B_meas{j}(:,2),'PCHIP')'; 
end
 
%%

figure

scatter(H_meas{1}(:),B_meas{1}(:))