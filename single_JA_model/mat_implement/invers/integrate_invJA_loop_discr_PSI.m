function [M_sim,H_sim] = integrate_invJA_loop_discr_PSI(B_int,M_sim,H_sim,param)

        my0 = 4.*pi.*1e-7;
        ja_Ms     = param(1);
        ja_a      = param(2);
        ja_alpha  = param(3);
        ja_k      = param(4);
        ja_c      = param(5);
        
        dM_dB = zeros(size(B_int));
        
        if B_int(1)<B_int(end)
            delta = 1; 
%         elseif B_int(1)==B_int(end)
%             a = 1+1;
%             error(" H_start equals H_end");
        else
            delta=-1; 
        end

        for k = 1:length(B_int)-1    
%             H_sim(k) = B_int(k)/my0 - M_sim(k);
            He = H_sim(k)+ja_alpha*M_sim(k);   
            M_an = ja_Ms*(coth(He/ja_a)-ja_a/He);    
            dMan_dHe = ja_Ms/ja_a*( 1 - coth(He/ja_a)^2 + (ja_a/He)^2);  
            Mirr = (M_sim(k)-ja_c*M_an)/(1-ja_c);
            dMirr_dBe = (M_an - Mirr)/(my0*delta*ja_k);
            dM_dB(k) = ( (1-ja_c)*dMirr_dBe +ja_c/my0*dMan_dHe )/ ...
                    ( 1+ my0*(1-ja_alpha)*(1-ja_c)*dMirr_dBe + ja_c*(1-ja_alpha)*dMan_dHe);                 
            M_sim(k+1) = M_sim(k) + dM_dB(k)*(B_int(k+1)-B_int(k));
            H_sim(k+1) = B_int(k+1)/my0 - M_sim(k+1);
        end

end