function [Curr_sim,PSI_sim] = evaluate_invJA_loops_PSI_cont(PSI_meas,Curr_meas,Curr_sim,PSI_sim,param)
    % an der Stelle PSI_sim(1,1) muss der Startwert M0 stehen und in Loops in Curr_sim müssen die gleiche Länge auf auf und absteigendem Ast haben 
    % Curr_sim is a matrix 
    
    % Curr_sim input as current!!!!!
    % PSI_sim input as PSI !!!!
    
     my0 = 4.*pi.*1e-7;
    
    f_psi  = param(6);
    f_i    = param(7);
    
    %scale from Psi to B
    B_meas = PSI_meas*f_psi;  
    H_meas =  Curr_meas*f_i;    
    M_meas =  B_meas/my0-H_meas;
% %     
    M0 =   M_meas(1);
%     H0 =   H_meas(1);
%         figure
        j_max = 2;
    for j = 1:j_max
    
        for i = 1:size(PSI_meas,2)  
            
                options=odeset('RelTol',1e-4,'AbsTol',1e-6,'MaxStep',abs(B_meas(end,i)-B_meas(1,i))./20,'InitialStep',(B_meas(end,i)-B_meas(1,i))./100);
                if B_meas(1,i)<=B_meas(end,i)
                   delta = 1; 
                else
                    delta = -1;
                end
                  fdMdB=@(B,M) dM_db(B,M,delta,param);
                  % ATTENTION! Here the output Curr_sim carrys M_sim
                 [B_sim_tmp,M_sim_tmp] = ode45(fdMdB,[B_meas(1,i) B_meas(end,i)],M0,options);
                 M0 = M_sim_tmp(end);
                 
                 if j ==j_max
                 % interpolate onto grid                             
                 % convert B to PSI
                 % PSI_sim(:,i) = B_sim_tmp(:,i)*f_psi;        
                    Curr_sim(:,i) = interp1(B_sim_tmp,M_sim_tmp,B_meas(:,i),'PCHIP'); % now it's M
                    Curr_sim(:,i) = (B_meas(:,i)/my0 - Curr_sim(:,i))/f_i; % now it's H
                    PSI_sim(:,i)  = PSI_meas(:,i);
             
                    
                    
                 end
        end    
    end
    

    
end