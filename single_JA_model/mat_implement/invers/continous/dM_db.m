function [dM_dB] = dM_db(B,M,delta,param)

        my0 = 4.*pi.*1e-7;
        ja_Ms     = param(1);
        ja_a      = param(2);
        ja_alpha  = param(3);
        ja_k      = param(4);
        ja_c      = param(5);
        
        H = B/my0 - M;
        He = H+ja_alpha*M;   
        M_an = ja_Ms*(coth(He/ja_a)-ja_a/He);    
        dMan_dHe = ja_Ms/ja_a*( 1 - coth(He/ja_a)^2 + (ja_a/He)^2);  
        Mirr = (M-ja_c*M_an)/(1-ja_c);
        dMirr_dBe = (M_an - Mirr)/(my0*delta*ja_k);
        dM_dB = ( (1-ja_c)*dMirr_dBe +ja_c/my0*dMan_dHe )/ ...
                ( 1+ my0*(1-ja_alpha)*(1-ja_c)*dMirr_dBe + ja_c*(1-ja_alpha)*dMan_dHe);                 
           

end