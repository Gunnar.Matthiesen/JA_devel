 function [Curr_sim,PSI_sim,sse_sum] = eval_invJaDatasetMAT_PSI(PSI_meas,Curr_meas,PSI_sim,Curr_sim,nmbr_sets,param)
        sse_sum = 0; 
        flg_continous = false; 
        figure
        for i = 1:nmbr_sets  
            
            if flg_continous == false
                 [Curr_sim{i},PSI_sim{i}] = evaluate_invJA_loops_PSI(PSI_meas{i},Curr_meas{i},Curr_sim{i},PSI_sim{i},param); 
            else
%                  [Curr_sim_tmp,PSI_sim{i}] = evaluate_invJA_loops_PSI(PSI_meas{i},Curr_meas{i},Curr_sim{i},PSI_sim{i},param); %                  
%                  
%                  Curr_sim{i} = interp1(    ,    ,PSI_meas);
            end
            
            sse1 = sum((Curr_meas{i}(:,1) - Curr_sim{i}(:,1)).^2);  
            sse2 = sum((Curr_meas{i}(:,2) - Curr_sim{i}(:,2)).^2); 
            
            plot(Curr_meas{i}(:,1),Curr_meas{i}(:,1) - Curr_sim{i}(:,1));
            hold on;
              plot(Curr_meas{i}(:,2),Curr_meas{i}(:,2) - Curr_sim{i}(:,2));
            sse_sum = sse_sum + sse1*1e6+sse2*1e6;
            
        end
 end
 
%  (PSI_meas,PSI_sim,Curr_sim,PSI0,param)