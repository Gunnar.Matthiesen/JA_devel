


sse = 0;
sse_sum = 0;
nmbr_sets = 4;
% param = [34129.0201912656,65987.3529748788,22035.4839487496,41648.5111339774,67342.1063908468];

param = [250437.862814894, 5, 1.0e-07,5,0.7];

for i = 1:4
 M_sim{i} = zeros(size(H_meas{i}));   
end

tic
param_opt = ones(size(param));
M_sim_out = evalJaDatasetMAT(M_meas,H_meas,M_sim,nmbr_sets,param)  
toc

sse = optimJA2(M_meas,H_meas,M_sim,nmbr_sets,param) ;  


%%
figure
for i = 1:4
    hold on
   plot(H_meas{i}(:),M_sim_out{i}(:));
end
 