 function [sse_sum] = optimJA_PSI_standard_sse(PSI_meas,Curr_meas,PSI_sim,M_sim,nmbr_sets,param,param_opt)     

         sse_sum = 0;  
        for i = 1:nmbr_sets  
            PSI_sim{i} = evaluate_JA_loops_PSI(Curr_meas{i},PSI_sim{i},M_sim{i},PSI_meas{i}(1),param.*param_opt); 
            sse1 = sum((PSI_meas{i}(:,1) - PSI_sim{i}(:,1)).^2);  
            sse2 = sum((PSI_meas{i}(:,2) - PSI_sim{i}(:,2)).^2); 
            sse_sum = sse_sum + sse1*1e6+sse2*1e6;
        end
 end