       

% select Data
select_dataMAT

%% parameter set working quite well

        my0 = 4.*pi.*1e-7;

        ja_Ms     = 250437.862814894;
        ja_a      = 1;
        ja_alpha  = 1.0e-05;
        ja_k      = 5*my0^2*100000;
        ja_c      = 0.7;
        
        param(1) = ja_Ms;
        param(2) = ja_a;
        param(3) = ja_alpha;
        param(4) = ja_k;
        param(5) = ja_c;
        param(6) = 1;%5;
        param(7) = 1;%1e4;
        
%        param = [1248145.54219593,0.0485382948605262,6.68428000725425e-09,3.55161871939975e-08,1.41149009717994,3.85519935908942,388379.779026173];
%      
                
% set up initial cell struct for sim output        
        nmbr_sets =size(H_meas,2);
        for i = 1:nmbr_sets
            PSI_sim{i} = zeros(size(H_meas{i}));
            M_sim{i} = zeros(size(H_meas{i}));
        end
%         call_optimJa = @(param_opt) optimJA_PSI_standard_sse(PSI_meas,Curr_meas,PSI_sim,M_sim,nmbr_sets,param,param_opt);
           call_optimJa = @(param_opt) optimJA_PSI_standard_sse(B_meas,H_meas,PSI_sim,M_sim,nmbr_sets,param,param_opt);
         
        
% set up optimization

        lb = [0.010,0.001,0.001,0.001,0.001,1,1];
        ub = [10,10,10,10,50,1,1];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        [bestParam,fval,exitflag,output] = particleswarm(call_optimJa,7,lb,ub,options);
        disp(num2str(param.*bestParam))
%% plot optimization vs measurement
        
        
    
        [PSI_sim_out,M_sim1,sse] = evalJaDatasetMAT_PSI(B_meas,H_meas,PSI_sim,M_sim,nmbr_sets,param.*bestParam);  
        
        figure
    for i = 1:1:nmbr_sets
        hold on
         plot(H_meas{i}(:),PSI_sim_out{i}(:),'-r');
%             plot(Curr_meas{i}(:),PSI_meas{i}(:),'-k');
%                     plot(abs(Curr_meas{i}(:)),abs(M_sim_out{i}(:)),'-r'); 
                    hold on
% 	              plot(abs(Curr_meas{i}(:)),abs(PSI_meas{i}(:)),'-k'); 
            plot((H_meas{i}(:)),(B_meas{i}(:)),'-k'); 
%         scatter(Curr_meas{i}(:),PSI_meas{i}(:));
    end
 grid on
 box on
 %%
 M_sim = {};
 H_sim = {};
         nmbr_sets =size(B_meas,2);
        for i = 1:nmbr_sets
            M_sim{i} = zeros(size(B_meas{i}));
            H_sim{i} = zeros(size(B_meas{i}));
        end
 
 [Curr_sim_out,M_sim2,sse] = eval_invJaDatasetMAT_PSI(B_meas,H_meas,M_sim,H_sim,nmbr_sets,param.*bestParam);  
%  Curr_sim_out{1}(:)
%  M_sim1{3}(:)
%  M_sim2{3}(1:10)
%%        
   figure
 for i = 3:3:nmbr_sets
        hold on
          plot(H_meas{i}(:),B_meas{i}(:),'-r');
          plot(Curr_sim_out{i}(:),B_meas{i}(:),'-k');
% %                     plot(abs(Curr_meas{i}(:)),abs(M_sim_out{i}(:)),'-r'); 
%                     hold on
% % 	              plot(abs(Curr_meas{i}(:)),abs(PSI_meas{i}(:)),'-k'); 
%             plot((Curr_meas{i}(:)),(PSI_meas{i}(:)),'-k'); 
% %         scatter(Curr_meas{i}(:),PSI_meas{i}(:));
end
