function M_sim = integrate_JA_loop_discr_PSI(H_int,M_sim,param)

        my0 = 4.*pi.*1e-7;
        ja_Ms     = param(1);
        ja_a      = param(2);
        ja_alpha  = param(3);
        ja_k      = param(4);
        ja_c      = param(5);
        
        if H_int(1)<H_int(end)
            delta = 1; 
        elseif H_int(1)==H_int(end)
            error(" H_start equals H_end");
        else
            delta=-1; 
        end

        for k = 1:length(H_int)-1              
            He = H_int(k)+ja_alpha*M_sim(k);    
            M_an = ja_Ms*(coth(He/ja_a)-ja_a/He);
%           He_up = H_int(k) + 1e-6 + ja_alpha*M_sim(k);
%           He_low = H_int(k) - 1e-6 + ja_alpha*M_sim(k);
%           dMan_dH = ( ja_Ms*(coth(He_up/ja_a)-ja_a/He_up) -  ja_Ms*(coth(He_low/ja_a)-ja_a/He_low))./2e-6;             
            dMan_dH =-ja_Ms*((coth((H_int(k) + M_sim(k)*ja_alpha)/ja_a)^2 - 1)/ja_a - ja_a/(H_int(k) + M_sim(k)*ja_alpha)^2);            
            dM_dH = (1/(1+ ja_c)) * ( M_an - M_sim(k)) /(   ja_k*delta/my0 - ja_alpha*(M_an - M_sim(k))) + ja_c/(1+ja_c)*dMan_dH;
            M_sim(k+1)= M_sim(k) + dM_dH*(H_int(k+1)-H_int(k));
        end

end