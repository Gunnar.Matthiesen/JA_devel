function bestParam= fit_JA_model_devel_opt(CURR_meas,PSI_meas,CRR_an,PSI_an)


% use the eanhysteretic curve of largest set for inital estimation of
% ja_Ms,ja_a,ja_alpha, fsc_psi and fsc_curr.



%% collect all anhyteretic curves
%     CRR_an_collected = [];
%     PSI_an_collected = [];
%     for i =1:size(PSI_an,2)
%         PSI_an_collected  = [PSI_an_collected , PSI_an{i}];
%         CRR_an_collected  = [CRR_an_collected , CRR_an{i}];
%     end
% setup optimization

%%
     % prepare initial vecotr
     % scale I and PSI       
     fsc_psi_opt_start     = 1;
     fsc_curr_opt_start    = 1;

%      HE_start = fsc_curr_opt_start*CRR_an_collected ;         
%      M_an_start = fsc_psi_opt_start*PSI_an_collected;
     
%      x = HE_start
%       y = M_an_start
%      start = [0.5*fsc_psi_opt_start,fsc_curr_opt_start*0.06];
     % applying initial fsc_psi and fsc_curr
%     [Ms_start,a_start]= estimate_Ms_n(HE_start,M_an_start,start);
    Ms_start = 0.4931;
    a_start = 0.0662;
% 
% 
%      param_start;   
%     
%      @optimJA param_start
%      
%     % for measurement run the calculation  
%      nmbr_sets = size(PSI_meas,2);
% 
%         param(1) = Ms_start; % ja_Ms
%         param(2) = a_start; %ja_a
%         param(3) = 2e-7*2; % ja_alpha
%         param(4) = 5; % ja_k
%         param(5) = 0.9; %ja_c
% %         param(6) = 1; % fsc_psi
% %         param(7) = 1; % fsc_curr       
        
        nmbr_sets = size(PSI_meas,2);
        
%         sse = optimJA(PSI_meas,CURR_meas,nmbr_sets,param) ;
        
        call_optimJa = @(param) optimJA(PSI_meas,CURR_meas,nmbr_sets,param);
         
%         fun = @(bestParam) call_optimJa(HmeasT,BmeasT,bestParam);


        lb = [0.5,0.001,1e-8,0.5,0.1];
        ub = [1e6,20,1e-4,10,200];
        options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
        bestParam = particleswarm(call_optimJa,5,lb,ub,options);



%         plotresults(PSI_meas,CURR_meas,nmbr_sets,param)
end