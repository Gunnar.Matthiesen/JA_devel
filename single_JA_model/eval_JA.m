
clf

mi0=4.*pi.*1e-7;
% i=8
% Ms0=max(max(B_meas{i}))./mi0;
% a0=5;
% alpha0=1e-7;
% k0=5;
% c0=0.7; 
fsc_psi = 1;
mi0=4.*pi.*1e-7;
% param = [5.73929836246509,4.12753114496137,0.999971107193456,260226.162266034*mi0,8.17160489788839e-08,1,1];
% param = [Ms0,a0,alpha0,k0,c0,1,1];
param = best_param
for i = 1:4   
        
    H_meas_Eval = H_meas{i}{1};              
    M0 = B_meas{i}{1}(1)/mi0 - H_meas{i}{1}(1);
    [M_sim{i}{1}] = integrate_JA_loop_discr(H_meas_Eval,M0,param); 
    
    plot(H_meas_Eval,M_sim{i}{1},'-r')
    hold on;
    
    M0 = M_sim{i}{1}(end);
    H_meas_Eval = [H_meas{i}{1}(end);H_meas{i}{2}];    
    tmp =  integrate_JA_loop_discr(H_meas_Eval,M0,param); 
    [M_sim{i}{2}] = tmp(2:end);
    plot(H_meas_Eval(2:end),M_sim{i}{2},'-r')
    M_meas1 = fsc_psi*B_meas{i}{1}/mi0 - H_meas{i}{1};
    M_meas2 = fsc_psi*B_meas{i}{2}/mi0 - H_meas{i}{2};     
        
    
    plot( H_meas{i}{1},M_meas1,'k')
    hold on
    plot( H_meas{i}{2},M_meas2,'k')
%     M_meas{i} = B_meas{i}/mi0 - H_meas{i};

%     M0_in = M_meas{i}(1);
%     H_in = H_meas{i};
%     tic
%     M_sim = integrate_JA_loop_discr(H_in,M0_in,param);

%     toc
end


% %%
% Ms = Ms0;
% M0=M0_in;
% tic
% a0 = best_param(2);
% k0 = best_param(4);
% c0 = best_param(5);
% Ms0 = best_param(1);
% alpha0 = best_param(3);
% [Hi, Mi]=JAsolver(a0,k0,c0,Ms0,alpha0,H_meas{i}(1),H_meas{i}(end),M0_in,1);
% toc
% plot(Hi,Mi)