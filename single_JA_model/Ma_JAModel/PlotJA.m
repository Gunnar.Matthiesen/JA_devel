function PlotJA(H_input,B_input,Test)

% Comparison Input& Sim
figure('NumberTitle', 'off', 'Name', 'Comparison Input & Sim Hysteresis loops ');
hold on;
for n=1:size(H_input,2)
plot(H_input(:,n)', B_input(:,n)','r',JA_Num(H_input(1,n),B_input(:,n)',Test), B_input(:,n)','k');
end
hold off;


figure('NumberTitle', 'off', 'Name', 'Comparison H_Input & H_Sim');
hold on;
for n=1:size(H_input,2)
H_ent(:,n)=(JA_Num(H_input(1,n),B_input(:,n)',Test));
end
plot(H_ent(1:end)');
plot(H_input(1:end)');
hold off;



figure('NumberTitle', 'off', 'Name', 'Error between H_Input & H_Sim');
hold on;
for n=1:size(H_input,2)
H_err(:,n)=H_input(:,n)-(JA_Num(H_input(1,n),B_input(:,n)',Test));
end
plot(H_err(1:end)');
hold off;

clear H_ent;





end