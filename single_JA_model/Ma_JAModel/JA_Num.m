%Berechnet  H aus B mit inversem JA model
function HSim = JA_Num(H_0,B,Param)
%% Param
Ms=    Param(1);  %(A/m)
a=     Param(2);  %(A/m)  
alpha= Param(3);  % 
k=     Param(4);  % (A/m)
c=     Param(5);  % 
XPsi=  Param(6);  % 
Yi=    Param(7);  % 

%% Konstanten
Mu0=4.*pi.*1e-7; %(N/A^2)

%% JA Num
B=XPsi*B;
H_0=Yi*H_0;
dB=diff(B);
M=zeros(length(B),1);
H=zeros(length(B),1);
H(1)=H_0;
delta=sign(dB(1,1));

for n=1:length(B)
M(n)=(B(n)/Mu0)-H(n);    
He=H(n)+alpha*M(n);

Man=Ms*(coth(He/a)-(a/He));  
Mirr=(M(n)-c*Man)/(1-c);
dMan_dHe=(Ms/a)*(1-(coth(He/a))^2+(a/He)^2);
dMirr_dBe=delta*(Man-Mirr)/(Mu0*k);                               

dM_dBe=(1-c)*dMirr_dBe+(c/Mu0)*dMan_dHe;  
dB_dBe=1+Mu0*(1-alpha)*(1-c)*dMirr_dBe+c*(1-alpha)*dMan_dHe; 
dM_dB=dM_dBe/dB_dBe;
M(n+1)=M(n)+dM_dB*dB(n);
H(n+1)=(B(n+1)/Mu0)-M(n+1);

if((H(n+1)-H(n))>=0)
    delta=1;
else 
    delta=-1;
end
if (n>=(length(B)-1))
    break;
end
end
HSim=(1/Yi)*H;