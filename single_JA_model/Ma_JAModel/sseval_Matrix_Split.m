%Berechnet den Differenzvektor von berechnetem H und gemessenem H, trennt
%dabei Magnetisierungs- und Entmagnetisierungskurve
function sse = sseval_Matrix_Split(H,B,Param)
sse=0;
s1=0;
s2=0;
for n=1:size(H,2) 
%% Separate
sep=find(islocalmax(H(:,n))); 
  
%% Eval H0 
H_0_1=H(1,n); %down
H_0_2=H(sep+1,n); %up

%% Eval sum funktion
%ohne Gewichtung
s1=s1+1e6*sum(abs(H(1:sep,n) - JA_Num(H_0_1,B(1:sep,n),Param)));
s2=s2+1e6*sum(abs(H(sep+1:end,n) - JA_Num(H_0_2,B(sep+1:end,n),Param)));
sse=sum(s1)+sum(s2);

end
