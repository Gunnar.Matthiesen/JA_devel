function bestParam=JAMatrixSchwarmFit(HmeasT,BmeasT,lb,ub,options)

%% Param est.
fun = @(bestParam)sseval_Matrix_Split(HmeasT,BmeasT,bestParam);
bestParam = particleswarm(fun,7,lb,ub,options);

%% Plot
PlotJA(HmeasT,BmeasT,bestParam);
end