load('Daten');

%% Schwarmfit
lb = [1e3,1e-9,1e-9,1e-9,1e-9];
ub = [1e6,50,1,50,50];
options = optimoptions('particleswarm','FunctionTolerance',1e-8,'UseParallel',true,'SwarmSize',3000,'PlotFcn',@pswplotbestf);
Param = JAMatrixSchwarmFit(HmeasT,BmeasT,lb,ub,options);


%% Plot Comparison Input & Sim Hysteresis loops

figure('NumberTitle', 'off', 'Name', 'Comparison Input & Sim Hysteresis loops ');
hold on;

for n=1:size(HmeasT,2)
plot(HmeasT(:,n)', BmeasT(:,n)','r',JA_Num(HmeasT(1,n),BmeasT(:,n)',Param), BmeasT(:,n)','k');
end
hold off;
clear n;
%% Plot Comparison H_Input & H_Sim
figure('NumberTitle', 'off', 'Name', 'Comparison H_Input & H_Sim');
hold on;
clear H_ent;
for n=1:size(HmeasT,2)
H_ent(:,n)=(JA_Num(HmeasT(1,n),BmeasT(:,n)',Param));
end
plot(H_ent(1:end)');
plot(HmeasT(1:end)');
hold off;
clear H_ent;