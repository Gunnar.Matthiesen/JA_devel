classdef flux_curve < handle     
       
    properties (SetObservable = true)   
        T_meas; %Sample Period
        psi_increase;
        psi_increase_sm;        
        psi_decrease;
        psi_decrease_sm;
        psi_grid_sm;
        current_decrease;
        current_decrease_sm;
        current_increase;
        current_increase_sm;
        current_dissp;
        current_dot_decrease;
        current_dot_increase;
        current_mean_sm;
        psi_mean;
        psi_mean_sm; % plotted as function of current_increase
        current_grid_sm;
        idx_increase_full_set; % full set means the dataset including current measurement at start
        idx_decrease_full_set; % full set means the dataset including current measurement at start
        voltage_ind_increase;
        voltage_ind_decrease;
        voltage_ind_mean;
    end    
    
    properties  (Access = private)
    HLCurrent_dot_decrease;
    HLCurrent_dot_increase;
    end
    
    methods    
        % CONSTRUCTOR:    
        function obj = flux_curve(T_meas,psi_increase,psi_decrease)
            % register listeners
            obj.HLCurrent_dot_decrease = addlistener(obj,'current_decrease','PostSet',...
            @(src,evnt)update_current_data(obj,src,evnt)); % Add obj to argument list
        
            obj.HLCurrent_dot_increase = addlistener(obj,'current_increase','PostSet',...
            @(src,evnt)update_current_data(obj,src,evnt)); % Add obj to argument list
            
            % store passed data
            obj.psi_increase = psi_increase;
            obj.psi_decrease = psi_decrease;  
            obj.T_meas = T_meas;           
        end
        
         function obj = update_current_data(obj,src,~)
            obj.smooth_measurement(src); 
            obj.calcGradient(src);
            obj.calc_mean_flux_curve(src);
         end    
         
         function obj = smooth_measurement(obj,src)              
           if strcmp(src.Name,'current_increase')
                obj.current_increase_sm = single(sgolayfilt(double(obj.current_increase),3,2*floor(0.0125/obj.T_meas/2)+1));  % window must be an odd number! 0.0125*daq_rate was chosen here due to good results
                obj.psi_increase_sm = single(sgolayfilt(double(obj.psi_increase),3,2*floor(0.0125/obj.T_meas/2)+1));            
           elseif strcmp(src.Name,'current_decrease')             
                obj.current_decrease_sm = single(sgolayfilt(double(obj.current_decrease),3,2*floor(0.0125/obj.T_meas/2)+1));  % window must be an odd number! 0.0125*daq_rate was chosen here due to good results
                obj.psi_decrease_sm = single(sgolayfilt(double(obj.psi_decrease),3,2*floor(0.0125/obj.T_meas/2)+1));  
           end 
            
         end
             
        function obj = calcGradient(obj,src,~)         
           if strcmp(src.Name,'current_increase')
           obj.current_dot_increase = gradient(obj.current_increase,obj.T_meas);
           elseif strcmp(src.Name,'current_decrease')
           obj.current_dot_decrease = gradient(obj.current_decrease,obj.T_meas);   
           else
              %nothing 
           end
        end
    
        function obj = calc_mean_flux_curve(obj,src)         
            % calc mean flux, as current grid 'current_increase' is used 
             % DIRTY IMPLEMENTATION TO AVOID NOT CORRET SEPERATED DATASETS
             % AND NON unique POINTS!!! REMIMPLEMENT
             
             


           
            if ~isempty(obj.current_decrease_sm) && ~isempty(obj.current_increase_sm)
            
                mean_diff_curr = (mean(abs(diff(obj.current_decrease_sm)))+mean(abs(diff(obj.current_increase_sm))))/2;    
                min_current = max(min(obj.current_decrease_sm),min(obj.current_increase_sm)) ;
                max_current = min(max(obj.current_decrease_sm),max(obj.current_increase_sm));
                obj.current_grid_sm = linspace(min_current,max_current, round((max_current-min_current)/mean_diff_curr));
                
                mean_diff_psi = (mean(abs(diff(obj.psi_decrease_sm)))+mean(abs(diff(obj.psi_increase_sm))))/2;    
                min_psi = max(min(obj.psi_decrease_sm),min(obj.psi_increase_sm)) ;
                max_psi = min(max(obj.psi_decrease_sm),max(obj.psi_increase_sm));
                obj.psi_grid_sm = linspace(min_psi,max_psi, round((max_psi-min_psi)/mean_diff_psi));                
    
                
                % DIRTY IMPLEMENTATION: cut of decreasing slope at begining, sort ascending and keep unqiue values  
               [~,min_idx]=min(obj.psi_increase_sm);
               [sorted_psi_increase_sm,sort_idx]=sort(obj.psi_increase_sm(min_idx:end));
               [sorted_psi_increase_sm,idx_uinque] = unique(sorted_psi_increase_sm);
               sorted_current_increase_sm = obj.current_increase_sm((min_idx:end));
               sorted_current_increase_sm = sorted_current_increase_sm(sort_idx);
               sorted_current_increase_sm = sorted_current_increase_sm(idx_uinque);
            

               sorted_voltage_ind_increase_sm = obj.voltage_ind_increase((min_idx:end));
               sorted_voltage_ind_increase_sm = sorted_voltage_ind_increase_sm(sort_idx);
               sorted_voltage_ind_increase_sm = sorted_voltage_ind_increase_sm(idx_uinque);
               
                current_upper = interp1(sorted_psi_increase_sm, sorted_current_increase_sm,obj.psi_grid_sm,'linear'); 
               
               [~,max_idx]=max(obj.psi_decrease_sm);
               [sorted_psi_psi_decrease_sm,sort_idx]=sort(obj.psi_decrease_sm(max_idx:end),'descend');               
               [sorted_psi_decrease_sm,idx_uinque] = unique(sorted_psi_psi_decrease_sm);
               
               sorted_current_decrease_sm = obj.current_decrease_sm((max_idx:end));
               sorted_current_decrease_sm = sorted_current_decrease_sm(sort_idx);
               sorted_current_decrease_sm = sorted_current_decrease_sm(idx_uinque);   
               
               sorted_voltage_ind_decrease_sm = obj.voltage_ind_decrease((max_idx:end));
               sorted_voltage_ind_decrease_sm = sorted_voltage_ind_decrease_sm(sort_idx);
               sorted_voltage_ind_decrease_sm = sorted_voltage_ind_decrease_sm(idx_uinque);  
               
                              
               current_lower = interp1(sorted_psi_decrease_sm, sorted_current_decrease_sm,obj.psi_grid_sm,'linear'); 
 
                
%                 current_upper = interp1(obj.psi_increase_sm, obj.current_increase_sm,obj.psi_grid_sm,'linear'); 
%                 current_lower = interp1(obj.psi_decrease_sm, obj.current_decrease_sm,obj.psi_grid_sm,'linear'); 
                obj.current_dissp = (current_upper-current_lower)/2; % corresponds to obj.psi_grid_sm               
                obj.current_mean_sm = (current_upper+current_lower)/2; % corresponds to obj.psi_grid_sm

                
                psi_upper = interp1(obj.current_increase_sm, obj.psi_increase,obj.current_grid_sm,'linear');
                psi_lower = interp1(obj.current_decrease_sm, obj.psi_decrease,obj.current_grid_sm,'linear');
                obj.psi_mean_sm = (psi_upper+psi_lower)/2;    %  corresponds  to obj.current_grid_sm           
                
            end
            
            if ~isempty(obj.voltage_ind_decrease) && ~isempty(obj.voltage_ind_increase)
%                 voltage_ind_mean   


%                [~,min_idx]=min(obj.psi_increase_sm);
%                [sorted_psi_psi_increase_sm,sort_idx]=sort(obj.psi_increase_sm(min_idx:end));
%                [sorted_psi_psi_increase_sm,idx_uinque] = unique(sorted_psi_psi_increase_sm);
%                sorted_current_increase_sm = obj.current_increase_sm((min_idx:end));
%                sorted_current_increase_sm = sorted_current_increase_sm(sort_idx);
%                sorted_current_increase_sm = sorted_current_increase_sm(idx_uinque);



                voltage_ind_upper = interp1(sorted_psi_increase_sm, sorted_voltage_ind_increase_sm,obj.psi_grid_sm,'linear'); 
                voltage_ind_lower = interp1(sorted_psi_decrease_sm, sorted_voltage_ind_decrease_sm,obj.psi_grid_sm,'linear'); 
%                 obj.current_dissp = (voltage_ind_upper-voltage_ind_lower)/2; % corresponds to obj.psi_grid_sm               
                obj.voltage_ind_mean= (abs(voltage_ind_upper)+abs(voltage_ind_lower))/2; % corresponds to obj.psi_grid_sm
%                 




            end
%             opts = fitoptions( 'Method', 'LinearLeastSquares' );
%             opts.Robust = 'Bisquare';
%             opts.Normalize = 'on';
%             % Fit model to data.
%             ft = fittype( 'poly9' );
%             [fitresult, ~] = fit( double(obj.current_increase'), double(obj.psi_mean') , ft , opts );
%             obj.psi_mean = feval(fitresult,current_grid_sm);
        end
        
      
    end
    
end
    
