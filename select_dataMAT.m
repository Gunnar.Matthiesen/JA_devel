
    idx_result = 2;
    idx_meas = 1;  
    idx_sel = 3:1:12;

    
    nmbr_meas = size(meas_raw(idx_meas).current,2) ;    
    nmbr_meas_sel = length(idx_sel);
    PSI_meas = cell(1,nmbr_meas_sel);
    Curr_meas = cell(1,nmbr_meas_sel);
    PSI_an = [];%cell(1,nmbr_meas);
    CRR_an = []; % cell(1,nmbr_meas);

    

    
    for i = 1:nmbr_meas_sel
        
        flag_cont_grid = false;
        current_grid_inc = 0.001;
        nmbr_grid_pnts = 70;        
        % we reduce the number of datapoints using interpolation
        % we do not check wether datapoints are unique or in ascending
        % order because we use preprocessed data and this has already been
        % done in other routines
        this_meas = meas_raw(idx_meas).current(idx_sel(i)).results(idx_result).flux_curve;    
               
        
        min_curr = min(min(this_meas.current_increase),min(this_meas.current_decrease));
        max_curr = max(max(this_meas.current_increase),max(this_meas.current_decrease));    
        
        if flag_cont_grid==true
        current_grid = current_grid_inc*(fix(min_curr./current_grid_inc): ...
                        1:ceil(max_curr./current_grid_inc))';    
        else
        current_grid = current_grid_inc*linspace(fix(min_curr./current_grid_inc),...
             ceil(max_curr./current_grid_inc),nmbr_grid_pnts)';  
        end
                    
        % increase current 
        c_meas_tmp = this_meas.current_increase_sm;
        psi_meas_tmp = this_meas.psi_increase_sm;            
        [~,idx_unique] = unique(c_meas_tmp);
        c_meas_tmp = c_meas_tmp(idx_unique);
        psi_meas_tmp = psi_meas_tmp(idx_unique);  
        [~,sort_idx ] = sort(c_meas_tmp);    
        c_meas_tmp = c_meas_tmp(sort_idx);
        psi_meas_tmp = psi_meas_tmp(sort_idx);      
        PSI_meas{i}(:,1) =   interp1(c_meas_tmp,psi_meas_tmp,current_grid,'PCHIP');      
        Curr_meas{i}(:,1)   = current_grid;
        

        psi_start = PSI_meas{i}(1:10,1);        
        if sum(abs(diff(psi_start))>3*mean(abs(diff(psi_start))) )>0 
            nbr_fit = length(psi_meas_tmp)*0.05;            
            [fitresult, gof] = fit( c_meas_tmp(1:nbr_fit)',psi_meas_tmp(1:nbr_fit)', 'poly5' , fitoptions( 'Method', 'LinearLeastSquares' ) );
            PSI_meas{i}(1:3,1) = feval(fitresult,Curr_meas{i}(1:3,1));
        end
        
        psi_end = PSI_meas{i}(end-10:end,1);        
        if sum(abs(diff(psi_end))>3*mean(abs(diff(psi_end))) )>0 
            nbr_fit = length(psi_meas_tmp)*0.05;            
            [fitresult, gof] = fit( c_meas_tmp(end-nbr_fit:end)',psi_meas_tmp(end-nbr_fit:end)', 'poly5' , fitoptions( 'Method', 'LinearLeastSquares' ) );
            PSI_meas{i}(end-3:end,1) = feval(fitresult,Curr_meas{i}(end-3:end,1));
        end
        
        
        % decreasing current       
        Curr_meas{i}(:,2)   = flip(current_grid);
        c_meas_tmp = this_meas.current_decrease_sm;
        psi_meas_tmp = this_meas.psi_decrease_sm;            
        [~,idx_unique] = unique(c_meas_tmp);
        c_meas_tmp = c_meas_tmp(idx_unique);
        psi_meas_tmp = psi_meas_tmp(idx_unique);  
        [~,sort_idx ] = sort(c_meas_tmp);    
        c_meas_tmp = c_meas_tmp(sort_idx);
        psi_meas_tmp = psi_meas_tmp(sort_idx);        
        Curr_meas{i}(:,2)   = flip(current_grid);
        PSI_meas{i}(:,2) =   interp1(c_meas_tmp,psi_meas_tmp,Curr_meas{i}(:,2),'PCHIP');  
        
        
        
        psi_start = PSI_meas{i}(1:10,2);        
        if sum(abs(diff(psi_start))>1.5*mean(abs(diff(psi_start))))>0 
            nbr_fit = length(psi_meas_tmp)*0.05;            
            [fitresult, gof] = fit( c_meas_tmp(end-nbr_fit:end)',psi_meas_tmp(end-nbr_fit:end)', 'poly5' , fitoptions( 'Method', 'LinearLeastSquares' ) );
            PSI_meas{i}(1:3,2) = feval(fitresult,Curr_meas{i}(1:3,2));

        end
                
        psi_end = PSI_meas{i}(end-10:end,2);        
        if sum(abs(diff(psi_end))>3*mean(abs(diff(psi_end))) )>0 
            nbr_fit = length(psi_meas_tmp)*0.05;            
            [fitresult, gof] = fit( c_meas_tmp(end-nbr_fit:end)',psi_meas_tmp(end-nbr_fit:end)', 'poly5' , fitoptions( 'Method', 'LinearLeastSquares' ) );
            PSI_meas{i}(end-3:end,2) = feval(fitresult,Curr_meas{i}(end-3:end,2));
        end
        
        update_figure('Psi Data selected');
        plot(Curr_meas{i}(:),PSI_meas{i}(:));
        xlabel('Current [A]');
        ylabel('Flux linkage [Wb]');
        hold on
        grid on
    end
   

    